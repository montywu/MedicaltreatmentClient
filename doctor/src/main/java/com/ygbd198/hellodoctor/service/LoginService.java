package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.bean.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/7/22.
 */

public interface LoginService {

    /**
     * 医生登录
     * @param phone
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("login/doctorIndex")
    Call<BaseCallModel<DoctorBean>> login(@Field("phone") String phone, @Field("password") String password);



    /**
     * 忘记密码／扫码领券时的验证码
     *
     * @param phone
     * @param type  类型，1 医生端  其他为用户端 2为扫码领券
     * @return
     */
    @GET("login/getCodeByType")
    Call<BaseCallModel<String>> getCodeByType(@Query("phone") String phone, @Query("type") int type);



    /**
     * 密码重置（忘记密码）
     *
     * @param phone
     * @param password
     * @param verifyCode 短信验证码
     * @return
     */
    @FormUrlEncoded
    @POST("login/doctorForgotPassword")
    Call<BaseCallModel<User>> userForgotPassword(@Field("phone") String phone, @Field("password") String password, @Field("code") String verifyCode);
}
