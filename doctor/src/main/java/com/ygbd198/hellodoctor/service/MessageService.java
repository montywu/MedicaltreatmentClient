
package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.DoctorMessageEnt;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 消息请求接口
 *
 * @author hubing
 * @version [1.0.0.0, 2017-10-22]
 */
public interface MessageService {

    /**
     * 医生登录
     * @param token 唯一标识
     * @param pageNo 页码
     * @param pageSize 每页显示个数
     * @return
     */
    @FormUrlEncoded
    @POST("doctor/getMessage")
    Call<BaseCallModel<ArrayList<DoctorMessageEnt>>> getDoctorMessages(@Field("token") String token, @Field("pageNo") int pageNo, @Field("pageSize") int pageSize);

}
