package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.VedioBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public interface LearningVideoService {


    /**
     * 获取培训视频列表
     *
     * @param
     * @return
     */
    @GET("doctor/getPracticeVideo")
    Call<BaseCallModel<List<VedioBean>>> getVedioList(@Query("token") String token);


    /**
     * 看完录像保存
     *
     * @param token
     * @param practiceId
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("doctor/seeVideo")
    Call<BaseCallModel<Object>> seeVideo(@Field("token") String token, @Field("practiceId") int practiceId);


}
