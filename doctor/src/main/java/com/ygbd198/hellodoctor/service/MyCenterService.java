package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.AgreenDataBean;
import com.ygbd198.hellodoctor.bean.AskForLeaveRecodeBean;
import com.ygbd198.hellodoctor.bean.CurVisitorTimeBean;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.ExchangeRecordBean;
import com.ygbd198.hellodoctor.bean.IntegralBean;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.bean.ReviewBean;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by guangjiqin on 2017/8/27.
 */

public interface MyCenterService {

    /**
     * 获取个人中心资料
     */
    @GET("doctor/getInfo")
    Call<BaseCallModel<DoctorInfBean>> getDoctorInf(@Query("token") String token);

    /**
     * 完成个人资料
     * @param token
     * @param localHospital
     * @param good
     * @param introduction
     * @param name
     * @param gender
     * @param job
     * @return
     */
    @FormUrlEncoded
    @POST("doctor/finishInfo")
    Call<BaseCallModel<String>> finishInfo(@Query("token") String token,
                                           @Field("localHospital") String localHospital,
                                           @Field("good") String good,
                                           @Field("introduction") String introduction,
                                           @Field("name") String name,
                                           @Field("gender") String gender,
                                           @Field("job") String job);

    /**
     * 完成个人资料
     * @param token
     * @param body
     * @return
     */
    @POST("doctor/finishInfo")
    Call<BaseCallModel<String>> finishInfo(@Query("token") String token, @Body RequestBody body);


    /**
     * 文件上传接口
     *
     * @param token file
     *              type  （1-头像,2-工作证,3-身份证,4-医师资历,5-坐诊照片,6自我介绍视频）
     * @return 上传文件的地址
     */
    @Multipart
    @POST("doctor/uploadFile")
    Call<BaseCallModel<String>> doctorPhotoUpload(@Part List<MultipartBody.Part> partList);

    /**
     * 获取兑换记录
     *
     * @param
     * @return
     */
    @GET("integral/transaction")
    Call<BaseCallModel<List<ExchangeRecordBean>>> getExchangeRecord(@Query("token") String token/*, @Query("pageNo") int pageNo, @Query("pageSize") int pageSize*/);


    /**
     * 获取积分记录
     *
     * @param
     * @return
     */
    @GET("integral/detail")
    Call<BaseCallModel<List<IntegralBean>>> getIntegralBean(@Query("token") String token/*, @Query("pageNo") int pageNo, @Query("pageSize") int pageSize*/);


    /**
     * 获取银行卡（我的账户）
     *
     * @param
     * @return
     */
    @GET("integral/get")
    Call<BaseCallModel<MyIntegralBean>> getCards(@Query("token") String token);

    /**
     * 绑定／更换银行卡
     *
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("integral/bindBank")
    Call<BaseCallModel<MyIntegralBean>> bindChangeCard(@Field("token") String token,
                                                       @Field("name") String name,
//                                                       @Field("type") String type,
                                                       @Field("number") long number,
                                                       @Field("code") String code);

    /**
     * 积分兑换接口
     *
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("integral/takeMoney")
    Call<BaseCallModel<Object>> takeMoney(@Field("token") String token,
                                          @Field("code") String money,
                                          @Field("integral") long integral);

    /**
     * 获取当前就诊时间接口
     *
     * @param
     * @return
     */
    @GET("sittingSet/get")
    Call<BaseCallModel<CurVisitorTimeBean>> getCurVisitorTime(@Query("token") String token);

    /**
     * 变更坐诊时间
     * public String monday;//每天工作时间
     * public String tuesday;
     * public String wednesday;
     * public String thursday;
     * public String friday;
     * public String saturday;
     * public String sunday;
     *
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("sittingSet/set")
    Call<BaseCallModel<Object>> changeVisitorTime(@Field("token") String token,
                                                  @Field("startTime") String startTime,
                                                  @Field("reason") String reason,
                                                  @Field("monday") String monday,
                                                  @Field("tuesday") String tuesday,
                                                  @Field("wednesday") String wednesday,
                                                  @Field("thursday") String thursday,
                                                  @Field("friday") String friday,
                                                  @Field("saturday") String saturday,
                                                  @Field("sunday") String sunday);

    /**
     * 获取请假记录
     *
     * @param token
     * @param pageNo        (非必传)
     * @param pageSize（非必传）
     * @return
     */
    @GET("leave/getRecord")
    Call<BaseCallModel<List<AskForLeaveRecodeBean>>> getAskForLeaveRecode(@Query("token") String token);

    /**
     * 申请请假
     *
     * @param token
     * @param startTime
     * @param endTime
     * @param type
     * @param reason
     * @return
     */
    @FormUrlEncoded
    @POST("leave/apply")
    Call<BaseCallModel<Object>> applyLeave(@Field("token") String token, @Field("startTime") String startTime,
                                           @Field("endTime") String endTime);
//                                           @Query("endTime") String endTime,@Query("type") String type,
//                                           @Query("reason") String reason);


    /**
     * 获取更多治疗类型
     *
     * @param token
     * @param typeId
     * @return
     */
    @FormUrlEncoded
    @POST("treatmentType/applyMoreTreatment")
    Call<BaseCallModel<Object>> applyMoreTYpe(@Field("token") String token, @Field("treatmentIds") List<Integer> ids);


    /**
     * 添加新药方
     *
     * @param token
     * @param typeId
     * @return
     */
    @FormUrlEncoded
    @POST("program/applyDrug")
    Call<BaseCallModel<Object>> applyNewScheme(@Field("token") String token,
                                               @Field("treatmentName") String treatmentName,
                                               @Field("drugType") String drugType,
                                               @Field("drugName") String drugName,
                                               @Field("producers") String producers,
                                               @Field("effect") String effect,
                                               @Field("useInfo") String useInfo,
                                               @Field("reason") String reason,
                                               @Field("sideEffect") boolean sideEffect);

    /**
     * 添加禁忌
     *
     * @param token
     * @param typeId
     * @return
     */
    @FormUrlEncoded
    @POST("program/applyTaboo")
    Call<BaseCallModel<Object>> applyNewTabooScheme(@Field("token") String token,
                                                    @Field("treatmentName") String treatmentName,
                                                    @Field("programName") String programName,
                                                    @Field("describe") String describe,
                                                    @Field("sideEffect") boolean sideEffect);

    /**
     * 添加饮食
     *
     * @param token
     * @param typeId
     * @return
     */
    @FormUrlEncoded
    @POST("program/applyDiet")
    Call<BaseCallModel<Object>> applyNewFoodScheme(@Field("token") String token,
                                                   @Field("treatmentName") String treatmentName,
                                                   @Field("name") String name,
                                                   @Field("effect") String effect,
                                                   @Field("useInfo") String useInfo,
                                                   @Field("sideEffect") boolean sideEffect);


    /**
     * 添加休息
     *
     * @param token
     * @param typeId
     * @return
     */
    @FormUrlEncoded
    @POST("program/applyRest")
    Call<BaseCallModel<Object>> applyRestScheme(@Field("token") String token,
                                                @Field("treatmentName") String treatmentName,
                                                @Field("programName") String programName,
                                                @Field("effect") String effect,
                                                @Field("describe") String describe,
                                                @Field("sideEffect") boolean sideEffect);


    /**
     * 获取平台药方
     *
     * @param token
     * @return
     */
    @GET("program/getDrug")
    Call<BaseCallModel<List<Prescription>>> getPrescribe(@Query("token") String token);


    /**
     * 获取成为医生的协议内容
     *
     * @param type   1-医生注册协议、2-患者注册协议、3-功能介绍、4-如何视频问诊、5-视频操作指导、6-关于我们
     * @param pageNo 页码  不传递获取全部
     * @return
     */
    @GET("getContent")
    Call<BaseCallModel<AgreenDataBean>> getTermsContent(@Query("token") String token, @Query("type") int type);


    /**
     * 获取银行种类接口
     *
     * @return
     */
    @GET("getBanks")
    Call<BaseCallModel<List<String>>> getBanks(@Query("token") String token);


    /**
     * 获取兑换验证码
     *
     * @return
     */
    @GET("integral/getCode")
    Call<BaseCallModel<String>> getCode(@Query("token") String token);

    /**
     * 绑定银行卡接口
     *
     * @return
     */
    @GET("integral/getCodeByBindBank")
    Call<BaseCallModel<String>> getCodeByBindBank(@Query("token") String token, @Query("phone") String phone);


    /**
     * 约定复查接口
     * 1-未到复查，2-已到复查，3-已过复查
     *
     * @return
     */
    @GET("diagnostic/getRevisits")
    Call<BaseCallModel<List<ReviewBean>>> getRevisits(@Query("token") String token, @Query("type") String type);

}
