/*
package com.ygbd198.hellodoctor.widget.dialog;

import android.view.View;
import android.widget.Button;

import com.kelin.client.R;

*/
/**
 * Created by monty on 2017/7/26.
 *//*


public class PictureChoicesDialog extends BaseBottomDialog implements View.OnClickListener {
    @Override
    public int getLayoutRes() {
        return R.base_dilog_layout.picture_choices_dialog;
    }

    @Override
    public void bindView(View v) {
        Button btnTakePhoto = (Button) v.findViewById(R.id.btn_takePhoto);
        Button btnChoicePhoto = (Button) v.findViewById(R.id.btn_choicePhoto);
        Button btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnTakePhoto.setOnClickListener(this);
        btnChoicePhoto.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_takePhoto:
                // TODO: 2017/7/26 打开照相机
                onClickListener.onTakePhotoClick();
                break;

            case R.id.btn_choicePhoto:
                // TODO: 2017/7/26 打开系统相册
                onClickListener.onChoicePhoto();
                break;

            case R.id.btn_cancel:
                this.dismissAllowingStateLoss();
                break;
        }
    }
    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener{
        void onTakePhotoClick();
        void onChoicePhoto();
    }

}
*/
