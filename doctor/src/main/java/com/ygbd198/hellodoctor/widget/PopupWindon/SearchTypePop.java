package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/9/1.
 */

public abstract class SearchTypePop extends PopupWindow {

    public abstract void onPopDismiss();
    public abstract void onItemOnClick(DiseaseTypeBean diseaseTypeBean);


    private Context context;
    private View contentView;

    private ListView typeList;

    private List<DiseaseTypeBean> diseaseTypeBeans;
    private SearchTypeAdpter adpter;
    private DiseaseTypeBean curDiseaseTypeBean;

    public void setDiseaseTypeBeans(List<DiseaseTypeBean> diseaseTypeBeans) {
        this.diseaseTypeBeans.clear();
        this.diseaseTypeBeans.addAll(diseaseTypeBeans);
        adpter.notifyDataSetChanged();
    }

    public void showPopTopCenter() {
        this.setAnimationStyle(R.style.pop_anim_style_top_center);
        this.showAsDropDown(((Activity) context).findViewById(R.id.lin_title), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);
    }

    public void showPopTopLeft() {
        this.setAnimationStyle(R.style.pop_anim_style_top_feft);
        this.showAsDropDown(((Activity) context).findViewById(R.id.lin_title), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);
    }

    public void showPopTopRight() {
        this.setAnimationStyle(R.style.pop_anim_style_top_right);
        this.showAsDropDown(((Activity) context).findViewById(R.id.lin_title), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);
    }

    public SearchTypePop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.pop_disease_type, null);

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        setSoftInputMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
                onPopDismiss();
            }
        });

        initView();

    }

    private void initView() {
        contentView.findViewById(R.id.lin_title).setVisibility(View.GONE);
        typeList = (ListView) contentView.findViewById(R.id.list_type);
        adpter = new SearchTypeAdpter(context, null == diseaseTypeBeans ? diseaseTypeBeans = new ArrayList<>() : diseaseTypeBeans);
        typeList.setAdapter(adpter);
        typeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!diseaseTypeBeans.get(position).isChecked) {
                    for (DiseaseTypeBean bean : diseaseTypeBeans) {
                        bean.isChecked = false;
                    }
                    diseaseTypeBeans.get(position).isChecked = true;
                    curDiseaseTypeBean = diseaseTypeBeans.get(position);
                    onItemOnClick(curDiseaseTypeBean);
                    adpter.notifyDataSetChanged();
                }
            }
        });
    }
}

//适配器
class SearchTypeAdpter extends BaseAdapter {

    private Context context;
    private List<DiseaseTypeBean> diseaseTypeBeans;

    public SearchTypeAdpter(Context context, List<DiseaseTypeBean> diseaseTypeBeans) {
        this.context = context;
        this.diseaseTypeBeans = diseaseTypeBeans;

    }

    @Override
    public int getCount() {
        return diseaseTypeBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return diseaseTypeBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_select_prescribe, parent, false);
            holder = new Holder();
            holder.name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.imgSelect = (ImageView) convertView.findViewById(R.id.img_select);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        DiseaseTypeBean diseaseTypeBean = diseaseTypeBeans.get(position);
        holder.name.setText(diseaseTypeBean.typeName);

        if (diseaseTypeBean.isChecked) {
            holder.name.setTextColor(Color.parseColor("#f8b9cd"));
            holder.imgSelect.setVisibility(View.VISIBLE);
        } else {
            holder.name.setTextColor(Color.parseColor("#11345e"));
            holder.imgSelect.setVisibility(View.GONE);
        }

        return convertView;
    }


    class Holder {
        TextView name;
        ImageView imgSelect;
    }

}
