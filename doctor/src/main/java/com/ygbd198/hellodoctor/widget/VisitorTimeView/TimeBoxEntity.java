package com.ygbd198.hellodoctor.widget.VisitorTimeView;

/**
 * Created by Administrator on 2017/8/5 0005.
 */

public class TimeBoxEntity {
    public float top;//位置
    public float bottom;
    public float left;
    public float right;
    public String time;//显示的时间
    public boolean isDel;//是否刪除的标记
    public boolean isAdd;//是否添加的标记
    public boolean isAddView;//是否是最后一个添加的View
}
