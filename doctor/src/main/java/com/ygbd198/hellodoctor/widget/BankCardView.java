package com.ygbd198.hellodoctor.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;

/**
 * Created by guangjiqin on 2017/8/28.
 * 银行卡view
 */

public class BankCardView extends LinearLayout {


    private int backgoundColorResId;
    private int iconResId;
    private String cardName;
    private String cardNumAllStr;
    private String cardNumStr;
    private String cardTypeStr;


    private LinearLayout relCardBackgound;
    private ImageView imgIcon;
    private TextView tvCardName;
    private TextView cardNumAll;
    private TextView cardNum;
    private TextView cardType;


    //设置银行卡背景、标志、名字、卡号、类别
    public void setCardNumAll(String cardNumAllStr) {
        this.cardNumAllStr = cardNumAllStr;
        cardNumAll.setText(cardNumAllStr);
    }

    public void setCardNum(String cardNumStr) {
        this.cardNumStr = cardNumStr;
        cardNum.setText(cardNumStr);
    }

    public void setCardsType(String cardTypeStr) {
        this.cardTypeStr = cardTypeStr;
        cardType.setText(cardTypeStr);
    }

    public void setBackgoundColorResId(int backgoundColorResId) {
        this.backgoundColorResId = backgoundColorResId;
        relCardBackgound.setBackgroundResource(backgoundColorResId);
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
        imgIcon.setImageResource(iconResId);
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
        tvCardName.setText(cardName);
    }

    public BankCardView(Context context) {
        super(context);
        initView();
    }

    public BankCardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public BankCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bank_cards, this,false);
        relCardBackgound = (LinearLayout) view.findViewById(R.id.rel_card);
        imgIcon = (ImageView)view.findViewById(R.id.img_bank_icon);
        tvCardName = (TextView)view.findViewById(R.id.tv_name_cards);
        cardNumAll = (TextView)view.findViewById(R.id.tv_num_1);
        cardNum = (TextView)view.findViewById(R.id.tv_num_2);
        cardType = (TextView)view.findViewById(R.id.tv_type);

        addView(view);
    }
}
