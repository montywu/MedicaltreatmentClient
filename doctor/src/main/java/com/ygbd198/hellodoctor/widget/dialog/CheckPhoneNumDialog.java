package com.ygbd198.hellodoctor.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.VerifyCodeEditText;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/6.
 */

public class CheckPhoneNumDialog extends Dialog implements View.OnClickListener {

    private Context context;

    private String code;
    private TextView tvPhoneNum;
    private VerifyCodeEditText tvCode;
    private Button cancal;
    private Button commit;
    private MyIntegralBean.BankBean bank;

    public void setTvPhoneNum(TextView tvPhoneNum) {
        this.tvPhoneNum = tvPhoneNum;
    }

    public CheckPhoneNumDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public CheckPhoneNumDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_check_phone_num, null);
        setContentView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        tvPhoneNum = (TextView) view.findViewById(R.id.tv_phone_num);
        tvCode = (VerifyCodeEditText) view.findViewById(R.id.tv_ver_code);
        cancal = (Button) view.findViewById(R.id.cancal);
        commit = (Button) view.findViewById(R.id.commit);


        String phone = String.valueOf(MySelfInfo.getInstance().getPhoneNum());
        phone = phone.substring(7, 11);
        tvPhoneNum.setText("手机号  *******" + phone);
        cancal.setOnClickListener(this);
        commit.setOnClickListener(this);

        tvCode.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {
            @Override
            public boolean onClick(View v) {
                RetrofitHelper.getInstance().createService(MyCenterService.class).getCode(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                        ToastUtils.showToast("验证码发送成功");
                        code = response.body().data;
                    }

                    @Override
                    public void onFailure(String message) {
                        ToastUtils.showToast("验证码发送失败");
                    }
                });

                return true;
            }
        });


    }

    public void showDialog() {
        CheckPhoneNumDialog.this.show();
    }

    public void hideDialog() {
        CheckPhoneNumDialog.this.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancal:
                CheckPhoneNumDialog.this.hide();
                break;
            case R.id.commit:
                if (tvCode.getVerifyCode().isEmpty()) {
                    ToastUtils.showToast("请输入验证码");
                    return;
                }
                if (!tvCode.getVerifyCode().equals(code)) {
                    ToastUtils.showToast("验证码不正确");
                    return;
                }
                CheckPhoneNumDialog.this.hide();
                ExchangeIntergralDialog exchangeIntergralDialog = new ExchangeIntergralDialog(context,code);
                exchangeIntergralDialog.setBank(bank);
                exchangeIntergralDialog.showDialog();
                break;
        }
    }

    public void setBank(MyIntegralBean.BankBean bank) {
        this.bank = bank;
    }
}
