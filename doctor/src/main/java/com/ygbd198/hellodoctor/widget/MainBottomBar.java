package com.ygbd198.hellodoctor.widget;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.ygbd198.hellodoctor.R;

import static com.ygbd198.hellodoctor.R.id.tab1;


/**
 * Created by wangbin on 2017/7/9.
 */

public class MainBottomBar extends FrameLayout implements View.OnClickListener {
    private View currentSelectedView;
    private onTabSelectedListener mOnTabSelectedListener;

    private LinearLayout container;

    public MainBottomBar(@NonNull Context context) {
        this(context, null);
    }

    public MainBottomBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainBottomBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View mainBottomView = LayoutInflater.from(context).inflate(R.layout.main_bottom_bar_lauyout, this);
        this.container = (LinearLayout) mainBottomView.findViewById(R.id.container);
        View tab1 = mainBottomView.findViewById(R.id.tab1);
        tab1.setOnClickListener(this);
        View tab2 = mainBottomView.findViewById(R.id.tab2);
        tab2.setOnClickListener(this);
        View tab3 = mainBottomView.findViewById(R.id.tab3);
        tab3.setOnClickListener(this);
    }

    public LinearLayout getContainer(){
        return container;
    }

    public void setOnTabSelectedListener(onTabSelectedListener mOnTabSelectedListener) {
        this.mOnTabSelectedListener = mOnTabSelectedListener;
    }

    public void setFistTab() {
        currentSelectedView = findViewById(tab1);
        currentSelectedView.setSelected(true);
        if (this.mOnTabSelectedListener != null) {
//            mOnTabSelectedListener.onTagSelected(0, tab1, "");
            currentSelectedView.performClick();
        }
    }

    @Override
    public void onClick(View v) {
        if (currentSelectedView == v) {
            return;
        }
        int tabId = 0;
        int index = 0;
        String tabName = "";
        if (currentSelectedView != null) {
            currentSelectedView.setSelected(false);
        }
        v.setSelected(true);
        currentSelectedView = v;
        switch (v.getId()) {
            case tab1:
                tabId = tab1;
                tabName = "诊疗";
                index = 0;
                break;
            case R.id.tab2:
                tabId = R.id.tab2;
                tabName = "消息";
                index = 1;
                break;
            case R.id.tab3:
                tabId = R.id.tab3;
                tabName = "我的";
                index = 2;
                break;

        }
        if (mOnTabSelectedListener != null) {
            mOnTabSelectedListener.onTagSelected(index,tabId, tabName);
        }

    }

    public interface onTabSelectedListener {
        void onTagSelected(int position,int tabId, String tabName);
    }
}
