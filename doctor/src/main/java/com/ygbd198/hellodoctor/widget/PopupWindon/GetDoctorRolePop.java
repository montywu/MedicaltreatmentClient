package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/22.
 */

public abstract class GetDoctorRolePop extends PopupWindow {
    public abstract void onClickCommit(DoctorRoleBean curDoctorRoleBean);


    private Context context;
    private View contentView;

    private ListView typeList;
    private Button btCommit;
    private TextView tvType;

    private DoctorRoleBean curDoctorRoleBean;
    private List<DoctorRoleBean> doctorRoleBeans;
    private DoctorAdpter adpter;

    public void setDoctorRoleBeans(List<DoctorRoleBean> doctorRoleBeans) {
        this.doctorRoleBeans.clear();
        this.doctorRoleBeans.addAll(doctorRoleBeans);
        adpter.notifyDataSetChanged();
    }


    public void showPop() {
        this.setAnimationStyle(R.style.pop_anim_style);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_main), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);

    }

    public GetDoctorRolePop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.pop_disease_type, null);

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        setSoftInputMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
            }
        });

        initView();

        setOnClick();
    }

    private void initView() {
        tvType = (TextView) contentView.findViewById(R.id.tv_type);
        tvType.setText(R.string.level_professional);
        btCommit = (Button) contentView.findViewById(R.id.bt_submit);
        btCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDoctorRolePop.this.dismiss();
                onClickCommit(null == curDoctorRoleBean ? doctorRoleBeans.get(0) : curDoctorRoleBean);
            }
        });
        typeList = (ListView) contentView.findViewById(R.id.list_type);
        adpter = new DoctorAdpter(context, null == doctorRoleBeans ? doctorRoleBeans = new ArrayList<>() : doctorRoleBeans);
        typeList.setAdapter(adpter);
        typeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                for (int i = 0; i < typeList.getChildCount(); i++) {
//                    View itemView = typeList.getChildAt(i);
//                    itemView.findViewById(R.id.tv_type).setBackgroundColor(Color.parseColor("#e6e6e6"));
//                }
//                tvType.setBackgroundColor(Color.parseColor("#ffffff"));
                tvType.setText(context.getString(R.string.level_professional) + doctorRoleBeans.get(position).role);
                curDoctorRoleBean = doctorRoleBeans.get(position);
            }
        });
    }

    private void setOnClick() {
    }
}

//适配器
class DoctorAdpter extends BaseAdapter {

    private Context context;
    private List<DoctorRoleBean> doctorRoleBeen;

    public DoctorAdpter(Context context, List<DoctorRoleBean> doctorRoleBeen) {
        this.context = context;
        this.doctorRoleBeen = doctorRoleBeen;

    }

    @Override
    public int getCount() {
        return doctorRoleBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return doctorRoleBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_disease_type, parent, false);
            holder = new Holder();
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        DoctorRoleBean doctorRoleBean = doctorRoleBeen.get(position);
        holder.tvType.setText(doctorRoleBean.role);
        return convertView;
    }


    class Holder {
        TextView tvType;
        TextView tvTime;
    }
}
