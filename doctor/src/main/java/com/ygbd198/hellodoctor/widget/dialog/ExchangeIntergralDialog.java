package com.ygbd198.hellodoctor.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.RegionNumberEditText;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/6.
 */

public class ExchangeIntergralDialog extends Dialog implements View.OnClickListener, RegionNumberEditText.InfTextChange {

    private Context context;
    private RegionNumberEditText etMoney;
    private TextView tvMoney;
    private TextView tvBank;

    private Button cancal;
    private Button commit;
    private MyIntegralBean.BankBean bank;
    private String code;

    protected ExchangeIntergralDialog(@NonNull Context context, String code) {
        super(context);
        this.context = context;
        this.code  = code;
        initView();

    }


    protected ExchangeIntergralDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_exchange_integral, null);
        setContentView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        etMoney = (RegionNumberEditText) view.findViewById(R.id.et_money);
        etMoney.setInputType(InputType.TYPE_CLASS_NUMBER);
        etMoney.setRegion(9999999, 0);
        etMoney.setSelection(etMoney.getText().length());
        etMoney.setInfTextChange(this);
        etMoney.setTextWatcher();
        tvMoney = (TextView) view.findViewById(R.id.tv_money);
        tvBank = (TextView) view.findViewById(R.id.tv_bank);

        cancal = (Button) view.findViewById(R.id.cancal);
        commit = (Button) view.findViewById(R.id.commit);

        cancal.setOnClickListener(this);
        commit.setOnClickListener(this);

    }


    public void showDialog() {
        ExchangeIntergralDialog.this.show();
    }

    public void hideDialog() {
        ExchangeIntergralDialog.this.dismiss();
    }


    public void setBank(MyIntegralBean.BankBean bank) {
        this.bank = bank;
        tvBank.setText(bank.name + "（" + bank.afterFour + "）");
    }

    @Override
    public void textChange(String s) {
        double intergral = Double.valueOf(s);
        tvMoney.setText("（¥" + (intergral / 100) + "）");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancal:
                ExchangeIntergralDialog.this.hideDialog();
                break;
            case R.id.commit:
                RetrofitHelper.getInstance().createService(MyCenterService.class).takeMoney(MySelfInfo.getInstance().getToken(), code,Long.valueOf(etMoney.getText().toString())).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

                    @Override
                    public void onSuccess(Response<BaseCallModel<Object>> response) {
                        ((Activity)context).finish();
                        ToastUtils.showToast("兑换成功");
                    }

                    @Override
                    public void onFailure(String msg) {
                        ToastUtils.showToast(msg);
                    }
                });
                break;
        }

    }
}
