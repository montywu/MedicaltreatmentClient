package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;

/**
 * Created by guangjiqin on 2017/8/4.
 * 选择医生等级的pop
 */

public abstract class GetImgWayPop extends PopupWindow implements View.OnClickListener {

    private Context context;
    private View contentView;


    private TextView tvPhotograph;
    private TextView tvGetPhone;
    private Button btCancel;

    public abstract String takePhoto();//拍照

    public abstract String getPhotoFromSD();//获取相册照片


    public void showPop() {
        this.setAnimationStyle(R.anim.pop_show);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_root), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);

    }

    public GetImgWayPop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.layout_way_get_img, null);


        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = (float) 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
            }
        });

        tvPhotograph = (TextView) contentView.findViewById(R.id.tvPhotograph);
        tvGetPhone = (TextView) contentView.findViewById(R.id.tv_get_phone);
        btCancel = (Button) contentView.findViewById(R.id.bt_cancel);

        tvPhotograph.setOnClickListener(this);
        tvGetPhone.setOnClickListener(this);
        btCancel.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvPhotograph://拍照
                takePhoto();
                break;
            case R.id.tv_get_phone://打开相册
                getPhotoFromSD();
                break;
            default://取消
                GetImgWayPop.this.dismiss();
                break;

        }
        GetImgWayPop.this.dismiss();
    }
}
