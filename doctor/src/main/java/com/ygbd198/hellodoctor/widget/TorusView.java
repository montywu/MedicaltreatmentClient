package com.ygbd198.hellodoctor.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.ygbd198.hellodoctor.util.DisplayUtil;

/**
 * Created by guangjiqin on 2017/8/24.
 * 自定义的圆环进度view
 */

public class TorusView extends View {

    private Context context;

    private int progress = 80;


    private Paint innerCirclePaint;//内圆
    private Paint outCirclePaint;//外圆
    private Paint progressPaint;//进度画笔
    private Paint textPaint;// 文字画笔

    private int radiusInner;
    private int radiusOut;


    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    public TorusView(Context context) {
        super(context);
        this.context = context;
        invalidate();
    }

    public TorusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initview();
    }


    public TorusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initview();
    }


    private void initview() {
        innerCirclePaint = new Paint();
        innerCirclePaint.setAntiAlias(true);
        innerCirclePaint.setColor(Color.parseColor("#11345e"));


        outCirclePaint = new Paint();
        outCirclePaint.setAntiAlias(true);
        outCirclePaint.setColor(Color.parseColor("#e6e6e6"));

        progressPaint = new Paint();
        progressPaint.setAntiAlias(true);
        progressPaint.setColor(Color.parseColor("#f8b9cd"));

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.parseColor("#f8b9cd"));

//        radiusInner = DisplayUtil.px2dp(context, radiusInner);
//        radiusOut = DisplayUtil.px2dp(context, radiusOut);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth() / 2;
        int y = getHeight() / 2;

        radiusOut = getWidth() / 3;
        radiusInner = radiusOut - DisplayUtil.dp2px(context, 15);

        //画外圆
        canvas.drawCircle(x, y, radiusOut, outCirclePaint);

        //画进度
        RectF oval = new RectF();                     //RectF对象
        oval.left = x - radiusOut;                              //左边
        oval.top = y - radiusOut;                                   //上边
        oval.right = x + radiusOut;                             //右边
        oval.bottom = y + radiusOut;
        canvas.drawArc(oval, -90, ((float) progress / 100) * 360, true, progressPaint);

        //画内圆
        canvas.drawCircle(x, y, radiusInner, innerCirclePaint);

        if (progress == 100) {

            textPaint.setTextSize(DisplayUtil.dp2px(context, 35));
            canvas.drawText("" + progress, x - DisplayUtil.dp2px(context, 40), y + DisplayUtil.dp2px(context, 10), textPaint);

            textPaint.setTextSize(DisplayUtil.dp2px(context, 17));
            canvas.drawText("%", x + DisplayUtil.dp2px(context, 20), y + DisplayUtil.dp2px(context, 10), textPaint);
        }else{
            textPaint.setTextSize(DisplayUtil.dp2px(context, 35));
            canvas.drawText("" + progress, x - DisplayUtil.dp2px(context, 30), y + DisplayUtil.dp2px(context, 10), textPaint);

            textPaint.setTextSize(DisplayUtil.dp2px(context, 17));
            canvas.drawText("%", x + DisplayUtil.dp2px(context, 15), y + DisplayUtil.dp2px(context, 10), textPaint);
        }
    }
}
