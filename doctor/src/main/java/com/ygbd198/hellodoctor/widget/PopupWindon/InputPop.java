package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;


/**
 * Created by guangjiqin on 2017/8/17.
 */

public abstract class InputPop extends PopupWindow implements View.OnClickListener {


    public abstract void onClickCommit(String content);


    private View contentView;
    private TextView tvTitle;
    private EditText etInput;
    private Button btCommit;
    private Button btCancel;

    private Context context;

    public EditText getEtInput() {
        return etInput;
    }

    public void setTitle(int resourcesId) {
        tvTitle.setText(resourcesId);
    }

    public void setTitle(String text) {
        tvTitle.setText(text);
    }

    public void clreanEdieText() {
        etInput.setText("");
    }


    public void showPop() {
        this.setAnimationStyle(R.style.pop_anim_style);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_main), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);
    }

    public InputPop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.pop_input, null);

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        setSoftInputMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
            }
        });

        initView();

        setOnClick();

    }

    public void setOnClick() {
        btCancel.setOnClickListener(this);
        btCommit.setOnClickListener(this);
    }


    private void initView() {
        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        etInput = (EditText) contentView.findViewById(R.id.et_input);
        btCommit = (Button) contentView.findViewById(R.id.bt_commit);
        btCancel = (Button) contentView.findViewById(R.id.bt_cancel);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_commit:
                onClickCommit(etInput.getText().toString());
                break;
            case R.id.bt_cancel:
                break;
        }
        InputPop.this.dismiss();
        etInput.setText("");
    }
}
