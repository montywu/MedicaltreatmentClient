package com.ygbd198.hellodoctor.widget.VisitorTimeView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.bigkoo.pickerview.TimePickerView;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.util.DisplayUtil;
import com.ygbd198.hellodoctor.widget.DatePickerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/4.
 * 添加坐诊时间的自定义view
 */

public class SetVisitorTimeView extends View {

    private Context context;
    private String TAG = getClass().getName();

    public List<TimeBoxEntity> timeBoxEntities;//view时间小盒子的实体
    private List<String> visitorTmies;//显示在界面上的时间集合
    private int num = 3;// 没一行的个数

    private Paint textAdd;//添加时间的笔
    private Paint timeBox;//时间盒子的画笔
    private Paint textTimeBox;//时间盒子里面的文字画笔
    private TimePickerView startTimePickerView;
    private TimePickerView endTimePickerView;

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    private boolean hasFocus = true;//有焦点、可议操作


    public void setTime(List<String> visitorTmies) {//设置访问时间
        this.visitorTmies = visitorTmies;
        initview();
        invalidate();
    }

    public SetVisitorTimeView(Context context) {
        super(context);
        this.context = context;
        initview();
    }

    public SetVisitorTimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initview();
    }

    public SetVisitorTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initview();
    }

    private void initview() {
        if (null == visitorTmies) {
            visitorTmies = new ArrayList<>();
        }

        //===测试数据====
//        visitorTmies.add("1");
//        visitorTmies.add("2");
//        visitorTmies.add("3");
//        visitorTmies.add("4");
//        visitorTmies.add("5");
//        visitorTmies.add("6");


        //===============

        timeBoxEntities = new ArrayList<>();
        for (String strTime : visitorTmies) {
            TimeBoxEntity timeBoxEntity = new TimeBoxEntity();
            timeBoxEntity.time = strTime;
            timeBoxEntities.add(timeBoxEntity);

        }

        if (timeBoxEntities.size() < 6 && hasFocus) {            //手动添加最后一个添加按钮
            TimeBoxEntity timeBoxEntity = new TimeBoxEntity();
            timeBoxEntity.time = getContext().getString(R.string.add_time);
            timeBoxEntity.isAddView = true;
            timeBoxEntities.add(timeBoxEntity);
        }


        timeBox = new Paint();
        timeBox.setAntiAlias(true);
        timeBox.setColor(Color.parseColor("#11345e"));

        textTimeBox = new Paint();
        textTimeBox.setTextSize(DisplayUtil.dp2px(context, 10));
        textTimeBox.setAntiAlias(true);
        textTimeBox.setColor(Color.parseColor("#ff7fb9"));

        textAdd = new Paint();
        textAdd.setTextSize(DisplayUtil.dp2px(context, 10));
        textAdd.setAntiAlias(true);
        textAdd.setColor(Color.parseColor("#ff7fb9"));

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = timeBoxEntities.size();
        if (size < 4) {
            for (int i = 0; i < size; i++) {
                TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);

                int x = (getMeasuredWidth() / (2 * num)) + ((i % num) * (getMeasuredWidth() / num));
                int y = getMeasuredHeight() / 2;


                if (timeBoxEntity.isAddView) {
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 3), textAdd);
                } else {
                    RectF rectf = new RectF(x - getWidth() / 8, y - getHeight() / 5, x + getWidth() / 8, y + getHeight() / 5);
                    canvas.drawRect(rectf, timeBox);
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 3), textTimeBox);
                    if (hasFocus) {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.apply_icon_del);
                        canvas.drawBitmap(bitmap, x + getWidth() / 9, y - getHeight() / 4, textTimeBox);
                    }

                }


                timeBoxEntity.top = y - 30;
                timeBoxEntity.bottom = y + 30;
                timeBoxEntity.left = x - 100;
                timeBoxEntity.right = x + 100;

            }
        } else {
            for (int i = 0; i < size; i++) {
                TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                int y = 0;
                if (i < 3) {
                    y = getMeasuredHeight() / 3 - (getHeight() / 10);
                } else {
                    y = ((2 * getMeasuredHeight()) / 3) + (getHeight() / 10);
                }
                int x = (getMeasuredWidth() / (2 * num)) + ((i % num) * (getMeasuredWidth() / num));

                if (timeBoxEntity.isAddView) {
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textAdd);
                } else {
                    RectF rectf = new RectF(x - getWidth() / 8, y - getHeight() / 5, x + getWidth() / 8, y + getHeight() / 5);
                    canvas.drawRect(rectf, timeBox);
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textTimeBox);
                    if (hasFocus) {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.apply_icon_del);
                        canvas.drawBitmap(bitmap, x + getWidth() / 9, y - getHeight() / 4, textTimeBox);
                    }
                }

                timeBoxEntity.top = y - 30;
                timeBoxEntity.bottom = y + 30;
                timeBoxEntity.left = x - 100;
                timeBoxEntity.right = x + 100;

            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int moveNum = 0;
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (int i = 0; i < timeBoxEntities.size(); i++) {
                    TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                    if (timeBoxEntity.left < x && x < timeBoxEntity.right && timeBoxEntity.top < y && y < timeBoxEntity.bottom) {
                        if (!timeBoxEntity.isAddView) {
                            timeBoxEntity.isDel = true;
                        } else {
                            timeBoxEntity.isAdd = true;
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                moveNum++;
                break;
            case MotionEvent.ACTION_UP:
                if (moveNum < 5) {
                    for (int i = 0; i < timeBoxEntities.size(); i++) {
                        TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                        if (timeBoxEntity.isDel) {
                            timeBoxEntities.remove(timeBoxEntity);
                            if (timeBoxEntities.size() == 5 && !timeBoxEntities.get(timeBoxEntities.size() - 1).isAddView) {
                                TimeBoxEntity addEntity = new TimeBoxEntity();
                                addEntity.time = getContext().getString(R.string.add_time);
                                addEntity.isAddView = true;
                                timeBoxEntities.add(addEntity);
                            }
                            invalidate();
                            break;
                        }
                        if (timeBoxEntity.isAdd && timeBoxEntity.isAddView && timeBoxEntities.size() < 8) {
                            showStartPop();
                            break;
                        }
                    }

                } else {
                    for (TimeBoxEntity entity : timeBoxEntities) {
                        entity.isAdd = false;
                        entity.isDel = false;
                    }
                }
                break;
        }
        return hasFocus;
    }

    private void showStartPop() {
//        DateChooseWheelViewDialog endDateChooseDialog = new DateChooseWheelViewDialog(context,
//                new DateChooseWheelViewDialog.DateChooseInterface() {
//                    @Override
//                    public void getDateTime(String time, boolean longTimeChecked) {
//                        showEndPop(time);
//                    }
//                });
//        endDateChooseDialog.setTimePickerGone(true);
//        endDateChooseDialog.setDateDialogTitle("开始时间");
//        endDateChooseDialog.showDateChooseDialog();


        startTimePickerView = DatePickerFactory.createTimePicker(context, "开始时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                startTimePickerView.dismissDialog();
                showEndPop(date);

            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimePickerView.returnData();
            }
        });
        startTimePickerView.show();
    }

    private void showEndPop(final Date startTime) {
//        DateChooseWheelViewDialog endDateChooseDialog = new DateChooseWheelViewDialog(context,
//                new DateChooseWheelViewDialog.DateChooseInterface() {
//                    @Override
//                    public void getDateTime(String time, boolean longTimeChecked) {
//                        if (checkTimeCur(startTime, time)) {
//                            TimeBoxEntity addEntity = new TimeBoxEntity();
//                            addEntity.time = startTime + "～" + time;
//                            timeBoxEntities.add(timeBoxEntities.size() - 1, addEntity);
//                            if (timeBoxEntities.size() == 7) {
//                                timeBoxEntities.remove(6);
//                            }
//                            invalidate();
//                        }
//
//                    }
//                });
//
//        endDateChooseDialog.setTimePickerGone(true);
//        endDateChooseDialog.setDateDialogTitle("结束时间");
//        endDateChooseDialog.showDateChooseDialog();

        endTimePickerView = DatePickerFactory.createTimePicker(context, "结束时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                endTimePickerView.dismissDialog();

                DateFormat df = new SimpleDateFormat("HH:mm");

                if (checkTimeCur(startTime, date)) {
                    TimeBoxEntity addEntity = new TimeBoxEntity();
                    addEntity.time = df.format(startTime) + "-" + df.format(date);
                    timeBoxEntities.add(timeBoxEntities.size() - 1, addEntity);
                    if (timeBoxEntities.size() == 7) {
                        timeBoxEntities.remove(6);
                    }
                    invalidate();
                }


            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTimePickerView.returnData();
            }
        });
        endTimePickerView.show();
    }

    private boolean checkTimeCur(Date startTime, Date endTime) {
        // TODO: 2017/8/21 (时间校对、校对开始时间、结束时间等正确性的校验)
        return true;
    }
}
