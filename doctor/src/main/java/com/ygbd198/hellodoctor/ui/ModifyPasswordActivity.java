package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.APP;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.User;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.LoginService;
import com.ygbd198.hellodoctor.util.LoginUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PasswordEditText;
import com.ygbd198.hellodoctor.widget.VerifyCodeEditText;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/3.
 */

public class ModifyPasswordActivity extends BaseActivity {

    private LoadingDialog loadingDialog;

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verifyCodeLayout)
    VerifyCodeEditText etVerifyCodeLayout;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    @BindView(R.id.et_verifyPwdLayout)
    PasswordEditText etVerifyPwdLayout;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_modify_ps, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.forget_password, R.drawable.forgot_password, 0);


        this.etVerifyCodeLayout.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {

            @Override
            public boolean onClick(View v) {
                String telPhone = etPhone.getText().toString();
                if (!LoginUtil.checkTelPhone(telPhone)) {
                    return false;
                }
                RetrofitHelper.getInstance().createService(LoginService.class).getCodeByType(telPhone, 1).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                        ToastUtils.showToast(R.string.getverify_success);
                    }

                    @Override
                    public void onFailure(String message) {
                        ToastUtils.showToast(message);
                    }
                });
                return true;
            }
        });

    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {

        String telPhone = etPhone.getText().toString();
        if (!LoginUtil.checkTelPhone(telPhone)) return;

        if (!LoginUtil.checkVerifyCode(etVerifyCodeLayout.getVerifyCode())) return;

        if (!LoginUtil.checkPassword(etPwdLayout.getPassword())) return;

        if (!LoginUtil.checkPassword(etVerifyPwdLayout.getPassword())) return;

        if (!etPwdLayout.getPassword().equals(etVerifyPwdLayout.getPassword())) {
            ToastUtils.showToast("两次输入的密码不一致");
            return;
        }
        showDialog("正在修改密码");
        RetrofitHelper.getInstance().createService(LoginService.class).userForgotPassword(telPhone, etVerifyPwdLayout.getPassword(), etVerifyCodeLayout.getVerifyCode()).enqueue(new BaseCallBack<BaseCallModel<User>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {
                ToastUtils.showToast("密码修改成功，请重新登录");
                ModifyPasswordActivity.this.finish();
                APP.getInstance().startLoginActivity();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeDialog();
            }
        });
    }


    private void showDialog(String msg) {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
            if (loadingDialog.isShowing()) {
                loadingDialog.close();
            }
            loadingDialog.show(msg);
        }
    }
    private void closeDialog() {
        if (loadingDialog != null) {
            loadingDialog.close();
        }
    }


}
