package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.PatientRecordBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.List;

/**
 * Created by Administrator on 2017/9/10 0010.
 */

public class PatientRecordAdapter extends MyBaseAdapter {

    public PatientRecordAdapter(Context context, List data) {
        super(context, data);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_patient_record, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_photo);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PatientRecordBean bean = (PatientRecordBean) mData.get(position);
        GlideImageHelper.showImage(mContext, bean.userPic, R.drawable.logo, R.drawable.logo, holder.imgPhoto );
        holder.tvName.setText(bean.userName);
        holder.tvTime.setText(bean.createTime);
        return convertView;
    }

    class Holder {
        ImageView imgPhoto;
        TextView tvName;
        TextView tvTime;
    }
}
