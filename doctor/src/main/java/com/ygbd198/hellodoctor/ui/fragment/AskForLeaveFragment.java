package com.ygbd198.hellodoctor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.DatePickerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class AskForLeaveFragment extends Fragment implements View.OnClickListener {

    private View view;
    private TextView tvStartTime;
    private TextView tvEndTime;
    private Button btCommit;

    private static AskForLeaveFragment askForLeaveFragment;
    private TimePickerView startTimePickerView;
    private TimePickerView endTimePickerView;

    public static AskForLeaveFragment createInstance() {
        if (null == askForLeaveFragment) {
            askForLeaveFragment = new AskForLeaveFragment();
        }
        return askForLeaveFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ask_for_leave, null);
        tvStartTime = (TextView) view.findViewById(R.id.tv_start);
        tvEndTime = (TextView) view.findViewById(R.id.tv_end);
        btCommit = (Button) view.findViewById(R.id.bt_commit);
        btCommit.setOnClickListener(this);
        tvStartTime.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start:
                showStartPop("开始时间", tvStartTime);
                break;
            case R.id.tv_end:
                showStartPop("结束时间", tvEndTime);
                break;
            case R.id.bt_commit:
                if (tvStartTime.getText().toString().isEmpty()) {
                    ToastUtils.showToast("未设置请假开始时间");
                    return;
                }
                if (tvEndTime.getText().toString().isEmpty()) {
                    ToastUtils.showToast("未设置请假结束时间");
                    return;
                }

                Log.e("gg","startTime = "+tvStartTime.getText().toString());
                Log.e("gg","endTime = "+tvEndTime.getText().toString());

                RetrofitHelper.getInstance().createService(MyCenterService.class).applyLeave(MySelfInfo.getInstance().getToken(), tvStartTime.getText().toString(), tvEndTime.getText().toString()).
                        enqueue(new BaseCallBack<BaseCallModel<Object>>() {

                            @Override
                            public void onSuccess(Response<BaseCallModel<Object>> response) {
                                ToastUtils.showToast("申请成功");
                                getActivity().finish();
                            }

                            @Override
                            public void onFailure(String msg) {
                                ToastUtils.showToast(msg);
                            }
                        });

                break;
        }
    }


    private void showStartPop(final String title, final TextView textView) {

        startTimePickerView = DatePickerFactory.createDatePicker("下一步", getActivity(), "请选择开始变更时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                startTimePickerView.dismissDialog();
                showEndPop(date, title, textView);

            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimePickerView.returnData();
            }
        });
        startTimePickerView.show();
    }

    private void showEndPop(final Date startTime, final String title, final TextView textView) {

        endTimePickerView = DatePickerFactory.createTimePicker(getActivity(), "请选择开始变更时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                endTimePickerView.dismissDialog();

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String [] times = (df.format(startTime) + df.format(date)).split(" ");
                textView.setText(times[0]+" "+times[2]);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTimePickerView.returnData();
            }
        });
        endTimePickerView.show();
    }
}
