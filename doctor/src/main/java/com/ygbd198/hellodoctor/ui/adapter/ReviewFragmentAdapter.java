package com.ygbd198.hellodoctor.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ygbd198.hellodoctor.ui.fragment.ReviewFragemntAlready;
import com.ygbd198.hellodoctor.ui.fragment.ReviewFragemntExpired;
import com.ygbd198.hellodoctor.ui.fragment.ReviewFragemntNo;

/**
 * Created by guangjiqin on 2017/8/10.
 */

public class ReviewFragmentAdapter extends MyFragmentPagerAdapter {

    int fragmentNum = 3;


    public ReviewFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ReviewFragemntNo unusedGiftCouponsFragement = ReviewFragemntNo.createInstance();
                return unusedGiftCouponsFragement;
            case 1:
                ReviewFragemntAlready usedGiftCouponsFragement = ReviewFragemntAlready.createInstance();
                return  usedGiftCouponsFragement;
            case 2:
                ReviewFragemntExpired exceedGiftCouponsFragement = ReviewFragemntExpired.createInstance();
                return exceedGiftCouponsFragement;

        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return fragmentNum;
    }
}
