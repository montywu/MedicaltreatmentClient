package com.ygbd198.hellodoctor.ui.live.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.ygbd198.hellodoctor.ui.live.fragment.LFAppointmentFragment;
import com.ygbd198.hellodoctor.ui.live.fragment.LFDiseaselistFragment;
import com.ygbd198.hellodoctor.ui.live.fragment.LFPrescriptionFragment;
import com.ygbd198.hellodoctor.ui.live.fragment.LFTabooFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by monty on 2017/8/26.
 */

public class TabFragmentPagerAdapter extends FragmentPagerAdapter{
    public static final int FRAGMENT_COUNT = 6;
    private Map<Integer,Fragment> fragments = new HashMap<>(FRAGMENT_COUNT);

    public TabFragmentPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        Log.d("LFSchemeTotalFragment", "TabFragmentPagerAdapter - oncreate");
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Log.d("LFSchemeTotalFragment", "TabFragmentPagerAdapter - getItem -> position >" + position);
        switch (position) {
            case 0:
                fragment = LFDiseaselistFragment.newInstance();
                break;
            case 1:
                fragment = LFPrescriptionFragment.newInstance();
                break;
            case 2:
                fragment = LFTabooFragment.newInstance(1);
                break;
            case 3:
                fragment = LFTabooFragment.newInstance(2);
                break;
            case 4:
                fragment = LFTabooFragment.newInstance(3);
                break;
            case 5:
                fragment = LFAppointmentFragment.newInstance();
                break;
        }
        fragments.put(position,fragment);
        return fragment;
    }

    public Fragment getFragment(int position){
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

}
