package com.ygbd198.hellodoctor.ui.live.event;

/**
 * Created by monty on 2017/9/12.
 */

public class TaboosEvent {
    public static final int DIET = 0x101; // 饮食
    public static final int REST = 0x102; // 休息
    public static final int TABOO = 0x103; // 禁忌
    private int type;
    private String ids;

    public TaboosEvent(int type, String ids) {
        this.type = type;
        this.ids = ids;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIds() {
        return ids;
    }

    public void setId(String ids) {
        this.ids = ids;
    }
}
