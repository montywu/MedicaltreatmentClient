package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.PatientRecordBean;
import com.ygbd198.hellodoctor.bean.PatientRecordIntegralBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/9/10 0010.
 */

public class PatientRecordIntegralAdapter extends MyBaseAdapter {

    public PatientRecordIntegralAdapter(Context context, List data) {
        super(context, data);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_patient_record_integral, null);
            holder.tvIntegral = (TextView) convertView.findViewById(R.id.tv_integral);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PatientRecordIntegralBean bean = (PatientRecordIntegralBean) mData.get(position);

        String integral = 0 < bean.integral ? "+" + bean.integral + "积分" : "" + bean.integral + "积分";
        holder.tvIntegral.setText(integral);

        holder.tvTime.setText(bean.createTime);
        return convertView;
    }

    class Holder {
        TextView tvIntegral;
        TextView tvTime;
    }
}
