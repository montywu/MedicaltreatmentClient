package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.ui.fragment.ActualOperatingFragment;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/10/19.
 */

public class ActualOperatingActivity extends FragmentActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private List<Integer> imgRes;
    private List<ActualOperatingFragment> fragments;
    private MyPagerAdapter pagerAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actual_operating);
        ButterKnife.bind(this);


        imgRes = new ArrayList<>();
        imgRes.add(R.drawable.sc1);
        imgRes.add(R.drawable.sc2);
        imgRes.add(R.drawable.sc3);
        imgRes.add(R.drawable.sc3);
        imgRes.add(R.drawable.sc4);
        imgRes.add(R.drawable.sc5);
        imgRes.add(R.drawable.sc6);
        imgRes.add(R.drawable.sc7);
        imgRes.add(R.drawable.sc8);
        imgRes.add(R.drawable.sc9);
        imgRes.add(R.drawable.sc10);
        imgRes.add(R.drawable.sc11);
        imgRes.add(R.drawable.sc12);
        imgRes.add(R.drawable.sc13);
        imgRes.add(R.drawable.sc14);
        imgRes.add(R.drawable.sc15);
        imgRes.add(R.drawable.sc16);
        imgRes.add(R.drawable.sc17);
        imgRes.add(R.drawable.sc18);
        imgRes.add(R.drawable.sc19);
        imgRes.add(R.drawable.sc20);


        fragments = new ArrayList<>();

        for (int i = 0; i < imgRes.size(); i++) {
            ActualOperatingFragment fragment = ActualOperatingFragment.newInstance(imgRes.get(i));
            fragments.add(fragment);
        }

        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(pagerAdapter);

    }

}

class MyPagerAdapter extends FragmentPagerAdapter {

    List<ActualOperatingFragment> fragments;

    public MyPagerAdapter(FragmentManager fm, List<ActualOperatingFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(final int position) {

        Log.e("gg", "item == " + position);
        if(position == 20){
            RetrofitHelper.getInstance().createService(ApplyBeDocService.class).practicePass(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                @Override
                public void onSuccess(Response<BaseCallModel<String>> response) {
                    ToastUtils.showToast("通过实操演练");
                    fragments.get(position).getActivity().finish();
                }

                @Override
                public void onFailure(String msg) {
                   ToastUtils.showToast(msg);
                }
            });
        }

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}