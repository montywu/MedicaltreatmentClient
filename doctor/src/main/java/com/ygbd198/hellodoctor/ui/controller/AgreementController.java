package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.AgreenDataBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/7.
 * 注册同意条款控制页面
 */

public abstract class AgreementController {

    private Context context;
    private String token;

    public abstract void getTermsContentSuccess(AgreenDataBean dataBean);

    public abstract void getTermsContentFailure(String msg);

    public abstract void agreeTermsContentSuccess();

    public abstract void agreeTermsContentFailure(String msg);

    public AgreementController(Context context, String token) {
        this.context = context;
        this.token = token/* = "eh1231871583wt"*/;
    }

    //获取协议内容
    public void getTermsContent(int type) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getTermsContent(token,type).enqueue(new BaseCallBack<BaseCallModel<AgreenDataBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<AgreenDataBean>> response) {
              getTermsContentSuccess(response.body().data);

            }

            @Override
            public void onFailure(String message) {
                getTermsContentFailure(message);
            }
        });

    }

    //同意协议内容
    public void agreeTermsContent() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).agreeTerms(token).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                agreeTermsContentSuccess();
            }

            @Override
            public void onFailure(String message) {
                agreeTermsContentFailure(message);
            }
        });
    }

}
