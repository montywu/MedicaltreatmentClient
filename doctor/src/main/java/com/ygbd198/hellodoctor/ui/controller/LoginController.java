package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.monty.library.BaseApp;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.tencent.TIMManager;
import com.tencent.ilivesdk.ILiveCallBack;
import com.tencent.ilivesdk.core.ILiveLoginManager;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.Constants;
import com.ygbd198.hellodoctor.service.LoginService;
import com.ygbd198.hellodoctor.ui.ActualOperatingActivity2;
import com.ygbd198.hellodoctor.ui.ApplyBeDocActivity;
import com.ygbd198.hellodoctor.ui.InputUserInfActivity;
import com.ygbd198.hellodoctor.ui.LoginActivity;
import com.ygbd198.hellodoctor.ui.MainActivity;
import com.ygbd198.hellodoctor.ui.ModifyPasswordActivity;
import com.ygbd198.hellodoctor.util.LoginUtil;
import com.ygbd198.hellodoctor.util.SharePreferencesHelp;
import com.ygbd198.hellodoctor.util.ToastUtils;

import retrofit2.Response;

/**
 * Created by Administrator on 2017/8/2 0002.
 * 新分支提交测试
 */

public class LoginController {
    private Context context;

    public LoginController(Context context) {
        this.context = context;
    }

    public void login(String name, String password) {
        Log.d("monty - login","name -> "+name+" ,password -> "+password);
        if (!LoginUtil.checkTelPhone(name.toString().trim()) || !LoginUtil.checkPassword(password)) {
            return;
        }
        ((LoginActivity) context).showLoadingDialog("");
        RetrofitHelper.getInstance().createService(LoginService.class).login(name.toString(), password).enqueue(new BaseCallBack<BaseCallModel<DoctorBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorBean>> response) {
                DoctorBean doctor = response.body().data;

                MySelfInfo.getInstance().setSig(doctor.getSig());
                MySelfInfo.getInstance().setId(doctor.getId());
                MySelfInfo.getInstance().setToken(doctor.getToken());
                MySelfInfo.getInstance().setStatu(doctor.getStatu());
                MySelfInfo.getInstance().setPracticeVideoIds(doctor.getPracticeVideoIds());
                MySelfInfo.getInstance().setPhoto(doctor.getPhoto());
                BaseApp.getInstance().setTime(doctor.getTime());
                MySelfInfo.getInstance().createTime = doctor.createTime;

                loginILive(doctor.getToken(), doctor.getSig(), doctor);
                ((LoginActivity) context).closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                ((LoginActivity) context).closeLoadingDialog();
            }
        });
    }

    /**
     * 登录直播平台
     *
     * @param id
     * @param sig
     */
    public void loginILive(String id, String sig, final DoctorBean doctor) {
        ((LoginActivity) context).showLoadingDialog("正在登录直播平台");
        ILiveLoginManager.getInstance().iLiveLogin(id, sig, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                Log.d("monty", "iLiveLogin->env: " + TIMManager.getInstance().getEnv());

                Log.d("monty", "status : " + doctor.getStatu());
                ((LoginActivity) context).closeLoadingDialog();
                checkStatus(doctor);
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Log.e("monty", "iLiveLogin - onError: " + errMsg);
                ToastUtils.showToast(errMsg);
                ((LoginActivity) context).closeLoadingDialog();
            }
        });
    }

    private void checkStatus(final DoctorBean doctor) {
        switch (doctor.getStatu()) {
            case 0: // 不可用，停留在登录页面，提示账号不可用
                ToastUtils.showToast("账号不可用");
                break;
            case 1:// 合格医生，走正常流程
                ((LoginActivity) context).finish();
                MainActivity.GotoActivity(context, false);
                break;
            case 101:// 已注册成功，资料未填写完成跳转到证书上传资料填写页面
                Intent intent = new Intent(context, ApplyBeDocActivity.class);
                intent.putExtra("bean", doctor);
                context.startActivity(intent);
                break;
            case 102:// 已提交申请，正在审核；停在登录页面 提示“正在审核您的信息，审核结果1-3个工作日内会以短信的形式通知您，请留意!”
                ToastUtils.showToast("正在审核您的信息，审核结果1-3个工作日内会以短信的形式通知您，请留意!");
                break;
            case 103: // 审核失败需重新提交申请；提示“审核失败请重新提交申请” 点确定 跳转到上传证书资料填写页面
                new AlertDialog.Builder(context).setMessage("审核失败请重新提交申请").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, ApplyBeDocActivity.class);
                        intent.putExtra("bean", doctor);
                        context.startActivity(intent);
                    }
                }).show();
                break;
            case 104: // 审核通过但未观看完视频；跳转到坐诊页面-显示视频观看进度Fragment TreatmentLearningFragment
                ((LoginActivity) context).finish();
                MainActivity.GotoActivity(context, true);
                break;
            case 105: // 观看完视频但未实操演练；跳转到坐诊页面-显示视频观看进度Fragment ，如果进度为100则实操演练可点击，跳转到功能指南页面 TreatmentLearningFragment
//                ((LoginActivity) context).finish();
//                context.startActivity(new Intent(context, FunctionActivity.class));
                context.startActivity(new Intent(context, ActualOperatingActivity2.class));
                break;
            case 106: // 通过实操演练未完善资料；跳转到坐诊页面-显示资料完成度(进度框),按钮显示完善资料 UserInfProgressFragment
//                context.startActivity(new Intent(context, InputUserInfActivity.class));
                ((LoginActivity) context).finish();
                MainActivity.GotoActivity(context, true);
                break;
            case 107: // 完善资料正在审核中，走正常流程，显示坐诊页面，点击坐诊提示资料正在审核中
                ((LoginActivity) context).finish();
                MainActivity.GotoActivity(context, true);
                break;
            case 108: // 审核失败需重新完善；跳转到完善资料页面

                ((LoginActivity) context).finish();
//                MainActivity.GotoActivity(context, true);
                context.startActivity(new Intent(context, InputUserInfActivity.class));
                break;
            case 109: // 显示坐诊页面点击坐诊时直接跳转到签订协议页面
                ((LoginActivity) context).finish();
                MainActivity.GotoActivity(context, true);
                break;
        }
    }

    public void clearPassword(Context context) {
        SharePreferencesHelp.removeValue(context, Constants.KEY_USER_NAME);
        SharePreferencesHelp.removeValue(context, Constants.KEY_PASS_WORD);
    }

    public void rememberPassword(Context context, String name, String ps) {
        if (name.isEmpty() || ps.isEmpty()) {
            ToastUtils.showToast(context, R.string.name_pasw_null);
        } else {
            SharePreferencesHelp.putString(context, Constants.KEY_USER_NAME, name);
            SharePreferencesHelp.putString(context, Constants.KEY_PASS_WORD, ps);
        }
    }

    public void forgottenPassword() {
        context.startActivity(new Intent(context, ModifyPasswordActivity.class));
    }
}
