package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.ReviewBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class ReviewListAdapter extends MyBaseAdapter {

    public ReviewListAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_review, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_photo);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ReviewBean bean = (ReviewBean) mData.get(position);
        holder.tvName.setText(bean.userName);
        holder.tvTime.setText(bean.revisitDate);
        GlideImageHelper.showImage(mContext, bean.userPic, R.drawable.logo, R.drawable.logo, holder.imgPhoto );
        return convertView;
    }

    class Holder {
        ImageView imgPhoto;
        TextView tvName;
        TextView tvTime;
    }
}
