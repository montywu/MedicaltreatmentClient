package com.ygbd198.hellodoctor.ui;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.util.DateUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.MyVideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoActivity extends BaseActivity {

    @BindView(R.id.videoView)
    MyVideoView videoView;
    @BindView(R.id.btn_pause)
    ImageButton btnPause;
    @BindView(R.id.prg_progress)
    SeekBar prgProgress;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.capView)
    View capView;

    private String videoUrl;
    private boolean isPlayer;

    private MediaPlayer mediaPlayer;

    public static void GotoVideoActivity(Context context, String videoUrl) {
        Intent intent = new Intent(context, VideoActivity.class);
        intent.putExtra("videoUrl", videoUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video2);
        ButterKnife.bind(this);


        titleBar.showCenterText("自我介绍视频", R.drawable.self_introduction, 0);

        videoUrl = getIntent().getStringExtra("videoUrl");

        /*if (BuildConfig.DEBUG) {
            videoUrl = "http://123.207.9.75:8081/upload-file/introduce_url/2017-11-12/27480b2b58a546c8ba6d2464bf10f031.mp4";
        }*/
        if (TextUtils.isEmpty(videoUrl)) {
            ToastUtils.showToast("视频地址不存在");
        } else {
            try {
                Uri uri = Uri.parse(videoUrl);
                initVideoView(uri);
            } catch (Exception e) {
                ToastUtils.showToast("视频地址错误");
            }
        }


    }


    private void initVideoView(Uri uri) {
//        String path = "http://123.207.9.75:8081/upload-file/introduce_url/2017-10-13/bd587411774f4ca99d8d10f0d09f5801.mp4";
//        final Uri uri = Uri.parse(path);
        videoView.setVideoURI(uri);
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer = mp;
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video started
                            Log.d("monty", "videoView -> video started");
//                            videoView.setBackgroundColor(Color.TRANSPARENT);
                            capView.setVisibility(View.GONE);
                            return true;
                        }
                        return false;
                    }
                });

                prgProgress.setMax(mp.getDuration());
                isPlayer = true;
                setBtnPause(true);
                handler.postDelayed(runnable, 100);
                Log.d("monty", "videoView -> OnPrepared");
                mp.start();
                Log.d("monty", "videoView -> start");
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                isPlayer = false;
                setBtnPause(false);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("monty", "videoView -> OnError");
                videoView.setVisibility(View.GONE);
                mp.release();
                handler.removeCallbacks(runnable);
                return false;
            }
        });

    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (isPlayer) {
                    prgProgress.setProgress(videoView.getCurrentPosition());
                    tvTime.setText(DateUtil.secondToMinute(videoView.getCurrentPosition() / 1000) + "/" + DateUtil.secondToMinute(mediaPlayer.getDuration() / 1000));
                    prgProgress.setSecondaryProgress(videoView.getBufferPercentage());
                }
                handler.post(runnable);
            } catch (Exception e) {
                e.printStackTrace();
                handler.removeCallbacks(runnable);
                tvTime.setText("");
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }

    @OnClick(R.id.btn_pause)
    public void onViewClicked() {
        if (isPlayer) {
            videoView.pause();
            btnPause.setImageResource(R.drawable.apply_icon_play);
        } else {
            videoView.start();
            btnPause.setImageResource(R.drawable.apply_icon_pause);
        }
        isPlayer = !isPlayer;
    }

    private void setBtnPause(boolean isPlay) {
        if (isPlay) {
            btnPause.setImageResource(R.drawable.apply_icon_pause);
        } else {
            btnPause.setImageResource(R.drawable.apply_icon_play);
        }
    }
}
