package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.ApplyMoreTypePop;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public class ApplyMoreTypeActivity extends BaseActivity {

    @BindView(R.id.tv_type)
    TextView tvType;

    private DoctorInfBean bean;

    private List<DiseaseTypeBean> diseaseTypeBeen;//获取的全部类型


    private ApplyMoreTypePop diseaseTypePop;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_apply_more_type, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        initData();

        getUserInf();
    }

    private void initSelectBean() {
        String[] typeName = bean.doctorAttach.treatmentNames.split(",");

        for (String name : typeName) {
            for (DiseaseTypeBean typeBean : diseaseTypeBeen) {
                if (name.equals(typeBean.typeName)) {
                    typeBean.isChecked = true;
                }
            }
        }
    }

    protected void initView() {
        tvType.setText(bean.doctorAttach.treatmentNames);
    }


    @OnClick(R.id.bt_next)
    public void commit() {
        List<Integer> ids = new ArrayList<>();
        for (DiseaseTypeBean bean : diseaseTypeBeen) {
            if (bean.isChecked) {
                ids.add(bean.id);
            }
        }

        Log.e("gg", "ids = " + ids.toString());
        RetrofitHelper.getInstance().createService(MyCenterService.class).applyMoreTYpe(MySelfInfo.getInstance().getToken(), ids).enqueue(new BaseCallBack<BaseCallModel<Object>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                ToastUtils.showToast("您的申请已提交，审核结果请注意系统通知。");
                ApplyMoreTypeActivity.this.finish();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    @OnClick(R.id.tv_type)
    public void showPop() {
        if (null == diseaseTypePop) {
            diseaseTypePop = new ApplyMoreTypePop(this) {
                @Override
                public void onClickCommit(List<DiseaseTypeBean> diseaseTypeBeans) {
                    String tempType = "";
                    for (DiseaseTypeBean bean : diseaseTypeBeans) {
                        if (bean.isChecked) {
                            if (tempType.isEmpty()) {
                                tempType = bean.typeName;
                            } else {
                                tempType = tempType + "," + bean.typeName;
                            }
                        }
                    }
                    tvType.setText(tempType);

                }

                @Override
                public void onClickCommit(DiseaseTypeBean diseaseTypeBean) {
                }

            };
        }
        diseaseTypePop.setDiseaseTypeBeans(diseaseTypeBeen);
        diseaseTypePop.showPop();
    }

    @Override
    protected void initData() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDiseaseType(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<DiseaseTypeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DiseaseTypeBean>>> response) {
                diseaseTypeBeen = response.body().data;
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }


    //获取个人资料
    public void getUserInf() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                bean = response.body().data;
                initView();
                initSelectBean();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void initTitle() {
        titleBar.showCenterText(R.string.more_scheme, R.drawable.apply_types, 0);
    }
}
