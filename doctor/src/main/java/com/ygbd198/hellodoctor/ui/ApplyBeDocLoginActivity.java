package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.monty.library.BaseApp;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.ApplyBeDocLoginController;
import com.ygbd198.hellodoctor.util.LoginUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PasswordEditText;
import com.ygbd198.hellodoctor.widget.VerifyCodeEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ygbd198.hellodoctor.R.id.btn_login;

/**
 * Created by guangjiqin on 2017/8/19.
 */

public class ApplyBeDocLoginActivity extends BaseActivity {

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_pwdLayout)
    VerifyCodeEditText etCode;
    @BindView(R.id.et_password)
    PasswordEditText etPassword;

    private ApplyBeDocLoginController controller;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View contentView = this.getLayoutInflater().inflate(R.layout.activity_apply_doc_login, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);
        titleBar.showCenterText("申请成为医生");
        initController();

        initView();

    }

    @OnClick(btn_login)
    public void loginToApplyBeDoc() {
        String telPhone = etPhone.getText().toString();
        if (!LoginUtil.checkTelPhone(telPhone)) return;

        if (!LoginUtil.checkVerifyCode(etCode.getVerifyCode())) return;

        String password = etPassword.getPassword().toString();
        if (!LoginUtil.checkPassword(password)) return;

        controller.verifyIdentity(telPhone, password, etCode.getVerifyCode());

    }

    @Override
    protected void initView() {
        etCode.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {

            @Override
            public boolean onClick(View v) {
                String telPhone = etPhone.getText().toString();
                if (!LoginUtil.checkTelPhone(telPhone)) {
                    return false;
                }

                controller.getCode(telPhone);
                return true;
            }
        });
    }

    private void initController() {
        controller = new ApplyBeDocLoginController(this) {
            @Override
            public void getCodeSuccess() {
                ToastUtils.showToast(R.string.getverify_success);
            }

            @Override
            public void getCodeFailure(String msg) {

            }

            @Override
            public void verifySuccess(DoctorBean bean) {
                finish();
                MySelfInfo.getInstance().setToken(bean.getToken());
                BaseApp.getInstance().setTime(bean.getTime());
                Intent intent = new Intent(ApplyBeDocLoginActivity.this, ApplyBeDocActivity.class);
                intent.putExtra("bean", bean);
                startActivity(intent);

            }

            @Override
            public void verifyFailure(String msg) {

                // FIXME: 2017/10/17 如果失败不做跳转
                /*if ("账号已存在".equals(msg)) {
                    finish();
                    Intent intentToMain = new Intent(ApplyBeDocLoginActivity.this, MainActivity.class);
                    intentToMain.putExtra("tag","BeDocLogin");
                    startActivity(intentToMain);
                }*/
                ToastUtils.showToast(msg);
            }
        };
    }


}
