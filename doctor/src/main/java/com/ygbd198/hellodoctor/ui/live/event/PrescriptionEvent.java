package com.ygbd198.hellodoctor.ui.live.event;

import com.ygbd198.hellodoctor.bean.Prescription;

import java.util.List;

/**
 * 药方Event
 * Created by monty on 2017/9/8.
 */

public class PrescriptionEvent {
    public List<Prescription> prescriptions;
    public PrescriptionEvent(List<Prescription> prescriptions){
        this.prescriptions = prescriptions;
    }
}
