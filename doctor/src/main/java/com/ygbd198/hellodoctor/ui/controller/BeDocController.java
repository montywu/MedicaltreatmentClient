package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.http.UploadRetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.ui.ApplyBeDocActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.DiseaseTypePop;
import com.ygbd198.hellodoctor.widget.PopupWindon.GetDoctorRolePop;
import com.ygbd198.hellodoctor.widget.PopupWindon.GetImgWayPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.InputPop;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


/**
 * Created by guangjiqin on 2017/8/4.
 */

public abstract class BeDocController {

    private Context context;
    private String token;

    private InputPop usernameInputPop;
    private InputPop hospitalInputPop;
    private DiseaseTypePop diseaseTypePop;
    private GetDoctorRolePop getDoctorRolePop;

    private List<DiseaseTypeBean> diseaseTypeBeen;
    private List<DoctorRoleBean> doctorRoleBeen;

    //获取治疗类型
    public abstract void getTypeSuccess(List<DiseaseTypeBean> diseaseTypeBeen);

    public abstract void getTypeFailure(String msg);

    //上传
    public abstract void uploadFileSuccess(int imgId, String url);

    public abstract void uploadFileFailure(String msg);

    //获取医生等级
    public abstract void getRoleSuccess(List<DoctorRoleBean> doctorRoleBeen);

    public abstract void getRoleFailure(String msg);

    // 提交医生资料
    public abstract void commitDoctorInfSuccess();

    public abstract void commitDoctorInfFailure(String msg);


    public BeDocController(Context context, String token) {
        this.context = context;
        this.token = token; /*= "eh1231871583wt";//测试*/
    }

    /**
     * 医生等级
     */
    public void getRole() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDoctorRole(token).enqueue(new BaseCallBack<BaseCallModel<List<DoctorRoleBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DoctorRoleBean>>> response) {
                doctorRoleBeen = response.body().data;
                getRoleSuccess(doctorRoleBeen);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getRoleFailure(msg);
            }
        });

    }


    /**
     * 获取治疗类型
     */
    public void getTypes() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDiseaseType(token).enqueue(new BaseCallBack<BaseCallModel<List<DiseaseTypeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DiseaseTypeBean>>> response) {
                diseaseTypeBeen = response.body().data;
                getTypeSuccess(diseaseTypeBeen);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getTypeFailure(msg);
            }
        });

    }

    /**
     * 视频文件上传
     */
    public void uploadVideoFile(final int imgId, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("", "");


        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

        List<MultipartBody.Part> parts = builder.build().parts();
        Log.e("gg", "开始视频上传请求");
        UploadRetrofitHelper.changeApiBaseUrl(UploadRetrofitHelper.VIDEO_UPLOAD_HOST);//视频上传接口使用8081
        UploadRetrofitHelper.getInstance().uploadImage(ApplyBeDocService.class).doctorVideoUpload(parts).enqueue(new Callback<BaseCallModel<List<String>>>() {
            @Override
            public void onResponse(Call<BaseCallModel<List<String>>> call, Response<BaseCallModel<List<String>>> response) {

                if (null == response || null == response.body() || null == response.body().data) {
                    uploadFileFailure("视频上传失败");
                    return;
                }
                saveURL(imgId, response.body().data);
            }


            @Override
            public void onFailure(Call<BaseCallModel<List<String>>> call, Throwable t) {
                t.printStackTrace();
                Log.e("gg", "视频上传失败" + call.request().body());
                uploadFileFailure("");
            }
        });
    }

    //保存文件上传url
    private void saveURL(final int imgId, List<String> data) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).saveIntroduceUrl(token, data.get(0)).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                uploadFileSuccess(imgId, "");
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }


    /**
     * 文件上传
     */
    public void uploadFile(final int imgId, File file, final int type) {
        Luban.with(context).load(file).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(File file) {

                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)//表单类型
                        .addFormDataPart("token", token)
                        .addFormDataPart("type", String.valueOf(type));

                RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

                List<MultipartBody.Part> parts = builder.build().parts();
                Log.e("gg", "开始上传请求 type = " + type);
                UploadRetrofitHelper.getInstance().uploadImage(ApplyBeDocService.class).doctorUpload(parts).enqueue(new Callback<BaseCallModel<List<String>>>() {
                    @Override
                    public void onResponse(Call<BaseCallModel<List<String>>> call, Response<BaseCallModel<List<String>>> response) {
                        Log.e("gg", "上传成功 = " + response.body().data);
                        if (null == response.body() || null == response.body().data) {
                            uploadFileSuccess(imgId, "");

                        } else {
                            uploadFileSuccess(imgId, response.body().data.get(0));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseCallModel<List<String>>> call, Throwable t) {
                        t.printStackTrace();
                        Log.e("gg", "上传失败" + call.request().body());
                        uploadFileFailure("");
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                ToastUtils.showToast("图片压缩失败，请重试");
            }
        }).launch();


    }

    /**
     * 提交资料
     * <p>
     * token true string 唯一标识token
     * name true string 医生名称
     * localHospital true string 所在医院
     * roleId true number 职称ID
     * treatmentTypeId true number 可坐诊类型ID
     * monday false string 坐诊时间-星期一
     * tuesday false string 坐诊时间-星期二
     * wednesday false string 坐诊时间-星期三
     * thursday false string 坐诊时间-星期四
     * friday false string 坐诊时间-星期五
     * saturday false string 坐诊时间-星期六
     * sunday false string 坐诊时间-星期天
     * workCertificate true string 工作证,多个地址用逗号隔开
     * identityCard true string 身份证,多个地址用逗号隔开
     * physicianCertificate true string 医师资历证,多个地址用逗号隔开
     * <p>
     * (这么多参数，就问你 怕不怕！！大声告诉我，你怕不怕？？！！)
     */
    public void commitDoctorInf(String name, String localHospital, int roleId, String treatmentTypeIds,
                                String monday, String tuesday, String wednesday, String thursday, String friday, String saturday, String sunday,
                                String workCertificate, String identityCard, String physicianCertificate) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).commitDoctorInf(token, name, localHospital, roleId, treatmentTypeIds,
                monday, tuesday, wednesday, thursday, friday, saturday, sunday,
                workCertificate, identityCard, physicianCertificate).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                commitDoctorInfSuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                commitDoctorInfFailure(msg);
            }
        });

    }

    //==============以下各种pop=============

    public void showDiseaseTypePop() {
        if (null == diseaseTypePop) {
            diseaseTypePop = new DiseaseTypePop(context);
//            diseaseTypePop = new DiseaseTypePop(context) {
//                @Override
//                public void onClickCommit(DiseaseTypeBean diseaseTypeBean) {
////                    ((ApplyBeDocActivity) context).tvType.setText(diseaseTypeBean.typeName);
////                    ((ApplyBeDocActivity) context).diseaseType = diseaseTypeBean.id;
//                }
//
//            };
        }
        diseaseTypePop.setDiseaseTypeBeans(diseaseTypeBeen);
        diseaseTypePop.showPop();
    }

    public void showUserNameInppop() {
        if (null == usernameInputPop) {
            usernameInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((ApplyBeDocActivity) context).tvName.setText(content);
                }
            };
            usernameInputPop.setTitle(R.string.input_user_name);
        }
        usernameInputPop.showPop();
    }

    public void showHospitalNameInpop() {
        if (null == hospitalInputPop) {
            hospitalInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((ApplyBeDocActivity) context).tvHospital.setText(content);
                }
            };
            hospitalInputPop.setTitle(R.string.input_hospital_name);
        }
        hospitalInputPop.showPop();
    }


    public void showSelectLevelPop() {
        if (null == getDoctorRolePop) {
            getDoctorRolePop = new GetDoctorRolePop(context) {

                @Override
                public void onClickCommit(DoctorRoleBean curDoctorRoleBean) {
                    ((ApplyBeDocActivity) context).tvLevel.setText(curDoctorRoleBean.role);
                    ((ApplyBeDocActivity) context).doctorRole = curDoctorRoleBean.id;
                }
            };
        }
        getDoctorRolePop.setDoctorRoleBeans(doctorRoleBeen);
        getDoctorRolePop.showPop();
    }

    public void showGetPhotoPop(final int viewId) {
        GetImgWayPop pop = new GetImgWayPop(context) {
            @Override
            public String takePhoto() {//
                Intent intentToCamera = new Intent();
                intentToCamera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);


                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                File path1 = new File(path);
                if (!path1.exists()) {
                    path1.mkdirs();
                }

                File file = new File(path1, String.valueOf(viewId) + ".jpg");//添加缓存文件
//                Uri mOutPutFileUri = Uri.fromFile(file);
                Uri mOutPutFileUri = FileProvider.getUriForFile(context, "gg.hello.dc", file);
                intentToCamera.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
                ((ApplyBeDocActivity) context).startActivityForResult(intentToCamera, viewId);
                return null;
            }

            @Override
            public String getPhotoFromSD() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra("return-data", true);
                ((ApplyBeDocActivity) context).startActivityForResult(intent, viewId * 100);


//                Intent intent = new Intent(Intent.ACTION_PICK, null);
//                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
//                ((ApplyBeDocActivity) context).startActivityForResult(intent, viewId * 100);

                return null;
            }
        };
        pop.showPop();
    }

    public void toMakeVideo(final int viewId) {
        Intent intentToVideo = new Intent();
        intentToVideo.setAction(MediaStore.ACTION_VIDEO_CAPTURE);

        String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
        File path1 = new File(path);
        if (!path1.exists()) {
            path1.mkdirs();
        }

        File file = new File(path1, String.valueOf(viewId) + ".mp4");//添加缓存文件
        Uri mOutPutFileUri = null;
        if (android.os.Build.VERSION.SDK_INT < 23) {
            mOutPutFileUri = Uri.fromFile(file);
        } else {
            mOutPutFileUri = FileProvider.getUriForFile(context, "gg.hello.dc", file);

        }
        intentToVideo.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
        ((ApplyBeDocActivity) context).startActivityForResult(intentToVideo, 11);
    }
}
