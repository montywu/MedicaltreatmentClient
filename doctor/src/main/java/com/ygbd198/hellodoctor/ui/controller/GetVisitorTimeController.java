package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.CurVisitorTimeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/4.
 */

public abstract class GetVisitorTimeController {

    public abstract void getDataSuccess(CurVisitorTimeBean curVisitorTimeBean);
    public abstract void getDataFsailrue(String msg);

    private Context context;
    private String token;


    public GetVisitorTimeController(Context context) {
        this.context = context;
        token = MySelfInfo.getInstance().getToken();
    }

    public void getData(){
        RetrofitHelper.getInstance().createService(MyCenterService.class).getCurVisitorTime(token).enqueue(new BaseCallBack<BaseCallModel<CurVisitorTimeBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<CurVisitorTimeBean>> response) {
                getDataSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getDataFsailrue(msg);
            }
        });
    }
}
