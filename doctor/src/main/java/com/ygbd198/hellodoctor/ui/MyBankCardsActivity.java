package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.MyBankCardsController;
import com.ygbd198.hellodoctor.widget.BankCardView;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/8/28.
 */

public class MyBankCardsActivity extends BaseActivity {

    private MyBankCardsController controller;
    private MyIntegralBean.BankBean card;
    private BankCardView cardView;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 1){
            setResult(1);
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_bank_cards, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText("银行卡");

        getIntentData();

        initView();

        initController();

    }

    private void initController() {
        controller = new MyBankCardsController(this) {
            @Override
            public void changeCardsFailure(String msg) {

            }

            @Override
            public void changeCardsSuccess(MyIntegralBean myIntegralBean) {

            }
        };
    }

    protected void initView() {

        cardView = (BankCardView) findViewById(R.id.card_view);
        if (null == card) {
            cardView.setVisibility(View.GONE);
        } else {
            cardView.setVisibility(View.VISIBLE);
            cardView.setCardName(card.name);
            cardView.setCardsType(card.type);
            cardView.setCardNum(String.valueOf(card.afterFour));
            switch (card.name) {
                case "工商银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_gs_bg);
                    cardView.setIconResId(R.drawable.icbc);
                    break;
                case "建设银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_js_bg);
                    cardView.setIconResId(R.drawable.ccb);
                    break;
                case "招商银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_zs_bg);
                    cardView.setIconResId(R.drawable.cmb);
                    break;
                case "农业银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_ny_bg);
                    cardView.setIconResId(R.drawable.abc);
                    break;
                case "中国银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_zg_bg);
                    cardView.setIconResId(R.drawable.boc);
                    break;
                case "交通银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_jt_bg);
                    cardView.setIconResId(R.drawable.bcm);
                    break;
                case "平安银行":
                    cardView.setBackgoundColorResId(R.drawable.button_bank_pa_bg);
                    cardView.setIconResId(R.drawable.ceb);
                    break;

            }
        }
    }

    private void getIntentData() {
        card = getIntent().getParcelableExtra("cardBean");
    }

    @OnClick(R.id.bt_change_card)
    public void changeCard() {
        startActivityForResult(new Intent(MyBankCardsActivity.this, ChangeBankCardsActivity.class),1);
    }
}

   