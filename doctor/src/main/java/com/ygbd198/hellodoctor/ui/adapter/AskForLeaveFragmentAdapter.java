package com.ygbd198.hellodoctor.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ygbd198.hellodoctor.common.MyFragmentPagerAdapter;
import com.ygbd198.hellodoctor.ui.fragment.AskForLeaveFragment;
import com.ygbd198.hellodoctor.ui.fragment.AskForLeaveRecordFragment;


/**
 * Created by guangjiqin on 2017/8/8.
 */

public class AskForLeaveFragmentAdapter extends MyFragmentPagerAdapter {


    private int fragmentCuont = 2;

    public AskForLeaveFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AskForLeaveFragment.createInstance();

            case 1:
                return AskForLeaveRecordFragment.createInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentCuont;
    }
}
