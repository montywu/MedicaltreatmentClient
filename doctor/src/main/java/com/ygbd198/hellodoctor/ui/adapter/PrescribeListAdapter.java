package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.bean.PrescriptionBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public class PrescribeListAdapter extends MyBaseAdapter {
    public PrescribeListAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.prescription_simple_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final PrescriptionBean.Prescription prescription = (PrescriptionBean.Prescription) mData.get(position);
        Glide.with(convertView.getContext())
                .load(TextUtils.isEmpty(prescription.pic) ? R.drawable.drug_icon : prescription.pic)
                .bitmapTransform(new RoundedCornersTransformation(convertView.getContext(), 10, 0))
                .into(viewHolder.pic);
        viewHolder.tvDrugName.setText(prescription.drugName);
        viewHolder.tvUseRules.setText("使用方法" + prescription.drugType);
        viewHolder.tvPrice.setText("￥" + prescription.price);
        viewHolder.checkbox.setVisibility(View.GONE);

        viewHolder.tvChengfen.setText("成分:" + prescription.component);
        viewHolder.tvZhengzhuang.setText("症状:" + prescription.treatmentName);
        viewHolder.tvGuige.setText("规格:" + prescription.drugStandard);
        viewHolder.tvShiyongfanwei.setText("适用范围:" + prescription.applicableScope);
        viewHolder.tvBuliangfanying.setText("不良反应:" + prescription.badEffect);
        viewHolder.tvJinji.setText("禁忌:" + prescription.taboo);
        viewHolder.tvZhuyishixiang.setText("注意事项:" + prescription.matters);
        viewHolder.tvYouxiaoqi.setText("有效期:" + prescription.effectiveTime);
        viewHolder.tvChucangfangshi.setText("贮藏方式:" + prescription.storage);
        viewHolder.tvShengchanchangshang.setText("生产厂商:" + prescription.producers);
        viewHolder.tvChanpinbiaozhunhao.setText("产品标准号:" + prescription.standardNumber);

        viewHolder.gonell.setVisibility(prescription.isOpen ? View.VISIBLE : View.GONE);
        viewHolder.ivPulldown.setImageResource(prescription.isOpen ? R.drawable.icon_pullup : R.drawable.icon_pulldown);
        viewHolder.ivPulldown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prescription.isOpen = !prescription.isOpen;
                notifyDataSetChanged();
            }
        });

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.checkbox)
        ImageView checkbox;
        @BindView(R.id.pic)
        ImageView pic;
        @BindView(R.id.tv_drugName)
        TextView tvDrugName;
        @BindView(R.id.tv_useRules)
        TextView tvUseRules;

        @BindView(R.id.tv_chengfen)
        TextView tvChengfen;
        @BindView(R.id.tv_zhengzhuang)
        TextView tvZhengzhuang;
        @BindView(R.id.tv_guige)
        TextView tvGuige;
        @BindView(R.id.tv_shiyongfanwei)
        TextView tvShiyongfanwei;
        @BindView(R.id.tv_buliangfanying)
        TextView tvBuliangfanying;
        @BindView(R.id.tv_jinji)
        TextView tvJinji;
        @BindView(R.id.tv_zhuyishixiang)
        TextView tvZhuyishixiang;
        @BindView(R.id.tv_youxiaoqi)
        TextView tvYouxiaoqi;
        @BindView(R.id.tv_chucangfangshi)
        TextView tvChucangfangshi;
        @BindView(R.id.tv_shengchanchangshang)
        TextView tvShengchanchangshang;
        @BindView(R.id.tv_chanpinbiaozhunhao)
        TextView tvChanpinbiaozhunhao;
        @BindView(R.id.gonell)
        LinearLayout gonell;
        @BindView(R.id.iv_pulldown)
        ImageView ivPulldown;

        @BindView(R.id.tv_price)
        TextView tvPrice;
        View itemView;

        ViewHolder(View view) {
            itemView = view;
            ButterKnife.bind(this, view);
        }
    }
}
