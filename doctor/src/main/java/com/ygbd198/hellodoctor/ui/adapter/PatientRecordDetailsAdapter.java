package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.PatientRecordDetailsBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class PatientRecordDetailsAdapter extends MyBaseAdapter {


    public PatientRecordDetailsAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_patient_record_details_contentview, null);
            holder.tvTop = (TextView) convertView.findViewById(R.id.tv_top);
            holder.imgGoods = (ImageView) convertView.findViewById(R.id.img_goods_photo);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_goods_name);
            holder.tvEffect = (TextView) convertView.findViewById(R.id.tv_goods_effect);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.tv_goods_price);
            holder.tvIntegral = (TextView) convertView.findViewById(R.id.integral);

            holder.relBottom = (RelativeLayout) convertView.findViewById(R.id.rel_bottom);
            holder.tvTotal = (TextView) convertView.findViewById(R.id.text_total);
            holder.tvRealTotal = (TextView) convertView.findViewById(R.id.tv_total);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        PatientRecordDetailsBean.RecordDrugsBean bean = (PatientRecordDetailsBean.RecordDrugsBean) mData.get(position);
        holder.tvName.setText(bean.drugName);
        holder.tvEffect.setText(bean.effect);
        holder.tvPrice.setText("¥" + String.valueOf(bean.price));

        String integral = "0";
        if (!TextUtils.isEmpty(bean.integral) && !bean.integral.equals("null")) {
            integral = bean.integral;
        }
        holder.tvIntegral.setText("+ " + integral + " 积分");
        GlideImageHelper.showImage(mContext, bean.pic, R.drawable.logo, R.drawable.logo, holder.imgGoods);


        if (0 == position) {
            holder.tvTop.setVisibility(View.VISIBLE);
        } else {
            holder.tvTop.setVisibility(View.GONE);
        }
        if (0 != position && mData.size() - 1 != position) {
            holder.tvTop.setVisibility(View.GONE);
            holder.relBottom.setVisibility(View.GONE);
        }
        if (mData.size() - 1 == position) {
            holder.relBottom.setVisibility(View.VISIBLE);
            double total = 0;
            for (Object detailsBean : mData) {
                PatientRecordDetailsBean.RecordDrugsBean tempDetailsBean = (PatientRecordDetailsBean.RecordDrugsBean) detailsBean;
                total = total + tempDetailsBean.price;
            }
            holder.tvTotal.setText("总计 " + String.valueOf(total) + " 元 实际支付: ");
//            holder.tvRealTotal.setText("¥" + String.valueOf(total));
            holder.tvRealTotal.setText("¥" + getRealTotalPrice());
        } else {
            holder.relBottom.setVisibility(View.GONE);
        }

        return convertView;
    }

    private double getRealTotalPrice() {
        double realTotalPrice = 0;
        for (int i = 0; i < mData.size(); i++) {
            PatientRecordDetailsBean.RecordDrugsBean drug = (PatientRecordDetailsBean.RecordDrugsBean) mData.get(i);
            if ("true".equals(drug.isBuy)) {
                realTotalPrice += drug.price;
            }
        }
        return realTotalPrice;
    }

    class Holder {
        TextView tvTop;
        ImageView imgGoods;
        TextView tvName;
        TextView tvEffect;
        TextView tvPrice;
        TextView tvIntegral;

        RelativeLayout relBottom;
        TextView tvTotal;
        TextView tvRealTotal;
    }
}
