package com.ygbd198.hellodoctor.ui.controller;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.ExchangeRecordBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/28.
 */

public abstract class ExchangeIntegralController {


    public abstract void getDataSuccess(List<ExchangeRecordBean> exchangeRecordBeen);

    public abstract void getDataFailrue();

    private String token;

    public ExchangeIntegralController() {
        this.token = MySelfInfo.getInstance().getToken();
    }

    public void getData() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getExchangeRecord(token).enqueue(new BaseCallBack<BaseCallModel<List<ExchangeRecordBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<ExchangeRecordBean>>> response) {
                getDataSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getDataFailrue();
            }
        });
    }
}
