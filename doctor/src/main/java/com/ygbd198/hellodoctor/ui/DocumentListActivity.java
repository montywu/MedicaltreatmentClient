package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DocumentBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.DocumentListAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/9/13.
 */

public class DocumentListActivity extends BaseActivity {

    private int activityCode = 0;//1学历证  2资历认证
    private List<DocumentBean> data;
    private String[] urls;
    private GridView gridView;

    private View contentView;
    private DocumentListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = this.getLayoutInflater().inflate(R.layout.activity_document_list, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        getIntentData();

        initTitle();

        initView();
    }

    protected void initView() {
        adapter = new DocumentListAdapter(this, data);
        gridView = (GridView) contentView.findViewById(R.id.document_list);
        gridView.setAdapter(adapter);

        if (2 == activityCode) {
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == data.size() - 1) {//最后一项 添加
                        ToastUtils.showToast("添加资历照");
                    }
                }
            });
        }
    }

    private void getIntentData() {
        activityCode = getIntent().getIntExtra("activityCode", -1);
        String tempString;
        if (1 == activityCode) {
            tempString = getIntent().getStringExtra("workCertificate");
        } else {
            tempString = getIntent().getStringExtra("physicianCertificate");
        }
        tempString = null == tempString ? "" : tempString;
        urls = tempString.split(",");
        data = new ArrayList<>();
        for (String url : urls) {
            if (!url.isEmpty()) {
                DocumentBean bean = new DocumentBean();
                bean.url = url;
                data.add(bean);
            }
        }
        if (activityCode == 2) {
            DocumentBean bean = new DocumentBean();
            data.add(bean);
        }
    }

    private void initTitle() {
        if (1 == activityCode) {
            titleBar.showCenterText("学历证");
        } else {
            titleBar.showCenterText("资历认证");
        }
    }
}
