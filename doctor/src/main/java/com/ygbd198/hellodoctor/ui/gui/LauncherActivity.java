package com.ygbd198.hellodoctor.ui.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.ui.GuideActivity;
import com.ygbd198.hellodoctor.util.PreferencesManager;

/**
 * Created by kelin on 2017/6/9.
 * 闪屏activity
 */

public class LauncherActivity extends AppCompatActivity {

    private boolean isFirst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ImageView imageV = (ImageView) findViewById(R.id.imageV);
        imageV.setImageResource(R.drawable.launcher);
        imageV.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkFirst();
                PreferencesManager.putBoolean("isFirstInstall", false);
            }
        }, 1500);
    }

    private void checkFirst() {
        isFirst = PreferencesManager.getBoolean("isFirstInstall", true);
        this.finish();
        if (!isFirst) {
            startActivity(new Intent(this,GuideActivity.class));
        }else{
//            startActivity(new Intent(this,ScanTkeLotteryActivity.class));
            startActivity(new Intent(this,GuiActivity.class));
        }
    }
}
