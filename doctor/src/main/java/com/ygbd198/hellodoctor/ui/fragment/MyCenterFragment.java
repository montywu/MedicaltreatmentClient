package com.ygbd198.hellodoctor.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.monty.library.EventBusUtil;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.event.UserInfoUptateEvent;
import com.ygbd198.hellodoctor.modules.contactservice.ContactServiceActivity;
import com.ygbd198.hellodoctor.ui.ApplyMoreTypeActivity;
import com.ygbd198.hellodoctor.ui.ApplyNewSchemeActivity;
import com.ygbd198.hellodoctor.ui.AskForLeaveActivity;
import com.ygbd198.hellodoctor.ui.ChangeVisitorTimeMainActivity;
import com.ygbd198.hellodoctor.ui.FunctionActivity;
import com.ygbd198.hellodoctor.ui.GetVisitorTimeActivity;
import com.ygbd198.hellodoctor.ui.MyIntegralActivity;
import com.ygbd198.hellodoctor.ui.MySettingActivity;
import com.ygbd198.hellodoctor.ui.PersonalInfActivity;
import com.ygbd198.hellodoctor.ui.PrescribeActivity;
import com.ygbd198.hellodoctor.ui.controller.MyCenterController;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by guangjiqin on 2017/8/7.
 */

public class MyCenterFragment extends Fragment implements View.OnClickListener {

    //暂时用不到
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private DoctorInfBean bean;
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    Unbinder unbinder;

    @BindView(R.id.img_user_photo)
    ImageView imgUserPhoto;
    @BindView(R.id.tv_user_name_cn)
    TextView tvNameCn;
    @BindView(R.id.tv_user_name_en)
    TextView tvNameEn;
    @BindView(R.id.rel_name_cn_en)
    RelativeLayout relName;

    @BindView(R.id.lin_order_inquiry)
    LinearLayout linOrderInquiry;
    @BindView(R.id.lin_cur_visitor_time)
    LinearLayout lincCurVisitorTime;
    @BindView(R.id.lin_change_visitor_time)
    LinearLayout linChangeTime;
    @BindView(R.id.lin_ask_for_leave)
    LinearLayout linAskLeave;
    @BindView(R.id.lin_more_scheme)
    LinearLayout linMoreScheme;
    @BindView(R.id.lin_new_scheme)
    LinearLayout linNewScheme;
    @BindView(R.id.lin_prescribe)
    LinearLayout linPrescribe;
    @BindView(R.id.lin_function)
    LinearLayout linFunction;
    @BindView(R.id.lin_contact_customer_service)
    LinearLayout linContact;


    private MyCenterController controller;

    public static MyCenterFragment newInstance(String param1, String param2) {
        MyCenterFragment fragment = new MyCenterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        EventBusUtil.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_center, container, false);
        unbinder = ButterKnife.bind(this, view);
        this.titleBar.setBackBtnVis(false);
        this.titleBar.showCenterText(R.string.personal_cen_title, R.drawable.personal_ata, 0);
        this.titleBar.setRightIcon(R.drawable.nbar_set_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MySettingActivity.class));
            }
        });

        initController();

        initOnClick();

        getData();

        return view;
    }

    private void initOnClick() {
        relName.setOnClickListener(this);
        linOrderInquiry.setOnClickListener(this);
        lincCurVisitorTime.setOnClickListener(this);
        linChangeTime.setOnClickListener(this);
        linAskLeave.setOnClickListener(this);
        linMoreScheme.setOnClickListener(this);
        linNewScheme.setOnClickListener(this);
        linPrescribe.setOnClickListener(this);
        linFunction.setOnClickListener(this);
        linContact.setOnClickListener(this);
    }

    private void getData() {
        controller.getUserInf();
    }

    private void initController() {
        controller = new MyCenterController() {
            @Override
            public void getInfSuccess(DoctorInfBean doctorInfBean) {
                Log.e("gg", "获取数据成功");
                bean = doctorInfBean;
                GlideImageHelper.showImage(getActivity(), doctorInfBean.photo, R.drawable.logo, R.drawable.logo, imgUserPhoto);
                tvNameCn.setText(doctorInfBean.doctorAttach.doctorRole.role);
                tvNameEn.setText(doctorInfBean.doctorAttach.name);

            }

            @Override
            public void getInfFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onUserPhotoEvent(UserInfoUptateEvent event){
        controller.getUserInf();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_name_cn_en://个人资料
                Intent intent = new Intent(getActivity(), PersonalInfActivity.class);
                intent.putExtra("bean", bean);
                startActivity(intent);
                break;
            case R.id.lin_order_inquiry://我的账户
                startActivity(new Intent(getActivity(), MyIntegralActivity.class));
                break;
            case R.id.lin_cur_visitor_time://当前坐诊时间
                startActivity(new Intent(getActivity(), GetVisitorTimeActivity.class));
                break;
            case R.id.lin_change_visitor_time://变更坐诊时间
                startActivity(new Intent(getActivity(), ChangeVisitorTimeMainActivity.class));
                break;
            case R.id.lin_ask_for_leave://请假
                startActivity(new Intent(getActivity(), AskForLeaveActivity.class));
                break;
            case R.id.lin_more_scheme://更多治疗方案
                startActivity(new Intent(getActivity(), ApplyMoreTypeActivity.class));
                break;
            case R.id.lin_new_scheme://添加更多治疗方案
                startActivity(new Intent(getActivity(), ApplyNewSchemeActivity.class));
                break;
            case R.id.lin_prescribe://平台药方
                startActivity(new Intent(getActivity(), PrescribeActivity.class));
                break;
            case R.id.lin_function://功能指南
                startActivity(new Intent(getActivity(), FunctionActivity.class));
                break;
            case R.id.lin_contact_customer_service:
                //联系客服
                Intent imIntent = new Intent(getContext(), ContactServiceActivity.class);
                imIntent.putExtra(ContactServiceActivity.KEY_TITLE_RESID, R.string.smart_service);
                startActivity(imIntent);
                break;
        }

    }
}
