package com.ygbd198.hellodoctor.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.event.UserInfoUptateEvent;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.controller.PersonalInfController;
import com.ygbd198.hellodoctor.util.FileUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/7.
 */

public class PersonalInfActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.img_user_photo)
    public ImageView imgUserPhoto;
    @BindView(R.id.tv_name_en)
    public TextView tvNameEn;
    @BindView(R.id.tv_gender)
    public TextView tvGender;
    @BindView(R.id.lin_gender)
    LinearLayout linGender;
    @BindView(R.id.tv_phone_num)
    public TextView tvPhoneNum;
    @BindView(R.id.lin_phone_num)
    LinearLayout linPhoneNum;
    @BindView(R.id.tv_education)
    public TextView tvEducation;
    @BindView(R.id.lin_education)
    LinearLayout linEducatuion;
    @BindView(R.id.tv_hospital)
    public TextView tvHospital;
    @BindView(R.id.lin_hospital)
    LinearLayout linHospital;
    @BindView(R.id.tv_level)
    public TextView tvLevel;
    @BindView(R.id.lin_level)
    LinearLayout linLevel;
    @BindView(R.id.tv_dutiy)
    public TextView tvDutiy;
    @BindView(R.id.lin_dutiy)
    LinearLayout linDutiy;
    @BindView(R.id.tv_specialty)
    public TextView tvSpecialty;
    @BindView(R.id.lin_specialty)
    LinearLayout linSpecialty;
    @BindView(R.id.lin_seniority)
    LinearLayout linSeniority;
    @BindView(R.id.lin_seducation_card)
    LinearLayout linSeducationCard;
    @BindView(R.id.lin_introduce_myself_video)
    LinearLayout linIntroduce;
    @BindView(R.id.lin_Indications)
    LinearLayout linIndications;
    @BindView(R.id.lin_credential)
    LinearLayout linCredential;
    @BindView(R.id.order_inquiry)
    LinearLayout linAccount;
    @BindView(R.id.btn_commit)
    Button btnCommit;


    private DoctorInfBean bean;
    private PersonalInfController controller;

    private String introduction; // 个人介绍

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Log.e("gg", "PersonalInfActivity onActivityResult =====");

        switch (requestCode) {
            case 1:
            case 100:
                File file = null;
                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                if (requestCode == 1) {

                    file = new File(path + "/" + String.valueOf(requestCode) + ".jpg");//拍照文件位置
                } else {
                    if (null == data || null == data.getData()) {
                        return;
                    }
                    if (null != data && null != data.getData()) {

                        file = new File(com.monty.library.FileUtil.getPath(this,data.getData()));//图库文件
                    }
//                    file = new File(data.getData().getLastPathSegment());//图库文件
                }

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;

                Bitmap bitmap = null;
                if (requestCode == 1) {
                    bitmap = BitmapFactory.decodeFile(path + "/" + String.valueOf(requestCode) + ".jpg", options);
                    if (null == bitmap) {
                        return;
                    }
                } else {
                    if (null != data && null != data.getData()) {
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                imgUserPhoto.setImageBitmap(bitmap);

                controller.uploadPhoto(requestCode, file, 1);

                break;
            case 0x168:
                if (resultCode == 0x1011) {
                    introduction = data.getStringExtra("introduction");
                }
                break;
            case 200:
                if (null != data && null != data.getData()) {
                    File vedio = new File(Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache" + "/" + String.valueOf(requestCode) + ".mp4");//录像文件位置
                    showLoadingDialog("视频上传中，请稍候");
                    dialog.setCancelable(false);
                    controller.uploadVideoFile(200, vedio);
                }
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        verifyStoragePermissions(this);

        View contentView = this.getLayoutInflater().inflate(R.layout.activity_person_inf, null);
        rootView.addView(contentView);

        ButterKnife.bind(this);

        getIntentData();

        initOnClick();

        initView();

        initController();

    }

    private void initController() {
        controller = new PersonalInfController(this) {
            @Override
            public void uploadVedioSuccess(int imgId, String path) {
                closeLoadingDialog();
                ToastUtils.showToast("视频上传成功");
            }

            @Override
            public void uploadVedioFail(String msg) {
                closeLoadingDialog();
                ToastUtils.showToast(msg);
            }

            @Override
            public void uploadPhotoSuccess(int imgId, String path) {

                String cachePath = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache/1.jpg";//设置缓存文件夹
                File file = new File(cachePath);
                if (file.exists()) {
                    file.delete();
                }
                ToastUtils.showToast("头像上传成功");
            }

            @Override
            public void uploadPhotoFailure(String msg) {
                ToastUtils.showToast("头像上传失败");
            }

            @Override
            public void getRoleSuccess(List<DoctorRoleBean> doctorRoleBeen) {

            }

            @Override
            public void getRoleFailure(String msg) {

            }
        };

    }

    protected void initView() {
        GlideImageHelper.showImage(this, bean.photo, R.drawable.logo, R.drawable.logo, imgUserPhoto);
        tvNameEn.setText(bean.doctorAttach.name);
        tvGender.setText(bean.gender);
        tvPhoneNum.setText("" + bean.phone);
        tvEducation.setText(bean.education);
        tvHospital.setText(bean.doctorAttach.localHospital);
        tvLevel.setText(bean.doctorAttach.doctorRole.role);
        tvDutiy.setText(bean.doctorAttach.job);
        tvSpecialty.setText(bean.good);
        introduction = bean.doctorAttach.introduction;

        titleBar.showCenterText(R.string.user_inf, R.drawable.personal_infomations, 0);
    }

    private void initOnClick() {
        imgUserPhoto.setOnClickListener(this);
        tvNameEn.setOnClickListener(this);
        linGender.setOnClickListener(this);
        linPhoneNum.setOnClickListener(this);
        linEducatuion.setOnClickListener(this);
        linHospital.setOnClickListener(this);
        linLevel.setOnClickListener(this);
        linDutiy.setOnClickListener(this);
        linSpecialty.setOnClickListener(this);
        linSeniority.setOnClickListener(this);
        linSeducationCard.setOnClickListener(this);
        linIntroduce.setOnClickListener(this);
        linIndications.setOnClickListener(this);
        linCredential.setOnClickListener(this);
        linAccount.setOnClickListener(this);
    }

    private void getIntentData() {
        bean = getIntent().getParcelableExtra("bean");
        if (bean != null) {
            Log.e("gg", "bean=" + bean.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_user_photo:
                controller.showGetPhotoPop(1);
                break;
            case R.id.tv_name_en:
                controller.showUserNameInppop();
                break;
            case R.id.lin_gender:
                controller.showGenderpPop();
                break;
            case R.id.lin_phone_num:
                //跳转
                break;
            case R.id.lin_education:
//                controller.showEducationPop();
                break;
            case R.id.lin_hospital:
                controller.showHospitalNameInpop();
                break;
            case R.id.lin_level:
//                controller.showSelectLevelPop();
                break;
            case R.id.lin_dutiy:
                controller.showFunctionInppop();
                break;
            case R.id.lin_specialty:
                controller.showSpecialtynppop();
                break;

            case R.id.lin_seducation_card:
                Intent intentSeniorityCard = new Intent(PersonalInfActivity.this, DocumentListActivity.class);
                intentSeniorityCard.putExtra("workCertificate", bean.workCertificate);
                intentSeniorityCard.putExtra("activityCode", 1);
                startActivity(intentSeniorityCard);
                break;
            case R.id.lin_seniority:
                Intent intentSeniority = new Intent(PersonalInfActivity.this, DocumentListActivity.class);
                intentSeniority.putExtra("physicianCertificate", bean.physicianCertificate);
                intentSeniority.putExtra("activityCode", 2);
                startActivity(intentSeniority);
                break;
            case R.id.lin_credential:
                Intent intentCredential = new Intent(PersonalInfActivity.this, CredentialActivity.class);
                intentCredential.putExtra("introduction", introduction);
                startActivityForResult(intentCredential, 0x168);
                break;

            case R.id.lin_Indications:
                Intent intentIndications = new Intent(PersonalInfActivity.this, AdeptActivity.class);
                if (null != bean.doctorAttach) {
                    intentIndications.putExtra("type", bean.doctorAttach.treatmentNames);
                } else {
                    intentIndications.putExtra("type", "");
                }
                startActivity(intentIndications);
                break;

            case R.id.lin_introduce_myself_video://自我介绍视频录制
                /*Intent intentRecordVideo = new Intent(PersonalInfActivity.this, RecordVideoActivity.class);
                startActivityForResult(intentRecordVideo, 2);*/
                if (bean != null && bean.doctorAttach != null) {
                    RecordVideoActivity.goToRecordVideoActivity(this, bean.doctorAttach.introduceUrl, 2, true);
                }
//                controller.toMakeVideo(200);
                break;
//            case R.id.order_inquiry:
//                startActivity(new Intent(PersonalInfActivity.this,MyIntegralActivity.class));
//                break;

//            Intent intentToVideo = new Intent();
//            intentToVideo.setAction(MediaStore.ACTION_VIDEO_CAPTURE);
//
//            String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
//            File path1 = new File(path);
//            if (!path1.exists()) {
//                path1.mkdirs();
//            }
//            File file = new File(path1, String.valueOf(viewId) + ".mp4");//添加缓存文件
//            Uri mOutPutFileUri = Uri.fromFile(file);
//            intentToVideo.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
//            ((ApplyBeDocActivity) context).startActivityForResult(intentToVideo, 11);
            default:
                break;
        }

    }


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.CAMERA"
    };


    private void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_commit)
    public void onCommitClicked() {
        String name = tvNameEn.getText().toString();
        String localHospital = tvHospital.getText().toString();
        String good = tvSpecialty.getText().toString();
        String introduction = this.introduction;
        String gender = tvGender.getText().toString();
        String job = tvDutiy.getText().toString();

        if (TextUtils.isEmpty(name)) {
            ToastUtils.showToast("名称不允许为空");
            return;
        } else if (TextUtils.isEmpty(localHospital)) {
            ToastUtils.showToast("所在医院不允许为空");
            return;
        } else if (TextUtils.isEmpty(good)) {
            ToastUtils.showToast("专长不允许为空");
            return;
        } else if (TextUtils.isEmpty(introduction)) {
            ToastUtils.showToast("个人介绍不允许为空");
            return;
        } else if (TextUtils.isEmpty(job)) {
            ToastUtils.showToast("学术职务不允许为空");
            return;
        }
        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("localHospital", localHospital);
        jsonObject.addProperty("good", good);
        jsonObject.addProperty("introduction", introduction);
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("gender", gender);
        jsonObject.addProperty("job", job);

        RequestBody body = RequestBody.create(MediaType.parse("Content-Type, application/json"), jsonObject.toString());

        RetrofitHelper.getInstance().createService(MyCenterService.class).finishInfo(MySelfInfo.getInstance().getToken(), body).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                ToastUtils.showToast("资料更新成功");
                EventBusUtil.post(new UserInfoUptateEvent());
                loadingDialog.close();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                loadingDialog.close();
            }
        });
    }

}
