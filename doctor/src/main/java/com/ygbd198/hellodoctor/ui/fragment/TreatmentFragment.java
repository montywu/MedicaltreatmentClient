package com.ygbd198.hellodoctor.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;
import com.yanzhenjie.permission.Rationale;
import com.yanzhenjie.permission.RationaleListener;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.AgreementActivity;
import com.ygbd198.hellodoctor.ui.ApplyNewSchemeActivity;
import com.ygbd198.hellodoctor.ui.MainActivity;
import com.ygbd198.hellodoctor.ui.PatientRecordActivity;
import com.ygbd198.hellodoctor.ui.ReviewActivity;
import com.ygbd198.hellodoctor.ui.adapter.AdAdapter;
import com.ygbd198.hellodoctor.ui.live.LiveActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by monty on 2017/7/7.
 */
public class TreatmentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.roll_view_pager)
    RollPagerView rollViewPager;
    Unbinder unbinder;
    @BindView(R.id.btn_startTreatment)
    LinearLayout btnStartTreatment;
    @BindView(R.id.btn_patientRecord)
    LinearLayout btnPatientRecord;
    @BindView(R.id.btn_review)
    LinearLayout btnReview;
    @BindView(R.id.btn_programAdd)
    LinearLayout btnProgramAdd;

    private int status;

    private MainActivity mainActivity;

    public TreatmentFragment() {
        // Required empty public constructor
    }

    public static TreatmentFragment newInstance() {
        TreatmentFragment fragment = new TreatmentFragment();
        return fragment;
    }

    public static TreatmentFragment newInstance(int status) {
        TreatmentFragment fragment = new TreatmentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        if (getArguments() != null) {
            status = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the base_dilog_layout for this fragment
        View view = inflater.inflate(R.layout.fragment_treatment, container, false);
        unbinder = ButterKnife.bind(this, view);
        this.titleBar.setBackBtnVis(false);
        this.titleBar.showCenterText(R.string.treatment_title, R.drawable.tr, 0);
        initADPager();
        return view;
    }

    private void initADPager() {
        //设置播放时间间隔
        rollViewPager.setPlayDelay(3000);
        //设置透明度
        rollViewPager.setAnimationDurtion(500);
        //设置适配器
        rollViewPager.setAdapter(new AdAdapter(rollViewPager));

        //mRollViewPager.setHintView(new IconHintView(this, R.drawable.point_focus, R.drawable.point_normal));
        rollViewPager.setHintView(new ColorPointHintView(this.getContext(), Color.parseColor("#f8b9cd"), R.color.black_cc000000));

        //mRollViewPager.setHintView(new TextHintView(this));
        //mRollViewPager.setHintView(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private LoadingDialog dialog;

    @OnClick(R.id.btn_startTreatment)
    public void onBtnStartTreatmentClicked() {
        if (dialog == null) {
            dialog = new LoadingDialog(getContext());
        }
        dialog.show("");
        RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                dialog.close();
                DoctorInfBean data = response.body().data;
                if (data.statu == 107) {
                    ToastUtils.showToast("资料正在审核中");
                } else if (data.statu == 108) {
                    ToastUtils.showToast("资料审核失败,请重新完善资料");
                    mainActivity.showUserInfProgressFragment();
                } else if (data.statu == 109) {
                    Intent intent = new Intent(getContext(), AgreementActivity.class);
                    intent.putExtra("token", MySelfInfo.getInstance().getToken());
                    getContext().startActivity(intent);
                } else {
                    AndPermission.with(getActivity()).requestCode(0x110).permission(new String[]{Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO}).callback(new PermissionListener() {
                        @Override
                        public void onSucceed(int requestCode, @NonNull List<String> grantPermissions) {
                            LiveActivity.startLiveActivity(getContext());
                        }

                        @Override
                        public void onFailed(int requestCode, @NonNull List<String> deniedPermissions) {

                        }
                    }).rationale(new RationaleListener() {
                        @Override
                        public void showRequestPermissionRationale(int requestCode, final Rationale rationale) {
                            AndPermission.rationaleDialog(getActivity(), rationale).show();
                        }
                    }).start();

                }
            }

            @Override
            public void onFailure(String message) {
                dialog.close();
                ToastUtils.showToast(message);
            }
        });


    }

    @OnClick(R.id.btn_patientRecord)
    public void onBtnPatientRecordClicked() {
        startActivity(new Intent(getActivity(), PatientRecordActivity.class));
    }

    @OnClick(R.id.btn_review)
    public void onBtnReviewClicked() {
        startActivity(new Intent(getActivity(), ReviewActivity.class));
    }

    @OnClick(R.id.btn_programAdd)
    public void onBtnProgramAddClicked() {
        startActivity(new Intent(getActivity(), ApplyNewSchemeActivity.class));
    }
}
