package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.SerializableMap;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class ChangeVisitorTimeMainActivity extends BaseActivity {

    public Map<String, String> workTime = new HashMap<>();//时间集合
    public String monday;//每天工作时间
    public String tuesday;
    public String wednesday;
    public String thursday;
    public String friday;
    public String saturday;
    public String sunday;
    public String startTime = "";

    @BindView(R.id.et_reason)
    EditText etReason;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {//拿回设置的时间
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 1) {
            workTime.clear();
            SerializableMap tempData = (SerializableMap) data.getExtras().get("workTime");
            workTime.putAll(tempData.getMap());
            monday = workTime.get("1");                               //工作时间
            tuesday = workTime.get("2");
            wednesday = workTime.get("3");
            thursday = workTime.get("4");
            friday = workTime.get("5");
            saturday = workTime.get("6");
            sunday = workTime.get("7");

            startTime = data.getStringExtra("startData");

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_main_change_visitor_time, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

    }

    @OnClick(R.id.rel_change_time)
    public void setTime() {
        Intent intent = new Intent(ChangeVisitorTimeMainActivity.this, ChangeVisitorTimeActivity.class);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.bt_commit)
    public void commit() {

        if (startTime.isEmpty()) {
            ToastUtils.showToast("请填写坐诊变更时间");
            return;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(startTime);
            Calendar now = Calendar.getInstance();
            if ((now.getTimeInMillis() + (7 * 24 * 60 * 60 * 1000) >= date.getTime())) {
                ToastUtils.showToast("时间填写至少一周后");
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String reason = etReason.getText().toString().trim();
        if (reason.isEmpty()) {
            ToastUtils.showToast("请填写变更原因");
            return;
        }


        RetrofitHelper.getInstance().createService(MyCenterService.class).changeVisitorTime(MySelfInfo.getInstance().getToken(),
                startTime, reason,
                monday, tuesday, wednesday, thursday, friday, saturday, sunday
        ).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                ToastUtils.showToast("申请提交成功");
                ChangeVisitorTimeMainActivity.this.finish();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });


    }

    private void initTitle() {
        titleBar.showCenterText(R.string.changetime_cen_title,R.drawable.change_time,0);
    }
}
