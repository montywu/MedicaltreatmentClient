package com.ygbd198.hellodoctor.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.http.UploadRetrofitHelper;
import com.monty.library.okhttp.RequestManger;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.io.File;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/7.
 * 医生注册、录制视频页面
 */

public class RecordVideoActivity extends BaseActivity {
    private boolean isPersonalInf;

    public static void goToRecordVideoActivity(Activity context, String videoUrl, int requestCode, boolean isPersonalInf) {
        Intent intent = new Intent(context, RecordVideoActivity.class);
        intent.putExtra("isPersonalInf", isPersonalInf);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("gg", "resultCode = " + resultCode + ",data = " + data);
        if (11 == requestCode && null != data && null != data.getData()) {

            String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
            File file = new File(path + "/" + String.valueOf(requestCode) + ".mp4");//录像文件位置


            MultipartBody.Builder builder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)//表单类型
                    .addFormDataPart("token", MySelfInfo.getInstance().getToken())
                    .addFormDataPart("type", "6");

            RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

            List<MultipartBody.Part> parts = builder.build().parts();
            Log.e("gg", "开始上传请求");
            showLoadingDialog("视频上传中，请稍候");
            dialog.setCancelable(false);
            UploadRetrofitHelper.changeApiBaseUrl(UploadRetrofitHelper.VIDEO_UPLOAD_HOST);//视频上传接口使用8081
            UploadRetrofitHelper.getInstance().uploadImage(ApplyBeDocService.class).doctorVideoUpload(parts).enqueue(new Callback<BaseCallModel<List<String>>>() {
                @Override
                public void onResponse(Call<BaseCallModel<List<String>>> call, Response<BaseCallModel<List<String>>> response) {


                    if (null == response || null == response.body() || null == response.body().data) {
                        ToastUtils.showToast("上传失败，请重新拍摄上传");
                        return;
                    }
                    saveURL(response.body().data);

//
//
//                    Log.e("gg", "上传成功 = " + response.body().data);
//                    closeLoadingDialog();
//                    NiceDialogFactory.createTipDialog("提示", "上传成功，审核结果会以短信的\n" +
//                            "形式通知您，请留意！", new NiceDialogFactory.OnPositiveClickListener() {
//                        @Override
//                        public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
//                            finish();
//                        }
//                    }).setOutCancel(false).show(getSupportFragmentManager());
                }

                @Override
                public void onFailure(Call<BaseCallModel<List<String>>> call, Throwable t) {
                    t.printStackTrace();
                    closeLoadingDialog();
                    ToastUtils.showToast("上传失败，请重新拍摄上传");
                    Log.e("gg", "上传失败" + call.request().body());
                }
            });
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_record_video, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        isPersonalInf = getIntent().getBooleanExtra("isPersonalInf", false);
        final String videoUrl = MySelfInfo.getInstance().getIntroduceUrl();
        if (!TextUtils.isEmpty(videoUrl)) { // 医生个人资料中有地址，说明已经录制过，那么跳转到播放页面
            rootView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    VideoActivity.GotoVideoActivity(RecordVideoActivity.this, videoUrl);
                }
            }, 200);
        }

        titleBar.showCenterText("自我介绍视频录制");

    }

    @OnClick(R.id.bt_take_vedio)
    public void takeVedio() {
        Intent intentToVideo = new Intent();
        intentToVideo.setAction(MediaStore.ACTION_VIDEO_CAPTURE);

        String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
        File path1 = new File(path);
        if (!path1.exists()) {
            path1.mkdirs();
        }
        File file = new File(path1, String.valueOf(11) + ".mp4");//添加缓存文件
        Uri mOutPutFileUri = null;
        if (android.os.Build.VERSION.SDK_INT < 23) {
            mOutPutFileUri = Uri.fromFile(file);
        } else {
            mOutPutFileUri = FileProvider.getUriForFile(this, "gg.hello.dc", file);

        }
        intentToVideo.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
        startActivityForResult(intentToVideo, 11);

    }


    //保存文件上传url
    private void saveURL(final List<String> data) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).saveIntroduceUrl(MySelfInfo.getInstance().getToken(), data.get(0)).enqueue(new BaseCallBack<BaseCallModel<Object>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                Log.e("gg", "上传成功 = " + response.body().data);
                closeLoadingDialog();
                if (isPersonalInf) {
                    Intent intent = new Intent();
                    intent.putExtra("videoUrl", data.get(0));
                    Log.d("monty", "monty_video 回传上传的视频地址->" + data.get(0));
                    MySelfInfo.getInstance().setIntroduceUrl(data.get(0));
                    setResult(RESULT_OK);
                    NiceDialogFactory.createTipDialog("提示", "视频上传成功！", new NiceDialogFactory.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                            baseNiceDialog.dismissAllowingStateLoss();
                        }
                    }).show(getSupportFragmentManager());
                    return;
                } else {
                    NiceDialogFactory.createTipDialog("提示", "上传成功，审核结果会以短信的\n" +
                            "形式通知您，请留意！", new NiceDialogFactory.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                            baseNiceDialog.dismissAllowingStateLoss();
                            RecordVideoActivity.this.finish();
                            MainActivity.GotoActivity(RecordVideoActivity.this, true);
                        }
                    }).setOutCancel(false).show(getSupportFragmentManager());
                }
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }

    @OnClick(R.id.bt_example)
    public void takeExample() {
        Intent intent = new Intent();
        intent.setClass(RecordVideoActivity.this, VideoPlayerActivity.class);
        intent.putExtra("id", "1");
        intent.putExtra("name", "自我介绍视频录制示范");
        intent.putExtra("introduction", "11");
        intent.putExtra("uri", RequestManger.baseUrl+"/introduceUrl/39732b106e364d50b7f28d23adf1b8ab.mp4");
        startActivity(intent);
    }
}
