package com.ygbd198.hellodoctor.ui.live;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.bigkoo.pickerview.TimePickerView;
import com.ygbd198.hellodoctor.R;

import java.io.File;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DemoActivity extends AppCompatActivity {

    @BindView(R.id.btn_startDialog)
    Button btnStartDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        ButterKnife.bind(this);
    }

    private TimePickerView datePicker;
    private TimePickerView startTimePicker;
    private TimePickerView endTimePicker;

    private Date date;
    private Date startDate;
    private Date endDate;

    /*private void createStartTimePicker() {
        startTimePicker = createTimePicker(this, "复诊 - 开始时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                startDate = date;
                String startTime = DatePickerFactory.dateFormat.format(date);
                ToastUtils.showToast("开始时间-》" + startTime);
                startTimePicker.dismissDialog();
                createEndTimePicker();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimePicker.returnData();
            }
        });
        startTimePicker.show();
    }

    private void createEndTimePicker() {
        endTimePicker = DatePickerFactory.createTimePicker(DemoActivity.this, "复诊 - 结束时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                if (date.getTime() > startDate.getTime()) {
                    endDate = date;
                    String s = formatDate(DemoActivity.this.date, startDate, endDate);
                    ToastUtils.showToast("提醒时间-》" + s);
                    endTimePicker.dismissDialog();
                } else {
                    ToastUtils.showToast("结束时间必须大于开始时间");
                }
            }
        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                endTimePicker.returnData();
            }
        });
        endTimePicker.show();
    }

    private String formatDate(Date date, Date startDate, Date endDate) {
        StringBuffer dateStr = new StringBuffer();
        dateStr.append(DatePickerFactory.dateFormat2.format(date));
        dateStr.append(" ");
        dateStr.append(DatePickerFactory.dateFormat3.format(startDate));
        dateStr.append(" ~ ");
        dateStr.append(DatePickerFactory.dateFormat3.format(endDate));
        return dateStr.toString();
    }*/

    @OnClick(R.id.btn_startDialog)
    public void onViewClicked() {
        String screenShortPath = Environment.getExternalStorageDirectory() + File.separator + "HelloDoctor";
        UploadActivity.GotoActivity(this,screenShortPath+File.separator+"a01591139321my");



        /*datePicker = DatePickerFactory.createDatePicker(this, "复诊时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                DemoActivity.this.date = date;
                createStartTimePicker();
                datePicker.dismissDialog();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.returnData();
            }
        });
        datePicker.show();*/
        /*PrescriptionDialog dialog = new PrescriptionDialog(new PrescriptionDialog.OnPrescriptionCheckChangeListener() {
            @Override
            public void onCheckedComplete(List<Prescription> checkedList) {

            }
        });
        dialog.show(getSupportFragmentManager());*/

    }
}
