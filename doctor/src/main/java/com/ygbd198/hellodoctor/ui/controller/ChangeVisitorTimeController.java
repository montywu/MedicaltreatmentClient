package com.ygbd198.hellodoctor.ui.controller;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.CurVisitorTimeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public abstract class ChangeVisitorTimeController {

    public abstract void getTimeSuccess(CurVisitorTimeBean curVisitorTimeBean);
    public abstract void getTimeFailrue(String msg);

    public abstract void setTimeSuccess();
    public abstract void setTimeFailrue(String msg);

    private String token;

    public ChangeVisitorTimeController(){
        token = MySelfInfo.getInstance().getToken();

    }

    public void getTime(){
        RetrofitHelper.getInstance().createService(MyCenterService.class).getCurVisitorTime(token).enqueue(new BaseCallBack<BaseCallModel<CurVisitorTimeBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<CurVisitorTimeBean>> response) {
                getTimeSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getTimeFailrue(msg);
            }
        });
    }

    public void setTime(){

    }

    //string时间字段转化为List集合方法
    public List<String> timeChange(String timeStr) {
        List<String> stringList = new ArrayList<>();
        if (null != timeStr && !timeStr.isEmpty()) {
            String[] strTemp = timeStr.split(",");
            for (String str : strTemp) {
                if (!str.isEmpty()) {
                    stringList.add(str);
                }
            }
        }
        return stringList;
    }

}
