package com.ygbd198.hellodoctor.ui.adapter;


import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.LoopPagerAdapter;
import com.ygbd198.hellodoctor.R;


/**
 * Created by monty on 2017/7/31.
 */

public class AdAdapter extends LoopPagerAdapter {
    private int[] imgs = {
            R.drawable.banner1,
            R.drawable.banner2,
            R.drawable.banner3
    };

    public AdAdapter(RollPagerView viewPager) {
        super(viewPager);
    }


    @Override
    public View getView(ViewGroup container, int position) {
        ImageView view = new ImageView(container.getContext());
        view.setImageResource(imgs[position]);
//        Glide.with(container.getContext()).load(imgs[position]).into(view);
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
    }

    @Override
    public int getRealCount() {
        return imgs.length;
    }
}
