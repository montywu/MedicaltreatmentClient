package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.SelectPopBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.ApplyNewSchemeActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.ApplyMoreTypePop;
import com.ygbd198.hellodoctor.widget.PopupWindon.ApplyOneTypePop;
import com.ygbd198.hellodoctor.widget.PopupWindon.CommonSelectPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.InputPop;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public abstract class ApplyNewSchemeController {

    public abstract void applySuccess();

    public abstract void applyFailure(String msg);

    public abstract void changeLayout(String type);

    private List<DiseaseTypeBean> diseaseTypeBeen;
    private ApplyOneTypePop diseaseTypePop;
    private CommonSelectPop schemeTypePop;
    private CommonSelectPop sideEffectPop;
    private InputPop inputPop;

    private String token;
    private Context context;

    private TextView textview;

    public void setTextview(TextView textview) {
        this.textview = textview;
    }

    public ApplyNewSchemeController(Context context) {
        token = MySelfInfo.getInstance().getToken();
        this.context = context;
    }


    //加药方
    public void applyNewScheme(String treatmentName, String drugType, String drugName, String producers, String effect, String useInfo, String reason, boolean sideEffect) {
        RetrofitHelper.getInstance().createService(MyCenterService.class).applyNewScheme(token, treatmentName, drugType, drugName, producers, effect, useInfo, reason, sideEffect).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                applySuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                applyFailure(msg);
            }
        });
    }


    //加禁忌
    public void applyNewTabooScheme(String treatmentName, String programName, String describe, boolean sideEffect) {
        RetrofitHelper.getInstance().createService(MyCenterService.class).applyNewTabooScheme(token, treatmentName, programName, describe, sideEffect).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                applySuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                applyFailure(msg);
            }
        });
    }

    //加饮食
    public void applyNewFoodScheme(String treatmentName, String name, String effect, String useInfo, boolean sideEffect) {
        Log.e("gg", "treatmentName = " + treatmentName + ",name = " + name + ",effect = " + effect + ",useInfo=" + useInfo + ",sideEffect = " + sideEffect);
        RetrofitHelper.getInstance().createService(MyCenterService.class).applyNewFoodScheme(token, treatmentName, name, effect, useInfo, sideEffect).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                applySuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                applyFailure(msg);
            }
        });
    }


    //加休息
    public void applyRestScheme(String treatmentName, String programName, String effect, String describe, boolean sideEffect) {
        RetrofitHelper.getInstance().createService(MyCenterService.class).applyRestScheme(token, treatmentName, programName, effect, describe, sideEffect).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                applySuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                applyFailure(msg);
            }
        });
    }

    public void getDiseaseType() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDiseaseType(token).enqueue(new BaseCallBack<BaseCallModel<List<DiseaseTypeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DiseaseTypeBean>>> response) {
                diseaseTypeBeen = response.body().data;
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }

    public void showDiseaseType() {
        if (null == diseaseTypePop) {
            diseaseTypePop = new ApplyOneTypePop(context) {

                @Override
                public void onClickCommit(DiseaseTypeBean diseaseTypeBean) {
                    ((ApplyNewSchemeActivity) context).tvPatientType.setText(diseaseTypeBean.typeName);
                }
            };
        }
        diseaseTypePop.setDiseaseTypeBeans(diseaseTypeBeen);
        diseaseTypePop.showPop();
    }

    public void showSchemeTypePop() {
        if (null == schemeTypePop) {
            schemeTypePop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((ApplyNewSchemeActivity) context).tvSchemeType.setText(bean.string);
                    changeLayout(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("生活习惯与作息"));
        been.add(new SelectPopBean("药品"));
        been.add(new SelectPopBean("饮食"));
        been.add(new SelectPopBean("禁忌"));
        schemeTypePop.setBeans(been);
        schemeTypePop.setTvType("请选择治疗方案类别：");
        schemeTypePop.showPop();
    }

    public void showSideEffectPop() {
        if (null == sideEffectPop) {
            sideEffectPop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((ApplyNewSchemeActivity) context).tvSideEffect.setText(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("是"));
        been.add(new SelectPopBean("否"));
        sideEffectPop.setBeans(been);
        sideEffectPop.setTvType("请选择是否有副作用：");
        sideEffectPop.showPop();
    }

    public void showInputPop(String title) {
        if (null == inputPop) {
            inputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    textview.setText(content);
                }
            };

        } else {
            inputPop.clreanEdieText();
        }
        inputPop.setTitle(title);
        inputPop.showPop();
    }

}
