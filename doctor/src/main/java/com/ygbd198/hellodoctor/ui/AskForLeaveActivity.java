package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.AskForLeaveFragmentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class AskForLeaveActivity extends BaseActivity {

    @BindView(R.id.viewPage)
    ViewPager viewPager;
    @BindView(R.id.tab1)
    RelativeLayout tab1;
    @BindView(R.id.tab2)
    RelativeLayout tab2;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;

    private AskForLeaveFragmentAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_ask_for_leave, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        initViewPage();

    }

    private void initTitle() {
        titleBar.showCenterText(R.string.ask_for_leave,R.drawable.leave_apply,0);
    }

    private void initViewPage() {
        adapter = new AskForLeaveFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        setSelect(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(0);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(1);
            }
        });
    }

    private void setSelect(int position) {
        reSetAll();
        switch (position) {
            case 0:
                tv1.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                break;
            case 1:
                tv2.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                break;
        }
        viewPager.setCurrentItem(position);
    }

    private void reSetAll() {
        tv1.setTextColor(getResources().getColor(R.color.blue_0b325a));
        tv2.setTextColor(getResources().getColor(R.color.blue_0b325a));
    }
}