package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.ExchangeRecordBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/28.
 */

public class ExchangeRecordAdapter extends MyBaseAdapter {


    private long curMonthFirstDay;
    private long curMOnthLastDay;

    public ExchangeRecordAdapter(Context context, List data) {
        super(context, data);
        getCurMonth();
    }

    private void getCurMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        curMonthFirstDay = c.getTimeInMillis();


        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        curMOnthLastDay = ca.getTimeInMillis();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_exchang_integral_record, null);
            holder.tvMonth = (TextView) convertView.findViewById(R.id.tv_month);
            holder.tvData = (TextView) convertView.findViewById(R.id.tv_data);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            holder.tvNum = (TextView) convertView.findViewById(R.id.tv_num);
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();

        }
        ExchangeRecordBean exchangeRecordBean = (ExchangeRecordBean) mData.get(position);
        String[] data = exchangeRecordBean.createTime.split(" ");
        holder.tvData.setText(data[0]);
        holder.tvTime.setText(data[1]);

        holder.tvNum.setText(String.valueOf(exchangeRecordBean.money));
        holder.tvType.setText(exchangeRecordBean.transactionName);

        String[] dataYearMonth = data[0].split("-");
        if (position == 0) {
            holder.tvMonth.setVisibility(View.VISIBLE);
            long curData = dataToSecond(exchangeRecordBean.createTime);
            if (curData > curMonthFirstDay && curData < curMOnthLastDay) {
                holder.tvMonth.setText("本月");
            } else {
                holder.tvMonth.setText(dataYearMonth[0] + "年" + dataYearMonth[1] + "月");
            }
        }else{
            ExchangeRecordBean exchangeRecordHeadBean = (ExchangeRecordBean) mData.get(position - 1);
            String[] dataHead = exchangeRecordHeadBean.createTime.split(" ");
            String[] dataYearMonthHead = dataHead[0].split("-");
            if (dataYearMonthHead[0].equals(dataYearMonth[0]) && dataYearMonthHead[1].equals(dataYearMonth[1])) {
                holder.tvMonth.setVisibility(View.GONE);
            }else{
                holder.tvMonth.setVisibility(View.VISIBLE);
                holder.tvMonth.setText(dataYearMonth[0] + "年" + dataYearMonth[1] + "月");
            }
        }
        return convertView;
    }

    class Holder {
        TextView tvData;
        TextView tvTime;
        TextView tvNum;
        TextView tvType;
        TextView tvMonth;

    }


    private long dataToSecond(String data) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return simpleDateFormat.parse(data).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
