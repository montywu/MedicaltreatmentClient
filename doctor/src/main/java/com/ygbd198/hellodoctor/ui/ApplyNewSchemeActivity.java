package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.ApplyNewSchemeController;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public class ApplyNewSchemeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tv_patient_type)
    public TextView tvPatientType;
    @BindView(R.id.tv_scheme_type)
    public TextView tvSchemeType;
    @BindView(R.id.tv_medicine_name)
    public TextView tvMedicineName;
    @BindView(R.id.tv_medicine_production_name)
    public TextView tvMedicineProductionName;
    @BindView(R.id.tv_effect)
    public TextView tvEffect;
    @BindView(R.id.tv_take_scheme)
    public TextView tvTakeScheme;
    @BindView(R.id.et_reason)
    EditText etReason;
    @BindView(R.id.tv_side_effect)
    public TextView tvSideEffect;
    @BindView(R.id.bt_next)
    Button btCommit;

    @BindView(R.id.lin_1)
    LinearLayout Lin1;
    @BindView(R.id.lin_2)
    LinearLayout Lin2;
    @BindView(R.id.lin_3)
    LinearLayout Lin3;
    @BindView(R.id.lin_4)
    LinearLayout Lin4;
    @BindView(R.id.lin_5)
    LinearLayout Lin5;
    @BindView(R.id.lin_6)
    LinearLayout Lin6;
    @BindView(R.id.lin_7)
    LinearLayout Lin7;

    @BindView(R.id.tv_1)
    TextView tv1;
    @BindView(R.id.tv_2)
    TextView tv2;
    @BindView(R.id.tv_3)
    TextView tv3;
    @BindView(R.id.tv_4)
    TextView tv4;
    @BindView(R.id.tv_5)
    TextView tv5;
    @BindView(R.id.tv_6)
    TextView tv6;
    @BindView(R.id.tv_7)
    TextView tv7;


    private ApplyNewSchemeController controller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_apply_new_scheme, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        initController();

        setOnClick();

        initData();
    }

    private void setOnClick() {
        tvPatientType.setOnClickListener(this);
        tvSchemeType.setOnClickListener(this);
        tvMedicineName.setOnClickListener(this);
        tvMedicineProductionName.setOnClickListener(this);
        tvEffect.setOnClickListener(this);
        tvTakeScheme.setOnClickListener(this);
        tvSideEffect.setOnClickListener(this);
        btCommit.setOnClickListener(this);

    }

    private void initController() {
        controller = new ApplyNewSchemeController(this) {
            @Override
            public void applySuccess() {
                ToastUtils.showToast("你的新治疗方案已提交，平台将进行审核，请留意系统信息");
                finish();
            }

            @Override
            public void applyFailure(String msg) {
                ToastUtils.showToast(msg);
            }

            @Override
            public void changeLayout(String type) {
                switch (type) {
                    case "生活习惯与作息":
                        showAddRestLay();
                        break;
                    case "药品":
                        showAddDurgLay();
                        break;
                    case "饮食":
                        showAddFoodLay();
                        break;
                    case "禁忌":
                        showAddTabooLay();
                        break;
                }
            }
        };
    }

    @Override
    protected void initData() {
        controller.getDiseaseType();

    }

    private void initTitle() {
//        titleBar.showCenterText("新治疗方案添加");
        titleBar.showCenterText(R.string.new_scheme, R.drawable.new_program, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_patient_type:
                controller.showDiseaseType();
                break;
            case R.id.tv_scheme_type:
                controller.showSchemeTypePop();
                break;
            case R.id.tv_medicine_name:
                controller.setTextview(tvMedicineName);
                controller.showInputPop(tv3.getText().toString());
                break;
            case R.id.tv_medicine_production_name:
                controller.setTextview(tvMedicineProductionName);
                controller.showInputPop(tv4.getText().toString());
                break;
            case R.id.tv_effect:
                controller.setTextview(tvEffect);
                controller.showInputPop(tv5.getText().toString());
                break;
            case R.id.tv_take_scheme:
                controller.setTextview(tvTakeScheme);
                controller.showInputPop(tv6.getText().toString());
                break;
            case R.id.tv_side_effect:
                controller.showSideEffectPop();
                break;
            case R.id.bt_next:
                String treatmentName = tvPatientType.getText().toString().trim();
                String drugType = tvSchemeType.getText().toString().trim();
                String drugName = tvMedicineName.getText().toString().trim();
                String producers = tvMedicineProductionName.getText().toString().trim();
                String effect = tvEffect.getText().toString().trim();
                String useInfo = tvTakeScheme.getText().toString().trim();
                String reason = etReason.getText().toString().trim();

                if (treatmentName.isEmpty()) {
                    ToastUtils.showToast("未选择患者类型");
                    return;
                }
                if (drugType.isEmpty()) {
                    ToastUtils.showToast("未选择治疗类别");
                    return;
                }
                if (drugName.isEmpty() && Lin3.isShown()) {
                    ToastUtils.showToast("未填写" + tv3.getText());
                    return;
                }
                if (producers.isEmpty() && Lin4.isShown()) {
                    ToastUtils.showToast("未填写" + tv4.getText());
                    return;
                }
                if (effect.isEmpty() && Lin5.isShown()) {
                    ToastUtils.showToast("未填写" + tv5.getText());
                    return;
                }
                if (useInfo.isEmpty() && Lin6.isShown()) {
                    ToastUtils.showToast("未填写" + tv6.getText());
                    return;
                }
                if (reason.isEmpty() && Lin7.isShown()) {
                    ToastUtils.showToast("未填写" + tv7.getText());
                    return;
                }
                boolean sideEffect = ("否".equals(tvSideEffect.getText().toString()) ? false : true);

                switch (drugType) {
                    case "生活习惯与作息":
                        controller.applyRestScheme(treatmentName, drugName, effect, reason, sideEffect);
                        break;
                    case "药品":
                        controller.applyNewScheme(treatmentName, drugType, drugName, producers, effect, useInfo, reason, sideEffect);
                        break;
                    case "饮食":
                        controller.applyNewFoodScheme(treatmentName, drugName, effect, reason, sideEffect);
                        break;
                    case "禁忌":
                        controller.applyNewTabooScheme(treatmentName, drugName, reason, sideEffect);
                        break;
                }

                break;
        }
    }

    //添加药品
    private void showAddDurgLay() {
        reset();
        Lin1.setVisibility(View.VISIBLE);
        Lin2.setVisibility(View.VISIBLE);
        Lin3.setVisibility(View.VISIBLE);
        Lin4.setVisibility(View.VISIBLE);
        Lin5.setVisibility(View.VISIBLE);
        Lin6.setVisibility(View.VISIBLE);
        Lin7.setVisibility(View.VISIBLE);

        tv1.setText("患者类型");
        tv2.setText("治疗方案类型");
        tv3.setText("药品名称");
        tv4.setText("药品生产商");
        tv5.setText("相应功效");
        tv6.setText("服用方法");
        tv7.setText("增加该药品的理由");

    }

    //添加饮食
    private void showAddFoodLay() {
        reset();
        Lin1.setVisibility(View.VISIBLE);
        Lin2.setVisibility(View.VISIBLE);
        Lin3.setVisibility(View.VISIBLE);
        Lin4.setVisibility(View.GONE);
        Lin5.setVisibility(View.VISIBLE);
        Lin6.setVisibility(View.GONE);
        Lin7.setVisibility(View.VISIBLE);

        tv1.setText("患者类型");
        tv2.setText("治疗方案类型");
        tv3.setText("名称");
        tv5.setText("相应功效");
        tv7.setText("做法");


    }

    //添加禁忌
    private void showAddTabooLay() {
        reset();
        Lin1.setVisibility(View.VISIBLE);
        Lin2.setVisibility(View.VISIBLE);
        Lin3.setVisibility(View.VISIBLE);
        Lin4.setVisibility(View.GONE);
        Lin5.setVisibility(View.GONE);
        Lin6.setVisibility(View.GONE);
        Lin7.setVisibility(View.VISIBLE);

        tv1.setText("患者类型");
        tv2.setText("治疗方案类型");
        tv3.setText("方案名称");
        tv7.setText("方案说明");
    }

    //添加作息
    private void showAddRestLay() {
        reset();
        Lin1.setVisibility(View.VISIBLE);
        Lin2.setVisibility(View.VISIBLE);
        Lin3.setVisibility(View.VISIBLE);
        Lin4.setVisibility(View.GONE);
        Lin5.setVisibility(View.VISIBLE);
        Lin6.setVisibility(View.GONE);
        Lin7.setVisibility(View.VISIBLE);

        tv1.setText("患者类型");
        tv2.setText("治疗方案类型");
        tv3.setText("方案名称");
        tv5.setText("相应功效");
        tv7.setText("方案说明");

    }


    //重置方法
    private void reset() {
        tvMedicineName.setText("");
        tvMedicineProductionName.setText("");
        tvEffect.setText("");
        tvTakeScheme.setText("");
        etReason.setText("");
    }


}
