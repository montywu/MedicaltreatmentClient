package com.ygbd198.hellodoctor.ui.live.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.monty.library.EventBusUtil;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.ui.live.event.PrescriptionEvent;
import com.ygbd198.hellodoctor.util.DisplayUtil;
import com.ygbd198.hellodoctor.widget.DatePickerFactory;
import com.ygbd198.hellodoctor.widget.HorizontalListView;
import com.ygbd198.hellodoctor.widget.NumberControlView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * 药方列表
 * Created by monty on 2017/8/22.
 */

public class PrescriptionAdapter extends BaseAdapter {
    private List<Prescription> prescriptions;
    private LayoutInflater inflater;
    private Context context;
    private TimePickerView timePickerView;

    public static DateFormat dateFormat = new SimpleDateFormat("HH:mm");

    public PrescriptionAdapter(Context context, List<Prescription> prescriptions) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.prescriptions = prescriptions;
    }

    @Override
    public int getCount() {
        return prescriptions.size();
    }

    @Override
    public Object getItem(int position) {
        return prescriptions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.prescription_totaken_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Prescription prescription = prescriptions.get(position);

        if (prescription.getUseType() == 1) {//1-内服，2-外用
            viewHolder.llNeifu.setVisibility(View.VISIBLE);
            viewHolder.llWaiyong.setVisibility(View.GONE);
            viewHolder.fuyongyongliang.setNumer(prescription.getDosage());// 服用用量
            viewHolder.fuyongyongliang.setOnNumberChangeListener(new NumberControlView.OnNumberChangeListener() {
                @Override
                public void onNumberChange(int number) {
                    prescription.setDosage(number);
                }
            });
            viewHolder.fuyongcishu.setNumer(prescription.getEverydayCount()); //服用次数
            viewHolder.tvFycs.setText("每日服用次数(次)");
        } else {
            viewHolder.llNeifu.setVisibility(View.GONE);
            viewHolder.llWaiyong.setVisibility(View.VISIBLE);
            UseTimeAdapter useTimeAdapter = new UseTimeAdapter(context, splitString(prescription.getUseInfos()), prescription.getDefaultUseInfo(), new UseTimeAdapter.OnUseTimeListener() {
                @Override
                public void onUseTime(String useInfo) {
                    prescription.setDefaultUseInfo(useInfo);
                }
            });
            viewHolder.lvUseInfos.setAdapter(useTimeAdapter);
            viewHolder.fuyongcishu.setNumer(prescription.getEverydayCount()); //使用次数
            viewHolder.fuyongcishu.setOnNumberChangeListener(new NumberControlView.OnNumberChangeListener() {
                @Override
                public void onNumberChange(int number) {
                    prescription.setEverydayCount(number);
                }
            });
            viewHolder.tvFycs.setText("每日使用次数(次)");
        }

        viewHolder.checkbox.setImageResource(prescription.isChecked() ? R.drawable.reg_checkbox_sel : R.drawable.reg_checkbox_nor);
        viewHolder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prescription.setChecked(!prescription.isChecked());
                notifyDataSetChanged();
//                EventBusUtil.post(new PrescriptionEvent(getCheckedData()));
            }
        });

        Glide.with(convertView.getContext())
                .load(TextUtils.isEmpty(prescription.getPic()) ? R.drawable.drug_icon : prescription.getPic())
                .bitmapTransform(new RoundedCornersTransformation(convertView.getContext(), 10, 0))
                .into(viewHolder.pic);
        viewHolder.tvDrugName.setText(prescription.getDrugName());
        viewHolder.tvUseRules.setText(prescription.getDrugType());
//        viewHolder.tvRemind.setText(prescription.getUseTimes());
        viewHolder.tvRemind.setText("每日提醒");
        if (prescription.isExpand()) {
            viewHolder.btnEdit.setBackgroundResource(R.drawable.button_blue_bg);
            viewHolder.btnEdit.setText("确定");
            viewHolder.btnEdit.setTextColor(ContextCompat.getColor(context, R.color.pink2));
            viewHolder.llUseRoles.setVisibility(View.VISIBLE);
        } else {
            viewHolder.btnEdit.setBackgroundResource(R.drawable.blue_stroke_bg);
            viewHolder.btnEdit.setText("修改");
            viewHolder.btnEdit.setTextColor(ContextCompat.getColor(context, R.color.blue));
            viewHolder.llUseRoles.setVisibility(View.GONE);
        }
        viewHolder.llUseRoles.setVisibility(prescription.isExpand() ? View.VISIBLE : View.GONE);
        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prescription.setExpand(!prescription.isExpand());
                notifyDataSetChanged();
            }
        });
        UseTimeAdapter useTimeAdapter = new UseTimeAdapter(context, splitString(prescription.getUseTimes()), prescription.getDefaultUseTime(), new UseTimeAdapter.OnUseTimeListener() {
            @Override
            public void onUseTime(String useTime) {
                prescription.setDefaultUseTime(useTime);
            }
        });
        viewHolder.lvUseTime.setAdapter(useTimeAdapter);

        viewHolder.llPromptTimes.removeAllViews();
        if (!TextUtils.isEmpty(prescription.getPromptTime())) {
            List<String> promptTimes = splitString(prescription.getPromptTime());
            for (int i = 0; i < promptTimes.size(); i++) {
                TextView textView = new TextView(context);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, DisplayUtil.dp2px(context, 25));
                layoutParams.leftMargin = DisplayUtil.dp2px(context, 5);
                textView.setLayoutParams(layoutParams);
                textView.setPadding(DisplayUtil.dp2px(context, 10), 0, DisplayUtil.dp2px(context, 10), 0);
                textView.setBackgroundResource(R.drawable.button_pink2_bg);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(ContextCompat.getColor(context, R.color.blue));
                textView.setText(promptTimes.get(i));
                final int finalI = i;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prescription.removePromptTime(finalI);
                        notifyDataSetChanged();
                    }
                });
                viewHolder.llPromptTimes.addView(textView);
                if (viewHolder.llPromptTimes.getChildCount() >= 4) {
                    viewHolder.btnAddRemindTime.setVisibility(View.GONE);
                }else{
                    viewHolder.btnAddRemindTime.setVisibility(View.VISIBLE);
                }
            }
        }

        viewHolder.btnAddRemindTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerView = DatePickerFactory.createTimePicker(context, "提醒时间", new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        String dateStr = dateFormat.format(date);
                        prescriptions.get(position).addPromptTime(dateStr);
                        notifyDataSetChanged();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timePickerView.returnData();
                        timePickerView.dismissDialog();
                    }
                });

                timePickerView.show();
            }
        });


        return convertView;
    }

    private List<String> splitString(String s) {
        String[] split = s.split(",");
        return Arrays.asList(split);
    }

    public List<Prescription> getCheckedData() {
        List<Prescription> checkedData = new ArrayList<>();
        for (Prescription p : prescriptions) {
            if (p.isChecked()) {
                checkedData.add(p);
            }
        }
        return checkedData;
    }

    public void notifyData(List<Prescription> checkedList) {
        this.prescriptions = new ArrayList<>(checkedList);
        notifyDataSetChanged();
    }

    public void addNotifyData(List<Prescription> checkedList){
        this.prescriptions.addAll(checkedList);
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        EventBusUtil.post(new PrescriptionEvent(getCheckedData()));
    }

    public List<Prescription> getData() {
        return this.prescriptions;
    }

    static class ViewHolder {
        @BindView(R.id.checkbox)
        ImageView checkbox;
        @BindView(R.id.pic)
        ImageView pic;
        @BindView(R.id.tv_drugName)
        TextView tvDrugName;
        @BindView(R.id.tv_useRules)
        TextView tvUseRules;
        @BindView(R.id.tv_remind)
        TextView tvRemind;
        @BindView(R.id.tv_fycs)
        TextView tvFycs;
        @BindView(R.id.btn_edit)
        CheckBox btnEdit;
        @BindView(R.id.lv_useTime)
        HorizontalListView lvUseTime;
        @BindView(R.id.btn_addRemindTime)
        Button btnAddRemindTime;
        @BindView(R.id.ll_promptTimes)
        LinearLayout llPromptTimes;
        @BindView(R.id.ll_useRoles)
        LinearLayout llUseRoles;
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        @BindView(R.id.ll_waiyong)
        LinearLayout llWaiyong;
        @BindView(R.id.ll_neifu)
        LinearLayout llNeifu;
        @BindView(R.id.lv_useInfos)
        HorizontalListView lvUseInfos;
        @BindView(R.id.fuyongyongliang)
        NumberControlView fuyongyongliang;
        @BindView(R.id.fuyongcishu)
        NumberControlView fuyongcishu;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    public static class UseTimeAdapter extends BaseAdapter {
        private List<String> useTimes;
        private String useTime;
        private LayoutInflater inflater;
        private OnUseTimeListener listener;

        public UseTimeAdapter(Context context, List<String> useTimes, String useTime, OnUseTimeListener onUseTimeListener) {
            this.inflater = LayoutInflater.from(context);
            this.useTime = useTime;
            this.useTimes = useTimes;
            this.listener = onUseTimeListener;
        }

        @Override
        public int getCount() {
            return useTimes.size();
        }

        @Override
        public Object getItem(int position) {
            return useTimes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.layout_usetime_item, parent, false);
            TextView tvUseTime = (TextView) convertView.findViewById(R.id.tv_useTime);
            tvUseTime.setText(useTimes.get(position));
            if (useTime.equals(useTimes.get(position))) {
                tvUseTime.setSelected(true);
            } else {
                tvUseTime.setSelected(false);
            }

            tvUseTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    useTime = ((TextView) v).getText().toString();
                    listener.onUseTime(useTime);
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }

        public interface OnUseTimeListener {
            void onUseTime(String useTime);
        }
    }

}
