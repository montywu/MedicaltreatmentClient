package com.ygbd198.hellodoctor.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AgreenDataBean;
import com.ygbd198.hellodoctor.bean.AgreenDetailBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.AgreenContentAdapter;
import com.ygbd198.hellodoctor.ui.controller.AgreementController;
import com.ygbd198.hellodoctor.util.AgreeMentContent;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/8/7.
 * 协议内容页面
 */

public class AgreementActivity extends BaseActivity {


    private AgreementController controller;
    private boolean alreadyNext;
    private String token;

    @BindView(R.id.bt_nextpage_uppage)
    Button btNextUpPage;
    @BindView(R.id.bt_agree)
    Button btAgree;
    @BindView(R.id.tv_agree_content)
    TextView tvContent;

    @BindView(R.id.list_content)
    ListView listAgreen;
    @BindView(R.id.imgbt_agree)
    CheckBox cbAgree;
    @BindView(R.id.lin_agree)
    LinearLayout linAgree;

    private AgreenContentAdapter adapter;
    private List<AgreenDetailBean> data;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_agreement, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);
        titleBar.showCenterText("协议",R.drawable.agreement,0);
        getIntentData();

        initController();

        initListView();

        initData();


    }

    private void initListView() {
        data = new ArrayList<>();
        adapter = new AgreenContentAdapter(this, data);
        listAgreen.setAdapter(adapter);
    }


    @Override
    protected void initData() {
//        controller.getTermsContent(1);//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.setText(Html.fromHtml(AgreeMentContent.AgreeMentContentStr,Html.FROM_HTML_MODE_LEGACY));
        }else{
            tvContent.setText(Html.fromHtml(AgreeMentContent.AgreeMentContentStr));
        }
//        tvContent.setText(AgreeMentContent.AgreeMentContentStr);
    }

    private void initController() {
        controller = new AgreementController(this, token) {
            @Override
            public void getTermsContentSuccess(AgreenDataBean bean) {
                data.clear();
                data.addAll(bean.detail);
                adapter.notifyDataSetChanged();

                titleBar.showCenterText(bean.title);
            }

            @Override
            public void getTermsContentFailure(String msg) {

            }

            @Override
            public void agreeTermsContentSuccess() {
                ToastUtils.showToast("注册完成");
                setResult(999);
                finish();
            }

            @Override
            public void agreeTermsContentFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        };
    }

    private void getIntentData() {
        token = getIntent().getStringExtra("token");
    }


    @OnClick(R.id.bt_nextpage_uppage)
    public void onClickNext() {
        if (alreadyNext) {
            linAgree.setVisibility(View.GONE);
            btAgree.setVisibility(View.GONE);
            btNextUpPage.setText(R.string.next_page);
        } else {
            linAgree.setVisibility(View.VISIBLE);
            btAgree.setVisibility(View.VISIBLE);
            btNextUpPage.setText(R.string.up_page);
        }
        alreadyNext = !alreadyNext;
    }

    @OnClick(R.id.bt_agree)
    public void agreeTerms() {
        if (cbAgree.isChecked()) {
            controller.agreeTermsContent();
        }
    }

}
