package com.ygbd198.hellodoctor.ui.gui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.ui.GuideActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


public class GuiActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private GuiAdapter adapter;

    private int currentPosition = 0;
    private int[] guis = {R.drawable.gui1,
            R.drawable.gui2,
            R.drawable.gui3,
            R.drawable.gui4,
            R.drawable.gui5};

    private List<View> imageViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui);
        viewPager = ButterKnife.findById(this, R.id.viewpager);
        imageViews = new ArrayList<>();
        for (int i = 0; i < guis.length; i++) {
            ImageView view = new ImageView(this);
            view.setBackgroundColor(Color.WHITE);
//            view.setImageResource(guis[i]);
            Glide.with(GuiActivity.this).load(guis[i]).into(view);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            if (i == guis.length - 1) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        startActivity(new Intent(GuiActivity.this, GuideActivity.class));
                    }
                });
            }
            imageViews.add(view);
        }

        adapter = new GuiAdapter(imageViews);
        viewPager.setAdapter(adapter);
        /*viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == adapter.getCount()-1){
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            startActivity(new Intent(GuiActivity.this,GuideActivity.class));
                        }
                    },500);
                }
                Log.d("monty", "onPageSelected -> position -> " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
    }

}
