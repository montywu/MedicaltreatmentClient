package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.CompleteLeariningVideoEvent;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.VedioBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.LearningVideoService;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.adapter.LearningVideoListAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public class LearningVideoActivity extends BaseActivity {

    @BindView(R.id.list_vedio)
    public ListView vedioList;

    private List<String> idList;
    private List<VedioBean> vedioBeen;
    private LearningVideoListAdapter adapter;
    private String token = MySelfInfo.getInstance().getToken();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View contentView = this.getLayoutInflater().inflate(R.layout.activity_video_list, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        initTitle();

        initListView();

        chackVideoId();

        initData();


    }

    @Subscribe(threadMode = ThreadMode.MAIN)//退出app
    public void refresh(CompleteLeariningVideoEvent event) {
        Log.e("gg", "请求视频列表页面数据");
        RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                MySelfInfo.getInstance().setPracticeVideoIds(response.body().data.practiceVideoIds);

                chackVideoId();

                initData();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void initTitle() {
        titleBar.showCenterText("实操视频");
    }

    @Override
    protected void initData() {
        RetrofitHelper.getInstance().createService(LearningVideoService.class).getVedioList(token).enqueue(new BaseCallBack<BaseCallModel<List<VedioBean>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<VedioBean>>> response) {
                vedioBeen.clear();
                vedioBeen.addAll(setIsReadVideo(response.body().data));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void initListView() {
        vedioBeen = new ArrayList<>();
        adapter = new LearningVideoListAdapter(this, vedioBeen);
        vedioList.setAdapter(adapter);
        vedioList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VedioBean bean = vedioBeen.get(position);

                //打开系统播放器
                // TODO: 2017/8/23 (写一个视屏播放页面) 
//                String type = "video/*";
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                intent.setDataAndType(Uri.parse(bean.videoUrl),type);
//                startActivity(intent);


                Intent intent = new Intent();
                intent.setClass(LearningVideoActivity.this, VideoPlayerActivity.class);
                intent.putExtra("id", bean.id);
                intent.putExtra("name", bean.name);
                intent.putExtra("introduction", bean.introduction);
                intent.putExtra("uri", bean.videoUrl);
                startActivity(intent);

            }
        });
    }

    private void chackVideoId() {
        String videoId = (null == MySelfInfo.getInstance().getPracticeVideoIds() ? "" : MySelfInfo.getInstance().getPracticeVideoIds());
        idList = new ArrayList<>();
        if(!videoId.isEmpty()){
            String[] ids = videoId.split(",");
            for (int i = 0; i < ids.length; i++) {
                int j = 0;
                for (String id : idList) {
                    if (!id.equals(ids[i])) {
                        j++;
                    }
                }
                if (j == idList.size()) {
                    idList.add(ids[i]);
                }
            }
        }

    }

    private List<VedioBean> setIsReadVideo(List<VedioBean> videoBeans) {
        for (VedioBean bean : videoBeans) {
            for (String isReadId : idList) {
                if (bean.id == Integer.valueOf(isReadId)) {
                    bean.isRead = true;
                    break;
                }
            }
        }
        return videoBeans;
    }
}
