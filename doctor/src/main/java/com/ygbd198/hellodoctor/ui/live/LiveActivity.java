package com.ygbd198.hellodoctor.ui.live;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.monty.library.EventBusUtil;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.tencent.av.sdk.AVView;
import com.tencent.ilivesdk.ILiveConstants;
import com.tencent.ilivesdk.core.ILiveRoomManager;
import com.tencent.ilivesdk.view.AVRootView;
import com.tencent.ilivesdk.view.AVVideoView;
import com.tencent.ilivesdk.view.BaseVideoView;
import com.tencent.ilivesdk.view.VideoListener;
import com.tencent.livesdk.ILVLiveManager;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;
import com.yanzhenjie.permission.Rationale;
import com.yanzhenjie.permission.RationaleListener;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.WaitingUser;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.PatientRecordIntegralActivity;
import com.ygbd198.hellodoctor.ui.live.event.BarrageEvent;
import com.ygbd198.hellodoctor.ui.live.fragment.LFSchemeTotalFragment;
import com.ygbd198.hellodoctor.ui.live.fragment.LFWaitingListFragment;
import com.ygbd198.hellodoctor.ui.live.presenters.LivePresenter;
import com.ygbd198.hellodoctor.ui.live.viewinterface.ILiveView;
import com.ygbd198.hellodoctor.util.DeviceInfoUtil;
import com.ygbd198.hellodoctor.util.DisplayUtil;
import com.ygbd198.hellodoctor.util.NetWork;
import com.ygbd198.hellodoctor.util.SoundManager;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 医生视频
 * Created by monty on 2017/8/8.
 */
public class LiveActivity extends BaseActivity implements ILiveView, LFWaitingListFragment.LFWaitingListListener, LFSchemeTotalFragment.OnSubmitRecordListener {

    @BindView(R.id.avRootView)
    AVRootView avRootView;
    @BindView(R.id.btn_pause)
    Button btnPause;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.tv_countDown)
    TextView tvCountDown;
    @BindView(R.id.tv_calling)
    TextView tvCalling;
    @BindView(R.id.tv_score)
    TextView tvScore;

    private LivePresenter livePresenter;
    private String screenShortPath = Environment.getExternalStorageDirectory() + File.separator + "HelloDoctor";

    private LFWaitingListFragment waitingListFragment;
    private int REQUEST_MEDIA_PROJECTION = 0x101;

    private List<File> photos = new ArrayList<>();

    public static void startLiveActivity(Context context) {
        context.startActivity(new Intent(context, LiveActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);
        mMediaProjectionManager = (MediaProjectionManager)
                getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        initView();
        initData();
        registerWifiBroadCast();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                createRoom();
            }
        }, 100);


        EventBusUtil.register(this);
        /*if(BuildConfig.DEBUG){
            onStartDiagnose();
        }
*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        ILVLiveManager.getInstance().onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        InitILiveHelper.initApp();
        ILVLiveManager.getInstance().onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
        unRegisterWifiBroadCast();
        if (livePresenter != null) {
            livePresenter.onDestory();
            livePresenter = null;
        }
    }

    private FrameLayout rightView;
    private TextView tvPhotosCount;

    @Override
    protected void initView() {
        rightView = (FrameLayout) LayoutInflater.from(this).inflate(R.layout.live_right_view, null);
        rightView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        tvPhotosCount = (TextView) rightView.findViewById(R.id.tv_count);

        this.titleBar.addCustomRightView(rightView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndPermission.with(LiveActivity.this).requestCode(100).permission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}).callback(new PermissionListener() {
                    @Override
                    public void onSucceed(int requestCode, @NonNull List<String> grantPermissions) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            startActivityForResult(mMediaProjectionManager.createScreenCaptureIntent(),
                                    REQUEST_MEDIA_PROJECTION);
                        } else {
                            ToastUtils.showToast("您的系统不支持截屏");
                        }
                    }

                    @Override
                    public void onFailed(int requestCode, @NonNull List<String> deniedPermissions) {
                        ToastUtils.showToast("请先开启SD卡读写权限");
                    }
                }).start();


            }
        });
        notifyPhotoCount();
        this.titleBar.showCenterText(R.string.treatment_room_title, R.drawable.treatment_room, 0);

        rightView.setVisibility(View.INVISIBLE);
        initAVRootView();
    }

    private void notifyPhotoCount() {
        if (photos.size() == 0) {
            tvPhotosCount.setVisibility(View.GONE);
        } else {
            tvPhotosCount.setVisibility(View.VISIBLE);
            tvPhotosCount.setText(photos.size() + "");
        }
    }

    private void clearPhotoCount() {
        tvPhotosCount.setText("0");
        tvPhotosCount.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MEDIA_PROJECTION) {
            if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this, "用户取消了", Toast.LENGTH_SHORT).show();
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                final ImageReader mImageReader = ImageReader.newInstance(DeviceInfoUtil.getWidth(this), DeviceInfoUtil.getHeight(this), 0x1, 2);
                MediaProjection mMediaProjection = null;

                mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);

                VirtualDisplay mVirtualDisplay = mMediaProjection.createVirtualDisplay("ScreenCapture",
                        DeviceInfoUtil.getWidth(this), DeviceInfoUtil.getHeight(this), getResources().getDisplayMetrics().densityDpi,
                        DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                        mImageReader.getSurface(), null, null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Image image = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            image = mImageReader.acquireLatestImage();
                        }
                        if (image == null) {
                            return;
                        }
                        int width = 0;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            width = image.getWidth();
                            int height = image.getHeight();
                            final Image.Plane[] planes = image.getPlanes();
                            final ByteBuffer buffer = planes[0].getBuffer();
                            int pixelStride = planes[0].getPixelStride();
                            int rowStride = planes[0].getRowStride();
                            int rowPadding = rowStride - pixelStride * width;
                            Bitmap mBitmap;
                            mBitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
                            mBitmap.copyPixelsFromBuffer(buffer);
                            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, width, height);
                            image.close();

                            if (mBitmap != null) {
                                //拿到mitmap
                                final Bitmap finalMBitmap = mBitmap;
                                String path = screenShortPath + File.separator + livePresenter.getCurrentUserToken();
                                File folder = new File(path);
                                if (!folder.exists()) {
                                    folder.mkdirs();
                                }
                                File file = new File(path + File.separator + System.currentTimeMillis() + ".jpg");

                                FileOutputStream fos = null;
                                try {
                                    fos = new FileOutputStream(file);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                finalMBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                photos.add(file);

                                ToastUtils.showToast("截图保存成功");
                                notifyPhotoCount();
                            }
                        }
                    }
                }, 300);
            } else {
                ToastUtils.showToast("系统版本5.0以下不支持视频截图");
            }
        } else if (requestCode == UploadActivity.REQUEST_CODE_UPLOAD) {
            if (resultCode == RESULT_OK) {
                resetVisiting();
            }
        }
    }

    @Override
    public void resetVisiting() {
        showWaitFragment(true);
        finishCountDown();
        clearPhotoCount();
        btnNext.setText("下一位");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                livePresenter.starDiagnose();
            }
        });
    }

    @Override
    public void showAlert(String s) {
        new AlertDialog.Builder(this).setMessage(s).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    MediaProjectionManager mMediaProjectionManager;

    private void initAVRootView() {
        ILVLiveManager.getInstance().setAvVideoView(avRootView);
        // 延时1秒执行美颜操作
        /*io.reactivex.Observable.timer(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) throws Exception {
                ILiveRoomManager.getInstance().enableBeauty(6); // 美颜 1-7
                ILiveRoomManager.getInstance().enableWhite(7); // 美白 1-9
            }
        });*/
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        avRootView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (displayMetrics.heightPixels - DisplayUtil.dp2px(this, 48)) / 2));
        ((View)frameLayout.getParent()).setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (displayMetrics.heightPixels - DisplayUtil.dp2px(this, 48)) / 2));
        avRootView.setLocalFullScreen(false);
        avRootView.setReRotationResize(false);
        avRootView.setFrontCamera(true);
        avRootView.setGravity(AVRootView.LAYOUT_GRAVITY_RIGHT);
        avRootView.setAutoOrientation(false);
        avRootView.setSubMarginY((displayMetrics.heightPixels - DisplayUtil.dp2px(this, 48)) / 2 - DisplayUtil.dp2px(this, 110) - DisplayUtil.dp2px(this, 10));
        avRootView.setSubMarginX(0);
        avRootView.setSubWidth(getResources().getDimensionPixelSize(R.dimen.small_area_width));
        avRootView.setSubHeight(getResources().getDimensionPixelSize(R.dimen.small_area_height));


        avRootView.setSubCreatedListener(new AVRootView.onSubViewCreatedListener() {
            @Override
            public void onSubViewCreated() {
                Log.d("monty", "setSubCreatedListener************");
                for (int i = 1; i < ILiveConstants.MAX_AV_VIDEO_NUM; i++) {
                    final int index = i;
                    AVVideoView avVideoView = avRootView.getViewByIndex(index);
                    avVideoView.setRotate(false);
                    avVideoView.setGestureListener(new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            avRootView.swapVideoView(0, index);
                            return super.onSingleTapConfirmed(e);
                        }
                    });
                }

                int min = Math.min(avRootView.getWidth(), avRootView.getHeight());
                avRootView.getViewByIndex(0).setPosTop(0);
                avRootView.getViewByIndex(0).setPosLeft((avRootView.getWidth() - min) / 2);
                avRootView.getViewByIndex(0).setPosWidth(min);
                avRootView.getViewByIndex(0).setPosHeight(min);
                avRootView.getViewByIndex(0).setSameDirectionRenderMode(BaseVideoView.BaseRenderMode.SCALE_TO_FIT);
                avRootView.getViewByIndex(0).setRotate(false);

//                avRootView.getViewByIndex(0).autoLayout();
                avRootView.getViewByIndex(0).setVideoListener(new VideoListener() {
                    @Override
                    public void onFirstFrameRecved(int width, int height, int angle, String identifier) {

                    }

                    @Override
                    public void onHasVideo(String identifier, int srcType) {
                        ILiveRoomManager.getInstance().enableBeauty(6); // 美颜 1-7
                        ILiveRoomManager.getInstance().enableWhite(7); // 美白 1-9
                    }

                    @Override
                    public void onNoVideo(String identifier, int srcType) {

                    }
                });
            }
        });


    }

    @Override
    protected void initData() {
        livePresenter = new LivePresenter(this);
        showWaitFragment(false);
    }

    private void showWaitFragment(boolean isVisiting) {
        if (!(waitingListFragment != null && waitingListFragment.isAdded() && waitingListFragment.isVisible())) {
            waitingListFragment = LFWaitingListFragment.newInstance(isVisiting);
            showFragment(waitingListFragment);
        }
        livePresenter.getWaitUser(null);
    }

    private void showFragment(Fragment fragment) {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }
//
//    public Fragment createFragment(int position) {
//        switch (position) {
//            case 0:
//                return LFWaitingListFragment.newInstance();
//            case 1:
//                return LFSchemeTotalFragment.newInstance();
//
//        }
//        return null;
//    }

    @OnClick(R.id.btn_pause)
    public void onBtnPauseClicked() {
        livePresenter.setReceive();
    }

    @OnClick(R.id.btn_next)
    public void onBtnNextClicked() {
        livePresenter.starDiagnose();
    }

    @Override
    public void setIsReceiveSuccess(boolean isReceive) {
        if (isReceive) {
            btnPause.setText("暂停接受新患者");
        } else {
            btnPause.setText("开始接受新患者");
        }
    }

    @Override
    public void setIsReceiveFailure(String msg) {
        ToastUtils.showToast(msg);
    }

    @Override
    public void quiteRoomComplete(boolean b) {
        Log.d("monty", "quiteRoomComplete --------------------> ");
        ToastUtils.showToast("退出房间");
    }

    @Override
    public void creageRoomComplete(boolean isSuccess, String errorMsg) {
        /*if (isSuccess) {
            rlWaitingView.setVisibility(View.INVISIBLE);
        } else {
            rlWaitingView.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public void enterRoomComplete(boolean b, String errorMsg) {

//        LFWaitingListFragment fragment = getFragment(DistributeFragment.WAITINGUSERLIST, LFWaitingListFragment.class);
        if (waitingListFragment != null && waitingListFragment.isVisible()) {
            if (b) {
                waitingListFragment.startComplete(true, "");
            } else {
                waitingListFragment.startComplete(false, errorMsg);
            }
        }
    }

    @Override
    public void refreshWaitingUserlist(List<WaitingUser> waitingUsers) {
        if (waitingListFragment != null && waitingListFragment.isVisible()) {
            waitingListFragment.notifyWaitingUserList(waitingUsers);
        }
    }

    /**
     * 开始连麦诊断
     *
     * @param diagnosticId 诊断记录Id
     */
    @Override
    public void onStartDiagnose(final long diagnosticId, final String name) {  // 此处开始配置治疗方案
//        this.titleBar.getRightIcon().setVisibility(View.VISIBLE);
        rightView.setVisibility(View.VISIBLE);
        btnNext.setText("回顾患者记录");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ToastUtils.showToast("回顾患者记录");
//                Intent intent = new Intent(LiveActivity.this, PatientRecordDetailsActivity.class);
//                intent.putExtra("id", diagnosticId);
//                startActivity(intent);

                Intent intent = new Intent();
                intent.putExtra("name", name);
                Log.e("gg", "livePresenter.getCurrentUserId() = " + livePresenter.getCurrentUserId());
                intent.putExtra("id", livePresenter.getCurrentUserId());
                intent.setClass(LiveActivity.this, PatientRecordIntegralActivity.class);
                startActivity(intent);
            }
        });
        LFSchemeTotalFragment lfSchemeTotalFragment = LFSchemeTotalFragment.newInstance(diagnosticId, livePresenter.getCurrentUserToken());
        showFragment(lfSchemeTotalFragment);
        startCountDown();
    }

    private SoundManager soundManager;

    @Override
    public void showCalling(boolean isShow) {
        if (isShow) {
            tvCalling.setVisibility(View.VISIBLE);
            tvCalling.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (tvCalling.isShown()) {
                        ToastUtils.showToast("用户无应答");
                        showCalling(false);
                        livePresenter.recrodBad();

                    }
                }
            }, 1000 * 30);
        } else {
            tvCalling.setVisibility(View.GONE);
        }
    }

    public void finishCountDown() {
        if (timerCountDown != null) {
            timerCountDown.cancel();
        }
        if (timerTimekeeping != null) {
            timerTimekeeping.cancel();
        }
        tvCountDown.setText("05 : 00");
        tvScore.setVisibility(View.GONE);
        tvCountDown.setTextColor(ContextCompat.getColor(LiveActivity.this, R.color.pink));
    }

    private Timer timerCountDown;

    /**
     * 倒计时
     */
    private void startCountDown() {
        timerCountDown = new Timer();
        TimerTask timerTask = new TimerTask() {
            int second = 60;
            int minute = 4;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvCountDown.setText("0" + minute + " : " + (second < 10 ? "0" + second : second));
                    }
                });

                if (second == 0 && minute == 0) {
                    timerCountDown.cancel();
                    Log.d("monty", "倒计时结束，开始正计时");
//                    tvCountDown.setTextColor(ContextCompat.getColor(LiveActivity.this,R.color.red));
                    startTimekeeping();
                }
                if (second == 0) {
                    minute--;
                    second = 60;
                }
                second--;
            }
        };
        timerCountDown.schedule(timerTask, 0, 1000);
    }

    private Timer timerTimekeeping;

    /**
     * 正计时
     */
    private void startTimekeeping() {
        timerTimekeeping = new Timer();
        TimerTask timerTask = new TimerTask() {
            int second = 0;
            int minute = 0;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvCountDown.setTextColor(Color.parseColor("#ff3333"));
                        tvCountDown.setText("- " + (minute < 10 ? "0" + minute : minute) + " : " + (second < 10 ? "0" + second : second));
                        tvScore.setVisibility(View.VISIBLE);

                    }
                });

                second++;
                if (second == 60) {
                    minute++;
                    final int score = (minute + 1) * 100;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvScore.setText("- " + score + "积分");
                        }
                    });
                    second = 0;
                }
            }
        };
        timerTimekeeping.schedule(timerTask, 0, 1000);

    }

    private void createRoom() {
        Log.d("monty", "activity createRoom");
        AndPermission.with(this)
                .requestCode(101)
                .rationale(new RationaleListener() {
                    @Override
                    public void showRequestPermissionRationale(int requestCode, Rationale rationale) {
                        AndPermission.rationaleDialog(LiveActivity.this, rationale).show();
                    }
                })
                .permission(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
                .callback(new PermissionListener() {
                    @Override
                    public void onSucceed(int requestCode, @NonNull List<String> grantPermissions) {
                        if (NetWork.isWifiConnected(LiveActivity.this)) {
//                            ToastUtils.showToast("已连接Wifi");
                            livePresenter.createRoom();
                        } else {
                            NiceDialogFactory.createComfirmDialog("提示", "您当前为非WIFI网络环境，确定要使用移动网络直播吗？", new NiceDialogFactory.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
//                                    livePresenter.applyCreateRoom();
                                    baseNiceDialog.dismiss();
                                    livePresenter.createRoom();
                                }
                            }).show(getSupportFragmentManager());
                        }
                    }

                    @Override
                    public void onFailed(int requestCode, @NonNull List<String> deniedPermissions) {
                        AndPermission.defaultSettingDialog(LiveActivity.this, requestCode).show();
                    }
                }).start();
    }

    @Override
    public void start() {
        // 开始前先检查权限
        AndPermission.with(this).requestCode(0x110).permission(new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}).callback(new PermissionListener() {
            @Override
            public void onSucceed(int requestCode, @NonNull List<String> grantPermissions) {
                livePresenter.applyCreateRoom();
            }

            @Override
            public void onFailed(int requestCode, @NonNull List<String> deniedPermissions) {

            }
        }).rationale(new RationaleListener() {
            @Override
            public void showRequestPermissionRationale(int requestCode, final Rationale rationale) {
                AndPermission.rationaleDialog(LiveActivity.this, rationale).show();
            }
        }).start();
    }

//    @Override
//    public void onDiseaseChecked(int position) {
//        //点击之后就显示药方
//        if (BuildConfig.DEBUG) {
//            ToastUtils.showToast("功能尚未完成...");
//            return;
//        }
//
////        showFragment(DistributeFragment.PRESCRIPTIONLIST);
//    }

    /**
     * 提交方案成功回调 （发消息给患者，让患者下麦，断开视频连接）
     */
    @Override
    public void onSubmitRecordSuccess() {

    }

    @Override
    public void onSubmitRecordFailure(String errorMsg) {

    }

    @Override
    public void showTipPopuWindow() {

        PopupWindow popupWindow = new PopupWindow(this);
        popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        final View inflate = LayoutInflater.from(this).inflate(R.layout.layout_popupwindow, null);

        ScaleAnimation scaleAni = new ScaleAnimation(0.8f, 1.0f, 0.8f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAni.setDuration(250);
        scaleAni.setRepeatMode(Animation.REVERSE);
        scaleAni.setRepeatCount(Animation.INFINITE);
        inflate.setAnimation(scaleAni);
        popupWindow.setContentView(inflate);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.showAsDropDown(rightView);

    }

    @Override
    public void finishDiagnose() {
        livePresenter.finishDiagnose();
    }

    @Override
    public boolean hasPhotos() {
        if (photos == null || photos.size() == 0) {
            return false;
        }
        return true;
    }

    private BroadcastReceiver receiver;

    private void registerWifiBroadCast() {
        receiver = new WifiReceive();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        registerReceiver(receiver, filter);
    }

    private void unRegisterWifiBroadCast() {
        unregisterReceiver(receiver);
    }

    private class WifiReceive extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
//            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {// 这个监听wifi的打开与关闭，与wifi的连接无关
//                int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
//                Log.e("H3c", "wifiState" + wifiState);
//                switch (wifiState) {
//                    case WifiManager.WIFI_STATE_DISABLED:
//                        break;
//                    case WifiManager.WIFI_STATE_DISABLING:
//                        break;
//                    //
//                }
//            }
            if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {//wifi连接上与否
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info.getState().equals(NetworkInfo.State.DISCONNECTED)) {
                    ToastUtils.showToast("wifi网络连接断开");
                } else if (info.getState().equals(NetworkInfo.State.CONNECTED)) {

                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                    //获取当前wifi名称
                    ToastUtils.showToast("连接到网络 " + wifiInfo.getSSID());

                }
            }
//            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
//                ConnectivityManager connectMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
//                NetworkInfo mobNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
//                NetworkInfo wifiNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//                if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
//                    ToastUtils.showToast("断网了");
//                }
//            }
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onRevisitEvent(BarrageEvent event) {
        livePresenter.sendBarrage(event.content);
    }

    public void goToAppDetailSettingIntent(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        context.startActivity(localIntent);
    }

    //房间异常
    @Override
    public void onException(int exceptionId, int errCode, String errMsg) {
        if (ILiveConstants.EXCEPTION_ENABLE_CAMERA_FAILED == exceptionId || ILiveConstants.EXCEPTION_ENABLE_MIC_FAILED == exceptionId) {
            new AlertDialog.Builder(this)
                    .setMessage("请打开\"相机\"或\"麦克风\"权限设置")
                    .setPositiveButton("前往", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goToAppDetailSettingIntent(LiveActivity.this);
                        }
                    })
                    .show();
        }
        Log.e("monty", "房间异常 onException -> exceptionId:" + exceptionId + ",errCode:" + errCode + ",errMsg:" + errMsg);
    }

    //请求画面回调
    @Override
    public void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg) {
        Log.e("monty", "请求画面回调 onComplete -> identifierList:" + identifierList.toString() + ",viewList:" + viewList.toString() + ",count:" + count + ",result:" + result + ",errMsg:" + errMsg);
    }

    //房间异常退出回调(一般为断网)
    @Override
    public void onRoomDisconnect(int errCode, String errMsg) {
        Log.e("monty", "房间异常退出回调 onRoomDisconnect -> errCode:" + errCode + ",errMsg:" + errMsg);
    }
}
