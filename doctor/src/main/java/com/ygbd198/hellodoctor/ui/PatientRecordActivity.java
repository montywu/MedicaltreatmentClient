package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.PatientRecordBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.PatientRecordService;
import com.ygbd198.hellodoctor.ui.adapter.PatientRecordAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Administrator on 2017/9/10 0010.
 * 患者记录
 */

public class PatientRecordActivity extends BaseActivity {

    @BindView(R.id.listview)
    ListView listView;

    private View contentView;
    private List<PatientRecordBean> data;
    private PatientRecordAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = this.getLayoutInflater().inflate(R.layout.activity_patient_record, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.patient_record,R.drawable.patient_records,0);

        initView();

        initData();
    }

    @Override
    protected void initView() {
        listView = (ListView) contentView.findViewById(R.id.listview);
        data = new ArrayList<>();
        adapter = new PatientRecordAdapter(this, data);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("name", data.get(position).userName);
                intent.putExtra("id",data.get(position).userId);
                intent.setClass(PatientRecordActivity.this, PatientRecordIntegralActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void initData() {
        RetrofitHelper.getInstance().createService(PatientRecordService.class).getRecords(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<PatientRecordBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<PatientRecordBean>>> response) {
                data.clear();
                data.addAll(response.body().data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }
}
