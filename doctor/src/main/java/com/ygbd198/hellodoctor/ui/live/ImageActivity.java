package com.ygbd198.hellodoctor.ui.live;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageActivity extends BaseActivity {
    @BindView(R.id.gridview)
    GridView gridview;
    @BindView(R.id.btn_upload)
    Button btnUpload;
    private String[] paths;

    public static void GotoActivity(Context context, String[] paths) {
        Intent intent = new Intent(context, ImageActivity.class);
        intent.putExtra("paths", paths);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);
        paths = getIntent().getStringArrayExtra("paths");
        btnUpload.setVisibility(View.GONE);
        titleBar.showCenterText("患者照片", R.drawable.records_details, 0);
        gridview.setAdapter(new ImageAdapter());
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PreviewActivity.GotoActivity(ImageActivity.this, paths[position]);
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {

        public ImageAdapter() {
        }

        @Override
        public int getCount() {
            return paths.length;
        }

        @Override
        public Object getItem(int position) {
            return paths[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(ImageActivity.this).inflate(R.layout.upload_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                int screenWidth = getScreenWidth(ImageActivity.this);
                AbsListView.LayoutParams param = new AbsListView.LayoutParams(screenWidth / 3, screenWidth / 3);
                convertView.setLayoutParams(param);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.checkbox.setVisibility(View.GONE);
            Glide.with(ImageActivity.this)
                    .load(paths[position])
                    .centerCrop()
                    .into(viewHolder.image);
            return convertView;
        }

        class ViewHolder {
            @BindView(R.id.image)
            ImageView image;
            @BindView(R.id.checkbox)
            CheckBox checkbox;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }

    public static int getScreenWidth(Context context) {
        WindowManager manager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        return display.getWidth();
    }
    /*public static int dipToPx(Context context, int dip) {
        if(density <= 0.0F) {
            density = context.getResources().getDisplayMetrics().density;
        }

        return (int)((float)dip * density + 0.5F);
    }*/


}
