package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/19.
 */

public abstract class ApplyBeDocLoginController {


    public abstract void getCodeSuccess();
    public abstract void getCodeFailure(String msg);

    public abstract void verifySuccess(DoctorBean bean);
    public abstract void verifyFailure(String msg);


    private Context context;

    public ApplyBeDocLoginController(Context context) {
        this.context = context;
    }

    public void getCode(String phone) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getVerifyCode(phone).enqueue(new BaseCallBack<BaseCallModel<String>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                getCodeSuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getCodeFailure(msg);
            }
        });
    }

    public void verifyIdentity(String phone, String password, String code) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).doctorApply(phone,password,code).enqueue(new BaseCallBack<BaseCallModel<DoctorBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<DoctorBean>> response) {
                DoctorBean bean = response.body().data;
                verifySuccess(bean);
            }

            @Override
            public void onFailure(String msg) {
                verifyFailure(msg);
            }
        });
    }
}
