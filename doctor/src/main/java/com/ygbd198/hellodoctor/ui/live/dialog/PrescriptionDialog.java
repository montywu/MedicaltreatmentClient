package com.ygbd198.hellodoctor.ui.live.dialog;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.monty.library.widget.dialog.BaseBottomDialog;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.ui.live.fragment.PrescriptionListFragment;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * 获取平台药方Dialog
 * Created by monty on 2017/8/27.
 */
@Deprecated
public class PrescriptionDialog extends BaseBottomDialog implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener, CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.rb_not_prescription)
    RadioButton rbNotPrescription;
    @BindView(R.id.rb_prescription)
    RadioButton rbPrescription;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    private int type;
    private MyPagerAdapter adapter;

    private OnPrescriptionCheckChangeListener listener;

    public PrescriptionDialog(OnPrescriptionCheckChangeListener listener){
        this.listener = listener;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.prescription_dialog;
    }

    @Override
    public int getHeight() {
        WindowManager wm = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        int height = wm.getDefaultDisplay().getHeight();
        return (int) (height * 0.8);
    }

    @Override
    public void bindView(View v) {
        unbinder = ButterKnife.bind(this, v);
        adapter = new MyPagerAdapter(getChildFragmentManager());
        viewpager.setAdapter(adapter);
        rbNotPrescription.setOnCheckedChangeListener(this);
        rbPrescription.setOnCheckedChangeListener(this);
        rbNotPrescription.setChecked(true);
        radioGroup.setOnCheckedChangeListener(this);
        viewpager.addOnPageChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_not_prescription:
                viewpager.setCurrentItem(0);
                break;
            case R.id.rb_prescription:
                viewpager.setCurrentItem(1);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            rbNotPrescription.setChecked(true);
        } else if (position == 1) {
            rbPrescription.setChecked(true);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonView.setTextColor(ContextCompat.getColor(getContext(), R.color.pink3));
        } else {
            buttonView.setTextColor(ContextCompat.getColor(getContext(), R.color.blue));
        }
    }


    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        ToastUtils.showToast("一共选择 " + adapter.getCheckedList().size() + " 项");
        listener.onCheckedComplete(adapter.getCheckedList());
        dismissAllowingStateLoss();
    }

    class MyPagerAdapter extends FragmentPagerAdapter {
        private List<PrescriptionListFragment> fragments = new ArrayList<>(2);

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            PrescriptionListFragment frgment = PrescriptionListFragment.getInstance(position);
            fragments.add(frgment);
            return frgment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        public List<Prescription> getCheckedList() {
            List<Prescription> checkedList = new ArrayList<>();
            for (PrescriptionListFragment preFragment : fragments) {
                checkedList.addAll(preFragment.getCheckedList());
            }
            return checkedList;
        }
    }
    public interface OnPrescriptionCheckChangeListener{
        void onCheckedComplete(List<Prescription> checkedList);
    }
}
