package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.IntegralBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.IntegraDetailsAdapter;
import com.ygbd198.hellodoctor.ui.controller.IntegraDteailController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class IntegraDetailActivity extends BaseActivity {
    @BindView(R.id.list_exchange_record)
    ListView listview;

    private List<IntegralBean> integralBeanList;
    private IntegraDteailController controller;
    private IntegraDetailsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_exchange_record, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initController();

        initTitle();

        initList();

        getData();
    }

    private void initTitle() {
        titleBar.showCenterText("积分明细",R.drawable.income_details,0);
    }

    private void getData() {
        controller.getIntegraDtealList();
    }

    private void initList() {
        integralBeanList = new ArrayList<>();
        adapter = new IntegraDetailsAdapter(this, integralBeanList);
        listview.setAdapter(adapter);
    }

    private void initController() {
        controller = new IntegraDteailController() {
            @Override
            public void getDataSuccess(List<IntegralBean> integralBeen) {
                integralBeanList.clear();
                sortByTime(integralBeen);
                integralBeanList.addAll(integralBeen);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void getDataFailrue() {

            }
        };
    }

    private void sortByTime(List<IntegralBean> integralBeen) {
        Collections.sort(integralBeen, new Comparator<IntegralBean>() {
            @Override
            public int compare(IntegralBean o1, IntegralBean o2) {
                if (dataToSecond(o1.createTime) > dataToSecond(o2.createTime)) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
    }

    private long dataToSecond(String data) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return simpleDateFormat.parse(data).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
