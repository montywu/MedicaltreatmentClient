package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.bean.Record;
import com.ygbd198.hellodoctor.ui.live.DoctorService;
import com.ygbd198.hellodoctor.ui.live.UploadActivity;
import com.ygbd198.hellodoctor.ui.live.adapter.TabFragmentPagerAdapter;
import com.ygbd198.hellodoctor.ui.live.dialog.PrescriptionDialog2;
import com.ygbd198.hellodoctor.ui.live.event.BarrageEvent;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseEvent;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseListEvent;
import com.ygbd198.hellodoctor.ui.live.event.PrescriptionEvent;
import com.ygbd198.hellodoctor.ui.live.event.RevisitEvent;
import com.ygbd198.hellodoctor.ui.live.event.TaboosEvent;
import com.ygbd198.hellodoctor.util.FileUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.CustomViewPager;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * 方案汇总页面
 */
public class LFSchemeTotalFragment extends Fragment {
    public static final String TAG = "LFSchemeTotalFragment";
    @BindView(R.id.btn_add)
    TextView btnAdd;
    @BindView(R.id.tablayout)
    TabLayout tablayout;
    @BindView(R.id.viewpager)
    CustomViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.tv_fragment_title)
    TextView tvFragmentTitle;
    @BindView(R.id.btn_previous)
    Button btnPrevious;
    @BindView(R.id.btn_dictate)
    Button btnDictate;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    private Fragment currentFragment;
    private int currentPosition;
    private boolean isTotal;
    private String screenShortPath = Environment.getExternalStorageDirectory() + File.separator + "HelloDoctor";
    private String[] fragmentTitle = {"治疗方案--患者类型"
            , "治疗方案--药方"
            , "治疗方案--饮食"
            , "治疗方案--生活习惯及作息"
            , "治疗方案--禁忌"
            , "治疗方案--复诊"};

    private String[] tabTitle = {
            "患者类型"
            , "药方"
            , "饮食"
            , "作息"
            , "禁忌"
            , "复诊"
    };

    /*要提交的数据*/
    private Record record = new Record();

    private TabFragmentPagerAdapter tabFragmentPagerAdapter;

    private OnSubmitRecordListener listener;

    private String userToken;

    public static LFSchemeTotalFragment newInstance(long diagnosticId, String userToken) {
        Log.d(TAG, "newInstance*************");
        Bundle bundle = new Bundle();
        bundle.putLong("diagnosticId", diagnosticId);
        bundle.putString("token", userToken);
        LFSchemeTotalFragment fragment = new LFSchemeTotalFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    /*public static LFSchemeTotalFragment newInstance(String param1, String param2) {
        LFSchemeTotalFragment fragment = new LFSchemeTotalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("monty", TAG + " -> onCreate");

        if (getArguments() != null) {
            long diagnosticId = getArguments().getLong("diagnosticId");
            record.diagnosticId = diagnosticId;
            userToken = getArguments().getString("token");
            screenShortPath += File.separator + userToken;
            deleteFile();
        }
    }

    private void deleteFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("monty", "delete files *********** start");
                Log.d("monty", "delete files *********** path ->" + screenShortPath);
                try {
                    FileUtil.deleteFile(new File(screenShortPath));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("monty", "delete files *********** exception ->" + e.getMessage());
                }
                Log.d("monty", "delete files *********** end");
            }
        }).start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach*************");
        this.listener = (OnSubmitRecordListener) context;
        EventBusUtil.register(this);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach*************");
        super.onDetach();
        this.listener = null;
        EventBusUtil.unregister(this);
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy*************");
        super.onDestroy();
        this.listener = null;
        EventBusUtil.unregister(this);
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scheme_total, container, false);
        unbinder = ButterKnife.bind(this, view);
        Log.d(TAG, "onCreateView*************");
        initViewpager();
        initTabLayout();
        showFragment(0);
        return view;
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onDiseaseSelectEvent(DiseaseEvent event) {
        record.treatmentName = event.typeName;
    }

    @Subscribe
    public void onPrescriptionEvent(PrescriptionEvent event) {
        record.drugs = new ArrayList<>();
        List<Prescription> prescriptions = event.prescriptions;

        for (int i = 0; i < prescriptions.size(); i++) {
            Record.Drug drug = new Record.Drug();
            Prescription prescription = prescriptions.get(i);
            drug.drugId = prescription.getId();
            drug.promptTime = prescription.getPromptTime();
            drug.useTime = prescription.getDefaultUseTime();
            drug.useInfo = prescription.getDefaultUseInfo();
            drug.useType = prescription.getUseType();
            drug.count = 1;
            record.drugs.add(drug);
        }
    }

    @Subscribe
    public void onRevisitEvent(RevisitEvent event) {
        record.revisits = event.revisit;
    }

    @Subscribe
    public void onTaboosEvent(TaboosEvent event) {
        switch (event.getType()) {
            case TaboosEvent.DIET:
                record.diets = event.getIds();
                break;
            case TaboosEvent.REST:
                record.rests = event.getIds();
                break;
            case TaboosEvent.TABOO:
                record.taboos = event.getIds();
                break;
        }
    }

    private List<DiseaseTypeBean> diseaseEvents;

    @Subscribe
    public void onDiseaseListEvent(DiseaseListEvent event) {
        this.diseaseEvents = event.diseaseTypes;
    }

    private void initViewpager() {
        Log.d(TAG, "initViewpager*************");
        tabFragmentPagerAdapter = new TabFragmentPagerAdapter(getChildFragmentManager());
        Log.d(TAG, "tabFragmentPagerAdapter*************" + tabFragmentPagerAdapter.hashCode());
        Log.d(TAG, "viewpager*************" + viewpager.hashCode());
        viewpager.setOffscreenPageLimit(TabFragmentPagerAdapter.FRAGMENT_COUNT);

        viewpager.setAdapter(tabFragmentPagerAdapter);
        Log.d(TAG, "setAdapter*************" + viewpager.hashCode());
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("monty", "initViewPager - onPageSelected - position ->" + position);
                showFragmentNotSetViewpager(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewpager.setIsCanScroll(false); //  设置默认是不支持滑动的
    }

    /**
     * 设置ViewPager是否可以滑动
     *
     * @param canScroll
     */
    public void setViewPagerCanScroll(boolean canScroll) {
        viewpager.setIsCanScroll(canScroll);
    }

    /**
     * 切换Fragment
     *
     * @param position
     */
    public void setCurrentItem(int position) {
        showFragment(position);
    }

    /**
     * 设置TabLayout是否可见
     *
     * @param show
     */
    public void showTabLayout(boolean show) {
        tablayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showFragmentNotSetViewpager(int position) {
        Log.d("monty", "showFragment -> " + position);
        tablayout.getTabAt(position).setText(tabTitle[position]).select();
        if (isTotal) {
            tvFragmentTitle.setText("治疗方案汇总");
        } else {
            tvFragmentTitle.setText(fragmentTitle[position]);
        }
        if (position == 1) {
            btnAdd.setVisibility(View.VISIBLE);
        } else {
            btnAdd.setVisibility(View.GONE);
        }
        showMenuButton(position);
        currentFragment = tabFragmentPagerAdapter.getFragment(position);
        currentPosition = position;
    }

    private void showFragment(int position) {
        Log.d("monty", TAG + " -> showFragment position : " + position);
        if (!isTotal) {
            String barrage = "";
            switch (position) {
                case 0:
                    barrage = "医生正在为你确定主要治疗类型";
                    break;
                case 1:
                    barrage = "医生正在为你选择药方";
                    break;
                case 2:
                    barrage = "医生正在为你确定饮食注意事项";
                    break;
                case 3:
                    barrage = "医生正在为你制定生活习惯内容";
                    break;
                case 4:
                    barrage = "医生正在为你确定禁忌内容";
                    break;
                case 5:
                    barrage = "医生正在确定复诊时间";
                    break;
            }
            EventBusUtil.post(new BarrageEvent(barrage));
        }
//        viewpager.setCurrentItem(position);
        showFragmentNotSetViewpager(position);
    }

    private boolean checkCanNext(int position) {
        switch (position) {
            case 0:
                if (TextUtils.isEmpty(record.treatmentName)) {
                    ToastUtils.showToast("请先选择患者类型再进行下一步操作");
                    return false;
                }
                return true;
            case 1:
                if (record.drugs == null || record.drugs.size() == 0) {
                    ToastUtils.showToast("请至少选择一个药方再进行下一步操作");
                    return false;
                }
                return true;
            case 2:
                if (TextUtils.isEmpty(record.diets)) {
                    ToastUtils.showToast("请至少选择一个饮食方案再进行下一步操作");
                    return false;
                }
                return true;
            case 3:
                if (TextUtils.isEmpty(record.rests)) {
                    ToastUtils.showToast("请至少选择一个生活习惯和作息方案再进行下一步操作");
                    return false;
                }
                return true;
            case 4:
                if (TextUtils.isEmpty(record.taboos)) {
                    ToastUtils.showToast("请至少选择一个禁忌方案再进行下一步操作");
                    return false;
                }
                return true;
            case 5:
                if (record.revisits == null || TextUtils.isEmpty(record.revisits.revisitDate) || TextUtils.isEmpty(record.revisits.revisitTime)) {
                    ToastUtils.showToast("请填写复诊时间再进行下一步操作");
                    return false;
                }
                return true;
        }
        return true;
    }

    private String[] splitStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        } else {
            return str.split(",");
        }

    }

    private CountDownTimer countDownTimer = new CountDownTimer(1000 * 11, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            Log.d("monty", "countDownTimer -> " + millisUntilFinished);
//            btnDictate.setText("口述病症(" + new BigDecimal(millisUntilFinished/1000).setScale(0, BigDecimal.ROUND_HALF_UP)  + "s)");
            if (btnDictate != null) {
                btnDictate.setText("口述(" + (millisUntilFinished / 1000 - 1) + "s)");
            }
        }

        @Override
        public void onFinish() {
            btnDictate.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
        }

    };

    private void showMenuButton(int position) {
        countDownTimer.cancel();
        btnPrevious.setVisibility(View.GONE);
        btnNext.setVisibility(View.GONE);
        btnConfirm.setVisibility(View.GONE);
        if (isTotal) {
            btnDictate.setVisibility(View.GONE);
            btnConfirm.setVisibility(View.VISIBLE);
        } else {
//            btnDictate.setText("口述病症(10s)");
            countDownTimer.start();
            btnDictate.setVisibility(View.VISIBLE);
            if (position != 0) {
                btnPrevious.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initTabLayout() {
        tablayout.addTab(tablayout.newTab().setText("患者类型"));
        tablayout.addTab(tablayout.newTab().setText("药方"));
        tablayout.addTab(tablayout.newTab().setText("饮食"));
        tablayout.addTab(tablayout.newTab().setText("作息"));
        tablayout.addTab(tablayout.newTab().setText("禁忌"));
        tablayout.addTab(tablayout.newTab().setText("复诊"));
        tablayout.setupWithViewPager(viewpager);  // 此处有个坑，这句代码一定要在viewpagersetAdapter后调，因为setadapter中会调用removeAllTabs();
        showTabLayout(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private List<Integer> checkedIds = new ArrayList<>(); // 缓存选中的药品id

    @OnClick(R.id.btn_add)
    public void onViewClicked() {
        Log.d("monty", "diseaseEvents -> " + diseaseEvents.toString());
        PrescriptionDialog2 dialog = new PrescriptionDialog2(checkedIds, diseaseEvents, new PrescriptionDialog2.OnPrescriptionCheckChangeListener() {
            @Override
            public void onCheckedComplete(List<Prescription> checkedList) {
                if (currentPosition == 1 && currentFragment instanceof LFPrescriptionFragment && currentFragment.isVisible()) {
                    LFPrescriptionFragment fragment = (LFPrescriptionFragment) currentFragment;
                    fragment.addNotifyData(checkedList);
                }
            }
        });
        dialog.show(getFragmentManager());
    }

    @OnClick(R.id.btn_previous)  // 上一步
    public void onBtnPreviousClicked() {
        showFragment(currentPosition - 1);

    }

    @OnClick(R.id.btn_dictate) // 口述，什么都不做
    public void onBtnDictateClicked() {
        // nothing
    }


    @OnClick(R.id.btn_next) // 下一步
    public void onBtnNextClicked() {
        if (!listener.hasPhotos()) {
            listener.showTipPopuWindow();
            ToastUtils.showToast("请对症状进行拍照(点击右上角拍照按钮)");
            return;
        }
        if (currentPosition == tabFragmentPagerAdapter.getCount() - 1) {
            isTotal = true;
            showTabLayout(true);
//            showFragment(0);
            showFragment(tabFragmentPagerAdapter.getCount() - 1);
            setViewPagerCanScroll(true);
        } else {
            if (checkCanNext(currentPosition))
                showFragment(currentPosition + 1);
        }
    }

    int clickCount = 0;

    @OnClick(R.id.btn_confirm) // 提交方案
    public void onBtnConfirmClicked() {
        // TODO: 2017/8/27 提交方案
//        record.diagnosticId = 72;
//        List<Record.Drug> drugs = new ArrayList<>();
//        Record.Drug drug = new Record.Drug();
//        drug.drugId = 3;
//        drug.count = 2;
//        drug.useType = 1;
//        drug.useInfo = "Android 测试";
//        drug.useTime = "2017-09-20";
//        drug.promptTime = "20:12-20:12";
//        drugs.add(drug);
//        drugs.add(drug);
//        record.drugs = drugs;
//        record.treatmentName = "熊猫眼";
//        record.diets = "2";
//        record.rests = "1";
//        record.taboos = "1";
        record.nurseId = 1;
//        Record.Revisit revisit = new Record.Revisit();
//        revisit.revisitContent = "Android测试";
//        revisit.revisitTime = "15:12-15:12";
//        revisit.revisitDate = "2017-09-03";
//        record.revisits = revisit;

        clickCount++;
        if (clickCount == 1) {
            NiceDialogFactory.createTip2Dialog("提醒患者可用现金券支付诊金，收取治疗方案", new NiceDialogFactory.OnPositiveClickListener() {
                @Override
                public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                    btnConfirm.setText("发送方案");
                    baseNiceDialog.dismiss();
                }
            }).show(getFragmentManager());
        } else if (clickCount == 2) {
            EventBusUtil.post(new BarrageEvent("医生正在发送方案"));
            sendScheme();
        } else if (clickCount == 3) { // 结束治疗
            clickCount = 0;
            showLoadingDialog();
            RetrofitHelper.getInstance().createService(DoctorService.class).endLive(MySelfInfo.getInstance().getToken(), userToken, MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
                @Override
                public void onSuccess(Response<BaseCallModel<String>> response) {
                    Log.d("monty", "endLive - onSuccess -> ");
                    UploadActivity.GotoActivity(getActivity(), screenShortPath);
                    closeLoadingDialog();
                    listener.finishDiagnose();
                }

                @Override
                public void onFailure(String message) {
                    Log.d("monty", "endLive - onFailure -> " + message);
                    closeLoadingDialog();
                    listener.finishDiagnose();
                }
            });

        }
    }

    private void sendScheme() {
        showLoadingDialog();
        Log.d("monty", "post params -> " + record.parse2Json());
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), record.parse2Json());
        RetrofitHelper.getInstance().createService(DoctorService.class).submitRecord(MySelfInfo.getInstance().getToken(), body).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                closeLoadingDialog();
                btnConfirm.setText("结束诊疗");
                listener.onSubmitRecordSuccess();
                NiceDialogFactory.createTip2Dialog("方案已发送", new NiceDialogFactory.OnPositiveClickListener() {
                    @Override
                    public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                        baseNiceDialog.dismiss();
                    }
                }).show(getFragmentManager());
            }

            @Override
            public void onFailure(String message) {
                closeLoadingDialog();
                ToastUtils.showToast("方案提交失败 - " + message);
                listener.onSubmitRecordFailure(message);
            }
        });
    }

    private LoadingDialog dialog;

    public void showLoadingDialog() {
        if (dialog == null) {
            dialog = new LoadingDialog(getContext());
        }
        dialog.show();
    }

    public void closeLoadingDialog() {
        if (dialog != null) {
            dialog.close();
        }
    }


    public interface OnSubmitRecordListener {
        void onSubmitRecordSuccess();

        void onSubmitRecordFailure(String errorMsg);

        void showTipPopuWindow();

        void finishDiagnose();

        boolean hasPhotos();
    }
}
