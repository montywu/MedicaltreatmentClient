package com.ygbd198.hellodoctor.ui.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.http.UploadRetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.SelectPopBean;
import com.ygbd198.hellodoctor.event.UserInfoUptateEvent;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.PersonalInfActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.CommonSelectPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.GetDoctorRolePop;
import com.ygbd198.hellodoctor.widget.PopupWindon.GetImgWayPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.InputPop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

/**
 * Created by guangjiqin on 2017/8/28.
 */

public abstract class PersonalInfController {

    private InputPop usernameInputPop;
    private CommonSelectPop genderpPop;
    private CommonSelectPop educationPop;
    private InputPop hospitalInputPop;
    private GetDoctorRolePop getDoctorRolePop;
    private InputPop functionInputPop;
    private InputPop specialtyInputPop;

    public abstract void uploadVedioSuccess(int imgId, String path);

    public abstract void uploadVedioFail(String msg);


    public abstract void uploadPhotoSuccess(int imgId, String path);

    public abstract void uploadPhotoFailure(String msg);

    //获取医生等级
    public abstract void getRoleSuccess(List<DoctorRoleBean> doctorRoleBeen);

    public abstract void getRoleFailure(String msg);

    private String token;
    private Context context;
    private List<DoctorRoleBean> doctorRoleBeen;

    public PersonalInfController(Context context) {
        token = MySelfInfo.getInstance().getToken();
        this.context = context;
        getRole();
    }


    /**
     * 医生等级
     */
    public void getRole() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDoctorRole(token).enqueue(new BaseCallBack<BaseCallModel<List<DoctorRoleBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DoctorRoleBean>>> response) {
                doctorRoleBeen = response.body().data;
                getRoleSuccess(doctorRoleBeen);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getRoleFailure(msg);
            }
        });

    }

    //添加自我介绍视频
    public void toMakeVideo(final int viewId) {
        Intent intentToVideo = new Intent();
        intentToVideo.setAction(MediaStore.ACTION_VIDEO_CAPTURE);

        String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
        File path1 = new File(path);
        if (!path1.exists()) {
            path1.mkdirs();
        }

        File file = new File(path1, String.valueOf(viewId) + ".mp4");//添加缓存文件
//        Uri mOutPutFileUri = Uri.fromFile(file);
        Uri mOutPutFileUri = null;
        if (android.os.Build.VERSION.SDK_INT < 23) {
            mOutPutFileUri = Uri.fromFile(file);
        } else {
            mOutPutFileUri = FileProvider.getUriForFile(context, "gg.hello.dc", file);

        }
        intentToVideo.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
        ((PersonalInfActivity) context).startActivityForResult(intentToVideo, 200);
    }


    /**
     * 视频文件上传
     */
    public void uploadVideoFile(final int imgId, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("", "");


        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

        List<MultipartBody.Part> parts = builder.build().parts();
        Log.e("gg", "开始视频上传请求");
        UploadRetrofitHelper.changeApiBaseUrl(UploadRetrofitHelper.VIDEO_UPLOAD_HOST);//视频上传接口使用8081
        UploadRetrofitHelper.getInstance().uploadImage(ApplyBeDocService.class).doctorVideoUpload(parts).enqueue(new Callback<BaseCallModel<List<String>>>() {
            @Override
            public void onResponse(Call<BaseCallModel<List<String>>> call, Response<BaseCallModel<List<String>>> response) {

                if (null == response || null == response.body() || null == response.body().data) {
                    uploadVedioFail("视频上传失败");
                    return;
                }
                saveURL(imgId, response.body().data);
            }


            @Override
            public void onFailure(Call<BaseCallModel<List<String>>> call, Throwable t) {
                t.printStackTrace();
                Log.e("gg", "视频上传失败" + call.request().body());
                uploadVedioFail("");
            }
        });
    }

    //保存文件上传url
    private void saveURL(final int imgId, List<String> data) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).saveIntroduceUrl(token, data.get(0)).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                uploadVedioSuccess(imgId, "");
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }


    /**
     * 图片提交
     */
    public void uploadPhoto(final int imgId, File file, final int type) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("正在上传头像");
        Luban.with(context).load(file).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {
                progressDialog.show();
            }

            @Override
            public void onSuccess(File file) {
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)//表单类型
                        .addFormDataPart("token", token)
                        .addFormDataPart("type", String.valueOf(type));

                RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

                List<MultipartBody.Part> parts = builder.build().parts();
                Log.e("gg", "开始上传请求");
                UploadRetrofitHelper.getInstance().uploadImage(MyCenterService.class).doctorPhotoUpload(parts).enqueue(new Callback<BaseCallModel<String>>() {
                    @Override
                    public void onResponse(Call<BaseCallModel<String>> call, Response<BaseCallModel<String>> response) {
                        Log.e("gg", "上传成功 = " + response.body().data);
                        uploadPhotoSuccess(imgId, response.body().data);
                        EventBusUtil.post(new UserInfoUptateEvent());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<BaseCallModel<String>> call, Throwable t) {
                        t.printStackTrace();
                        Log.e("gg", "上传失败" + t.getMessage());
                        uploadPhotoFailure("");
                        progressDialog.dismiss();
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                ToastUtils.showToast("头像上传失败");
                progressDialog.dismiss();
            }
        }).launch();


    }

    //======各种pop=====//

    //显示获取图片途径的pop
    public void showGetPhotoPop(final int viewId) {
        GetImgWayPop pop = new GetImgWayPop(context) {
            @Override
            public String takePhoto() {//
                Intent intentToCamera = new Intent();
                intentToCamera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);


                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                File path1 = new File(path);
                if (!path1.exists()) {
                    path1.mkdirs();
                }

                File file = new File(path1, String.valueOf(viewId) + ".jpg");//添加缓存文件
//                Uri mOutPutFileUri = Uri.fromFile(file);

                Uri mOutPutFileUri = FileProvider.getUriForFile(context, "gg.hello.dc", file);

                intentToCamera.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
                ((PersonalInfActivity) context).startActivityForResult(intentToCamera, viewId);
                return null;
            }

            @Override
            public String getPhotoFromSD() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra("return-data", true);
                ((PersonalInfActivity) context).startActivityForResult(intent, viewId * 100);
                return null;
            }
        };
        pop.showPop();
    }

    //英文名输入的pop
    public void showUserNameInppop() {
        if (null == usernameInputPop) {
            usernameInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((PersonalInfActivity) context).tvNameEn.setText(content);
                }
            };
            usernameInputPop.setTitle(R.string.input_user_name);
        }
        usernameInputPop.showPop();
    }


    //选择性别pop
    public void showGenderpPop() {
        if (null == genderpPop) {
            genderpPop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((PersonalInfActivity) context).tvGender.setText(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("男"));
        been.add(new SelectPopBean("女"));
        genderpPop.setBeans(been);
        genderpPop.setTvType("请选择您的性别：");
        genderpPop.showPop();
    }


    //选择学历pop
    public void showEducationPop() {
        if (null == educationPop) {
            educationPop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((PersonalInfActivity) context).tvEducation.setText(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("专科"));
        been.add(new SelectPopBean("本科"));
        been.add(new SelectPopBean("硕士"));
        been.add(new SelectPopBean("博士"));
        educationPop.setBeans(been);
        educationPop.setTvType("请选择您的学历：");
        educationPop.showPop();
    }


    //输入所在医院
    public void showHospitalNameInpop() {
        if (null == hospitalInputPop) {
            hospitalInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((PersonalInfActivity) context).tvHospital.setText(content);
                }
            };
            hospitalInputPop.setTitle(R.string.input_hospital_name);
        }
        hospitalInputPop.showPop();
    }


    //输入职务级别
    public void showSelectLevelPop() {
        if (null == getDoctorRolePop) {
            getDoctorRolePop = new GetDoctorRolePop(context) {

                @Override
                public void onClickCommit(DoctorRoleBean curDoctorRoleBean) {
                    ((PersonalInfActivity) context).tvLevel.setText(curDoctorRoleBean.role);
//                    ((PersonalInfActivity) context).doctorRole = curDoctorRoleBean.id;
                }
            };
        }
        getDoctorRolePop.setDoctorRoleBeans(doctorRoleBeen);
        getDoctorRolePop.showPop();
    }


    //输入学术职务
    public void showFunctionInppop() {
        if (null == functionInputPop) {
            functionInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((PersonalInfActivity) context).tvDutiy.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            functionInputPop.setTitle("请输入您的学术职务");
        }
        functionInputPop.showPop();
    }


    //输入特长pop
    public void showSpecialtynppop() {
        if (null == specialtyInputPop) {
            specialtyInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((PersonalInfActivity) context).tvSpecialty.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            specialtyInputPop.setTitle("请输入您的专长");
        }
        specialtyInputPop.showPop();
    }
}
