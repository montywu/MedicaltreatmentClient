package com.ygbd198.hellodoctor.ui.live.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.WaitingUser;

import java.util.List;

/**
 * 注释
 *
 * @e-mail mwu@szrlzz.com
 * Created by monty on 2017/8/20.
 */

public class WaitingUserAdapter extends BaseAdapter {
    public List<WaitingUser> waitingUsers;
    private Context mContext;

    public WaitingUserAdapter(Context context, List<WaitingUser> waitingUsers) {
        this.mContext = context;
        this.waitingUsers = waitingUsers;
    }

    @Override
    public int getCount() {
        return waitingUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return waitingUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_waiting_user_item, parent, false);
        TextView tvWaitingUser = (TextView) convertView.findViewById(R.id.tv_waitingUser);
        TextView tvZhiding = (TextView) convertView.findViewById(R.id.tv_zhiding);
        String showName = "";
        if (TextUtils.isEmpty(waitingUsers.get(position).getUserName())) {
            showName = waitingUsers.get(position).getUserPhone() + "";
        } else {
            showName = waitingUsers.get(position).getUserName();
        }
        tvWaitingUser.setText((position + 1) + "." + showName);
        if (position == 0) {
            tvZhiding.setVisibility(View.VISIBLE);
        } else {
            tvZhiding.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void notifyDataSetChanged(List<WaitingUser> waitingUsers) {
        this.waitingUsers = waitingUsers;
        this.notifyDataSetChanged();
    }
}
