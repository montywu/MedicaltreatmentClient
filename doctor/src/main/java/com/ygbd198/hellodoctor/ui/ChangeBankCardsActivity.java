package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.controller.MyBankCardsController;
import com.ygbd198.hellodoctor.util.LoginUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.VerifyCodeEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/4.
 */

public class ChangeBankCardsActivity extends BaseActivity implements View.OnClickListener {


    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_card_num)
    public TextView tvCardNum;
    @BindView(R.id.tv_bank)
    public TextView tvBanck;
    @BindView(R.id.bt_change_card)
    Button btChangeBank;

    @BindView(R.id.tv_phone_num)
    public TextView tvPhonrNum;
    @BindView(R.id.tv_ver_code)
    public VerifyCodeEditText etCode;


    private MyBankCardsController controller;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_change_bank_cards, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);
        etCode.setEditLeftIconRes(0); //去掉左边图标
        initTitle();

        initController();

        setOnClick();

        initCodeView();

    }

    private void initCodeView() {
        this.etCode.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {

            @Override
            public boolean onClick(View v) {
                String telPhone = tvPhonrNum.getText().toString();
                if (!LoginUtil.checkTelPhone(telPhone)) {
                    return false;
                }
                RetrofitHelper.getInstance().createService(MyCenterService.class).getCodeByBindBank(MySelfInfo.getInstance().getToken(), tvPhonrNum.getText().toString().trim()).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                        ToastUtils.showToast(R.string.getverify_success);
                    }

                    @Override
                    public void onFailure(String message) {
                        ToastUtils.showToast(message);
                    }
                });
                return true;
            }
        });
    }

    private void setOnClick() {
        tvBanck.setOnClickListener(this);
        tvPhonrNum.setOnClickListener(this);
        tvCardNum.setOnClickListener(this);
        btChangeBank.setOnClickListener(this);
    }


    private void initController() {
        controller = new MyBankCardsController(this) {
            @Override
            public void changeCardsFailure(String msg) {

            }

            @Override
            public void changeCardsSuccess(MyIntegralBean myIntegralBean) {
                setResult(1);
                finish();
            }
        };
    }

    private void initTitle() {
        titleBar.showCenterText("更换银行卡");
        tvName.setText(MySelfInfo.getInstance().getName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_name:
                controller.showNameInppop();
                break;
            case R.id.tv_card_num:
                controller.showNumInppop();
                break;
            case R.id.tv_bank:
                controller.showBanksPop();
                break;
            case R.id.bt_change_card:
                if (tvCardNum.getText().toString().isEmpty()) {
                    ToastUtils.showToast("银行卡号不能为空");
                    return;
                }
                if (tvBanck.getText().toString().isEmpty()) {
                    ToastUtils.showToast("所属银行不能为空");
                    return;
                }
                if (tvPhonrNum.getText().toString().isEmpty()) {
                    ToastUtils.showToast("银行预留手机号不能为空");
                    return;
                }
                if (etCode.getVerifyCode().isEmpty()) {
                    ToastUtils.showToast("验证码不能为空");
                    return;
                }
                controller.changeCard(tvBanck.getText().toString().trim(), Long.valueOf(tvCardNum.getText().toString().trim()),etCode.getVerifyCode());
                break;
            case R.id.tv_phone_num:
                controller.showPhoneNumPop();
                break;
        }

    }
}
