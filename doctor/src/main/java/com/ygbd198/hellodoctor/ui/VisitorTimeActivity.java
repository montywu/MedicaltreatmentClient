package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.VisitorTimeController;
import com.ygbd198.hellodoctor.util.SerializableMap;
import com.ygbd198.hellodoctor.widget.VisitorTimeView.SetVisitorTimeView;
import com.ygbd198.hellodoctor.widget.VisitorTimeView.TimeBoxEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/4.
 */

public class VisitorTimeActivity extends BaseActivity {


    private VisitorTimeController controller;
    //    private Map<String, String> workTime = new HashMap<>();
    private Map<String, String> workTime = ApplyBeDocActivity.workTime;

    @BindView(R.id.timeview1)
    SetVisitorTimeView timeView1;
    @BindView(R.id.timeview2)
    SetVisitorTimeView timeView2;
    @BindView(R.id.timeview3)
    SetVisitorTimeView timeView3;
    @BindView(R.id.timeview4)
    SetVisitorTimeView timeView4;
    @BindView(R.id.timeview5)
    SetVisitorTimeView timeView5;
    @BindView(R.id.timeview6)
    SetVisitorTimeView timeView6;
    @BindView(R.id.timeview7)
    SetVisitorTimeView timeView7;
    @BindView(R.id.bt_next)
    Button btNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_visitor_time, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        controller = new VisitorTimeController(this);

        initTitle();

        timeView1.setTime(timeChange(workTime.get("1")));
        timeView2.setTime(timeChange(workTime.get("2")));
        timeView3.setTime(timeChange(workTime.get("3")));
        timeView4.setTime(timeChange(workTime.get("4")));
        timeView5.setTime(timeChange(workTime.get("5")));
        timeView6.setTime(timeChange(workTime.get("6")));
        timeView7.setTime(timeChange(workTime.get("7")));

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               backup();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backup();
    }

    private void initTitle() {
        titleBar.showCenterText("坐诊时间", R.drawable.treatmenttime, 0);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backup();
            }
        });
    }


    private void backup() {
        //构造返回结果
        List<SetVisitorTimeView> timeViews = new ArrayList<>();
        timeViews.add(timeView1);
        timeViews.add(timeView2);
        timeViews.add(timeView3);
        timeViews.add(timeView4);
        timeViews.add(timeView5);
        timeViews.add(timeView6);
        timeViews.add(timeView7);
        for (int i = 0; i < timeViews.size(); i++) {
            List<TimeBoxEntity> timeBoxs = timeViews.get(i).timeBoxEntities;
            String tempTime = "";
            for (TimeBoxEntity timeBoxEntity : timeBoxs) {
                if (!"添加时间".equals(timeBoxEntity.time)) {
                    tempTime = tempTime + "," + timeBoxEntity.time;
                }
            }
            workTime.put(String.valueOf(i + 1), tempTime);
        }

        //发送返回结果
        Intent intent = new Intent();
        Bundle bundle = new Bundle();

        SerializableMap tmpmap = new SerializableMap();
        tmpmap.setMap(workTime);

        bundle.putSerializable("workTime", tmpmap);
        intent.putExtras(bundle);
        setResult(10, intent);
        finish();

    }

    //string时间字段转化为List集合方法
    private List<String> timeChange(String timeStr) {
        List<String> stringList = new ArrayList<>();
        if (null != timeStr && !timeStr.isEmpty()) {
            String[] strTemp = timeStr.split(",");
            for (String str : strTemp) {
                if (!str.isEmpty()) {
                    stringList.add(str);
                }
            }
        }
        return stringList;
    }

}
