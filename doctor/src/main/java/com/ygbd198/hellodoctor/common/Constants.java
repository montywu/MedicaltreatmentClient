package com.ygbd198.hellodoctor.common;

/**
 * Created by Administrator on 2017/8/2 0002.
 */

public class Constants {
    public static final String spName = "helloDoctorSp";//sp文件名
    public static final String KEY_USER_NAME = "userName";
    public static final String KEY_PASS_WORD = "passWord";

    public static final int LEVEL_PHYSICIAN = 0;//医师等级
    public static final int LEVEL_SPECIALIST = 1;//专家等级
    public static final int LEVEL_DOCTOR = 2;//博士级别专家等级

    // 随心播
    public static final int SDK_APPID = 1400035448;
    public static final int ACCOUNT_TYPE = 14141;

    /**
     * 角色权限
     */
    public static final String HOST_ROLE = "LiveMaster";
    public static final String VIDEO_MEMBER_ROLE = "LiveGuest";
    public static final String NORMAL_MEMBER_ROLE = "Guest";
}
