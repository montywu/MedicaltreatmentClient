
package com.ygbd198.hellodoctor.modules.contactservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.kelin.client.im.DoctorMessageFragment;
import com.ygbd198.hellodoctor.R;

/**
 * 联系客服界面
 *
 * @author hubing
 * @version [1.0.0.0, 2017-09-24]
 */
public class ContactServiceActivity extends AppCompatActivity {

    public static final String KEY_TITLE_RESID = "im_title_resid";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_service_activity);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fl_service, DoctorMessageFragment.newInstance(intent.getIntExtra(KEY_TITLE_RESID, 0)));
        ft.commit();
    }
}
