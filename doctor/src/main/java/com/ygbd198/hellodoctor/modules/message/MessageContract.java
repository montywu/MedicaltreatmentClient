
package com.ygbd198.hellodoctor.modules.message;

import com.ygbd198.hellodoctor.bean.DoctorMessageEnt;

import java.util.List;

/**
 * 登录界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
public class MessageContract {

    /**
     * 消息界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    public interface IMessagePresenter<V extends IMessageView> {

        /**
         * 获取医生消息列表
         * 
         * @param pageNo 页码
         * @param pageSize 每页显示个数
         */
        void getDoctorMessages(int pageNo, int pageSize);

        /**
         * 释放资源
         */
        void release();

    }

    /**
     * 消息界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    public interface IMessageView {

        /**
         * 获取消息列表成功
         * 
         * @param messageEnts 返回的消息列表
         */
        void onLoadMessagesSuccess(List<DoctorMessageEnt> messageEnts);

        /**
         * 获取消息列表失败
         */
        void onLoadMessagesFailed();

    }

}
