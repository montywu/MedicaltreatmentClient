package com.ygbd198.hellodoctor.bean;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class PatientRecordDetailsBean  {
    /**
     * id : 7
     * doctorId : 105
     * userId : 2
     * userName : null
     * userPic : null
     * treatmentName : 熊猫眼
     * nurseId : 1
     * putTime : 2017-09-05 15:39:49
     * recordPic : null
     * isProgram : true
     * createTime : 2017-08-25 14:30:39
     * endTime : 2017-08-25 15:10:11
     * diets : 2
     * rests : 1
     * taboos : 2
     * recordDrugs : [{"id":10,"diagnosticId":7,"doctorId":105,"userId":2,"drugId":2,"drugType":"美白祛斑综合治疗法","drugName":"皮肤灵","useTime":"餐后","useInfo":"每日2次，每次3粒","useType":1,"price":40,"maxUnit":"瓶","drugstorePrice":0,"effect":"能很好的修复皮肤","count":2,"pic":null,"promptTime":"09:00,18:00","isBuy":null,"buyStatu":null,"buyTime":null,"integral":null,"orderId":null},{"id":11,"diagnosticId":7,"doctorId":105,"userId":2,"drugId":1,"drugType":"美白祛斑综合治疗法","drugName":"滴眼液","useTime":"餐前","useInfo":"每日3次，每次2粒","useType":1,"price":20,"maxUnit":"盒","drugstorePrice":0,"effect":"非常好","count":5,"pic":null,"promptTime":"09:00,18:00","isBuy":null,"buyStatu":null,"buyTime":null,"integral":null,"orderId":null}]
     */

    public int id;
    public int doctorId;
    public int userId;
    public String userName;
    public String userPic;
    public String treatmentName;
    public int nurseId;
    public String putTime;
    public String recordPic;
    public boolean isProgram;
    public String createTime;
    public String endTime;
    public String diets;
    public String rests;
    public String taboos;
    public List<RecordDrugsBean> recordDrugs;

    public static class RecordDrugsBean {
            /**
     * id : 11
     * diagnosticId : 7
     * doctorId : 105
     * userId : 2
     * drugId : 1
     * drugType : 美白祛斑综合治疗法
     * drugName : 滴眼液
     * useTime : 餐前
     * useInfo : 每日3次，每次2粒
     * useType : 1
     * price : 20
     * maxUnit : 盒
     * drugstorePrice : 0
     * effect : 非常好
     * count : 5
     * pic : null
     * promptTime : 09:00,18:00
     * isBuy : null
     * buyStatu : null
     * buyTime : null
     * integral : null
     * orderId : null
     */

    public int id;
    public int diagnosticId;
    public int doctorId;
    public int userId;
    public int drugId;
    public String drugType;
    public String drugName;
    public String useTime;
    public String useInfo;
    public int useType;
    public double price;
    public String maxUnit;
    public double drugstorePrice;
    public String effect;
    public int count;
    public String pic;
    public String promptTime;
    public String isBuy;
    public String buyStatu;
    public String buyTime;
    public String integral;
    public String orderId;

    }

}
