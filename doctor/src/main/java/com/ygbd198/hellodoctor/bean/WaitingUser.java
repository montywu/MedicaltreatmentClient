package com.ygbd198.hellodoctor.bean;

/**
 * 等待队列用户
 * Created by monty on 2017/8/14.
 */

public class WaitingUser {
    private long id;
    /**
     * 医生ID
     */
    private int doctorId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户token
     */
    private String userToken;
    /**
     * 用户手机号
     */
    private long userPhone;
    /**
     * 用户申请连麦时间
     */
    private String applyTime;
    /**
     * 用户开始连麦时间
     */
    private String startTime;
    /**
     * 用户排队的号码
     */
    private String number;
    /**
     * 诊断记录ID
     */
    private int recordId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(long userPhone) {
        this.userPhone = userPhone;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    @Override
    public String toString() {
        return "WaitingUser{" +
                "id=" + id +
                ", doctorId=" + doctorId +
                ", userName='" + userName + '\'' +
                ", userToken='" + userToken + '\'' +
                ", userPhone=" + userPhone +
                ", applyTime='" + applyTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", number='" + number + '\'' +
                ", recordId=" + recordId +
                '}';
    }
}
