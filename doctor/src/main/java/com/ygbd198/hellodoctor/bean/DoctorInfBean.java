package com.ygbd198.hellodoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/27.
 */

public class DoctorInfBean implements Parcelable {
    /**
     * id : 105
     * phone : 13332965193
     * password : 5701117D3416
     * workCertificate : 11111111
     * identityCard : 22222222
     * physicianCertificate : 33333333
     * photo : http://localhost:8080/hello-doctor\photo\dc2363bcd4694207a83b51c894a8befc.jpg
     * c : 男
     * education : 博士
     * good : 皮肤病
     * statu : 1
     * appointmentTime : null
     * integral : 0
     * token : b1502871319994
     * createTime : 2017-08-16 16:15:19
     * sig : null
     * doctorAttach : {"id":105,"token":"b1502871319994","roleId":2,"name":"lyj测试","localHospital":"深圳科技园","job":"主任医师","introduction":"我是一名优秀的医师","sittingPhotos":null,"introduceUrl":null,"treatmentNames":null,"count":100,"sumTime":0,"expiredTime":null,"statu":true,"doctorRole":{"id":2,"role":"专家医生","englishName":"EXPERT","price":"60元/次","priceExplain":"专家医生的价格说明"}}
     */

    public int id;
    public long phone;
    public String password;
    public String workCertificate;
    public String identityCard;
    public String physicianCertificate;
    public String photo;
    public String gender;
    public String education;
    public String good;
    public int statu;
    public String appointmentTime;
    public int integral;
    public String token;
    public String createTime;
    public String sig;
    public String practiceVideoIds;


    public DoctorAttachBean doctorAttach;

    public static class DoctorAttachBean implements Parcelable {
        /**
         * id : 105
         * token : b1502871319994
         * roleId : 2
         * name : lyj测试
         * localHospital : 深圳科技园
         * job : 主任医师
         * introduction : 我是一名优秀的医师
         * sittingPhotos : null
         * introduceUrl : null
         * treatmentNames : null
         * count : 100
         * sumTime : 0
         * expiredTime : null
         * statu : true
         * doctorRole : {"id":2,"role":"专家医生","englishName":"EXPERT","price":"60元/次","priceExplain":"专家医生的价格说明"}
         */

        public int id;
        public String token;
        public int roleId;
        public String name;
        public String localHospital;
        public String job;
        public String introduction;
        public String sittingPhotos;
        public String introduceUrl;
        public String treatmentNames;
        public int count;
        public int sumTime;
        public String expiredTime;
        public boolean statu;
        public DoctorRoleBean doctorRole;

        public static class DoctorRoleBean implements Parcelable {
            /**
             * id : 2
             * role : 专家医生
             * englishName : EXPERT
             * price : 60元/次
             * priceExplain : 专家医生的价格说明
             */

            public int id;
            public String role;
            public String englishName;
            public String price;
            public String priceExplain;

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeString(this.role);
                dest.writeString(this.englishName);
                dest.writeString(this.price);
                dest.writeString(this.priceExplain);
            }

            public DoctorRoleBean() {
            }

            protected DoctorRoleBean(Parcel in) {
                this.id = in.readInt();
                this.role = in.readString();
                this.englishName = in.readString();
                this.price = in.readString();
                this.priceExplain = in.readString();
            }

            public static final Creator<DoctorRoleBean> CREATOR = new Creator<DoctorRoleBean>() {
                @Override
                public DoctorRoleBean createFromParcel(Parcel source) {
                    return new DoctorRoleBean(source);
                }

                @Override
                public DoctorRoleBean[] newArray(int size) {
                    return new DoctorRoleBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.token);
            dest.writeInt(this.roleId);
            dest.writeString(this.name);
            dest.writeString(this.localHospital);
            dest.writeString(this.job);
            dest.writeString(this.introduction);
            dest.writeString(this.sittingPhotos);
            dest.writeString(this.introduceUrl);
            dest.writeString(this.treatmentNames);
            dest.writeInt(this.count);
            dest.writeInt(this.sumTime);
            dest.writeString(this.expiredTime);
            dest.writeByte(this.statu ? (byte) 1 : (byte) 0);
            dest.writeParcelable(this.doctorRole, flags);
        }

        public DoctorAttachBean() {
        }

        protected DoctorAttachBean(Parcel in) {
            this.id = in.readInt();
            this.token = in.readString();
            this.roleId = in.readInt();
            this.name = in.readString();
            this.localHospital = in.readString();
            this.job = in.readString();
            this.introduction = in.readString();
            this.sittingPhotos = in.readString();
            this.introduceUrl = in.readString();
            this.treatmentNames = in.readString();
            this.count = in.readInt();
            this.sumTime = in.readInt();
            this.expiredTime = in.readString();
            this.statu = in.readByte() != 0;
            this.doctorRole = in.readParcelable(DoctorRoleBean.class.getClassLoader());
        }

        public static final Creator<DoctorAttachBean> CREATOR = new Creator<DoctorAttachBean>() {
            @Override
            public DoctorAttachBean createFromParcel(Parcel source) {
                return new DoctorAttachBean(source);
            }

            @Override
            public DoctorAttachBean[] newArray(int size) {
                return new DoctorAttachBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.phone);
        dest.writeString(this.password);
        dest.writeString(this.workCertificate);
        dest.writeString(this.identityCard);
        dest.writeString(this.physicianCertificate);
        dest.writeString(this.photo);
        dest.writeString(this.gender);
        dest.writeString(this.education);
        dest.writeString(this.good);
        dest.writeInt(this.statu);
        dest.writeString(this.appointmentTime);
        dest.writeInt(this.integral);
        dest.writeString(this.token);
        dest.writeString(this.createTime);
        dest.writeString(this.sig);
        dest.writeString(this.practiceVideoIds);
        dest.writeParcelable(this.doctorAttach, flags);
    }

    public DoctorInfBean() {
    }

    protected DoctorInfBean(Parcel in) {
        this.id = in.readInt();
        this.phone = in.readLong();
        this.password = in.readString();
        this.workCertificate = in.readString();
        this.identityCard = in.readString();
        this.physicianCertificate = in.readString();
        this.photo = in.readString();
        this.gender = in.readString();
        this.education = in.readString();
        this.good = in.readString();
        this.statu = in.readInt();
        this.appointmentTime = in.readString();
        this.integral = in.readInt();
        this.token = in.readString();
        this.createTime = in.readString();
        this.sig = in.readString();
        this.practiceVideoIds = in.readString();
        this.doctorAttach = in.readParcelable(DoctorAttachBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<DoctorInfBean> CREATOR = new Parcelable.Creator<DoctorInfBean>() {
        @Override
        public DoctorInfBean createFromParcel(Parcel source) {
            return new DoctorInfBean(source);
        }

        @Override
        public DoctorInfBean[] newArray(int size) {
            return new DoctorInfBean[size];
        }
    };
}
