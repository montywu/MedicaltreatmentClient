package com.ygbd198.hellodoctor.bean;

/**
 * Created by monty on 2017/8/7.
 */

public class MySelfInfo {
    private static MySelfInfo mySelfInfo = new MySelfInfo();
    private String introduceUrl;

    private MySelfInfo(){}
    public static MySelfInfo getInstance(){
        return mySelfInfo;
    }

    private int id;
    private String sig;
    private String token;
    private String name;
    private Long phoneNum;
    private String practiceVideoIds;

    /** 头像 */
    private String photo;

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public int getStatu() {
        return statu;
    }

    public void setStatu(int statu) {
        this.statu = statu;
    }

    private int statu;

    /** 注册时间，格式-->2017-10-17 18:39:10 */
    public String createTime;


    public String getPracticeVideoIds() {
        return practiceVideoIds;
    }

    public void setPracticeVideoIds(String practiceVideoIds) {
        this.practiceVideoIds = practiceVideoIds;
    }


    public Long getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(Long phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setIntroduceUrl(String introduceUrl) {
        this.introduceUrl = introduceUrl;
    }
    public String getIntroduceUrl(){
        return introduceUrl;
    }
}
