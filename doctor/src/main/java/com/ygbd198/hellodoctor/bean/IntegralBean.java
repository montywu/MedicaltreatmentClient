package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class IntegralBean {

    /**
     * 积分实例javabean
     * id : 1
     * doctorId : 105
     * name : 诊断费
     * orderId : null
     * diagnosticId : 4
     * transactionId : null
     * badRecordId : null
     * integral : 1000
     * createTime : 2017-08-24 14:27:47
     */

    public int id;
    public int doctorId;
    public String name;
    public Object orderId;
    public int diagnosticId;
    public Object transactionId;
    public Object badRecordId;
    public int integral;
    public String createTime;
}
