
package com.ygbd198.hellodoctor.bean;

/**
 * 医生端消息界面实体类
 *
 * @author hubing
 * @version [1.0.0.0, 2017-10-22]
 */
public class DoctorMessageEnt {

    /** 消息id */
    public int id;

    /** 医生id */
    public int doctorId;

    /** 消息名称 */
    public String name;

    /** 消息主题 */
    public String title;

    /** 消息详细内容 */
    public String content;

    /** 消息类型 */
    public int type;

    /** 消息产生的时间 */
    public String createTime;

    /** 操作 */
    public String operation;

}
