package com.ygbd198.hellodoctor.bean;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/26.
 */

public class AgreenDataBean {

    public String title;
    public String content;
    public List<AgreenDetailBean> detail;
    public String pic;

    @Override
    public String toString() {
        return "AgreenDataBean{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", detail=" + detail +
                ", pic=" + pic +
                '}';
    }
}
