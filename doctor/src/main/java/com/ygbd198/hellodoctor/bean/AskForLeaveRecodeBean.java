package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class AskForLeaveRecodeBean {
    /**
     * id : 3
     * doctorId : 105
     * type : 病假
     * reason : 感冒
     * startTime : 2017-08-10 14:00:00
     * endTime : 2017-08-11 18:00:00
     * createTime : 2017-08-09 18:47:05
     * statu : true
     * passTime : 2017-08-18 19:38:14
     */

    public int id;
    public int doctorId;
    public String type;
    public String reason;
    public String startTime;
    public String endTime;
    public String createTime;
    public boolean statu;
    public String passTime;
}
