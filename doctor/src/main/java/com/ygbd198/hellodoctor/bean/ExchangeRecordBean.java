package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/28.
 * 兑换记录的javabean
 */

public class ExchangeRecordBean {
    /**
     * id : 367f060f92e8403e9efecbffff2ec0c7
     * doctorId : 105
     * bindBankId : 4
     * afterFour : 6788
     * bankName : 中国银行
     * transactionName : 账户提现
     * transactionType : 1
     * money : 9
     * createTime : 2017-08-24 15:28:58
     */

    public String id;
    public int doctorId;
    public int bindBankId;
    public int afterFour;
    public String bankName;
    public String transactionName;
    public int transactionType;
    public double money;
    public String createTime;
}
