package com.ygbd198.hellodoctor.bean;

/**
 * Created by monty on 2017/8/29.
 */

public class Taboo {
    private int id;
    /******作息、禁忌******/
    private String describe;
    private String programName;
    /******作息、禁忌******/

    /******饮食******/
    private String name;
    private String effect; // 功效
    /******饮食******/

    private String pic;
    private boolean isChecked;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }
}
