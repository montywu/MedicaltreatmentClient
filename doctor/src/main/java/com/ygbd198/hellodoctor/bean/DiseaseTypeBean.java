package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/20.
 */

public class DiseaseTypeBean {
    public DiseaseTypeBean(){

    }
    public DiseaseTypeBean(String typeName, boolean isChecked) {
        this.typeName = typeName;
        this.isChecked = isChecked;
    }

    /**
     * 获取治疗类型的实体类
     * id : 1
     * typeName : 痘痘
     * picUrl : http://123.207.9.75:8080/treamType/8cfb6e79a77f46aaaa887e7e6daa2c10.png
     * createTime : 2017-07-28 16:48:13
     */

    public int id;
    public String typeName;
    public String picUrl;
    public String createTime;

    public boolean isChecked;  // monty增加，我这边有需要，其他人可以不用care,guangji也需要，有修改知会下


    @Override
    public String toString() {
        return "DiseaseTypeBean{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", createTime='" + createTime + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
