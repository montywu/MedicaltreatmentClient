package com.ygbd198.hellodoctor.util;

import android.text.TextUtils;


/**
 * Created by monty on 2017/7/22.
 */

public class LoginUtil {
    /**
     * 校验手机号是否合法
     *
     * @param telPhone
     * @return
     */
    public static boolean checkTelPhone(String telPhone) {
        if (TextUtils.isEmpty(telPhone)) {
            ToastUtils.showToast("请输入手机号");
            return false;
        }
        if (!VerificationUtil.isValidTelNumber(telPhone)) {
            ToastUtils.showToast("请输入正确的手机号");
            return false;
        }
        return true;
    }

    /**
     * 校验验证码是否合法
     *
     * @param verifyCode
     * @return
     */
    public static boolean checkVerifyCode(String verifyCode) {
        if (TextUtils.isEmpty(verifyCode)) {
            ToastUtils.showToast("请输入验证码");
            return false;
        }
        return true;
    }

    /**
     * 校验密码是否合法
     *
     * @param password
     * @return
     */
    public static boolean checkPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            ToastUtils.showToast("请输入密码");
            return false;
        }
        if (password.length() < 6) {
            ToastUtils.showToast("密码长度最少为6位");
            return false;
        }

        return true;
    }



}
