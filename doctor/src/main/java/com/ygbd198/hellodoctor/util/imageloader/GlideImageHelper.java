package com.ygbd198.hellodoctor.util.imageloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.ygbd198.hellodoctor.util.MyLog;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * glide图片加载器 Created by kelin
 */
public class GlideImageHelper {


    private static final String TAG = "GlideImageHelper";


    /**
     * 使用自定义option 加载图片
     *
     * @param mActivity
     * @param imgUrl
     * @param loadding
     * @param resError
     * @param imageView
     */
    public static void displayImage(Activity mActivity, String imgUrl, int loadding,
                             int resError, ImageView imageView) {
        try {
            customOptions(Glide.with(mActivity).load(imgUrl), loadding, resError)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .crossFade(200)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     * 使用自定义option 加载图片
     *
     * @param fragment
     * @param imgUrl
     * @param loadding
     * @param resError
     * @param imageView
     */
    public static void displayImage(Fragment fragment, String imgUrl, int loadding,
                             int resError, ImageView imageView) {
        try {

            customOptions(Glide.with(fragment).load(imgUrl), loadding, resError)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .crossFade(200)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     * 加载图片
     *
     * @param fragment
     * @param imgUrl
     * @param imageView
     */
    public static void displayImage(Fragment fragment, String imgUrl, ImageView imageView) {
        try {
            Glide.with(fragment).load(imgUrl)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .crossFade(200)
                    .fitCenter()
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     * 使用自定义option 加载图片
     *
     * @param fragmentActivity
     * @param imgUrl
     * @param loadding
     * @param resError
     * @param imageView
     */
    public static void displayImage(FragmentActivity fragmentActivity, String imgUrl,
                             int loadding, int resError, ImageView imageView) {
        try {
            customOptions(
                    Glide.with(fragmentActivity).load(imgUrl), loadding, resError)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .crossFade(200)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     *  自定义下载图片
     *
     * @param mContext     上下文
     * @param imgUrl       图片加载地址
     * @param simpleTarget The type of resource that this target will receive.
     */
    public static void loadImageAsBitmap(Context mContext, String imgUrl,
                                  SimpleTarget<Bitmap> simpleTarget) {
        try {
            Glide.with(mContext)
                    .load(imgUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(simpleTarget);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     *  自定义下载图片
     *
     * @param mContext     上下文
     * @param imgUrl       图片加载地址
     * @param simpleTarget The type of resource that this target will receive.
     */
    public static void loadImageAsBitmap(Activity mContext, String imgUrl,
                                  SimpleTarget<Bitmap> simpleTarget) {
        try {
            Glide.with(mContext)
                    .load(imgUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(simpleTarget);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }

    /**
     *  显示图片
     *
     * @param mContext             上下文
     * @param imgUrl               图片加载地址
     * @param loadding             加载中图片
     * @param resError             加载失败图片
     * @param bitmapTransformation 转化图片
     * @param imageView
     */
    public static void showTransformationImage(Context mContext, String imgUrl,
                                        int loadding, int resError,
                                        BitmapTransformation bitmapTransformation, ImageView imageView) {
        try {
            customOptions(Glide.with(mContext)
                    .load(imgUrl), loadding, resError)
                    .transform(bitmapTransformation)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade(200)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
    }


    /**
     *  显示图片
     *
     * @param mContext             上下文
     * @param imgUrl               图片加载地址
     * @param loadding             加载中图片
     * @param resError             加载失败图片
     * @param imageView
     */
    public static void showImage(Context mContext, String imgUrl,
                                               int loadding, int resError,
                                               ImageView imageView) {
        Glide.with(mContext).load(imgUrl)
                .crossFade(200)
                .placeholder(loadding)
                .error(resError)
                .bitmapTransform(new CenterCrop(mContext),new RoundedCornersTransformation(mContext,10,0))
                .into(imageView);
        /*try {
            customOptions(Glide.with(mContext)
                    .load(imgUrl), loadding, resError)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade(200)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }*/
    }

    /**
     * 自定义资源显示图片
     *
     * @param mContext
     * @param resId
     * @param bitmapTransformation
     * @return
     */

    public static BitmapRequestBuilder showTransformation(Context mContext, int resId,
                                                   BitmapTransformation bitmapTransformation) {

        try {
            return Glide.with(mContext)
                    .load(resId).asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(bitmapTransformation);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }
        return null;
    }


    /**
     * 自定义options
     *
     * @param mDrawableRequestBuilder
     * @param resIdError
     * @param resIdLoding
     * @return
     */
    private static DrawableRequestBuilder customOptions(
            DrawableRequestBuilder mDrawableRequestBuilder, int resIdLoding,
            int resIdError) {
        return mDrawableRequestBuilder.error(resIdError)
                .placeholder(resIdLoding).crossFade().fitCenter();
    }


    /**
     * 加载gif图片
     *
     * @param context
     * @param imgUrl
     */
    public static void loadImageAsGif(final Context context, String imgUrl, SimpleTarget<GifDrawable> simpleTarget) {
        Glide.with(context).load(imgUrl).asGif().fitCenter().into(simpleTarget);
    }


    /**
     * 下载图片
     *
     * @param context
     * @param url     图片下载地址
     */
    public static FutureTarget<File> downloadOnly(Context context, String url) {
        try {
            return Glide.with(context)
                    .load(url)
                    .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.e(TAG, "error = e" + e);
        }

        return null;

    }
}
