
package com.kelin.client.im.model;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.im.ChatAdapter;
import com.tencent.TIMCustomElem;
import com.tencent.TIMMessage;
import com.ygbd198.hellodoctor.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * 自定义消息
 */
public class CustomMessage extends Message {

    private String TAG = getClass().getSimpleName();

    private final int TYPE_TYPING = 14;

    /** 患者已支付 */
    private static final String ALREADY_PAID = "alreadyPaid";

    /** 患者未支付 */
    private static final String UN_PAID = "unpaid";

    /** 提示支付消息 */
    public static final String REMINDER_PAY = "reminderPay";

    /** 纯文本消息 */
    public static final String TEXT = "text";

    /** 药方消息 */
    private static final String PRESCRIPTION = "prescription";

    /** 语音消息 */
    private static final String SOUND = "sound";

    /** 药品消息 */
    private static final String DRUGS = "drugs";

    /** 促销活动消息 */
    private static final String ACTIVITY = "activity";

    /** 治疗方案 */
    private static final String TREATMENT_PLAN = "treatmentPlan";

    private Type type;

    private String desc;

    private String data;

    /** 消息类型 */
    private String msgType;

    /** 诊断记录ID */
    private String msgData;

    private String msgDes;

    public CustomMessage(TIMMessage message) {
        this.message = message;
        TIMCustomElem elem = (TIMCustomElem) message.getElement(0);
        parse(elem);
        type = Type.INVALID;
    }

    public CustomMessage(Type type) {
        message = new TIMMessage();
        String data = "";
        JSONObject dataJson = new JSONObject();
        try {
            switch (type) {
                case TYPING:
                    dataJson.put("userAction", TYPE_TYPING);
                    dataJson.put("actionParam", "EIMAMSG_InputStatus_Ing");
                    data = dataJson.toString();
            }
        } catch (JSONException e) {
            Log.e(TAG, "generate json error");
        }
        TIMCustomElem elem = new TIMCustomElem();
        elem.setData(data.getBytes());
        message.addElement(elem);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    private void parse(byte[] data) {
        type = Type.INVALID;
        try {
            String str = new String(data, "UTF-8");
            Log.d(TAG, "customMsg = " + str);
            JSONObject jsonObj = new JSONObject(str);
            int action = jsonObj.getInt("userAction");
            switch (action) {
                case TYPE_TYPING:
                    type = Type.TYPING;
                    this.data = jsonObj.getString("actionParam");
                    if (this.data.equals("EIMAMSG_InputStatus_End")) {
                        type = Type.INVALID;
                    }
                    break;
            }
        } catch (IOException | JSONException e) {
            Log.e(TAG, "parse json error");

        }
    }

    private void parse(TIMCustomElem elem) {
        if (elem == null)
            return;
        try {
            msgDes = elem.getDesc();
            msgType = new String(elem.getExt(), "UTF-8");
            msgData = new String(elem.getData(), "UTF-8");
            Log.d(TAG, "desc = " + msgDes);
            Log.d(TAG, "ext = " + msgType);
            Log.d(TAG, "data = " + msgData);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "parse json error");
        }
    }

    /**
     * 获取诊断id
     * @return 返回诊断id，如果没有，则返回-1
     */
    public int getDiagnosticId() {
        if (!TextUtils.isEmpty(msgData)) {
            try {
                return Integer.parseInt(msgData);
            } catch (NumberFormatException e) {
                Log.e(TAG, "Parse diagnostic id error", e);
            }
        }
        return -1;
    }

    /**
     * 显示消息
     *
     * @param viewHolder 界面样式
     * @param context 显示消息的上下文
     */
    @Override
    public void showMessage(ChatAdapter.ViewHolder viewHolder, final Context context) {
        clearView(viewHolder);

        View customView = null;

        switch (msgType) {
            case ALREADY_PAID:
                customView = View.inflate(context, R.layout.item_custom_message, null);
                ImageView alreadyPaidIv = (ImageView) customView.findViewById(R.id.im_custom_image_item);
                alreadyPaidIv.setImageResource(R.drawable.icon_im_custom_treatment_plan);
                customView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Intent intent = new Intent(context, TherapeuticMainActivity.class);
//                        intent.putExtra(TherapeuticMainActivity.KEY_DIAGNOSTIC_ID, getDiagnosticId());
//                        context.startActivity(intent);
                    }
                });
                break;
            case UN_PAID:

                break;
            case REMINDER_PAY:
                customView = View.inflate(context, R.layout.item_custom_pay_message, null);
                TextView tv_tips = (TextView) customView.findViewById(R.id.tv_tips);
                Button btn_pay = (Button) customView.findViewById(R.id.btn_pay);
                btn_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                break;
            case TEXT:

                break;
            case PRESCRIPTION:

                break;
            case SOUND:

                break;
            case DRUGS:

                break;
            case ACTIVITY:

                break;
            case TREATMENT_PLAN:
                customView = View.inflate(context, R.layout.item_custom_message, null);
                ImageView treatmentPlanIamge = (ImageView) customView.findViewById(R.id.im_custom_image_item);
                treatmentPlanIamge.setImageResource(R.drawable.icon_im_custom_treatment_plan);
                customView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Intent intent = new Intent(context, TherapeuticMainActivity.class);
//                        intent.putExtra(TherapeuticMainActivity.KEY_DIAGNOSTIC_ID, getDiagnosticId());
//                        context.startActivity(intent);
                    }
                });
                break;
        }

        if (customView != null)
            getBubbleView(viewHolder).addView(customView);

        showStatus(viewHolder);
    }

    /**
     * 获取消息摘要
     * 
     * @return
     */
    @Override
    public String getSummary() {
        return null;
    }

    /**
     * 保存消息或消息文件
     */
    @Override
    public void save() {

    }

    public enum Type {
        TYPING, INVALID,
    }
}
