package com.monty.library;

import android.app.Application;

/**
 * Created by monty on 2017/9/13.
 */

public abstract class BaseApp extends Application {
    protected static BaseApp instance;

    private String time;

    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }

    public static BaseApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public abstract void startLoginActivity();


}
