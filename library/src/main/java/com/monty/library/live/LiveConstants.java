package com.monty.library.live;

import com.tencent.livesdk.ILVLiveConstants;

/**
 * 补充信令消息状态码
 * Created by monty on 2017/8/14.
 */

public class LiveConstants extends ILVLiveConstants {
    /** 开始排队，C2C消息 */
    public static final int ILVLIVE_CMD_IN_QUEUE    = 0x800;        // 2048
    /** 取消排队，C2C消息 */
    public static final int ILVLIVE_CMD_OUT_QUEUE   = 0x801;        // 2049
    /** 弹幕消息，C2C消息 */
    public static final int ILVLIVE_CMD_BARRAGE   = 0x802;        // 2050
    /** 医生退出，群组消息 */
    public static final int ILVLIVE_CMD_DOCTOR_OUT   = 0x901;
    public static void main(String[] args){
        System.out.println(ILVLIVE_CMD_IN_QUEUE);
        System.out.println(ILVLIVE_CMD_OUT_QUEUE);
    }
}
