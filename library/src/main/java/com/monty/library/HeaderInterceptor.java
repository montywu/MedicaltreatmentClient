package com.monty.library;

import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Header拦截器，为Okhttp添加统一Header
 *
 * @author monty
 * @date 2017/9/29
 */

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (TextUtils.isEmpty(BaseApp.getInstance().getTime())) {
            return chain.proceed(chain.request());
        }
        Request request = chain.request().newBuilder()
                .addHeader("time", BaseApp.getInstance().getTime())
                .build();
        Log.d("monty","HeaderInterceptor -> headers:"+request.headers().toString());
        return chain.proceed(request);
    }
}
