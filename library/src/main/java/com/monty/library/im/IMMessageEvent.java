package com.monty.library.im;


import android.util.Log;

import com.tencent.TIMManager;
import com.tencent.TIMMessage;
import com.tencent.TIMMessageListener;

import java.util.List;
import java.util.Observable;

/**
 * 消息通知事件，上层界面可以订阅此事件
 */
public class IMMessageEvent extends Observable implements TIMMessageListener {


    private volatile static IMMessageEvent instance;

    private IMMessageEvent() {
        //注册消息监听器
        TIMManager.getInstance().addMessageListener(this);
    }

    public static IMMessageEvent getInstance() {
        if (instance == null) {
            synchronized (IMMessageEvent.class) {
                if (instance == null) {
                    instance = new IMMessageEvent();
                }
            }
        }
        return instance;
    }

    @Override
    public boolean onNewMessages(List<TIMMessage> list) {
        Log.d("monty", "onNewMessages -> " + list.toString());
        for (TIMMessage item : list) {
            setChanged();
            notifyObservers(item);
        }
        return false;
    }

    /**
     * 主动通知新消息
     */
    public void onNewMessage(TIMMessage message) {
        setChanged();
        notifyObservers(message);
    }

    /**
     * 清理消息监听
     */
    public void clear() {
        instance = null;
    }
}
