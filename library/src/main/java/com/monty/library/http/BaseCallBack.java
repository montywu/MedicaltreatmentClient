package com.monty.library.http;


import android.util.Log;
import android.widget.Toast;

import com.monty.library.BaseApp;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by monty on 2017/7/18.
 */

public abstract class BaseCallBack<T extends BaseCallModel> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.d("monty","（成功）[BaseCallBack] request -> "+call.request().method()+" - "+call.request().url());
        if (response.raw().code() == 200) {
            if (response.body().code == 200) {
                Log.d("monty","[BaseCallBack] onResponse -> " +response.body().toString());
                onSuccess(response);
                finish();
            } else if (response.body().code == 0) { // 无效的Token，token已过期，需要重新跳转到登录页面重新登录
                Log.d("monty","[BaseCallBack] startLoginActivity -> " +response.body().toString());
//                onFailure(call,new Throwable(response.body().msg));
                Toast.makeText(BaseApp.getInstance(),"token已过期，请重新登录",Toast.LENGTH_SHORT).show();
                startLoginActivity();
            } else {
                Log.d("monty","[BaseCallBack] == other -> " +response.body().toString());
                onFailure(call,new Throwable(response.body().msg));
            }
        } else {
            try {
                Log.d("monty","[BaseCallBack] raw().code() == other -> " +response.raw().code());
                onFailure(call,new Throwable(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFailure(t.getMessage());

        Log.e("monty","（失败）[BaseCallBack] request -> "+call.request().method()+" - "+call.request().url());
        Log.e("monty","[BaseCallBack] onFailure -> " +t.getMessage());
        finish();
    }

    public abstract void onSuccess(Response<T> response);

    public abstract void onFailure(String message);

    public void finish(){}

    public void startLoginActivity() {
        BaseApp.getInstance().startLoginActivity();
    }
}
