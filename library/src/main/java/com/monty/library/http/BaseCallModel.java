package com.monty.library.http;

/**
 * Created by monty on 2017/7/18.
 */

public class BaseCallModel<T> {
    public int code;
    public String msg;
    public T data;

    @Override
    public String toString() {
        return "BaseCallModel{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
