package com.kelin.client.gui.medicinebox.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.medicinebox.entity.NoPurchasedEntity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 * 未购买列表适配器
 */

public class NoPurchaseListAdapter extends MyBaseAdapter {

    private List<NoPurchasedEntity> noPurchasedEntities;

    public NoPurchaseListAdapter(Context context, List<NoPurchasedEntity> noPurchasedEntities) {
        super(context);
        mData = noPurchasedEntities;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_no_purchase, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(position == 0){
            holder.relTop.setVisibility(View.VISIBLE);
        }else{
            holder.relTop.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.rel_top)
        RelativeLayout relTop;
        @BindView(R.id.imgbt_select_many)
        CheckBox imgBtSelectMany;
//        @BindView(R.id.tv_title)
//        TextView tvTitle;
        @BindView(R.id.imgbt_select_one)
        CheckBox imgBtSelectOne;
        @BindView(R.id.img_goods_photo)
        ImageView imgGoodsPhoto;

        @BindView(R.id.tv_goods_name)
        TextView tvGoodsName;
        @BindView(R.id.tv_dosage)
        TextView tvDosage;
        @BindView(R.id.tv_effect)
        TextView tvEffect;
        @BindView(R.id.tv_price)
        TextView tvPrice;



        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
