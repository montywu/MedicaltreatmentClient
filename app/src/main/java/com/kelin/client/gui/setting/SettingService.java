package com.kelin.client.gui.setting;

import com.kelin.client.gui.login.bean.User;
import com.monty.library.http.BaseCallModel;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Administrator on 2017/8/16 0016.
 */

public interface SettingService {

    /**
     * 修改密码
     */
    @FormUrlEncoded
    @POST("user/updatePassword")
    Call<BaseCallModel<Object>> resetPassword(@Field("token") String token,
                                              @Field("phone") String phone,
                                              @Field("password") String password,
                                              @Field("code") String code);


    /**
     * 获取验证码
     */
    @GET("user/getCodeByUpdate")
    Call<BaseCallModel<Object>> resetPassword(@Query("token") String token,
                                              @Query("phone") String phone);


    /**
     * 密码重置（忘记密码）
     *
     * @param phone
     * @param password
     * @param verifyCode 短信验证码
     * @return
     */
    @FormUrlEncoded
    @POST("login/userForgotPassword")
    Call<BaseCallModel<User>> userForgotPassword(@Field("phone") String phone,
                                                 @Field("password") String password,
                                                 @Field("code") String verifyCode);

}
