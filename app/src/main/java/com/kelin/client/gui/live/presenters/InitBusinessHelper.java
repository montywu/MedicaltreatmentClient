package com.kelin.client.gui.live.presenters;

import android.content.Context;

import com.kelin.client.gui.live.model.MySelfInfo;
import com.monty.library.live.Constants;
import com.monty.library.live.event.MessageEvent;
import com.tencent.TIMManager;
import com.tencent.ilivesdk.ILiveSDK;
import com.tencent.livesdk.ILVLiveConfig;
import com.tencent.livesdk.ILVLiveManager;


/**
 * 初始化
 * 包括imsdk等
 */
public class InitBusinessHelper {
    private static String TAG = "InitBusinessHelper";

    private InitBusinessHelper() {
    }


    /**
     * 初始化App
     */
    public static void initApp(final Context context) {
        //初始化avsdk imsdk
        TIMManager.getInstance().disableBeaconReport();
        MySelfInfo.getInstance().getCache(context);
        // 初始化ILiveSDK
        ILiveSDK.getInstance().initSdk(context, Constants.SDK_APPID, Constants.ACCOUNT_TYPE);

    }

    public static void initLive(){
        // 初始化直播模块
        ILVLiveConfig liveConfig = new ILVLiveConfig();
        liveConfig.setLiveMsgListener(MessageEvent.getInstance());
        ILVLiveManager.getInstance().init(liveConfig);
    }
    public static void clearMsgListener(){
        MessageEvent.getInstance().clear();
    }
}
