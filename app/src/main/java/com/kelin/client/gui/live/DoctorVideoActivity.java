package com.kelin.client.gui.live;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.live.event.JumpToIMFragemtEvent;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.DoctorService;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.pay.AliPayEntity;
import com.kelin.client.gui.pay.DiagnosticInfo;
import com.kelin.client.gui.pay.PayService;
import com.kelin.client.util.DisplayUtil;
import com.kelin.client.util.ToastUtils;
import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.NiceDialog;
import com.othershe.nicedialog.ViewConvertListener;
import com.othershe.nicedialog.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by monty on 2017/8/14.
 * 医生介绍视频页面
 */

public class DoctorVideoActivity extends BaseActivity {
    private Doctor doctor;

    public static void startLiveActivity(Context context, Doctor doctor) {
        Intent intent = new Intent(context, DoctorVideoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("doctor", doctor);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @BindView(R.id.ib_back)
    ImageButton ibBack;
    @BindView(R.id.iv_doctorIcon)
    ImageView ivDoctorIcon;
    @BindView(R.id.doctor_photo)
    ImageView iv_doctorPhoto;
    @BindView(R.id.tv_doctorName)
    TextView tvDoctorName;
    @BindView(R.id.tv_hospital)
    TextView tvHospital;
    @BindView(R.id.tv_level)
    TextView tvLevel;
    @BindView(R.id.ll_doctor_details)
    LinearLayout llDoctorDetails;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.videoView)
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_video);
        ButterKnife.bind(this);
        ibBack.setScaleType(ImageView.ScaleType.FIT_CENTER);
        ibBack.setLayoutParams(new LinearLayout.LayoutParams(DisplayUtil.dp2px(this, 48), DisplayUtil.dp2px(this, 48)));
        ibBack.setImageResource(R.drawable.more_blue);
        doctor = getIntent().getParcelableExtra("doctor");
        Glide.with(this).load(doctor.getSittingPhotos()).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivDoctorIcon) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                ivDoctorIcon.setImageDrawable(circularBitmapDrawable);
            }
        });
        tvDoctorName.setText(doctor.getName());
        tvHospital.setText(doctor.getLocalHospital());
        tvLevel.setText(doctor.getRole());

        initVideoView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        iv_doctorPhoto.setVisibility(View.VISIBLE);
    }

    private void initVideoView() {
//        String path = "http://123.207.9.75:8081/upload-file/introduce_url/2017-10-13/bd587411774f4ca99d8d10f0d09f5801.mp4";
//        final Uri uri = Uri.parse(path);
        if(TextUtils.isEmpty(doctor.getIntroduceUrl())){
            return;
        }

        final Uri uri = Uri.parse(doctor.getIntroduceUrl());

        videoView.setVideoURI(uri);
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video started
                            Log.d("monty","videoView -> video started");
//                            videoView.setBackgroundColor(Color.TRANSPARENT);
                            iv_doctorPhoto.setVisibility(View.GONE);
                            return true;
                        }
                        return false;
                    }
                });

                Log.d("monty","videoView -> OnPrepared");
                mp.start();
                Log.d("monty","videoView -> start");
                mp.setLooping(true);
                Log.d("monty","videoView -> setLooping");
            }
        });
//        videoView.setZOrderOnTop(true);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.d("monty","videoView -> onCompletion");
                videoView.setVideoURI(uri);
                videoView.start();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("monty","videoView -> OnError");
                videoView.setVisibility(View.GONE);
                mp.release();
                return false;
            }
        });
    }

    @OnClick({R.id.ib_back, R.id.ll_doctor_details, R.id.btn_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ib_back:
                onBackPressed();
                break;
            case R.id.ll_doctor_details:
                DoctorDetailsActivity.GotoDoctorDetailsActivity(this, doctor);
                break;
            case R.id.btn_confirm:
                LiveActivity.startLiveActivity(this, doctor, LiveActivity.REQUEST_CODE);
                break;
        }
    }

    private long diagnosticId; // 诊断记录Id

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LiveActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                NiceDialogFactory.createStarDialog("评价", new NiceDialogFactory.OnStarDialogListener() {
                    @Override
                    public void onNegativeClick(BaseNiceDialog baseNiceDialog) {
//                        ToastUtils.showToast("投诉");
                        baseNiceDialog.dismiss();
                        ComplainActivity.GoToComplainActivity(DoctorVideoActivity.this);

//                        getDiagnostic();
                    }

                    @Override
                    public void onPositiveClick(BaseNiceDialog baseNiceDialog, AppCompatRatingBar ratingBar) {
                        int rating = (int) ratingBar.getRating();
                        baseNiceDialog.dismiss();
                        RetrofitHelper.getInstance().createService(DoctorService.class).evaluateSave(MyselfInfo.getLoginUser().getToken(), String.valueOf(rating)).enqueue(new BaseCallBack<BaseCallModel<String>>() {
                            @Override
                            public void onSuccess(Response<BaseCallModel<String>> response) {
                                ToastUtils.showToast("评分成功");
                            }

                            @Override
                            public void onFailure(String message) {
                                ToastUtils.showToast("评分失败");
                            }
                        });
                        getDiagnostic();

                    }
                }).setOutCancel(false).show(getSupportFragmentManager());
            }
        } else if (requestCode == ComplainActivity.REQUEST_CODE) {
                getDiagnostic();
        }

    }

    /**
     * 获取诊断信息
     */
    private void getDiagnostic() {
        showLoadingDialog("");
        RetrofitHelper.getInstance().createService(PayService.class).getDiagnostic(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<DiagnosticInfo>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DiagnosticInfo>> response) {
                closeLoadingDialog();
                DiagnosticInfo diagnosticInfo = response.body().data;
                diagnosticId = diagnosticInfo.getDiagnosticId();
                double price = diagnosticInfo.getPrice();
                if (diagnosticInfo != null && diagnosticInfo.getCoupon() != null && diagnosticInfo.getCoupon().size() > 0) {
                    List<DiagnosticInfo.Coupon> coupon = diagnosticInfo.getCoupon();
                    double money = 0;
                    List<Integer> couponIds = new ArrayList<>();
                    for (int i = 0; i < coupon.size(); i++) {
                        if (money < price) {
                            money += coupon.get(i).getMoney();
                            couponIds.add(coupon.get(i).getId());
                        } else {
                            couponIds.add(coupon.get(i).getId());
                            break;
                        }
                    }
                    final double finalMoney = money;
                    if (money >= price) { // 现金券是否足够支付本次诊金，如果足够支付就弹出使用现金券支付的Dialog，否则就弹出现金支付的Dialog
                        StringBuilder couponIdStrBuilder = new StringBuilder();
                        for (int i : couponIds) {
                            couponIdStrBuilder.append("" + i);
                            couponIdStrBuilder.append(",");
                        }
                        String couponIdStr = couponIdStrBuilder.toString();
                        couponIdStr = couponIdStr.substring(0, couponIdStr.length() - 1);

                        showCouponDialog(finalMoney, couponIdStr);
                    } else {
                        showGotoPayDialog(diagnosticId);
                    }
                } else {
                    showGotoPayDialog(diagnosticId);
                }
            }

            @Override
            public void onFailure(String message) {
                closeLoadingDialog();
                ToastUtils.showToast("请求诊断信息失败");
                showGotoPayDialog(-1);
            }
        });
    }

    private void showCouponDialog(final double finalMoney, final String couponIds) {
        NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setVisibility(View.GONE);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText("使用现金券" + finalMoney + "元\n(使用期限：3个月)");
                        ((Button) viewHolder.getView(R.id.btn_positive)).setText("立即使用");
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                                showLoadingDialog("");
                                RetrofitHelper.getInstance().createService(PayService.class).payDiagnosticByCoupon(MyselfInfo.getLoginUser().getToken(), diagnosticId, couponIds).enqueue(new BaseCallBack<BaseCallModel<AliPayEntity>>() {
                                    @Override
                                    public void onSuccess(Response<BaseCallModel<AliPayEntity>> response) {
                                        closeLoadingDialog();
                                        new AlertDialog.Builder(DoctorVideoActivity.this).setMessage("支付成功,是否前往我的护士查看治疗方案?").setPositiveButton("前往查看", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                DoctorVideoActivity.this.finish();
                                                EventBusUtil.post(new JumpToIMFragemtEvent());
                                            }
                                        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        ToastUtils.showToast("现金券支付失败");
                                        closeLoadingDialog();
                                    }
                                });
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog).show(getSupportFragmentManager());
    }

    private void showGotoPayDialog(final long diagnosticId) {
        /*new AlertDialog.Builder(DoctorVideoActivity.this).setMessage("诊疗已结束,请支付诊金(可用现金券),护士会发送治疗方案给您!").setPositiveButton("前往支付", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DiagnosePayActivity.GotoPayActivity(DoctorVideoActivity.this, diagnosticId);
                dialog.dismiss();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();*/

        NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setVisibility(View.GONE);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setTextColor(ContextCompat.getColor(DoctorVideoActivity.this, R.color.black));
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText("诊疗已结束,请支付诊金(可用现金券),护士会发送治疗方案给您!");
                        ((Button) viewHolder.getView(R.id.btn_positive)).setText("前往支付");
//                        ((Button) viewHolder.getView(R.id.btn_negative)).setText("取消");
                        ((Button) viewHolder.getView(R.id.btn_negative)).setVisibility(View.GONE);
                        ((Button) viewHolder.getView(R.id.btn_positive)).setTextColor(ContextCompat.getColor(DoctorVideoActivity.this, R.color.blue));
                        ((Button) viewHolder.getView(R.id.btn_positive)).setBackgroundResource(R.drawable.dialog_right_btn_white_bg);
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
//                                DiagnosePayActivity.GotoPayActivity(DoctorVideoActivity.this, diagnosticId);
                                finish();
                                EventBusUtil.post(new JumpToIMFragemtEvent());
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog).show(getSupportFragmentManager());
    }
}
