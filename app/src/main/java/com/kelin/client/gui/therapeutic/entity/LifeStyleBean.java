package com.kelin.client.gui.therapeutic.entity;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class LifeStyleBean {
    /**
     * id : 2
     * adminId : null
     * doctorId : 105
     * treatmentName : 脱发
     * programName : 多休息
     * effect : 睡眠充足，能养精神，脱发有很好的帮助
     * describe : 每天至少8小时
     * pic :
     * sideEffect : false
     * applyTime : 2017-08-18 18:55:45
     * statu : null
     * passTime : null
     */

    public int id;
    public String adminId;
    public int doctorId;
    public String treatmentName;
    public String programName;
    public String effect;
    public String describe;
    public String pic;
    public boolean sideEffect;
    public String applyTime;
    public boolean statu;
    public String passTime;
}
