package com.kelin.client.gui.pay;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.common.Constans;
import com.kelin.client.common.IntentConstants;
import com.kelin.client.gui.pay.event.WxEvent;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.monty.library.EventBusUtil;
import com.tsy.sdk.pay.alipay.Alipay;
import com.tsy.sdk.pay.weixin.WXPay;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 药方支付
 */
public class DrugPayActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.ll_alipay)
    LinearLayout llAlipay;
    @BindView(R.id.ll_wechat)
    LinearLayout llWechat;
    @BindView(R.id.btn_pay)
    Button btnPay;
    @BindView(R.id.iv_alipaySelect)
    ImageView ivAlipaySelect;
    @BindView(R.id.iv_wechatSelect)
    ImageView ivWechatSelect;

    /**
     * 优惠券id
     */
    private int couponId;

    /**
     * 支付类型
     */
    private int payType = 0;

    /**
     * 支付价格
     */
    private double price = 0;

    /**
     * 诊断id
     */
    private int diagnosticId;

    /**
     * 微信支付SDK需要的参数
     */
    private String wxParams;

    /**
     * 支付宝支付SDK需要的参数
     */
    private String aliParams;

    /**
     * 支付诊金入口
     *
     * @param context
     * @param couponId     优惠券
     * @param diagnosticId 诊断记录Id
     * @param price        金额
     */
    public static void gotoPayActivity(Context context, int couponId, int diagnosticId, double price) {
        Intent intent = new Intent(context, DrugPayActivity.class);
        intent.putExtra(IntentConstants.KEY_COUPON_ID, couponId);
        intent.putExtra(IntentConstants.KEY_DIAGNOSTIC_ID, diagnosticId);
        intent.putExtra(IntentConstants.KEY_PRICE, price);
        context.startActivity(intent);
    }

    /**
     * 购买药方入口
     *
     * @param context
     * @param wxParams  微信支付SDK需要的参数(json格式)
     *                  {
     *                  "package": "Sign=WXPay",
     *                  "orderId": "91ae477007774d29bc860809967a7339",
     *                  "appid": "wxe953d5573e5fdf32",
     *                  "sign": "Sign=3484DC5D3311DD1BB6BE68FA5FF54B38C38C84F5FBC35C2E33F8221DAFDCE515",
     *                  "partnerid": "1485908052",
     *                  "prepayid": "wx2017091011112913d8ccd0260119495068",
     *                  "noncestr": "8Q6BnoNjlKH6OrBM",
     *                  "timestamp": 1505013072
     *                  }
     * @param aliParams 支付宝支付SDK需要的参数(String字符串 content中的数据)
     * @param price     界面上需要显示的价钱
     */
    public static void gotoPayActivityByDrug(Activity context, String wxParams, String aliParams, double price) {
        Intent intent = new Intent(context, DrugPayActivity.class);
        intent.putExtra(IntentConstants.KEY_WXPAY_PARAMS, wxParams);
        intent.putExtra(IntentConstants.KEY_ALIPAY_PARAMS, aliParams);
        intent.putExtra(IntentConstants.KEY_PRICE, price);
        context.startActivityForResult(intent,1);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drug_pay);
        ButterKnife.bind(this);
        initData();
        initView();
        EventBusUtil.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            couponId = intent.getIntExtra(IntentConstants.KEY_COUPON_ID, 0);
            diagnosticId = intent.getIntExtra(IntentConstants.KEY_DIAGNOSTIC_ID, 0);
            price = intent.getDoubleExtra(IntentConstants.KEY_PRICE, 0);
            wxParams = intent.getStringExtra(IntentConstants.KEY_WXPAY_PARAMS);

            if (!TextUtils.isEmpty(wxParams)) {
                JsonParser parser = new JsonParser();
                JsonObject obj = parser.parse(wxParams).getAsJsonObject();
                wxParams = obj.get("data").toString();
                wxParams = wxParams.replace("packageValue", "package");
            }

            String alipayJson = intent.getStringExtra(IntentConstants.KEY_ALIPAY_PARAMS);
            if (!TextUtils.isEmpty(alipayJson)) {
                JsonParser parser = new JsonParser();
                JsonObject obj = parser.parse(alipayJson).getAsJsonObject();
                JsonObject data = obj.get("data").getAsJsonObject();
                aliParams = data.get("content").getAsString();
            }
        }
    }

    @Override
    protected void initView() {
        this.titleBar.showCenterText("PAYMENT","支付");
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.tvPrice.setText("￥" + price);
        selectPay(0);
    }

    @Subscribe
    public void onWxEvent(WxEvent wxEvent) {
        switch (wxEvent.code) {
            case 0:
                ToastUtils.showToast("支付成功");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(101);
                        finish();
                    }
                }, 100);
                break;
            case -1:
                ToastUtils.showToast("支付失败");
                break;
            case -2:
                ToastUtils.showToast("支付取消");
                break;
            default:
                ToastUtils.showToast("支付失败");
                break;
        }
    }

    public void aliPay() {
        new Alipay(this, aliParams, new Alipay.AlipayResultCallBack() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplication(), "支付成功", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(101);
                        finish();
                    }
                }, 100);
            }

            @Override
            public void onDealing() {
                Toast.makeText(getApplication(), "支付处理中...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int error_code) {
                switch (error_code) {
                    case Alipay.ERROR_RESULT:
                        Toast.makeText(getApplication(), "支付失败:支付结果解析错误", Toast.LENGTH_SHORT).show();
                        break;

                    case Alipay.ERROR_NETWORK:
                        Toast.makeText(getApplication(), "支付失败:网络连接错误", Toast.LENGTH_SHORT).show();
                        break;

                    case Alipay.ERROR_PAY:
                        Toast.makeText(getApplication(), "支付错误:支付码支付失败", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                        Toast.makeText(getApplication(), "支付错误", Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplication(), "支付取消", Toast.LENGTH_SHORT).show();
            }
        }).doPay();
    }

    public void wechatPay() {
        WXPay.init(getApplicationContext(), Constans.APP_ID_WX_PAY);      //要在支付前调用
        WXPay.getInstance().doPay(wxParams, new WXPay.WXPayResultCallBack() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplication(), "支付成功", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(int error_code) {
                switch (error_code) {
                    case WXPay.NO_OR_LOW_WX:
                        Toast.makeText(getApplication(), "未安装微信或微信版本过低", Toast.LENGTH_SHORT).show();
                        break;

                    case WXPay.ERROR_PAY_PARAM:
                        Toast.makeText(getApplication(), "参数错误", Toast.LENGTH_SHORT).show();
                        break;

                    case WXPay.ERROR_PAY:
                        Toast.makeText(getApplication(), "支付失败", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplication(), "支付取消", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick({R.id.ll_alipay, R.id.ll_wechat, R.id.btn_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_alipay:
                selectPay(0);
                break;
            case R.id.ll_wechat:
                selectPay(1);
                break;
            case R.id.btn_pay:
                if (payType == 0) { // 支付宝
                    aliPay();
                } else {
                    wechatPay(); // 微信
                }
                break;
        }
    }

    /**
     * 选择支付方式： 0 - 支付宝；1 - 微信
     *
     * @param type
     */
    private void selectPay(int type) {
        if (type == 0) {
            ivAlipaySelect.setVisibility(View.VISIBLE);
            ivWechatSelect.setVisibility(View.GONE);
        } else {
            ivAlipaySelect.setVisibility(View.GONE);
            ivWechatSelect.setVisibility(View.VISIBLE);
        }
        payType = type;
    }
}
