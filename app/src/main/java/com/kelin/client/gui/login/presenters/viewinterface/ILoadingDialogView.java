package com.kelin.client.gui.login.presenters.viewinterface;

/**
 * Created by monty on 2017/7/31.
 */

public interface ILoadingDialogView {
    void showLoadingDialog(String msg);
    void closeLoadingDialog();
}
