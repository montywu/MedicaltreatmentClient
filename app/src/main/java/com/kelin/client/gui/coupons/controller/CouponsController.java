package com.kelin.client.gui.coupons.controller;

import android.util.Log;

import com.kelin.client.gui.coupons.CouponsService;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public abstract  class CouponsController {

    public abstract void getCouponsListSuccess(List<CouponsEntity> treatmentType);
    public abstract void getCouponsListFailure(String message);

    private final String TAG = getClass().getName();
    public String token;

    public CouponsController(String token){
        this.token = token;
    }

    public void getCouponListData(int flag){
        RetrofitHelper.getInstance().createService(CouponsService.class).getCouponList(token,flag).enqueue(new BaseCallBack<BaseCallModel<List<CouponsEntity>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<CouponsEntity>>> response) {
                List<CouponsEntity> treatmentType = response.body().data;
                Log.d(TAG, "getAddressList - onSuccess -> " + treatmentType.toString());
                getCouponsListSuccess(treatmentType);
            }

            @Override
            public void onFailure(String message) {
                getCouponsListFailure(message);
            }
        });


    }
}
