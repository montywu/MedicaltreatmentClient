package com.kelin.client.gui.mydoctor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.gui.live.DoctorVideoActivity;
import com.kelin.client.gui.mydoctor.bean.BaseItem;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 注释
 *
 * @e-mail mwu@szrlzz.com
 * Created by monty on 2017/8/18.
 */

public class DoctorAdapter3 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DoctorAdapter2";
    private List<BaseItem> list;
    private LayoutInflater inflater;
    private Context context;

    public DoctorAdapter3(Context context, List<BaseItem> list) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder viewType -> " + viewType);
        RecyclerView.ViewHolder viewHolder = null;
        View view;
        switch (viewType) {
            case BaseItem.DOCTOR:
                view = inflater.inflate(R.layout.layout_doctor_item, parent, false);
                viewHolder = new DcotorViewHolder(view);
                break;
            case BaseItem.DOCTOR_LEFT:
                view = inflater.inflate(R.layout.layout_doctor_item_left, parent, false);
                viewHolder = new DcotorViewHolder(view);
                break;
            case BaseItem.DOCTOR_RIGHT:
                view = inflater.inflate(R.layout.layout_doctor_item_right, parent, false);
                viewHolder = new DcotorViewHolder(view);
                break;
            case BaseItem.DOCTOR_CLASSIFY:
                view = inflater.inflate(R.layout.layout_doctor_classify_item, parent, false);
                viewHolder = new DoctorClassifyViewHolder(view);
                break;
            case BaseItem.FOOTER:
                view = inflater.inflate(R.layout.layout_doctor_footer, parent, false);
                viewHolder = new FooterViewHolder(view);
                break;
            case BaseItem.EMPTYLEFT:
                view = inflater.inflate(R.layout.layout_doctor_item_left, parent, false);
                viewHolder = new DcotorViewHolder(view);
                ((ViewGroup) view).removeAllViews();
                break;
            case BaseItem.EMPTYRIGHT:
                view = inflater.inflate(R.layout.layout_doctor_item_right, parent, false);
                viewHolder = new DcotorViewHolder(view);
                ((ViewGroup) view).removeAllViews();
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder position -> " + position);
        if (list.get(position).getType() == BaseItem.DOCTOR) {
            DcotorViewHolder viewHolder = (DcotorViewHolder) holder;
            final Doctor doctor = (Doctor) list.get(position);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DoctorVideoActivity.startLiveActivity(inflater.getContext(), doctor);
                }
            });
            String photo = doctor.getSittingPhotos();
            Glide.with(context).load(TextUtils.isEmpty(photo) ? R.drawable.doctor_photo2 : photo).centerCrop().into(viewHolder.ivDoctorPhoto);
            viewHolder.tvDoctorName.setText(doctor.getName());
            if (doctor.getDoctorTreatments().size() > 0) {
                viewHolder.tvTreatmentTypeName.setText(doctor.getTreatmentNames()[0]);
            }
            viewHolder.tvWaitingCount.setText(Html.fromHtml("<span style=\"color:#ffffff\">" + doctor.getWaitCount() + "</span>人等待"));
            viewHolder.tvWaitingDuration.setText("约" + Html.fromHtml("<span style=\"color:#ffffff\">" + 15 + "</span>") + "分钟");
            viewHolder.tvHistoryCount.setText(doctor.getCount() + "人");
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, doctor, position);
                }
            });

        } else if (list.get(position).getType() == BaseItem.DOCTOR_CLASSIFY) {
            DoctorClassifyViewHolder viewHolder = (DoctorClassifyViewHolder) holder;
            final DoctorClassify doctorClassify = (DoctorClassify) list.get(position);
            viewHolder.tvClassifyIcon.setText(doctorClassify.getEnglishName());
            viewHolder.tvRole.setText(doctorClassify.getRole());
            viewHolder.tvPrice.setText("(￥" + doctorClassify.getPrice() + ")");
            viewHolder.tvMore.setTag(doctorClassify);
            viewHolder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMoreClickListener.onMoreClick(v, doctorClassify, position);
                }
            });

        } /*else if(list.get(position).getType() == BaseItem.DOCTOR_LEFT||list.get(position).getType() == BaseItem.DOCTOR_RIGHT){
            DcotorViewHolder viewHolder = (DcotorViewHolder) holder;
            ((ViewGroup)viewHolder.itemView).removeAllViews();
        }*/
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId -> " + position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType -> " + position);
        BaseItem baseItem = list.get(position);
        if (baseItem instanceof Doctor) {
            Doctor doctor = (Doctor) baseItem;
            if (doctor.isLeft()) {
                return BaseItem.DOCTOR_LEFT;
            }
            if (doctor.isRight()) {
                return BaseItem.DOCTOR_RIGHT;
            }
        }
        return list.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void notifyDataSetChanged(List<BaseItem> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    private OnMoreClickListener onMoreClickListener;
    private OnItemClickListener onItemClickListener;

    public void setOnMoreClickListener(OnMoreClickListener onMoreClickListener) {
        this.onMoreClickListener = onMoreClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnMoreClickListener {
        void onMoreClick(View view, DoctorClassify doctorClassify, int position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Doctor doctor, int position);
    }


    static class DcotorViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_doctorPhoto)
        ImageView ivDoctorPhoto;
        @BindView(R.id.tv_enter_room)
        TextView tvEnterRoom;
        @BindView(R.id.tv_doctorName)
        TextView tvDoctorName;
        @BindView(R.id.tv_treatmentTypeName)
        TextView tvTreatmentTypeName;
        @BindView(R.id.tv_waitingCount)
        TextView tvWaitingCount;
        @BindView(R.id.tv_waitingDuration)
        TextView tvWaitingDuration;
        @BindView(R.id.tv_historyCount)
        TextView tvHistoryCount;

        DcotorViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class DoctorClassifyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_classifyIcon)
        TextView tvClassifyIcon;
        @BindView(R.id.tv_role)
        TextView tvRole;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_more)
        TextView tvMore;

        DoctorClassifyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }


    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static void setPaddings(View v, int l, int t, int r, int b) {
        v.setPadding(l, t, r, b);
        v.requestLayout();
    }
}
