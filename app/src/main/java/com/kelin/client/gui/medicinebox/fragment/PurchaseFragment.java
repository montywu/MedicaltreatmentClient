package com.kelin.client.gui.medicinebox.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.kelin.client.R;
import com.kelin.client.gui.medicinebox.adapter.PurchaseExpListViewAdapter;
import com.kelin.client.gui.medicinebox.controller.PurchaseController;
import com.kelin.client.gui.medicinebox.entity.PurchasedBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class PurchaseFragment extends Fragment {

    private View view;
    private List<String> dataKey;
    private Map<String,PurchasedBean>data;
    private PurchaseExpListViewAdapter adapter;
    private PtrClassicFrameLayout mPtrFrame;
    private ExpandableListView listView;
    private static PurchaseFragment purchaseFragment;

    private PurchaseController controller;

    public static PurchaseFragment createInstance() {
        if(null == purchaseFragment){
            purchaseFragment = new PurchaseFragment();
        }
        return purchaseFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_purchase, null);

        initView();

        initPullRefresh();

        initController();

        initData();

        return view;
    }

    private void initData() {
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initController() {
        controller = new PurchaseController(getActivity()) {
            @Override
            public void getDataSuccess(Map<String, PurchasedBean> map) {
                data.clear();
                dataKey.clear();
                data.putAll(map);
                for(String key:map.keySet()){
                    dataKey.add(key);
                }
                for (int i = 0; i < dataKey.size(); i++) {//默认全部展开
                    listView.expandGroup(i);
                }

                adapter.notifyDataSetChanged();

                mPtrFrame.refreshComplete();

            }

            @Override
            public void getDataFailrue(String msg) {
                mPtrFrame.refreshComplete();
            }
        };
    }

    private void initView() {
        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        listView = (ExpandableListView) view.findViewById(R.id.listview);
        listView.setGroupIndicator(null);
        dataKey = new ArrayList<>();
        data = new HashMap<>();
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {//设置点击不收缩

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,int groupPosition, long id) {
                return true;
            }
        });
        adapter = new PurchaseExpListViewAdapter(getActivity(),dataKey,data);
        listView.setAdapter(adapter);
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                controller.getPurchaseDataType(1);
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);

    }
}
