package com.kelin.client.gui.address.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.address.EditAddressActivity;
import com.kelin.client.gui.address.MyAddressActivity;
import com.kelin.client.gui.address.entity.MyAddressEntity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/9.
 */

public class AddressListAdapter extends MyBaseAdapter implements View.OnClickListener {

    private List<MyAddressEntity> addressEntities;

    private OnDelClickListener listener;
    public interface OnDelClickListener{
        void onDelClick(int addressEntityId);
    }

    public AddressListAdapter(Context context, List<MyAddressEntity> addressEntities,OnDelClickListener listener) {
        super(context);
        mData = addressEntities;
        this.listener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_address, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final MyAddressEntity addressEntity = (MyAddressEntity) mData.get(position);
        if (addressEntity.isDefault) {
            holder.tvDefault.setText("默认地址");
            holder.tvDefault.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reg_checkbox_sel,0,0,0);
        } else {
            holder.tvDefault.setText("设为默认");
            holder.tvDefault.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reg_checkbox_nor,0,0,0);
        }
        holder.tvName.setText(addressEntity.receiveName);
        holder.tvPhoneNum.setText(addressEntity.receivePhone);
        holder.tvAddress.setText(addressEntity.area +"  "+ addressEntity.receiveAddress);
        holder.tvEdit.setOnClickListener(this);
        holder.tvEdit.setTag(position);

        holder.tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onDelClick(addressEntity.id);
                }
            }
        });

        return convertView;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        MyAddressEntity addressEntity = (MyAddressEntity) mData.get(position);
        Intent intent = new Intent();
        intent.putExtra("addressEntity", addressEntity);
        intent.setClass(mContext, EditAddressActivity.class);
        ((MyAddressActivity) mContext).startActivityForResult(intent, 1);

    }

    public void notifyDataSetChanged(List<MyAddressEntity> addressEntities){
        this.addressEntities = addressEntities;
        notifyDataSetChanged();
    }

    class ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_phone_num)
        TextView tvPhoneNum;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.bianji)
        TextView tvEdit;
        @BindView(R.id.tv_default)
        TextView tvDefault;
        @BindView(R.id.tv_del)
        TextView tvDel;

        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
