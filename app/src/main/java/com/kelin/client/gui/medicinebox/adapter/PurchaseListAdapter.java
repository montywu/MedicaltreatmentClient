package com.kelin.client.gui.medicinebox.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.medicinebox.entity.PurchasedEntity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 * 已购买列表适配器
 */

public class PurchaseListAdapter extends MyBaseAdapter {

    List<PurchasedEntity> purchasedEntities;

    public PurchaseListAdapter(Context context, List<PurchasedEntity> purchasedEntities) {
        super(context);
        mData = purchasedEntities;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_purchase, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        PurchasedEntity pillboxListEntity = (PurchasedEntity) mData.get(position);
        holder.relTop.setVisibility(View.GONE);
        holder.relBottom.setVisibility(View.GONE);

        if (position == 0) {
            holder.relTop.setVisibility(View.VISIBLE);
            holder.relBottom.setVisibility(View.GONE);
        }
        if(position == mData.size()-1){
            holder.relTop.setVisibility(View.GONE);
            holder.relBottom.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.rel_top)
        RelativeLayout relTop;
        @BindView(R.id.tv_title)
        TextView tvTutle;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.img_goods_photo)
        ImageView imgGoodsPhoto;
        @BindView(R.id.tv_goods_name)
        TextView tvGoodsName;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;
        @BindView(R.id.rel_bottom)
        RelativeLayout relBottom;
        @BindView(R.id.tv_total)
        TextView tvToal;


        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
