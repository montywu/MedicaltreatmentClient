package com.kelin.client.gui.therapeutic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.therapeutic.entity.LifeStyleBean;
import com.kelin.client.gui.therapeutic.entity.TabooBean;
import com.kelin.client.gui.therapeutic.fragment.TabooFragment;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class TabooAdapter extends MyBaseAdapter {

    public TabooAdapter(Context context, List<TabooBean> data) {
        super(context);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_taboo, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        TabooBean bean = (TabooBean) mData.get(position);
        holder.tvSubjectContent.setText(bean.describe);

        if (position == 0) {
            holder.imgTop.setVisibility(View.VISIBLE);
        } else {
            holder.imgTop.setVisibility(View.GONE);

        }
        if (null != bean.pic && !bean.pic.isEmpty()) {
            holder.imgTaboo.setVisibility(View.VISIBLE);
            GlideImageHelper.showImage(mContext, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgTaboo);
        } else {
            holder.imgTaboo.setVisibility(View.GONE);
        }

        return convertView;
    }

    class Holder {
        @BindView(R.id.tv_subject_content)
        TextView tvSubjectContent;
        @BindView(R.id.img_top)
        ImageView imgTop;
        @BindView(R.id.img_taboo)
        ImageView imgTaboo;
        View itemView;

        Holder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
