package com.kelin.client.gui.login.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.login.LoginService;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.MainActivity;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;
import com.kelin.client.widget.VerifyCodeEditText;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


/**
 * Created by monty on 2017/7/8.
 */
public class RegisterAndBindFragment extends BaseLoadingFragment {
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verifyCodeLayout)
    VerifyCodeEditText etVerifyCodeLayout;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    @BindView(R.id.btn_confirm)
    Button btnRegister;
    @BindView(R.id.cb_agree)
    CheckBox cbAgree;
    @BindView(R.id.tv_privacyPolicy)
    TextView tvPrivacyPolicy;
    Unbinder unbinder;

    private String verifyCode;

    private boolean isRegister;

    private String openId;
    private int flag;
    private String photo;

    public RegisterAndBindFragment() {
    }

    public static RegisterAndBindFragment newInstance(String openId, int flag,String photo) {
        RegisterAndBindFragment fragment = new RegisterAndBindFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isRegister", false);
        bundle.putString("openId", openId);
        bundle.putInt("flag", flag);
        bundle.putString("photo",photo);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static RegisterAndBindFragment newInstance() {
        RegisterAndBindFragment fragment = new RegisterAndBindFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isRegister", true);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isRegister = getArguments().getBoolean("isRegister");
            if (!isRegister) {
                openId = getArguments().getString("openId");
                flag = getArguments().getInt("flag");
                photo = getArguments().getString("photo");
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_register, container, false);

        unbinder = ButterKnife.bind(this, containerView);
        this.titleBar = (BaseTitleBar) containerView.findViewById(R.id.titleBar);
        this.titleBar.setBackBtnVis(true);
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        this.titleBar.showCenterText(isRegister?R.string.bind_phone:R.string.register, 18);
        this.btnRegister.setText(isRegister ? "注册" : "注册并绑定");
//        this.tvPrivacyPolicy.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); // 给TextView添加下划线并添加抗锯齿
        this.etVerifyCodeLayout.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {
            @Override
            public boolean onClick(View v) {
                String telPhone = etPhone.getText().toString();
                if (!MyselfInfo.checkTelPhone(telPhone)) {
                    return false;
                }
                RetrofitHelper.getInstance().createService(LoginService.class).getVerifyCode(telPhone).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                        Log.d("monty", "response->" + response.body().data);
                        ToastUtils.showToast(R.string.getverify_success);
//                        verifyCode = response.body().data;
                    }

                    @Override
                    public void onFailure(String message) {
                        ToastUtils.showToast(message);
                        Log.d("monty", "Throwable->" + message);
                    }
                });
                return true;
            }

        });
        this.cbAgree.setChecked(true);
        return containerView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_confirm, R.id.tv_privacyPolicy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                String telPhone = etPhone.getText().toString();
                if (!MyselfInfo.checkTelPhone(telPhone)) return;

                if (!MyselfInfo.checkVerifyCode(etVerifyCodeLayout.getVerifyCode())) return;

                if (!MyselfInfo.checkPassword(etPwdLayout.getPassword())) return;

                if(!cbAgree.isChecked()){
                    ToastUtils.showToast("请先同意用户许可协议和隐私条款");
                    return;
                }

                if (isRegister) {
                    register(telPhone, etPwdLayout.getPassword(), etVerifyCodeLayout.getVerifyCode());
                } else {
                    userBind(telPhone, etPwdLayout.getPassword(), etVerifyCodeLayout.getVerifyCode(), openId, flag, photo);
                }

                break;
            case R.id.tv_privacyPolicy:
//                Toast.makeText(getContext(), "隐私条款", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void userBind(String telPhone, String password, String verifyCode, String openId, int flag, String photo) {
        showLoadingDialog("正在绑定账号");
        RetrofitHelper.getInstance().createService(LoginService.class).userBind(telPhone, password, verifyCode, openId, flag, photo).enqueue(new BaseCallBack<BaseCallModel<User>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {
                User userReg = response.body().data;
                MyselfInfo.saveLoginUser(userReg);
                MainActivity.startMainActivity(getActivity());

                Log.d("monty", "userBind - onResponse -> " + userReg.toString());
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                Log.d("monty", "userBind - onFailure -> " + message);
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });

    }

    private void register(String telPhone, String password, String verifyCode) {
        showLoadingDialog("正在注册账号");
        RetrofitHelper.getInstance().createService(LoginService.class).register(telPhone, password, verifyCode).enqueue(new BaseCallBack<BaseCallModel<User>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {
                User data = response.body().data;
                MyselfInfo.saveLoginUser(data);
                MainActivity.startMainActivity(getActivity());
//                ToastUtils.showToast("注册成功 -> " + data.toString());
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("注册失败 -> " + message);
                closeLoadingDialog();
            }
        });

    }


}
