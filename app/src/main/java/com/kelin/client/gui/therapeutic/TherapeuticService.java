package com.kelin.client.gui.therapeutic;

import com.kelin.client.gui.therapeutic.entity.FootBean;
import com.kelin.client.gui.therapeutic.entity.LifeStyleBean;
import com.kelin.client.gui.therapeutic.entity.PrescribeBean;
import com.kelin.client.gui.therapeutic.entity.ReCheckBean;
import com.kelin.client.gui.therapeutic.entity.TabooBean;
import com.monty.library.http.BaseCallModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/9/7.
 */

public interface TherapeuticService {


    /**
     * 获取药方列表
     * */
    @GET("record/getDrug")
    Call<BaseCallModel<Map<String,PrescribeBean>>> getDrug(@Query("token") String token,@Query("diagnosticId") long diagnosticId);

    /**
     * 获取复诊
     * */
    @GET("record/getRevisit")
    Call<BaseCallModel<ReCheckBean>> getRevisit(@Query("token") String token, @Query("diagnosticId") long diagnosticId);

    /**
     * 获取菜谱列表
     * */
    @GET("record/getDiet")
    Call<BaseCallModel<List<FootBean>>> getDiet(@Query("token") String token, @Query("diagnosticId") long diagnosticId);

    /**
     * 获取生活习惯列表
     * */
    @GET("record/getRest")
    Call<BaseCallModel<List<LifeStyleBean>>> getRest(@Query("token") String token, @Query("diagnosticId") long diagnosticId);

    /**
     * 获取禁忌列表
     * */
    @GET("record/getTaboo")
    Call<BaseCallModel<List<TabooBean>>> getTaboo(@Query("token") String token, @Query("diagnosticId") long diagnosticId);

}
