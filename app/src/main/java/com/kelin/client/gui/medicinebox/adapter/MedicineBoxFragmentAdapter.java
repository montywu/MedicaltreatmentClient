package com.kelin.client.gui.medicinebox.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.kelin.client.gui.medicinebox.fragment.NoPurchaseFragment;
import com.kelin.client.gui.medicinebox.fragment.PurchaseFragment;
import com.kelin.client.common.MyFragmentPagerAdapter;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class MedicineBoxFragmentAdapter extends MyFragmentPagerAdapter {


    private int fragmentCuont = 2;

    public MedicineBoxFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PurchaseFragment.createInstance();

            case 1:
                return NoPurchaseFragment.createInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentCuont;
    }
}
