package com.kelin.client.gui.live;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kd.easybarrage.Barrage;
import com.kd.easybarrage.BarrageView;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.live.adapter.NotificationListAdapter;
import com.kelin.client.gui.live.model.UpMenber;
import com.kelin.client.gui.live.presenters.LivePresenter;
import com.kelin.client.gui.live.presenters.viewinterface.ILiveView;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.util.DisplayUtil;
import com.kelin.client.util.SoundManager;
import com.kelin.client.util.ToastUtils;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.tencent.av.sdk.AVView;
import com.tencent.ilivesdk.ILiveConstants;
import com.tencent.ilivesdk.core.ILiveRoomManager;
import com.tencent.ilivesdk.view.AVRootView;
import com.tencent.ilivesdk.view.AVVideoView;
import com.tencent.ilivesdk.view.VideoListener;
import com.tencent.livesdk.ILVLiveManager;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;
import com.yanzhenjie.permission.Rationale;
import com.yanzhenjie.permission.RationaleListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 患者视频
 */
public class LiveActivity extends BaseActivity implements ILiveView {
    public static int REQUEST_CODE = 0x1102;
    public static final String TAG = LiveActivity.class.getSimpleName() + " - monty";
    @BindView(R.id.barrageView)
    BarrageView barrageView;
    @BindView(R.id.tv_coverView)
    TextView tvCoverView;

    private boolean isQueuing = false; // 记录用户是否已经在排队了


    public static void startLiveActivity(Activity context, Doctor doctor, int requestCode) {
        Intent intent = new Intent(context, LiveActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("doctor", doctor);
        intent.putExtras(bundle);
        context.startActivityForResult(intent, requestCode);
    }

    @BindView(R.id.av_root_view)
    AVRootView avRootView;
    @BindView(R.id.ib_back)
    ImageButton ibBack;
    @BindView(R.id.iv_doctorIcon)
    ImageView ivDoctorIcon;
    @BindView(R.id.tv_doctorName)
    TextView tvDoctorName;
    @BindView(R.id.tv_hospital)
    TextView tvHospital;
    @BindView(R.id.tv_level)
    TextView tvLevel;
    @BindView(R.id.ll_doctor_details)
    LinearLayout llDoctorDetails;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.lv_noticeList)
    ListView lvNoticeList;

    private Doctor doctor;

    private LivePresenter livePresenter;

    private NotificationListAdapter notificationListAdapter;

    private List<String> noticeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  // 保持屏幕常亮
        ButterKnife.bind(this);

        doctor = getIntent().getParcelableExtra("doctor");
        initView();
        noticeList.add("您可以先看别人诊断，别人是看不见你的");
        livePresenter = new LivePresenter(this, doctor);
        livePresenter.joinRoom();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ILVLiveManager.getInstance().onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ILVLiveManager.getInstance().onResume();
    }

    @Override
    protected void onDestroy() {
        barrageView.destroy();

        if (livePresenter != null) {
            livePresenter.onDestory();
        }
        mHandler = null;
        super.onDestroy();
    }


    @Override
    protected void initView() {
        Glide.with(this).load(doctor.getSittingPhotos()).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivDoctorIcon) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                ivDoctorIcon.setImageDrawable(circularBitmapDrawable);
            }
        });
        ibBack.setScaleType(ImageView.ScaleType.FIT_CENTER);
        ibBack.setLayoutParams(new LinearLayout.LayoutParams(DisplayUtil.dp2px(this, 48), DisplayUtil.dp2px(this, 48)));
        ibBack.setImageResource(R.drawable.more_blue);
        tvDoctorName.setText(doctor.getName());
        tvHospital.setText(doctor.getLocalHospital());
        tvLevel.setText(doctor.getRole());

        notificationListAdapter = new NotificationListAdapter(this, noticeList);
        lvNoticeList.setAdapter(notificationListAdapter);

        //设置渲染层
        ILVLiveManager.getInstance().setAvVideoView(avRootView);
        initAVRootView();

    }

    private void initAVRootView() {
        avRootView.setAutoOrientation(false);
        avRootView.setGravity(AVRootView.LAYOUT_GRAVITY_RIGHT);
        avRootView.setSubMarginY(getResources().getDimensionPixelSize(R.dimen.small_area_margin_top));
        avRootView.setSubMarginX(getResources().getDimensionPixelSize(R.dimen.small_area_marginright));
//        avRootView.setSubPadding(getResources().getDimensionPixelSize(R.dimen.small_area_marginbetween));
        avRootView.setSubWidth(getResources().getDimensionPixelSize(R.dimen.small_area_width));
        avRootView.setSubHeight(getResources().getDimensionPixelSize(R.dimen.small_area_height));
        avRootView.setSubCreatedListener(new AVRootView.onSubViewCreatedListener() {
            @Override
            public void onSubViewCreated() {
                for (int i = 1; i < ILiveConstants.MAX_AV_VIDEO_NUM; i++) {
                    final int index = i;
                    AVVideoView avVideoView = avRootView.getViewByIndex(index);
                    avVideoView.setRotate(false);
                    // 点击交换视频
                    avVideoView.setGestureListener(new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            avRootView.swapVideoView(0, index);
//                            backGroundId = avRootView.getViewByIndex(0).getIdentifier();
                            return super.onSingleTapConfirmed(e);
                        }
                    });
                }
                avRootView.getViewByIndex(0).setRotate(false);
                avRootView.getViewByIndex(0).setVideoListener(new VideoListener() {
                    @Override
                    public void onFirstFrameRecved(int width, int height, int angle, String identifier) {

                    }

                    @Override
                    public void onHasVideo(String identifier, int srcType) {
                        ILiveRoomManager.getInstance().enableBeauty(6); // 美颜 1-7
                        ILiveRoomManager.getInstance().enableWhite(7); // 美白 1-9
                    }

                    @Override
                    public void onNoVideo(String identifier, int srcType) {

                    }
                });
            }
        });
    }

    @OnClick(R.id.ib_back)
    public void onIbBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.ll_doctor_details)
    public void onLlDoctorDetailsClicked() {
        // TODO: 2017/8/2 跳转到医生详情页面
        DoctorDetailsActivity.GotoDoctorDetailsActivity(this, doctor);
    }

    @OnClick(R.id.btn_confirm)
    public void onBtnConfirmClicked() {
        if (isQueuing) { // 如果已经排队了
            /*btnConfirm.setTextColor(ContextCompat.getColor(this, R.color.blue));
            btnConfirm.setBackgroundResource(R.drawable.white_cir_bg);*/
            btnConfirm.setBackgroundResource(R.drawable.icon_pd_grey);
            ToastUtils.showToast("到您麦序了自动呼叫您");
        } else {
            AndPermission.with(this).requestCode(0x110).permission(new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}).callback(new PermissionListener() {
                @Override
                public void onSucceed(int requestCode, @NonNull List<String> grantPermissions) {
                    showLoadingDialog("正在进行排队");
                    btnConfirm.setClickable(false);
                    btnConfirm.setAlpha(0.8f);
                    livePresenter.applyConnect();
                }

                @Override
                public void onFailed(int requestCode, @NonNull List<String> deniedPermissions) {

                }
            }).rationale(new RationaleListener() {
                @Override
                public void showRequestPermissionRationale(int requestCode, final Rationale rationale) {
                    AndPermission.rationaleDialog(LiveActivity.this, rationale).show();
                }
            }).start();

        }
    }

    @Override
    public void applyConnected(UpMenber upMenber) {
        closeLoadingDialog();
        btnConfirm.setClickable(true);
        btnConfirm.setAlpha(1f);
        isQueuing = upMenber != null;
        if (isQueuing) {
//            btnConfirm.setText("到了呼叫我");
            btnConfirm.setBackgroundResource(R.drawable.icon_pd_grey);
            notifyNoticeList(String.format("您是%1$s号，前面还有%2$s位，等待时间约%3$s分钟，排到号了医生会呼叫您的。", upMenber.number, upMenber.waitCount, upMenber.waitCount * 5));
            /*notifyNoticeList("旁观提示：您可以先看别人诊断，别人是看见不您的！");
            if (upMenber.waitCount <= 1) {
                notifyNoticeList("马上就轮到您了！");
            }*/
        } else {
           /* NiceDialogFactory.createComfirmDialog("排队失败", "您已经在其他医生的队列中，是否要取消之前的排队？", new NiceDialogFactory.OnPositiveClickListener() {
                @Override
                public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                    // TODO: 2017/9/6 退出队列
                    livePresenter.cancelApplyConnect();
                    baseNiceDialog.dismiss();
                }
            }).show(getSupportFragmentManager());*/
        }
    }

    @Override
    public void enterRoomComplete(boolean succ) {
        if (succ) {
            Log.d(TAG, "enterRoomComplete - success" + succ);
        } else {
            ToastUtils.showToast("连接房间失败,请退出后重新进入");
        }
    }

    @Override
    public void quiteRoomComplete(boolean succ, Object liveinfo) {
        // 目前不需要做处理
    }

    @Override
    public void upToVideoMemberComplete(boolean succ) {
        if (succ) {
            isQueuing = false;
            Log.d(TAG, "上麦成功");
            btnConfirm.setVisibility(View.INVISIBLE);
            notifyNoticeList(doctor.getName() + " 医生为您诊疗！");

        } else {
            ToastUtils.showToast("连麦失败，请重新尝试排队连麦");
        }
    }

    private void notifyNoticeList(String string) {
        noticeList.add(string);
        notificationListAdapter.notifyDataSetChanged();
        lvNoticeList.setSelection(noticeList.size() - 1);
    }

    @Override
    public void downToNorMemberComplete(boolean succ) {
        btnConfirm.setVisibility(View.VISIBLE);
//        btnConfirm.setText("开始排队");
        btnConfirm.setBackgroundResource(R.drawable.icon_pd_blue);
        isQueuing = false;
        setResult(RESULT_OK);
        finish();
    }

    private Handler mHandler = new Handler();
    private BaseNiceDialog dialog;
    private SoundManager soundManager;

    @Override
    public void doctorCalled() {
        if (dialog != null) {
            dialog.dismiss();
        }
        if (soundManager == null) {
            soundManager = new SoundManager();
        }
        dialog = NiceDialogFactory.createTipDialog("", doctor.getName() + " 医生正在呼叫您", new NiceDialogFactory.OnPositiveClickListener() {
            @Override
            public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                livePresenter.upToVideoMember();
                baseNiceDialog.dismiss();
                soundManager.stopRingtoneAndVibrator();
            }
        }).setOutCancel(false);

        dialog.show(getSupportFragmentManager());
        soundManager.playRingtoneAndVibrator();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if(soundManager!=null){
                    soundManager.stopRingtoneAndVibrator();
                }
            }
        }, 30 * 1000);

//        CallActivity.goToActivityForResult(this, 1, doctor);
    }

    @Override
    public void addBarrage(String content) {
        if (barrageView != null) {
            List<Barrage> barrages = new ArrayList<>();
            barrages.add(new Barrage(content, R.color.white));
            barrageView.setBarrages(barrages);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                livePresenter.upToVideoMember();
            } else if (resultCode == RESULT_CANCELED) {
                ToastUtils.showToast("如果连续拒绝该医生2次呼叫，系统将自动从队列中将您移除");
            }
        }
    }

    public void goToAppDetailSettingIntent(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        context.startActivity(localIntent);
    }

    @Override
    public void onException(int exceptionId, int errCode, String errMsg) {
        if (ILiveConstants.EXCEPTION_ENABLE_CAMERA_FAILED == exceptionId || ILiveConstants.EXCEPTION_ENABLE_MIC_FAILED == exceptionId) {
            new AlertDialog.Builder(this)
                    .setMessage("请打开\"相机\"或\"麦克风\"权限设置")
                    .setPositiveButton("前往", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goToAppDetailSettingIntent(LiveActivity.this);
                        }
                    })
                    .show();
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("医生网络异常，请稍候")
                    .setPositiveButton("退出", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("继续等待", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setCancelable(false)
                    .show();
        }
    }

    @Override
    public void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg) {

    }

    @Override
    public void onRoomDisconnect(int errCode, String errMsg) {
        new AlertDialog.Builder(this)
                .setMessage("医生异常退出房间，请稍等")
                .setPositiveButton("退出", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("继续等待", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false)
                .show();
    }

    @Override
    public void coverGuestView(boolean b) {
        tvCoverView.setVisibility(b ? View.VISIBLE : View.GONE);
        avRootView.setOnClickListener(null);
        tvCoverView.setOnClickListener(null);
    }
}
