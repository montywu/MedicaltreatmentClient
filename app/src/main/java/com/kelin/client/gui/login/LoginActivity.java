package com.kelin.client.gui.login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.bean.DistributeFragment;
import com.kelin.client.gui.login.fragment.LoginFragment;
import com.kelin.client.gui.login.fragment.ModifyPwdFragment;
import com.kelin.client.gui.login.fragment.RegisterAndBindFragment;
import com.kelin.client.widget.dialog.LoadingDialog;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by monty on 2017/7/8.
 */
public class LoginActivity extends BaseActivity implements LoginFragment.OnFragmentInteractionListener, ModifyPwdFragment.OnModifyPwdListener {
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    private FragmentManager fragmentManager = null;
    private FragmentTransaction fragmentTransaction = null;

    private static final int FRAGMENT_REGISTER_FLAG = 0x01;
    private static final int FRAGMENT_LOGIN_FLAG = 0x02;
    private static final int FRAGMENT_MODIFYPWD_FLAG = 0x03;
    private static final int FRAGMENT_BINDPHONENUM_FLAG = 0x04;

    private LoadingDialog dialog;
    private String phone = "";
    private String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        phone = getIntent().getStringExtra("phone");
        password = getIntent().getStringExtra("pas");

        fragmentManager = getSupportFragmentManager();

        startFragment(new DistributeFragment(FRAGMENT_LOGIN_FLAG));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.close();
            dialog = null;
        }
    }


    private void startFragment(DistributeFragment distributeFragment) {
        Fragment fragment = null;
        switch (distributeFragment.flag) {
            case FRAGMENT_LOGIN_FLAG:
                if (phone != null && !phone.isEmpty()) {
                    fragment = LoginFragment.newInstance(phone, password);
                } else {
                    fragment = LoginFragment.newInstance("", "");
                }
                break;
            case FRAGMENT_REGISTER_FLAG:
                fragment = RegisterAndBindFragment.newInstance();
                break;
            case FRAGMENT_MODIFYPWD_FLAG:
                fragment = ModifyPwdFragment.newInstance(ModifyPwdFragment.OPERATE_FORGOT);
                break;
            case FRAGMENT_BINDPHONENUM_FLAG:
                String openId = distributeFragment.bundle.getString("openId");
                int flag = distributeFragment.bundle.getInt("flag");
                String photo = distributeFragment.bundle.getString("photo");
                fragment = RegisterAndBindFragment.newInstance(openId,flag,photo);
                break;
            default:
                new IllegalArgumentException("参数错误");
                break;
        }
        fragmentTransaction = fragmentManager.beginTransaction();
        if (distributeFragment.flag != FRAGMENT_LOGIN_FLAG) {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void startRegisterFragment() {
        startFragment(new DistributeFragment(FRAGMENT_REGISTER_FLAG));
    }

    @Override
    public void startModifyPwdFragment() {
        startFragment(new DistributeFragment(FRAGMENT_MODIFYPWD_FLAG));
    }

    @Override
    public void startBindPhoneNumFragment(String openId,int flag,String photo) {
        Bundle bundle = new Bundle();
        bundle.putString("openId", openId);
        bundle.putInt("flag",flag);
        bundle.putString("photo",photo);
        startFragment(new DistributeFragment(FRAGMENT_BINDPHONENUM_FLAG, bundle));
    }

    @Override
    public void startLiginFragment() {
        startFragment(new DistributeFragment(FRAGMENT_LOGIN_FLAG));
    }
}

