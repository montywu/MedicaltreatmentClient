package com.kelin.client.gui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hellodoctor.facerecord.modules.facerecord.FaceRecordFragment;
import com.hellodoctor.facerecord.modules.favorite.FavoriteActivity;
import com.hellodoctor.facerecord.modules.history.HistoryActivity;
import com.hellodoctor.facerecord.modules.myrecord.MyRecordActivity;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.OnMenuToggleListener;
import com.kelin.client.gui.address.MyAddressActivity;
import com.kelin.client.gui.coupons.CouponsActivity;
import com.kelin.client.gui.live.event.JumpToIMFragemtEvent;
import com.kelin.client.gui.login.LoginActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.fragment.MedicalRecordActivity;
import com.kelin.client.gui.medicinebox.MyMedicineBoxActivity;
import com.kelin.client.gui.mydoctor.MyDoctorFragment;
import com.kelin.client.gui.personalcenter.PersonalCenterActivity;
import com.kelin.client.gui.setting.SettingActivity;
import com.kelin.client.im.MyNurseFragment;
import com.kelin.client.slidingmenu.lib.SlidingMenu;
import com.kelin.client.util.imageloader.GlideImageHelper;
import com.kelin.client.widget.MainBottomBar;
import com.monty.library.AppManager;
import com.monty.library.EventBusUtil;

import org.greenrobot.eventbus.Subscribe;

/**
 * 主界面
 */
public class MainActivity extends BaseActivity implements OnMenuToggleListener,MainBottomBar.onTabSelectedListener {
    private long lastClickTime;
    private MainBottomBar mainBottomBar;
    private Fragment currentFragment;
    private SlidingMenu menu;
    private TextView loginBtn,userName;//登录or退出登录
    private ImageView headImg;//用户头像
    public static void startMainActivity(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
//        return !linstener.dispatchTouchEvents(ev);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBusUtil.register(this);
        //SlidingMenu配置
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.slide_menu_layout);
        initView();
//        int[] sdkver = TXLivePusher.getSDKVersion();
//        if (sdkver != null && sdkver.length >= 3) {
//            MyLog.d("rtmpsdk", "rtmp sdk version is:" + sdkver[0] + "." + sdkver[1] + "." + sdkver[2]);
//        }

    }

    @Subscribe
    public void OnJumpToIMFragmentEvent(JumpToIMFragemtEvent event){
        mainBottomBar.setTab(1);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - lastClickTime < 2000) {
            super.onBackPressed();
            AppManager.getAppManager().exitApp(this);
        } else {
            lastClickTime = System.currentTimeMillis();
            Toast.makeText(this, "再按一次退出Hello医生", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void initView() {
        mainBottomBar = (MainBottomBar) findViewById(R.id.mainBottomBar);
        mainBottomBar.setOnTabSelectedListener(this);
        mainBottomBar.setFistTab();
        loginBtn= (TextView) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
        userName= (TextView) findViewById(R.id.userName);
        headImg= (ImageView) findViewById(R.id.headImg);
        findViewById(R.id.topLayout).setOnClickListener(this);
        findViewById(R.id.myMedicalkitLayout).setOnClickListener(this);
        findViewById(R.id.myCouponLayout).setOnClickListener(this);
        findViewById(R.id.myRecordLayout).setOnClickListener(this);
        findViewById(R.id.myAddressLayout).setOnClickListener(this);
        findViewById(R.id.settingLayout).setOnClickListener(this);
        findViewById(R.id.fl_my_record_video).setOnClickListener(this);
        findViewById(R.id.tv_menu_favorite).setOnClickListener(this);
        findViewById(R.id.tv_menu_history).setOnClickListener(this);

        //登录成功设置名字 头像
        userName.setText(MyselfInfo.getLoginUser().getName());
        GlideImageHelper.showImage(this, MyselfInfo.getLoginUser().getPhoto(), R.drawable.ic_launcher , R.drawable.ic_launcher, headImg);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isFinishing()&&userName!=null){
            userName.setText(MyselfInfo.getLoginUser().getName());
        }
        GlideImageHelper.showImage(this, MyselfInfo.getLoginUser().getPhoto(), R.drawable.ic_launcher , R.drawable.ic_launcher, headImg);
    }

    @Override
    public void onTagSelected(int tabId, String tabName) {
        setFragment(tabId, tabName);
    }

    private void setFragment(int tabId, String tabName) {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        if (currentFragment != null && currentFragment.isAdded() && currentFragment.isVisible()) {
            fragmentTransaction.hide(currentFragment);
        }
        Fragment fragment = supportFragmentManager.findFragmentByTag(tabName);
        if (fragment != null) {
            currentFragment = fragment;
            fragmentTransaction.show(fragment);
        } else {
            Fragment fragmentInstance = createFragmentByTabId(tabId);
            currentFragment = fragmentInstance;
            fragmentTransaction.add(R.id.mainContent, fragmentInstance, tabName);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * 创建tab fragment
     *
     * @param tabId
     * @return
     */
    private Fragment createFragmentByTabId(int tabId) {
        Fragment fragment;
        switch (tabId) {
            case R.id.tab0:
                FaceRecordFragment faceRecordFragment = FaceRecordFragment.newInstance();
                faceRecordFragment.setOnHeadClickListener(new FaceRecordFragment.OnHeadClickListener() {
                    @Override
                    public void onClick() {
                        menu.toggle();
                    }
                });
                fragment = faceRecordFragment;
                break;
            case R.id.tab1:
                fragment = MyNurseFragment.createInstance();
                break;
            case R.id.tab2:
                fragment = MyDoctorFragment.createInstance();
                break;
            /*case R.id.tab3:
                fragment = MedicalRecordActivity.createInstance();
                break;*/
            default:
                fragment = new Fragment();
                break;

        }
        return fragment;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.loginBtn://登录，退出登录
                finish();
                startActivity(new Intent(this,LoginActivity.class));
                break;
            case R.id.topLayout://侧滑菜单用户信息区域
                startActivity(new Intent(this, PersonalCenterActivity.class));
                break;
            case R.id.myMedicalkitLayout://我的药箱
                startActivity(new Intent(this, MyMedicineBoxActivity.class));
                break;
            case R.id.myCouponLayout://我的现金券
                startActivity(new Intent(this, CouponsActivity.class));
                break;
            case R.id.myRecordLayout://治疗记录
                startActivity(new Intent(this, MedicalRecordActivity.class));
                break;
            case R.id.myAddressLayout://我的地址
                startActivity(new Intent(this, MyAddressActivity.class));
                break;
            case R.id.settingLayout://设置
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.fl_my_record_video:
                // 收藏
                startActivity(new Intent(this, MyRecordActivity.class));
                break;
            case R.id.tv_menu_favorite:
                // 收藏
                startActivity(new Intent(this, FavoriteActivity.class));
                break;
            case R.id.tv_menu_history:
                // 历史
                startActivity(new Intent(this, HistoryActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onMenuToggle() {
        menu.toggle();
    }
}
