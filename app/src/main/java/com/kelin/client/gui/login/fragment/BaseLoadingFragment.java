package com.kelin.client.gui.login.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.kelin.client.gui.login.presenters.viewinterface.ILoadingDialogView;
import com.kelin.client.widget.dialog.LoadingDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseLoadingFragment extends Fragment implements ILoadingDialogView {

    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            loadingDialog.close();
            loadingDialog = null;
        }
    }

    @Override
    public void showLoadingDialog(String msg) {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(getActivity());
        }
        if(loadingDialog.isShowing()){
            loadingDialog.close();
        }
        loadingDialog.show(msg);
    }

    @Override
    public void closeLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.close();
        }
    }
}
