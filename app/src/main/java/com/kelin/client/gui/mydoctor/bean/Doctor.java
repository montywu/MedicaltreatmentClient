package com.kelin.client.gui.mydoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monty on 2017/8/1.
 */

public class Doctor implements Parcelable, BaseItem {
    private boolean isLeft;
    private boolean isRight;

    public Doctor() {
    }


    protected Doctor(Parcel in) {
        isLeft = in.readByte() != 0;
        isRight = in.readByte() != 0;
        id = in.readInt();
        token = in.readString();
        roleId = in.readInt();
        name = in.readString();
        localHospital = in.readString();
        level = in.readString();
        englishName = in.readString();
        price = in.readDouble();
        role = in.readString();
        job = in.readString();
        introduction = in.readString();
        sittingPhotos = in.readString();
        introduceUrl = in.readString();
        createTime = in.readString();
        enable = in.readByte() != 0;
        count = in.readInt();
        userCount = in.readInt();
        sumTime = in.readInt();
        expiredTime = in.readString();
        doctorRole = in.readString();
        waitCount = in.readInt();
        treatmentNames = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isLeft ? 1 : 0));
        dest.writeByte((byte) (isRight ? 1 : 0));
        dest.writeInt(id);
        dest.writeString(token);
        dest.writeInt(roleId);
        dest.writeString(name);
        dest.writeString(localHospital);
        dest.writeString(level);
        dest.writeString(englishName);
        dest.writeDouble(price);
        dest.writeString(role);
        dest.writeString(job);
        dest.writeString(introduction);
        dest.writeString(sittingPhotos);
        dest.writeString(introduceUrl);
        dest.writeString(createTime);
        dest.writeByte((byte) (enable ? 1 : 0));
        dest.writeInt(count);
        dest.writeInt(userCount);
        dest.writeInt(sumTime);
        dest.writeString(expiredTime);
        dest.writeString(doctorRole);
        dest.writeInt(waitCount);
        dest.writeString(treatmentNames);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Doctor> CREATOR = new Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        @Override
        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
    }

    /**
     * 医生id
     */
    private int id;
    private String token;
    private int roleId;
    private String name;
    /**
     * 所在医院
     */
    private String localHospital;
    /**
     * 级别
     */
    private String level;
    /**
     * 职称英文
     */
    private String englishName;

    /**
     * 价格
     */
    private double price;

    /**
     * 职称中文
     */
    private String role;

    /**
     *
     */
    private String job;
    /**
     * 简介
     */
    private String introduction;
    /**
     * 头像
     */
    private String sittingPhotos;
    /**
     * 简介链接
     */
    private String introduceUrl;
    private String createTime;
    private boolean enable;
    /**
     * 医生共诊断次数
     */
    private int count;
    /**
     * 医生共诊断人数
     */
    private int userCount;
    /**
     * 医生共诊断时间，单位分钟
     */
    private int sumTime;
    private String expiredTime;
    private String doctorRole;
    /**
     * 等待人数
     */
    private int waitCount;
    private List<DoctorTreatmentBean> doctorTreatments;

    //    private String[] treatmentNames;
    private String treatmentNames;

    public String getTreatmentName(){
        return treatmentNames;
    }

    public String[] getTreatmentNames() {
        if (TextUtils.isEmpty(treatmentNames)) {
            return treatmentNames.split(",");
        }
        return new String[]{};
    }

    public void setTreatmentNames(String treatmentNames) {
        this.treatmentNames = treatmentNames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalHospital() {
        return localHospital;
    }

    public void setLocalHospital(String localHospital) {
        this.localHospital = localHospital;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSittingPhotos() {
        return sittingPhotos;
    }

    public void setSittingPhotos(String sittingPhotos) {
        this.sittingPhotos = sittingPhotos;
    }

    public String getIntroduceUrl() {
        return introduceUrl;
    }

    public void setIntroduceUrl(String introduceUrl) {
        this.introduceUrl = introduceUrl;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getSumTime() {
        return sumTime;
    }

    public void setSumTime(int sumTime) {
        this.sumTime = sumTime;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    public Object getDoctorRole() {
        return doctorRole;
    }

    public void setDoctorRole(String doctorRole) {
        this.doctorRole = doctorRole;
    }

    public int getWaitCount() {
        return waitCount;
    }

    public void setWaitCount(int waitCount) {
        this.waitCount = waitCount;
    }

    public List<DoctorTreatmentBean> getDoctorTreatments() {
        if (doctorTreatments == null) {
            doctorTreatments = new ArrayList<>();
        }
        return doctorTreatments;
    }

    public void setDoctorTreatments(List<DoctorTreatmentBean> doctorTreatments) {
        this.doctorTreatments = doctorTreatments;
    }


    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int getType() {
        return BaseItem.DOCTOR;
    }

    /**
     * 医生主治类型
     */
    public static class DoctorTreatmentBean {

        private int id;
        /**
         * 医生Id
         */
        private int doctorId;
        /**
         * 主治类型Id
         */
        private int treatmentTypeId;
        /**
         * 主治类型名称
         */
        private String treatmentTypeName;
        /**
         * 执照
         */
        private String physicianCertificate;
        private boolean applyStatu;
        private long createTime;
        private long applyTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public int getTreatmentTypeId() {
            return treatmentTypeId;
        }

        public void setTreatmentTypeId(int treatmentTypeId) {
            this.treatmentTypeId = treatmentTypeId;
        }

        public String getTreatmentTypeName() {
            return treatmentTypeName;
        }

        public void setTreatmentTypeName(String treatmentTypeName) {
            this.treatmentTypeName = treatmentTypeName;
        }

        public String getPhysicianCertificate() {
            return physicianCertificate;
        }

        public void setPhysicianCertificate(String physicianCertificate) {
            this.physicianCertificate = physicianCertificate;
        }

        public boolean isApplyStatu() {
            return applyStatu;
        }

        public void setApplyStatu(boolean applyStatu) {
            this.applyStatu = applyStatu;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getApplyTime() {
            return applyTime;
        }

        public void setApplyTime(long applyTime) {
            this.applyTime = applyTime;
        }

        @Override
        public String toString() {
            return "DoctorTreatmentBean{" +
                    "id=" + id +
                    ", doctorId=" + doctorId +
                    ", treatmentTypeId=" + treatmentTypeId +
                    ", treatmentTypeName='" + treatmentTypeName + '\'' +
                    ", physicianCertificate='" + physicianCertificate + '\'' +
                    ", applyStatu=" + applyStatu +
                    ", createTime=" + createTime +
                    ", applyTime=" + applyTime +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "isLeft=" + isLeft +
                ", isRight=" + isRight +
                ", id=" + id +
                ", token='" + token + '\'' +
                ", roleId=" + roleId +
                ", name='" + name + '\'' +
                ", localHospital='" + localHospital + '\'' +
                ", level='" + level + '\'' +
                ", job='" + job + '\'' +
                ", introduction='" + introduction + '\'' +
                ", sittingPhotos='" + sittingPhotos + '\'' +
                ", introduceUrl='" + introduceUrl + '\'' +
                ", createTime='" + createTime + '\'' +
                ", enable=" + enable +
                ", count=" + count +
                ", userCount=" + userCount +
                ", sumTime=" + sumTime +
                ", expiredTime='" + expiredTime + '\'' +
                ", doctorRole='" + doctorRole + '\'' +
                ", waitCount=" + waitCount +
                ", doctorTreatments=" + doctorTreatments +
                ", treatmentNames='" + treatmentNames + '\'' +
                '}';
    }
}
