package com.kelin.client.gui.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.LoginService;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.setting.controller.ResetPasswordController;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;
import com.kelin.client.widget.VerifyCodeEditText;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by Administrator on 2017/8/16 0016.
 */

public class ResetPasswordsActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verifyCodeLayout)
    VerifyCodeEditText etVerifyCodeLayout;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    @BindView(R.id.et_verifyPwdLayout)
    PasswordEditText etVerifyPwdLayout;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    private boolean isResetPassword;
    private ResetPasswordController controller;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_modifypwd);
        ButterKnife.bind(this);

        getIntentData();

        initController();

        initTitle();

        iniView();

    }


    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        String telPhone = etPhone.getText().toString();
        if (!MyselfInfo.checkTelPhone(telPhone)) return;

        if (!MyselfInfo.checkVerifyCode(etVerifyCodeLayout.getVerifyCode())) return;

        if (!MyselfInfo.checkPassword(etPwdLayout.getPassword())) return;

        if (!MyselfInfo.checkPassword(etVerifyPwdLayout.getPassword())) return;

        if (!etPwdLayout.getPassword().equals(etVerifyPwdLayout.getPassword())) {
            ToastUtils.showToast("两次输入的密码不一致");
            return;
        }
        showLoadingDialog("正在修改密码");
        if (isResetPassword) {
            controller.resetPassword(telPhone, etVerifyPwdLayout.getPassword(), etVerifyCodeLayout.getVerifyCode());
        } else {
            controller.forgetPassword(telPhone, etVerifyPwdLayout.getPassword(), etVerifyCodeLayout.getVerifyCode());
        }
    }


    private void iniView() {
        etPwdLayout.setHint("请输入新密码");
        etVerifyPwdLayout.setHint("再次输入新密码");

        etVerifyCodeLayout.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {
            @Override
            public boolean onClick(View v) {
                String telPhone = etPhone.getText().toString();
                if (!MyselfInfo.checkTelPhone(telPhone)) {
                    return false;
                }

                if (isResetPassword) {
                    RetrofitHelper.getInstance().createService(SettingService.class).resetPassword(MyselfInfo.getLoginUser().getToken(), telPhone).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

                        @Override
                        public void onSuccess(Response<BaseCallModel<Object>> response) {
                            ToastUtils.showToast(R.string.getverify_success);
                        }

                        @Override
                        public void onFailure(String message) {
                            ToastUtils.showToast(message);
                        }
                    });
                } else {
                    RetrofitHelper.getInstance().createService(LoginService.class).getCodeByType(telPhone, 0).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                        @Override
                        public void onSuccess(Response<BaseCallModel<String>> response) {
                            ToastUtils.showToast(R.string.getverify_success);
                        }

                        @Override
                        public void onFailure(String message) {
                            ToastUtils.showToast(message);
                        }
                    });
                }
                return true;
            }

        });
    }

    private void initController() {
        controller = new ResetPasswordController(this) {
            @Override
            public void resetPasswordSuccess() {
                ToastUtils.showToast("密码修改成功，请重新登录");
                closeLoadingDialog();
                finish();
            }

            @Override
            public void resetPasswordFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }

            @Override
            public void forgetPasswordSuccess() {
                ToastUtils.showToast("密码修改成功，请重新登录");
                closeLoadingDialog();
            }

            @Override
            public void forgetPasswordFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        };
    }

    private void getIntentData() {
        if ("reSet".equals(getIntent().getStringExtra("type"))) {
            isResetPassword = true;
        } else {
            isResetPassword = false;
        }
    }

    private void initTitle() {
        if (!isResetPassword) {
            titleBar.showCenterText(R.string.forgot_password, 18);
        } else {
            titleBar.showCenterText(R.string.modify_password,18);
        }
    }

}
