package com.kelin.client.gui.personalcenter.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.kelin.client.R;
import com.kelin.client.bean.ProvinceBean;
import com.kelin.client.common.Constans;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.personalcenter.PersonalCenterActivity;
import com.kelin.client.gui.personalcenter.PersonalCenterService;
import com.kelin.client.gui.personalcenter.entity.PersonalCenterEntity;
import com.kelin.client.util.GrallyAndPhotoUtils;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.dialog.PictureChoicesDialog;
import com.kelin.client.widget.popupwindow.GetImgWayPop;
import com.kelin.client.widget.popupwindow.InputPop;
import com.monty.library.RxUpload;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;


/**
 * Created by guangjiqin on 2017/8/15.
 */

public abstract class PersonalCenterController {

    private String imageUrl;

    public abstract void savePersonalInfSuccess();

    public abstract void savePersonalInfFailure(String message);

    public abstract void uploadPhotoSuccess(int imgId,String msg);

    public abstract void uploadPhotoFailure(String msg);

    public abstract void showLoadingDialog();

    public abstract void closeLoadingDialog();

    private Context context;
    private TimePickerView ageOption = null;
    private OptionsPickerView pvOptions = null;
    private String token;

    public PersonalCenterController(Context context) {
        this.context = context;
        token = MyselfInfo.getLoginUser().getToken();
    }


    /**
     * 保存个人信息
     */
    public void savePersonalInf(final String name, final String phone, final String gender, final int age, final String area) {

        showLoadingDialog();
        RetrofitHelper.getInstance().createService(PersonalCenterService.class).editPersonalInf(token, name, phone, gender, age, area).enqueue(new BaseCallBack<BaseCallModel<PersonalCenterEntity>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<PersonalCenterEntity>> response) {
                User user = MyselfInfo.getLoginUser();
                user.setName(name);
                user.setPhone(phone);
                user.setGender(gender);
                user.setAge(String.valueOf(age));
                user.setArea(area);

                savePersonalInfSuccess();
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                savePersonalInfFailure(message);
                closeLoadingDialog();
            }
        });
    }


    /**
     * 照片选择、拍照
     */


    public void showGetPhotoPop(final int viewId) {
        GetImgWayPop pop = new GetImgWayPop(context) {
            @Override
            public String takePhoto() {//
                Intent intentToCamera = new Intent();
                intentToCamera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);


                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                File path1 = new File(path);
                if (!path1.exists()) {
                    path1.mkdirs();
                }

                File file = new File(path1, String.valueOf(viewId) + ".jpg");//添加缓存文件
//                Uri mOutPutFileUri = Uri.fromFile(file);
                Uri mOutPutFileUri = FileProvider.getUriForFile(context,"gg.hello.patient", file);
                intentToCamera.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);
                ((PersonalCenterActivity) context).startActivityForResult(intentToCamera, viewId);
                return null;
            }

            @Override
            public String getPhotoFromSD() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra("return-data", true);
                ((PersonalCenterActivity) context).startActivityForResult(intent, viewId * 100);
                return null;
            }
        };
        pop.showPop();
    }

    public void getPhoto(final Context context) {
        PictureChoicesDialog dialog = new PictureChoicesDialog();
        dialog.setOnClickListener(new PictureChoicesDialog.OnClickListener() {
            @Override
            public void onTakePhotoClick() {
                //todo(安卓7。0有异常 )file:///storage/emulated/0/Android/data/com.ygbd198.hellodoctor.client/files/Pictures/1502808889496.jpg exposed beyond app through ClipData.Item.getUri()
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion < 24) {
                    String photoPath = GrallyAndPhotoUtils.getPicturesDir() + File.separator + System.currentTimeMillis() + ".jpg";
                    ((PersonalCenterActivity) context).photoPath = photoPath;
                    GrallyAndPhotoUtils.openCamera((PersonalCenterActivity) context, photoPath);
                } else {
                    ToastUtils.showToast("7.0");
                }

            }

            @Override
            public void onChoicePhoto() {
                GrallyAndPhotoUtils.openGrally((PersonalCenterActivity) context);
            }
        });
        dialog.show((((PersonalCenterActivity) context).getSupportFragmentManager()));
    }

    //显示年龄选择框
    public void showAgeOptions() {
        ageOption = new TimePickerView.Builder(context, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调

                String curAge = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - (date.getYear() + 1900));
                ((PersonalCenterActivity) context).tvAge.setText(curAge);

            }
        }).setLayoutRes(R.layout.dialog_age_choise, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ageOption.returnData();
                        ageOption.dismiss();
                    }
                });

            }
        })
                .setRange(1900, 2017)
                .setType(new boolean[]{true, false, false, false, false, false})
                .setLabel("", "", "", "", "", "")
                .setDividerColor(Color.parseColor("#ffffff"))//设置分割线的颜色
                .setTextColorCenter(Color.parseColor("#ff7fb9"))
                .setTextColorOut(Color.parseColor("#0f345e"))
                .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isCyclic(true)
                .build();

//        ageOption.setPicker(age);//二级选择器
        ageOption.show();
    }


    /**
     * 地址选择框
     */
    public void showCityChoies() {

        final ArrayList<ProvinceBean> options1Items = new ArrayList<>();
        final ArrayList<ArrayList<String>> options2Items = new ArrayList<>();

        options1Items.clear();
        options2Items.clear();

        for (String key : Constans.provinces.keySet()) {
            options1Items.add(new ProvinceBean(-1, key, "", ""));
            ArrayList<String> options2Items_01 = new ArrayList<>();
            for (String value : Constans.provinces.get(key)) {
                options2Items_01.add(value);
            }
            options2Items.add(options2Items_01);
        }

        pvOptions = new OptionsPickerView.Builder(context, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText() + options2Items.get(options1).get(options2)
                       /* + options3Items.get(options1).get(options2).get(options3).getPickerViewText()*/;
//                ToastUtils.showToast(tx);

                ((PersonalCenterActivity) context).tvRegion.setText(tx);

            }
        }).setLayoutRes(R.layout.dialog_city_choise, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pvOptions.returnData();
                        pvOptions.dismiss();
                    }
                });

            }
        })
//                .setTitleText("城市选择")
                .setContentTextSize(16)//设置滚轮文字大小
                .setDividerColor(Color.parseColor("#0f345e"))//设置分割线的颜色
                .setSelectOptions(0, 1)//默认选中项
                .setTextColorCenter(Color.parseColor("#f5b8d3"))
                .setTextColorOut(Color.parseColor("#0f345e"))
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                .setLabels("省", "市", "区")
                .setBackgroundId(0x66000000) //设置外部遮罩颜色
                .build();

        pvOptions.setPicker(options1Items, options2Items);//二级选择器
        pvOptions.show();
    }

    /**
     * 显示重命名对话框
     */
    public void shwoResetNamePop() {
        final PersonalCenterActivity activity = (PersonalCenterActivity) context;
        InputPop pop = new InputPop(context) {
            @Override
            public void onClickCommit(String content) {
                activity.tvName.setText(content.trim());
            }
        };
        pop.setTitle(R.string.input_name);
        pop.showPop();
    }

    /**
     * 显示电话输入对话框
     */
    public void shwoResetPhoneNumPop() {
        final PersonalCenterActivity activity = (PersonalCenterActivity) context;
        InputPop pop = new InputPop(context) {
            @Override
            public void onClickCommit(String content) {
                activity.tvTelphone.setText(content.trim());
            }
        };
        pop.getEtInput().setInputType(InputType.TYPE_CLASS_PHONE);
        pop.setTitle(R.string.input_phone);
        pop.showPop();
    }

    /**
     * 图片提交
     */
    public void uploadPhoto(final int imgId, File file, int type) {
        final List<File> files = new ArrayList<>();
        files.add(file);
        RxUpload.userUpload(context,files,MyselfInfo.getLoginUser().getToken(),type).subscribe(new Observer<RxUpload.UploadProcess>() {
            ProgressDialog progressDialog;

            @Override
            public void onSubscribe(Disposable d) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("正在上传:" + 0 + "/" + files.size());
                progressDialog.show();
            }

            @Override
            public void onNext(RxUpload.UploadProcess value) {
                progressDialog.setMessage("正在上传:" + value.position + "/" + value.count);

                if (value.position == value.count) {
                    uploadPhotoSuccess(imgId, value.data);
                    ToastUtils.showToast("文件上传完成");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e("monty", "文件上传 -> onError -> " + e.toString());
                ToastUtils.showToast("文件上传失败 - " + e.toString());
                uploadPhotoFailure(e.toString());
                progressDialog.dismiss();
            }

            @Override
            public void onComplete() {
                progressDialog.dismiss();
            }
        });

        /*MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)//表单类型
                .addFormDataPart("token", token)
                .addFormDataPart("type", String.valueOf(type));

        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

        List<MultipartBody.Part> parts = builder.build().parts();
        Log.e("gg", "开始上传请求");
        UploadRetrofitHelper.getInstance().uploadImage(PersonalCenterService.class).doctorUpload(parts).enqueue(new Callback<BaseCallModel<List<String>>>() {
            @Override
            public void onResponse(Call<BaseCallModel<List<String>>> call, Response<BaseCallModel<List<String>>> response) {
                Log.e("gg", "上传成功 = " + response.body().data);
                uploadPhotoSuccess(imgId, response.body().data);
            }

            @Override
            public void onFailure(Call<BaseCallModel<List<String>>> call, Throwable t) {
                t.printStackTrace();
                Log.e("gg", "上传失败" + call.request().body());
                uploadPhotoFailure("");
            }
        });*/

    }
}
