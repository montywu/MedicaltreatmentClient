package com.kelin.client.gui.mydoctor;

import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.monty.library.http.BaseCallModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/8/1.
 */

public interface DoctorService {
    /**
     * 获取所有症状类型
     *
     * @param token
     * @return
     */
    @GET("treatmentType/getAll")
    Call<BaseCallModel<List<TreatmentType>>> getAllTreatentType(@Query("token") String token);

    /**
     * 根据症状类型获取房间列表
     *
     * @param token 用户token
     * @param treatmentTypeId 治疗类型Id
     * @param count 请求的数量
     * @return
     */
    @GET("userAttach/getRoomList")
    Call<BaseCallModel<List<DoctorClassify>>> getRoomList(@Query("token") String token, @Query("treatmentTypeId") int treatmentTypeId, @Query("count") int count);

    /**
     * 根据关键字搜索医生列表
     * @param token
     * @param name 关键字
     * @param pageNo
     * @param pageSize
     * @return
     */
    @GET("userAttach/searchRoom")
    Call<BaseCallModel<List<Doctor>>> searchRoom(@Query("token") String token, @Query("name") String name, @Query("pageNo") int pageNo);

    /**
     * 根据症状类型和角色等级获取房间列表
     *
     * @param token 用户token
     * @param roleId 角色Id
     * @param treatmentTypeName 治疗类型名称
     * @return
     */
    @GET("userAttach/getMoreList")
//    Call<BaseCallModel<List<Doctor>>> getMoreRoomList(@Query("token") String token, @Query("roleId") int roleId, @Query("treatmentTypeName") String treatmentTypeName,@Query("pageNo") int pageNo);
    Call<BaseCallModel<List<Doctor>>> getMoreRoomList(@Query("token") String token, @Query("roleId") int roleId, @Query("pageNo") int pageNo);

    /**
     * 获取空闲医生列表
     *
     * @param token 用户token
     * @param roleId 角色Id
     * @param treatmentTypeName 治疗类型id
     * @return
     */
    @GET("userAttach/getFreeList")
    Call<BaseCallModel<List<Doctor>>> getFreeRoomList(@Query("token") String token, @Query("roleId") int roleId, @Query("treatmentTypeName") String treatmentTypeName,@Query("pageNo") int pageNo);

    /**
     * 对医生进行评价
     * @param token
     * @param level 星级 0-5
     * @return
     */
    @FormUrlEncoded
    @POST("evaluate/save")
    Call<BaseCallModel<String>> evaluateSave(@Field("token") String token, @Field("level") String level);

    /**
     * 对医生进行投诉
     * @param token
     * @param content 投诉内容，最大字符数65535
     * @param contactInfo 联系方式
     * @param picUrl 截图路径，若有多个请用‘|’分隔符
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @FormUrlEncoded
    @POST("evaluate/complaint")
    Call<BaseCallModel<String>> complaint(@Field("token") String token, @Field("content") String content,@Field("contactInfo") String contactInfo,@Field("picUrl") String picUrl);


}
