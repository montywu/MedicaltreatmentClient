package com.kelin.client.gui.therapeutic.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.therapeutic.TherapeuticService;
import com.kelin.client.gui.therapeutic.adapter.TabooAdapter;
import com.kelin.client.gui.therapeutic.entity.TabooBean;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Response;

/**
 * 治疗方案-禁忌
 * Created by guangjiqin on 2017/8/18.
 */

public class TabooFragment extends BaseFragment {

    private final String TAG = getClass().getName();

    private View view;
    private long diagnosticId;

    private PtrClassicFrameLayout mPtrFrame;
    private ListView listView;
    private TabooAdapter adapter;
    private List<TabooBean> datas;

    public static TabooFragment createInstance(long diagnosticId) {
//        if (null == tabooFragment) {
        TabooFragment tabooFragment = new TabooFragment();
        tabooFragment.diagnosticId = diagnosticId;
//        }
        return tabooFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_taboo_style, null);

        initView();

        initPullRefresh();

        return view;
    }

    private void initPullRefresh() {
        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                requestData();
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initView() {
        datas = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.listview);
        adapter = new TabooAdapter(getActivity(), datas);
        listView.setAdapter(adapter);

    }

    @Override
    protected void requestData() {
        RetrofitHelper.getInstance().createService(TherapeuticService.class).getTaboo(MyselfInfo.getLoginUser().getToken(),
                diagnosticId).enqueue(new BaseCallBack<BaseCallModel<List<TabooBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<TabooBean>>> response) {
                mPtrFrame.refreshComplete();
                datas.clear();
                datas.addAll(response.body().data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                mPtrFrame.refreshComplete();
            }
        });
    }
}
