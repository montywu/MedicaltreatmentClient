package com.kelin.client.gui.mydoctor.presenters.viewinterface;

import com.kelin.client.gui.login.presenters.viewinterface.ILoadingDialogView;
import com.kelin.client.gui.mydoctor.bean.Doctor;

import java.util.List;

/**
 * Created by monty on 2017/8/9.
 */

public interface IMoreDoctorsView extends ILoadingDialogView {
    void getDoctorRoomSuccess(List<Doctor> doctors);
    void getDoctorRoomFailure(String errorMsg);
    void refreshComplete();
}
