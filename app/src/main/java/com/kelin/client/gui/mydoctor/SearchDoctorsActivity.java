package com.kelin.client.gui.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.adapter.DoctorAdapter;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.presenters.DoctorPresenter;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IMoreDoctorsView;
import com.kelin.client.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by monty on 2017/8/9.
 * 更多医生
 */
public class SearchDoctorsActivity extends BaseActivity implements IMoreDoctorsView {

    @BindView(R.id.jp_title_back)
    ImageView jpTitleBack;
    @BindView(R.id.et_key)
    EditText etKey;
    @BindView(R.id.btn_search)
    TextView btnSearch;
    @BindView(R.id.gv_doctors)
    GridView gvDoctors;
    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;
    private boolean isLoadMore;
    private int pageNo = 1;

    public static void goActivity(Context context) {
        Intent intent = new Intent(context, SearchDoctorsActivity.class);
        context.startActivity(intent);
    }

    private DoctorPresenter presenter;

    private DoctorAdapter doctorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_doctors);
        ButterKnife.bind(this);
        initView();
        initData();

    }

    @Override
    protected void initView() {
        initPullRefresh();
    }

    @Override
    protected void initData() {
        presenter = new DoctorPresenter(this);
        doctorAdapter = new DoctorAdapter(LayoutInflater.from(this), new ArrayList<Doctor>());
        gvDoctors.setAdapter(doctorAdapter);
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                isLoadMore = true;
                presenter.searchRoom(MyselfInfo.getLoginUser().getToken(), etKey.getText().toString(), pageNo);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                isLoadMore = false;
                presenter.searchRoom(MyselfInfo.getLoginUser().getToken(), etKey.getText().toString(), 1);
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
    }

    @Override
    public void getDoctorRoomSuccess(List<Doctor> doctors) {
        if (isLoadMore) {
            if (doctors != null && doctors.size() > 0) {
                pageNo++;
                doctorAdapter.addNotifyData(doctors);
            }
        } else {
            pageNo = 2;
            if (doctors == null || doctors.size() == 0) {
                ToastUtils.showToast("未查询到匹配的医生");
            }
            doctorAdapter.notifyDataSetChanged(doctors);
        }
        mPtrFrame.refreshComplete();
    }

    @Override
    public void getDoctorRoomFailure(String errorMsg) {
        ToastUtils.showToast("请求失败:" + errorMsg);
        mPtrFrame.refreshComplete();
    }

    @Override
    public void refreshComplete() {

    }

    @OnClick({R.id.jp_title_back, R.id.btn_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.jp_title_back:
                onBackPressed();
                break;
            case R.id.btn_search:
                String key = etKey.getText().toString();
                if(TextUtils.isEmpty(key)){
                    ToastUtils.showToast("请输入要搜索的关键字");
                }else{
                    mPtrFrame.autoRefresh();
                }
                break;
        }
    }
}
