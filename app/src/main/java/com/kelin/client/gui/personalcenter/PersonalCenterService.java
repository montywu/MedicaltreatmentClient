package com.kelin.client.gui.personalcenter;

import com.kelin.client.gui.personalcenter.entity.PersonalCenterEntity;
import com.monty.library.http.BaseCallModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public interface PersonalCenterService {

    /**
     * 编辑我的收货地址页面
     */
    @FormUrlEncoded
    @POST("user/submitInfo")
    Call<BaseCallModel<PersonalCenterEntity>> editPersonalInf(@Field("token") String token,
                                                              @Field("name") String name,
                                                              @Field("phone") String phone,
                                                              @Field("gender") String gender,
                                                              @Field("age") int age,
                                                              @Field("area") String area);

}
