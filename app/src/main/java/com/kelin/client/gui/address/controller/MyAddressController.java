package com.kelin.client.gui.address.controller;


import android.util.Log;

import com.kelin.client.gui.address.MyAddressService;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/13.
 */

public abstract class MyAddressController {


    public abstract void getAddressSuccess(List<MyAddressEntity> addressList);

    public abstract void getAddressFailure(String message);

    public abstract void delAddressSuccess(int addressId);

    public abstract void delAddressFailure(String message);

    public abstract void setDefaultAddressSuccess(int addressId, boolean isDefault);

    public abstract void setDefaultAddressFailure(String message);

    private String token;

    public MyAddressController(String token) {
        this.token = token;
    }


    //获取收货地址
    public void getAddressListData() {
        RetrofitHelper.getInstance().createService(MyAddressService.class).getAddressList(token).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
                List<MyAddressEntity> treatmentType = response.body().data;
                Log.d("gg", "getAddressList - onSuccess -> " + treatmentType.toString());
                getAddressSuccess(treatmentType);
            }

            @Override
            public void onFailure(String message) {
                getAddressFailure(message);
            }
        });
    }

    //删除收货地址
    public void delAddressData(final int addressId) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).delAddressList(token, addressId).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
                delAddressSuccess(addressId);
            }

            @Override
            public void onFailure(String message) {
                delAddressFailure(message);
            }
        });
    }

    //默认收货地址
    public void setDefaultAddress(final int addressId, final boolean isDefault) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).setDefaultAddressList(token, addressId, isDefault).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
                setDefaultAddressSuccess(addressId,isDefault);
            }

            @Override
            public void onFailure(String message) {
                setDefaultAddressFailure(message);
            }
        });
    }
}
