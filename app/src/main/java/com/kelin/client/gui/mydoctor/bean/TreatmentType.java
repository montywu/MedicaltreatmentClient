package com.kelin.client.gui.mydoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * 症状类型
 * Created by monty on 2017/8/1.
 */

public class TreatmentType implements Parcelable {

    private int id;
    private String typeName;
    private String picUrl;
    private String createTime;
    private String otherPicture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOtherPicture() {
        if (!TextUtils.isEmpty(otherPicture)) {
            if (otherPicture.startsWith(",")) {
                otherPicture = otherPicture.substring(1, otherPicture.length());
            }
            String[] split = otherPicture.split(",");
            if (split.length > 0) {
                return split[0];
            }
        }
        return "";
    }

    public void setOtherPicture(String otherPicture) {
        this.otherPicture = otherPicture;
    }

    @Override
    public String toString() {
        return "TreatmentType{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.typeName);
        dest.writeString(this.picUrl);
        dest.writeString(this.createTime);
        dest.writeString(this.otherPicture);
    }

    public TreatmentType() {
    }

    protected TreatmentType(Parcel in) {
        this.id = in.readInt();
        this.typeName = in.readString();
        this.picUrl = in.readString();
        this.createTime = in.readString();
        this.otherPicture = in.readString();
    }

    public static final Parcelable.Creator<TreatmentType> CREATOR = new Parcelable.Creator<TreatmentType>() {
        @Override
        public TreatmentType createFromParcel(Parcel source) {
            return new TreatmentType(source);
        }

        @Override
        public TreatmentType[] newArray(int size) {
            return new TreatmentType[size];
        }
    };
}
