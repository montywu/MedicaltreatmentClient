package com.kelin.client.gui.mydoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by monty on 2017/8/1.
 */

public class DoctorClassify implements Parcelable,BaseItem {

    /**
     * 角色id
     */
    private int id;
    /**
     * 角色名称
     */
    private String role;
    /**
     * 类别名称
     */
    private String englishName;
    /**
     * 价格
     */
    private double price;
    /**
     * 价格描述
     */
    private String priceExplain;
    /**
     * 医生列表
     */
    private List<Doctor> doctorAttachList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPriceExplain() {
        return priceExplain;
    }

    public void setPriceExplain(String priceExplain) {
        this.priceExplain = priceExplain;
    }

    public List<Doctor> getDoctorAttachList() {
        return doctorAttachList;
    }

    public void setDoctorAttachList(List<Doctor> doctorAttachList) {
        this.doctorAttachList = doctorAttachList;
    }

    public static Creator<DoctorClassify> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "DoctorClassify{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", englishName='" + englishName + '\'' +
                ", price='" + price + '\'' +
                ", priceExplain='" + priceExplain + '\'' +
                ", doctorAttachList=" + doctorAttachList +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.role);
        dest.writeString(this.englishName);
        dest.writeDouble(this.price);
        dest.writeString(this.priceExplain);
        dest.writeTypedList(this.doctorAttachList);
    }

    public DoctorClassify() {
    }

    protected DoctorClassify(Parcel in) {
        this.id = in.readInt();
        this.role = in.readString();
        this.englishName = in.readString();
        this.price = in.readDouble();
        this.priceExplain = in.readString();
        this.doctorAttachList = in.createTypedArrayList(Doctor.CREATOR);
    }

    public static final Creator<DoctorClassify> CREATOR = new Creator<DoctorClassify>() {
        @Override
        public DoctorClassify createFromParcel(Parcel source) {
            return new DoctorClassify(source);
        }

        @Override
        public DoctorClassify[] newArray(int size) {
            return new DoctorClassify[size];
        }
    };

    @Override
    public int getType() {
        return BaseItem.DOCTOR_CLASSIFY;
    }
}
