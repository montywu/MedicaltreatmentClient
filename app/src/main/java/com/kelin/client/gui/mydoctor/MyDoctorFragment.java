package com.kelin.client.gui.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.kelin.client.R;
import com.kelin.client.gui.OnMenuToggleListener;
import com.kelin.client.gui.coupons.CouponsActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.adapter.AdAdapter;
import com.kelin.client.gui.mydoctor.adapter.DoctorCategoryAdapter;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.kelin.client.gui.mydoctor.presenters.DoctorPresenter;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IDoctorView;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.dialog.LoadingDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;


/**
 * Created by monty on 2017/7/9.
 * 我的医生fragment
 */

public class MyDoctorFragment extends Fragment implements IDoctorView{
    Unbinder unbinder;

    private LoadingDialog loadingDialog;

    @BindView(R.id.roll_view_pager)
    RollPagerView rollViewPager;
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    /*@BindView(R.id.diseaseList)
    GridView diseaseList;*/
//    @BindView(R.id.store_house_ptr_frame)
//    PtrClassicFrameLayout mPtrFrame;
//    @BindView(R.id.lv_doctor_classify)
//    RecyclerView recyclerView;
  /*  @BindView(R.id.scrollView)
    VerticalPager scrollLayout;*/
    @BindView(R.id.btn_search)
    TextView btnSearch;
    @BindView(R.id.lv_doctor_category)
    ListView lv_doctor_category;
    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;

    private DoctorPresenter doctorPresenter;

    private DoctorCategoryAdapter adapter;


    private AdAdapter adAdapter;

    private OnMenuToggleListener onMenuToggleListener;

    public static MyDoctorFragment createInstance() {
        MyDoctorFragment fragment = new MyDoctorFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doctorPresenter = new DoctorPresenter(this);
    }

    @Override
    public void onAttach(Context context) {
        onMenuToggleListener = (OnMenuToggleListener) context;
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mydoctor_fragment_layout, null);
        unbinder = ButterKnife.bind(this, view);
        loadingDialog = new LoadingDialog(getContext());
        titleBar.showCenterText("My Dr.","颜值医生");
        titleBar.setCenterLeftTextSize(26);
        titleBar.setBackBtnImg(R.drawable.home_icon_personal);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuToggleListener.onMenuToggle();
            }
        });

        titleBar.setRightText("领券", 0, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CouponsActivity.class));
            }
        });

        initPullRefresh();
        initADPager();
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
        return view;
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new in.srain.cube.views.ptr.PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                doctorPresenter.getAllTreatmentType(MyselfInfo.getLoginUser().getToken());;
            }
        });

        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
    }
    private void initADPager() {
        //设置播放时间间隔
        rollViewPager.setPlayDelay(3000);
        //设置透明度
        rollViewPager.setAnimationDurtion(500);
        adAdapter = new AdAdapter(rollViewPager);
        //设置适配器
        rollViewPager.setAdapter(adAdapter);
//        rollViewPager.setPlayDelay(0); // 0则不播放
        //mRollViewPager.setHintView(new IconHintView(this, R.drawable.point_focus, R.drawable.point_normal));
        rollViewPager.setHintView(new ColorPointHintView(this.getContext(), Color.parseColor("#f8b9cd"), R.color.black_cc000000));

        //mRollViewPager.setHintView(new TextHintView(this));
        //mRollViewPager.setHintView(null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void getTreatmentTypeSuccess(List<TreatmentType> treatmentTypeList) {

        if(adapter==null){
            adapter = new DoctorCategoryAdapter(getContext(),treatmentTypeList);
            lv_doctor_category.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged(treatmentTypeList);
        }
        mPtrFrame.refreshComplete();
    }

    @Override
    public void getDoctorRoomSuccess(List<DoctorClassify> doctorClassifies) {

    }

    @Override
    public void getTreatmentTypeFailure(String message) {
        mPtrFrame.refreshComplete();
    }

    @Override
    public void getDoctorRoomFailure(String message) {

    }


    @OnClick(R.id.btn_search)
    public void onViewClicked() {
        SearchDoctorsActivity.goActivity(getContext());
    }
}
