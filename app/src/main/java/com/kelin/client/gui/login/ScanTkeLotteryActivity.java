package com.kelin.client.gui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;
import com.kelin.client.widget.VerifyCodeEditText;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/13.
 * 扫码领券activity
 */

public class ScanTkeLotteryActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar title;


    @BindView(R.id.et_ticket_num)
    EditText etTicketNum;

    @BindView(R.id.et_phone)
    EditText etPhoneNum;

    @BindView(R.id.et_pwdLayout)
    VerifyCodeEditText etCode;

    @BindView(R.id.et_password)
    PasswordEditText etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_take_lottery);
        ButterKnife.bind(this);

        initTitle();

        etCode.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {
            @Override
            public boolean onClick(View v) {
                String telPhone = etPhoneNum.getText().toString();
//                if (!MyselfInfo.checkTelPhone(telPhone)) {
//                    return false;
//                }

                RetrofitHelper.getInstance().createService(LoginService.class).getLotteryCode(telPhone).enqueue(new BaseCallBack<BaseCallModel<String>>() {
                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                    }

                    @Override
                    public void onFailure(String message) {
                        if("手机号已存在".equals(message)){
                            startLoginActivity();
                        }
                        ToastUtils.showToast(message);
                    }
                });
                return true;
            }

        });
    }

    private void initTitle() {
        title.showCenterText("扫码领券");
        title.setRightText("跳过", 15, getResources().getColor(R.color.white), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScanTkeLotteryActivity.this.finish();
                startActivity(new Intent(ScanTkeLotteryActivity.this, LoginActivity.class));
            }
        });
    }

    @OnClick(R.id.btn_login)
    public void getLottery() {
        if (etTicketNum.getText().toString().isEmpty()) {
            ToastUtils.showToast("现金圈密码不能为空");
            return;
        }
        if (etPhoneNum.getText().toString().isEmpty()) {
            ToastUtils.showToast("手机号不能为空");
            return;
        }
        if (etCode.getVerifyCode().isEmpty()) {
            ToastUtils.showToast("短信验证码不能为空");
            return;
        }
        if (etPassword.getPassword().isEmpty()) {
            ToastUtils.showToast("密码不能为空");
            return;
        }

        //扫码领圈  领券成功登录
        RetrofitHelper.getInstance().createService(LoginService.class).getLottery(etPhoneNum.getText().toString(),
                etTicketNum.getText().toString(),
                etCode.getVerifyCode(),
                etPassword.getPassword()).enqueue(new BaseCallBack<BaseCallModel<Object>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                ScanTkeLotteryActivity.this.finish();
                Intent intent = new Intent(ScanTkeLotteryActivity.this, LoginActivity.class);
                intent.putExtra("phone",etPhoneNum.getText().toString());
                intent.putExtra("pas",etPassword.getPassword());
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });

    }

}
