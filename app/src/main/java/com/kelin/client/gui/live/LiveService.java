package com.kelin.client.gui.live;

import com.kelin.client.gui.live.model.UpMenber;
import com.monty.library.http.BaseCallModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/8/1.
 */

public interface LiveService {
    /**
     * 用户进入房间上报
     *
     * @param token
     * @param doctorId 医生id
     * @return
     */
    @FormUrlEncoded
    @POST("userAttach/enterRoom")
    Call<BaseCallModel<String>> enterRoom(@Field("token") String token,@Field("doctorId") int doctorId);/**


    /**
     * 用户退出房间上报
     *
     * @param token
     * @return
     */
    @GET("userAttach/outRoom")
    Call<BaseCallModel<String>> outRoom(@Query("token") String token);

    /**
     * 用户心跳
     *
     * @param token 用户token
     * @return
     */
    @GET("userAttach/userTimer")
    Call<BaseCallModel<String>> userTimer(@Query("token") String token);

    /**
     * 用户申请连麦（进入医生的排队列表）
     *
     * @param token
     * @param doctorId 医生id
     * @return
     */
    @GET("userAttach/applyConnect")
    Call<BaseCallModel<UpMenber>> applyConnect(@Query("token") String token, @Query("doctorId") int doctorId);

    /**
     * 用户取消连麦（从医生的排队列表中移除）
     *
     * @param token
     * @return
     */
    @GET("userAttach/cancelApply")
    Call<BaseCallModel<String>> cancelApply(@Query("token") String token);


}
