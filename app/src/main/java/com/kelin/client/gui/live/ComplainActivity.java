package com.kelin.client.gui.live;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.DoctorService;
import com.kelin.client.util.GrallyAndPhotoUtils;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.dialog.PictureChoicesDialog;
import com.monty.library.RxUpload;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;


/**
 * 投诉页面
 * Created by monty on 2017/10/10.
 */

public class ComplainActivity extends BaseActivity {
    public static int REQUEST_CODE = 0x1101;
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.et_content)
    EditText etContent;
    @BindView(R.id.btn_addImage)
    ImageButton btnAddImage;
    @BindView(R.id.et_contactInformation)
    EditText etContactInformation;
    @BindView(R.id.btn_commit)
    Button btnCommit;

    public static void GoToComplainActivity(Context context) {
        Intent intent = new Intent(context, ComplainActivity.class);
        ((Activity) context).startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);
        ButterKnife.bind(this);

        initView();
    }

    @Override
    protected void initView() {
        this.titleBar.showCenterText("投诉", R.drawable.complain, 0);

    }

    private String photoPath;

    public void getPhoto(final Context context) {
        final PictureChoicesDialog dialog = new PictureChoicesDialog();
        dialog.setOnClickListener(new PictureChoicesDialog.OnClickListener() {
            @Override
            public void onTakePhotoClick() {
                //todo(安卓7。0有异常 )file:///storage/emulated/0/Android/data/com.ygbd198.hellodoctor.client/files/Pictures/1502808889496.jpg exposed beyond app through ClipData.Item.getUri()
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion < 24) {
                    photoPath = GrallyAndPhotoUtils.getPicturesDir() + File.separator + System.currentTimeMillis() + ".jpg";

                    GrallyAndPhotoUtils.openCamera(ComplainActivity.this, photoPath);
                } else {
                    ToastUtils.showToast("7.0");
                }
                dialog.dismiss();

            }

            @Override
            public void onChoicePhoto() {
                GrallyAndPhotoUtils.openGrally(ComplainActivity.this);
                dialog.dismiss();

            }
        });
        dialog.show(getSupportFragmentManager());
    }

    @OnClick({R.id.btn_addImage, R.id.btn_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_addImage:
                getPhoto(this);
                break;
            case R.id.btn_commit:
                String content = etContent.getText().toString();
                String userInfo = etContactInformation.getText().toString();
                if (TextUtils.isEmpty(content)) {
                    ToastUtils.showToast("请填写您的意见或建议");
                } else if (TextUtils.isEmpty(userInfo)) {
                    ToastUtils.showToast("请填写您的联系方式");
                } else {
                    uploadImage();
                }
                break;
        }
    }

    private void commit() {
        showLoadingDialog("");
        RetrofitHelper.getInstance().createService(DoctorService.class).complaint(MyselfInfo.getLoginUser().getToken(), etContent.getText().toString(), etContactInformation.getText().toString(), imageUrl).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                ToastUtils.showToast("投诉成功");
                finish();
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("投诉失败，请重试(" + message + ")");
                closeLoadingDialog();
            }
        });
    }

    private String imageUrl;

    private void uploadImage() {
        File file = new File(photoPath);
        final List<File> files = new ArrayList<>();
        files.add(file);
        RxUpload.upload(this, files, MyselfInfo.getLoginUser().getToken(), 2).subscribe(new Observer<RxUpload.UploadProcess>() {
            ProgressDialog progressDialog;

            @Override
            public void onSubscribe(Disposable d) {
                progressDialog = new ProgressDialog(ComplainActivity.this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("正在上传:" + 0 + "/" + files.size());
                progressDialog.show();
            }

            @Override
            public void onNext(RxUpload.UploadProcess value) {
                progressDialog.setMessage("正在上传:" + value.position + "/" + value.count);
                imageUrl = value.data;
                if (value.position == value.count) {
                    ToastUtils.showToast("文件上传完成");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e("monty", "文件上传 -> onError -> " + e.toString());
                ToastUtils.showToast("文件上传失败 - " + e.toString());
                progressDialog.dismiss();
            }

            @Override
            public void onComplete() {
                progressDialog.dismiss();
                commit();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GrallyAndPhotoUtils.REQUEST_CODE_TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
//                    ToastUtils.showToast(photoPath);
//                    btnAddImage.setClickable(false);
                    showImage(photoPath);
                } else {
                    ToastUtils.showToast("拍照失败");
                }

                break;
            case GrallyAndPhotoUtils.REQUEST_CODE_OPEN_GRALLY:
                if (resultCode == RESULT_OK) {
                    //获取intent中返回的图片路径(如果是打开相册部分机型可能将图片路径存放在intent中返回)
                    photoPath = GrallyAndPhotoUtils.getBmpPathFromContent(this, data);
                    showImage(photoPath);
//                    ToastUtils.showToast(tempPath);
                } else {
                    ToastUtils.showToast("照片选择失败");
                }
                break;
        }
    }

    private void showImage(String imagePath) {
        Glide.with(this).load(imagePath).centerCrop().into(btnAddImage);
    }


}
