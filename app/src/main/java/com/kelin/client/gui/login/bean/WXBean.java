package com.kelin.client.gui.login.bean;

/**
 * Created by monty on 2017/7/27.
 */

public class WXBean {
    public String access_token;
    public int expires_in;
    public String refresh_token;
    public String openid;
    public String scope;
    public String unionid;
}