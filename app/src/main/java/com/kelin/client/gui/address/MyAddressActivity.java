package com.kelin.client.gui.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.address.adapter.AddressListAdapter;
import com.kelin.client.gui.address.controller.MyAddressController;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by guangjiqin on 2017/8/9.
 */

public class MyAddressActivity extends BaseActivity {

    private final String TAG = getClass().getName();
    private final int REFRESH_CODE = 1;

    //    SwipeMenuListView listview;
    @BindView(R.id.listview)
    ListView listview;
    @BindView(R.id.titleBar)
    BaseTitleBar title;

    private List<MyAddressEntity> addressEntites;
    private AddressListAdapter adapter;

    private MyAddressController controller;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (REFRESH_CODE == requestCode && requestCode == 1) {
            getDataFromNet();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_address);

        ButterKnife.bind(this);

        initTitle();

        initListView();

        initController();

        getDataFromNet();
    }

    private void initController() {
        controller = new MyAddressController(MyselfInfo.getLoginUser().getToken()) {
            @Override
            public void getAddressSuccess(List<MyAddressEntity> addressList) {
                Log.i(TAG, "获取收货地址成功！！");
                addressEntites.clear();
                addressEntites.addAll(addressList);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void getAddressFailure(String message) {
                Log.i(TAG, "获取收货地址失败！！");
                ToastUtils.showToast(MyAddressActivity.this, message);
            }


            @Override
            public void delAddressSuccess(int addressId) {
                Log.i(TAG, "删除收货地址成功！！");
                for (MyAddressEntity entity : addressEntites) {
                    if (addressId == entity.id) {
                        addressEntites.remove(entity);
                        break;
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void delAddressFailure(String message) {
                Log.i(TAG, "删除收货地址失败！！");
                ToastUtils.showToast(MyAddressActivity.this, message);
            }

            @Override
            public void setDefaultAddressSuccess(int addressId, boolean isDefault) {
                Log.i(TAG, "设置默认收货地址成功！！");
                for (MyAddressEntity entity : addressEntites) {
                    if (addressId == entity.id) {
                        entity.isDefault = true;
                    } else {
                        entity.isDefault = false;
                    }
                }

                Collections.sort(addressEntites, new SortByIsDefault());

                adapter.notifyDataSetChanged();
            }

            @Override
            public void setDefaultAddressFailure(String message) {
                Log.i(TAG, "设置默认地址失败！！");
                ToastUtils.showToast(MyAddressActivity.this, message);
            }
        };

    }

    class SortByIsDefault implements Comparator<MyAddressEntity> {

        @Override
        public int compare(MyAddressEntity o1, MyAddressEntity o2) {
            if (o1.isDefault && !o2.isDefault) {
                return -1;
            }
            return 1;
        }
    }


    private void getDataFromNet() {
        controller.getAddressListData();

    }

    @OnClick(R.id.bt_add_address)
    public void toEditAddress() {
        startActivityForResult(new Intent(MyAddressActivity.this, EditAddressActivity.class), REFRESH_CODE);
    }

    private void initListView() {
        addressEntites = new ArrayList<>();
        adapter = new AddressListAdapter(this, addressEntites, new AddressListAdapter.OnDelClickListener() {
            @Override
            public void onDelClick(int addressEntityId) {
                controller.delAddressData(addressEntityId);
            }
        });

        /*SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));
                // set item title
                openItem.setTitle(getString(R.string.del_address));
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };*/
        // set creator

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                controller.setDefaultAddress(addressEntites.get(position).id, true);
            }
        });

        /*listview.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // 删除地址
                        controller.delAddressData(addressEntites.get(position).id);
                        break;
                }
                return false;
            }
        });*/
//        listview.setMenuCreator(creator);
        listview.setAdapter(adapter);
    }

    private void initTitle() {
        title.showCenterText("MY ADDRESS","我的地址");
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

}
