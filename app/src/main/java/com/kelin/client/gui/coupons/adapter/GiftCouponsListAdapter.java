package com.kelin.client.gui.coupons.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.coupons.entity.CouponsEntity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/10.
 */

public class GiftCouponsListAdapter extends MyBaseAdapter {

    private List<CouponsEntity> couponsEntities;
    private boolean isOverdue;

    public GiftCouponsListAdapter(Context context, List<CouponsEntity> couponsEntities, boolean isOverdue) {
        super(context);
        mData = couponsEntities;
        this.isOverdue = isOverdue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_unused_coupons, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        CouponsEntity couponsEntity = (CouponsEntity) mData.get(position);
        holder.tvContent.setText(couponsEntity.couponName);
        holder.tvMoney.setText(String.valueOf(couponsEntity.money));
        if(couponsEntity.isUse){//已使用
            holder.tvTime.setText(mContext.getString(R.string.used_time) + couponsEntity.useTime);
           /* holder.relBg.setBackgroundResource(R.drawable.image_bg);
            holder.tvMoney.setTextColor(Color.parseColor("#0b325a"));
            holder.tvMoneyText.setTextColor(Color.parseColor("#0b325a"));*/
            holder.relBg.setBackgroundResource(R.drawable.image_bg1);
            holder.tvMoney.setTextColor(Color.parseColor("#4f4f4f"));
            holder.tvMoneyText.setTextColor(Color.parseColor("#4f4f4f"));
            holder.tvContent.setTextColor(Color.parseColor("#4f4f4f"));
        }else{
            if(isOverdue){//过期
                holder.tvTime.setText(mContext.getString(R.string.service_time) + couponsEntity.startTime + "\n至" + couponsEntity.endTime+"（"+mContext.getString(R.string.overdueout)+"）");
                holder.relBg.setBackgroundResource(R.drawable.image_bg1);
                holder.tvMoney.setTextColor(Color.parseColor("#4f4f4f"));
                holder.tvMoneyText.setTextColor(Color.parseColor("#4f4f4f"));
                holder.tvContent.setTextColor(Color.parseColor("#4f4f4f"));
            }else{//未过期
                holder.tvTime.setText(mContext.getString(R.string.service_time) + couponsEntity.startTime + "\n至" + couponsEntity.endTime);
                holder.relBg.setBackgroundResource(R.drawable.image_bg);
                holder.tvMoney.setTextColor(Color.parseColor("#0b325a"));
                holder.tvMoneyText.setTextColor(Color.parseColor("#0b325a"));
                holder.tvMoneyText.setTextColor(Color.parseColor("#0b325a"));
            }
        }
        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.tv_content)
        TextView tvContent;

        @BindView(R.id.tv_time)
        TextView tvTime;

        @BindView(R.id.tv_money)
        TextView tvMoney;

        @BindView(R.id.tv_100)
        TextView tvMoneyText;

        @BindView(R.id.rel_bg)
        LinearLayout relBg;

        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
