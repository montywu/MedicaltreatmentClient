package com.kelin.client.gui.live.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kelin.client.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 注释
 * Created by monty on 2017/8/13.
 */

public class NotificationListAdapter extends BaseAdapter {
    private List<String> notifications;
    private LayoutInflater inflater;

    public NotificationListAdapter(Context context, List<String> notifications) {
        this.inflater = LayoutInflater.from(context);
        this.notifications = notifications;

//        if(BuildConfig.DEBUG&& notifications.size()==0){
//            notifications.add("您是**号，前面还有**位患者，需要等待**分钟，请先看看别人的诊断过程，别人是看不见你的哦");
//            notifications.add("轮到您了，欢迎光临。我将竭诚为您服务");
//        }
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Object getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_notification_item, parent, false);
            viewholder = new ViewHolder(convertView);
            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }

        viewholder.tvNotice.setText(notifications.get(position));

        return convertView;
    }

    public void addNotice(String notice){
        notifications.add(notice);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.tv_notice)
        TextView tvNotice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
