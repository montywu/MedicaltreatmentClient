package com.kelin.client.gui.live;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.mydoctor.bean.Doctor;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class CallActivity extends BaseActivity {
    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @BindView(R.id.btn_accept)
    Button btnAccept;
    @BindView(R.id.iv_doctorIcon)
    ImageView ivDoctorIcon;
    @BindView(R.id.tv_doctorName)
    TextView tvDoctorName;
    @BindView(R.id.tv_hospital)
    TextView tvHospital;
    @BindView(R.id.tv_level)
    TextView tvLevel;

    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;
    private CountDownTimer timer;
    private Doctor doctor;


    public static void goToActivityForResult(Activity context, int requestCode,Doctor doctor) {
        Intent intent = new Intent(context, CallActivity.class);
        intent.putExtra("doctor",doctor);
        context.startActivityForResult(intent, requestCode);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);// 保持屏幕常亮
        setContentView(R.layout.activity_call);
        this.doctor = getIntent().getParcelableExtra("doctor");
        ButterKnife.bind(this);
        /*Glide.with(this)
                .load(R.drawable.doctor_photo2)
                //第二个参数是圆角半径，第三个是模糊程度，3-5之间效果较好。
                .bitmapTransform(new BlurTransformation(this, 25, 3))
                .into(ivBackground);*/
        Glide.with(this)
                .load(TextUtils.isEmpty(doctor.getSittingPhotos())?R.drawable.doctor_icon:doctor.getSittingPhotos())
                .bitmapTransform(new CropCircleTransformation(this))
                .into(ivDoctorIcon);
        playRingtoneAndVibrator();
        startCountDown();
    }

    private void startCountDown() {
        /**
         * 倒计时器
         * long millisInFuture 总时长
         * long countDownInterval 间隔
         */
        timer = new CountDownTimer(1000 * 30, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("monty", "还剩 -> " + millisUntilFinished/1000 + "s");
            }

            @Override
            public void onFinish() {
//                btnReject.performClick();
                answer(false);
            }
        }.start();

    }


    private void stopCountDown() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopRingtoneAndVibrator();
        stopCountDown();
    }

   /* // 通知栏通知
    // 返回Notification id
    public static int PlaySound(final Context context) {
        NotificationManager mgr = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification nt = new Notification();
        nt.defaults = Notification.DEFAULT_SOUND;
        int soundId = new Random(System.currentTimeMillis())
                .nextInt(Integer.MAX_VALUE);
        mgr.notify(soundId, nt);
        return soundId;
    }*/

    /**
     * 播放铃声和震动
     */
    private void playRingtoneAndVibrator() {
        try {
            playRingtone(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        startVibrator();
    }

    /**
     * 停止铃声和震动
     */
    private void stopRingtoneAndVibrator() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }
    }


    /**
     * 播放铃声
     *
     * @param context
     * @throws IOException
     */
    private void playRingtone(Context context) throws IOException {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(context, RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_RINGTONE));
        mediaPlayer.prepare();
        mediaPlayer.start();
        mediaPlayer.setLooping(true); //循环播放
    }

    /**
     * 震动
     */
    private void startVibrator() {
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        //按照指定的模式去震动。
        //vibrator.vibrate(1000);
        //数组参数意义：第一个参数为等待指定时间后开始震动，震动时间为第二个参数。后边的参数依次为等待震动和震动的时间
        //第二个参数为重复次数，-1为不重复，0为一直震动
        vibrator.vibrate(new long[]{1000, 1000}, 0);
    }

    @OnClick(R.id.btn_accept)
    public void onViewClicked(View view) {
        /*switch (view.getId()) {
            case R.id.btn_reject:
                setResult(RESULT_CANCELED);
                break;
            case R.id.btn_accept:
                setResult(RESULT_OK);
                break;
        }*/
        answer(true);
    }

    private void answer(boolean isAnswer){
        if(isAnswer){
            setResult(RESULT_OK);
        }else{
            setResult(RESULT_CANCELED);
        }
        stopRingtoneAndVibrator();
        stopCountDown();
        onBackPressed();
    }

}
