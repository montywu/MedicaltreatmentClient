package com.kelin.client.gui.mydoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.widget.MyGridView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by monty on 2017/8/1.
 */

public class DoctorClassifyAdapter extends BaseAdapter implements View.OnClickListener {
    private List<DoctorClassify> doctorClassifies;
    private LayoutInflater inflater;
    private Context context;

    public DoctorClassifyAdapter(Context context, List<DoctorClassify> doctorClassifies) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.doctorClassifies = doctorClassifies;
    }

    @Override
    public int getCount() {
        return doctorClassifies.size();
    }

    @Override
    public Object getItem(int position) {
        return doctorClassifies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_doctor_classify_item, parent, false);
            viewholder = new ViewHolder(convertView);
            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }
        DoctorClassify doctorClassify = doctorClassifies.get(position);
        viewholder.tvClassifyIcon.setText(doctorClassify.getEnglishName());

        viewholder.tvRole.setText(doctorClassify.getRole());
        viewholder.tvPrice.setText("(￥" + doctorClassify.getPrice() + ")");
        viewholder.tvMore.setTag(doctorClassify);
        viewholder.tvMore.setOnClickListener(this);
        viewholder.gvDoctors.setAdapter(new DoctorAdapter(inflater, doctorClassify.getDoctorAttachList(), 4));
        return convertView;
    }

    public void notifyDataSetChanged(List<DoctorClassify> doctorClassifies) {
        this.doctorClassifies = doctorClassifies;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        DoctorClassify doctorClassify = (DoctorClassify) v.getTag();

//        MoreDoctorsActivity.goActivity(context, doctorClassify);
    }

    static class ViewHolder {
        @BindView(R.id.tv_classifyIcon)
        TextView tvClassifyIcon;
        @BindView(R.id.tv_role)
        TextView tvRole;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_more)
        TextView tvMore;
        @BindView(R.id.gv_doctors)
        MyGridView gvDoctors;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
