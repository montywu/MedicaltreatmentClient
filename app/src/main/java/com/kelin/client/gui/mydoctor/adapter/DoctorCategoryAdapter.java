package com.kelin.client.gui.mydoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.gui.mydoctor.MoreDoctorsActivity;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.kelin.client.util.ToastUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by monty on 2018/4/19.
 */

public class DoctorCategoryAdapter extends BaseAdapter{
    private List<TreatmentType> treatmentTypes;
    private LayoutInflater inflater;
    private Context context;

    public DoctorCategoryAdapter(Context context, List<TreatmentType> treatmentTypes) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.treatmentTypes = treatmentTypes;
    }

    @Override
    public int getCount() {
        return treatmentTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return treatmentTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_doctor_category_item, parent, false);
            viewholder = new ViewHolder(convertView);
            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }
        final TreatmentType treatmentType = treatmentTypes.get(position);
        Glide.with(context).load(treatmentType.getPicUrl()).into(viewholder.ivCategory);
        viewholder.ivCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != treatmentType) {
                    MoreDoctorsActivity.goActivity(context, treatmentType);
                } else {
                    ToastUtils.showToast("暂无该分类数据");
                }
            }
        });
        return convertView;
    }

    public void notifyDataSetChanged(List<TreatmentType> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_category)
        ImageView ivCategory;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
