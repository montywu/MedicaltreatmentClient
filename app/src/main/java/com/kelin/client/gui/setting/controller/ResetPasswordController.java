package com.kelin.client.gui.setting.controller;

import android.content.Context;
import android.util.Log;

import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.setting.SettingService;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Response;

/**
 * Created by Administrator on 2017/8/16 0016.
 */

public abstract  class ResetPasswordController {

    private final  String TAG = getClass().getName();
    public abstract void resetPasswordSuccess();
    public abstract void resetPasswordFailure(String message);

    public abstract void forgetPasswordSuccess();
    public abstract void forgetPasswordFailure(String message);

    private Context context;
    private String token;

    public ResetPasswordController(Context context) {
        this.context = context;
        token = MyselfInfo.getLoginUser().getToken();
    }

    //重置密码
    public void resetPassword(String phone, String password, String code) {
        RetrofitHelper.getInstance().createService(SettingService.class).resetPassword(token, phone, password, code).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                Log.i(TAG,"resetPasswordSuccess");
                resetPasswordSuccess();
            }

            @Override
            public void onFailure(String message) {
                Log.i(TAG,"resetPasswordFailure");
                resetPasswordFailure(message);
            }
        });
    }

    //忘记密码
    public void forgetPassword(String phone, String password, String code) {
        RetrofitHelper.getInstance().createService(SettingService.class).userForgotPassword( phone, password, code).enqueue(new BaseCallBack<BaseCallModel<User>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {
                Log.i(TAG,"forgetPasswordSuccess");
               forgetPasswordSuccess();
            }

            @Override
            public void onFailure(String message) {
                Log.i(TAG,"forgetPasswordFailure");
                forgetPasswordFailure(message);
            }
        });
    }
}
