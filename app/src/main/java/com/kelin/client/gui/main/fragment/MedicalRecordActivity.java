package com.kelin.client.gui.main.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.OnMenuToggleListener;
import com.kelin.client.gui.main.adapter.MedicalRecordFragmentAdapter;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by wangbin on 2017/7/9.
 * 医疗记录fragment
 */

public class MedicalRecordActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    Unbinder unbinder;
    private MedicalRecordFragmentAdapter fragmentAdapter;
    private ViewPager viewPager;
    private View line1, line2;
    private RelativeLayout tab1, tab2;
    private TextView tv1, tv2;
    private OnMenuToggleListener onMenuToggleListener;


    public static MedicalRecordActivity createInstance() {
        MedicalRecordActivity fragment = new MedicalRecordActivity();
        return fragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medical_record_fragment_layout);
        unbinder = ButterKnife.bind(this);
        titleBar.showCenterText("RECORDS","治疗记录");
        viewPager = (ViewPager) findViewById(R.id.viewPage);

        initView();

        initViewPage();
    }

    @Override
    protected void initView() {
        tab1 = (RelativeLayout) findViewById(R.id.tab1);
        tab2 = (RelativeLayout) findViewById(R.id.tab2);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        line1 = findViewById(R.id.line_1);
        line2 = findViewById(R.id.line_2);

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(0);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(1);
            }
        });
    }

    private void initViewPage() {
        fragmentAdapter = new MedicalRecordFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);
        setSelect(0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setSelect(int position) {
        reSetAll();
        switch (position) {
            case 0:
                tv1.setTextColor(getResources().getColor(R.color.pink_new));
                line1.setVisibility(View.VISIBLE);
                break;
            case 1:
                tv2.setTextColor(getResources().getColor(R.color.pink_new));
                line2.setVisibility(View.VISIBLE);
                break;
        }
        viewPager.setCurrentItem(position);
    }

    private void reSetAll() {
        tv1.setTextColor(Color.WHITE);
        line1.setVisibility(View.GONE);
        tv2.setTextColor(Color.WHITE);
        line2.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
