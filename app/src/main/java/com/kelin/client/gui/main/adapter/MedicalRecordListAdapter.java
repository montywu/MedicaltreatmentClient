package com.kelin.client.gui.main.adapter;

import android.content.Context;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.main.activity.CheckInfActivity;
import com.kelin.client.gui.main.activity.DiagnoseInfActivity;
import com.kelin.client.gui.main.bean.MedicalRecordBean;
import com.kelin.client.gui.main.bean.MedicalRecordEntity;
import com.kelin.client.util.imageloader.GlideImageHelper;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class MedicalRecordListAdapter extends MyBaseAdapter {

    private List<MedicalRecordBean> medicalRecordBeen;

    public MedicalRecordListAdapter(Context context, List<MedicalRecordBean> medicalRecordBeen) {
        super(context);
        mData = medicalRecordBeen;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_treatment_record, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MedicalRecordListAdapter.ViewHolder) convertView.getTag();
        }
        final MedicalRecordBean entity = (MedicalRecordBean) mData.get(position);

        viewHolder.tvTime.setText(entity.revisitDate);
        viewHolder.tvName.setText(entity.doctor.name);
        viewHolder.tvTreatmentTime.setText("诊治时间：" + entity.revisitDate + " " + entity.revisitStartTime + "--" + entity.revisitEndTime);
        GlideImageHelper.showImage(mContext, entity.doctor.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, viewHolder.imgPhoto);

        viewHolder.btReCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, CheckInfActivity.class);
                intent.putExtra("entity", entity);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.img_user_photo)
        ImageView imgPhoto;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_treatment_time)
        TextView tvTreatmentTime;
        @BindView(R.id.bt_re_check)
        Button btReCheck;

        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
