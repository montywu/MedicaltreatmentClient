package com.kelin.client.gui.address.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.address.EditAddressActivity;
import com.kelin.client.gui.address.MyAddressActivity;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.kelin.client.widget.swipemenulistview.BaseSwipListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/14.
 */

public class AddressListAdapter2 extends BaseSwipListAdapter implements View.OnClickListener {

    private LayoutInflater mLayoutInflater;
    private Context context;
    private List<MyAddressEntity> myAddressEntities;

    public AddressListAdapter2(Context context, List<MyAddressEntity> myAddressEntities) {
        this.context = context;
        this.myAddressEntities = myAddressEntities;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return myAddressEntities.size();
    }

    @Override
    public Object getItem(int position) {
        return myAddressEntities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        {
            ViewHolder holder;
            if (null == convertView) {
//                convertView = mLayoutInflater.inflate(R.base_dilog_layout.item_address, parent, false);
                convertView = View.inflate(context,R.layout.item_address,null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            MyAddressEntity addressEntity =  myAddressEntities.get(position);
            holder.tvName.setText(addressEntity.receiveName);
            holder.tvPhoneNum.setText(addressEntity.receivePhone);
            holder.tvAddress.setText(addressEntity.receiveAddress);
//            holder.relEdit.setOnClickListener(this);
            holder.relEdit.setTag(position);
            return convertView;
        }
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        MyAddressEntity addressEntity = myAddressEntities.get(position);
        Intent intent = new Intent();
        intent.putExtra("addressEntity", addressEntity);
        intent.setClass(context, EditAddressActivity.class);
        ((MyAddressActivity) context).startActivityForResult(intent, 1);

    }

    @Override
    public boolean getSwipEnableByPosition(int position) {
        if(position % 2 == 0){
            return false;
        }
        return true;
    }

    class ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_phone_num)
        TextView tvPhoneNum;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.rel_edit)
        RelativeLayout relEdit;


        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
