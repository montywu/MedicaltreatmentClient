package com.kelin.client.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.ScanTkeLotteryActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.PreferencesManager;

/**
 * Created by kelin on 2017/6/9.
 * 闪屏activity
 */

public class LauncherActivity extends BaseActivity implements View.OnClickListener {

    private boolean isFirst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ImageView imageV = (ImageView) findViewById(R.id.imageV);
        imageV.setImageResource(R.drawable.launcher);
        imageV.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkFirst();
                PreferencesManager.putBoolean("isFirstInstall", false);
            }
        }, 1500);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "nihao", Toast.LENGTH_LONG).show();
    }

    private void checkFirst() {
        isFirst = PreferencesManager.getBoolean("isFirstInstall", true);
        this.finish();
        if (!isFirst) {
            MyselfInfo.startLoginActivity();
        }else{
//            startActivity(new Intent(this,ScanTkeLotteryActivity.class));
            startActivity(new Intent(this,GuiActivity.class));
        }
    }
}
