package com.kelin.client.gui.pay;

/**
 * Created by monty on 2017/9/13.
 */

public class WechatPayEntity {

    /**
     * packageValue : Sign=WechatPayEntity
     * orderId : 91ae477007774d29bc860809967a7339
     * appid : wxe953d5573e5fdf32
     * sign : Sign=3484DC5D3311DD1BB6BE68FA5FF54B38C38C84F5FBC35C2E33F8221DAFDCE515
     * partnerid : 1485908052
     * prepayid : wx2017091011112913d8ccd0260119495068
     * noncestr : 8Q6BnoNjlKH6OrBM
     * timestamp : 1505013072
     */

    private String packageValue;
    private String orderId;
    private String appid;
    private String sign;
    private String partnerid;
    private String prepayid;
    private String noncestr;
    private long timestamp;

    public String getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(String packageValue) {
        this.packageValue = packageValue;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
