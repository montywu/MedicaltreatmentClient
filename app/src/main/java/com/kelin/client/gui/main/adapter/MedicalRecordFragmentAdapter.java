package com.kelin.client.gui.main.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.kelin.client.gui.main.fragment.subclassfragment.CheckFragment;
import com.kelin.client.gui.main.fragment.subclassfragment.DiagnoseFragment;
import com.kelin.client.common.MyFragmentPagerAdapter;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class MedicalRecordFragmentAdapter extends MyFragmentPagerAdapter {


    private int fragmentCuont = 2;

    public MedicalRecordFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DiagnoseFragment.createInstance();

            case 1:
                return CheckFragment.createInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentCuont;
    }
}
