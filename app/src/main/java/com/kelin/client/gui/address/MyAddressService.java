package com.kelin.client.gui.address;

import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.monty.library.http.BaseCallModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/13.
 */

public interface MyAddressService {

    /**
     * 获取我的收货地址页面
     */
    @GET("userAddress/get")
    Call<BaseCallModel<List<MyAddressEntity>>> getAddressList(@Query("token") String token);


    /**
     * 添加我的收货地址页面
     */
    @FormUrlEncoded
    @POST("userAddress/add")
    Call<BaseCallModel<MyAddressEntity>> addAddressList(@Field("token") String token,
                                                              @Field("receiveName") String receiveName,
                                                              @Field("receivePhone") String receivePhone,
                                                              @Field("area") String area,
                                                              @Field("receiveAddress") String receiveAddress,
                                                              @Field("flag") boolean flag);


    /**
     * 编辑我的收货地址页面
     */
    @FormUrlEncoded
    @POST("userAddress/edit")
    Call<BaseCallModel<List<MyAddressEntity>>> editAddressList(@Field("token") String token,
                                                               @Field("addressId") int addressId,
                                                               @Field("receiveName") String receiveName,
                                                               @Field("receivePhone") String receivePhone,
                                                               @Field("area") String area,
                                                               @Field("receiveAddress") String receiveAddress,
                                                               @Field("flag") boolean flag);
    /**
     * 删除我的收货地址页面
     * (post或者get都行)
     */
    @FormUrlEncoded
    @POST("userAddress/delete")
    Call<BaseCallModel<List<MyAddressEntity>>> delAddressList(@Field("token") String token,
                                                              @Field("addressId") int addressId);

    /**
     * 设置默认收货地址
     * (post或者get都行)
     */
    @FormUrlEncoded
    @POST("userAddress/set")
    Call<BaseCallModel<List<MyAddressEntity>>> setDefaultAddressList(@Field("token") String token,
                                                                     @Field("addressId") int addressId,
                                                                     @Field("flag") boolean flag);
}
