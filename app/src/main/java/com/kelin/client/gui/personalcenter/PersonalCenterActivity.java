package com.kelin.client.gui.personalcenter;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.personalcenter.controller.PersonalCenterController;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.util.VerificationUtil;
import com.kelin.client.util.imageloader.GlideImageHelper;
import com.kelin.client.widget.BaseTitleBar;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 用户个人中心
 */
public class PersonalCenterActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.titleBar)
    public BaseTitleBar titleBar;
    @BindView(R.id.tv_iconText)
    public TextView tvIconText;
    @BindView(R.id.iv_picture)
    public ImageView ivPicture;
    @BindView(R.id.tv_nameText)
    public TextView tvNameText;
    @BindView(R.id.tv_name)
    public EditText tvName;
    @BindView(R.id.tv_telphoneText)
    public TextView tvTelphoneText;
    @BindView(R.id.tv_telphone)
    public EditText tvTelphone;
    @BindView(R.id.tv_sexText)
    public TextView tvSexText;
    @BindView(R.id.rg_sex)
    public RadioGroup rgSex;
    @BindView(R.id.tv_ageText)
    public TextView tvAgeText;
    @BindView(R.id.tv_age)
    public TextView tvAge;
    @BindView(R.id.tv_regionText)
    public TextView tvRegionText;
    @BindView(R.id.tv_region)
    public TextView tvRegion;
    @BindView(R.id.rdo_man)
    public RadioButton rdoMan;
    @BindView(R.id.rdo_woman)
    public RadioButton rdoWoman;
    @BindView(R.id.ll_age)
    public LinearLayout llAge;

    private PersonalCenterController controller;
    public String photoPath = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("gg", "PersonalInfActivity onActivityResult =====");
        if (resultCode == RESULT_CANCELED && requestCode == 1) {
            ToastUtils.showToast("取消拍照或相机异常");
            return;
        } else if (resultCode == RESULT_CANCELED && requestCode == 100) {
            ToastUtils.showToast("取消图片选择");
            return;
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                case 100:
                    File file = null;
                    String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                    if (requestCode == 1) {
                        file = new File(path + "/" + String.valueOf(requestCode) + ".jpg");//拍照文件位置
                    } else {
                        if (null == data || null == data.getData()) {
                            return;
                        }
                        file = new File(com.monty.library.FileUtil.getPath(this, data.getData()));//图库文件

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 4;

                        Bitmap bitmap = null;
                        if (requestCode == 1) {
                            bitmap = BitmapFactory.decodeFile(path + "/" + String.valueOf(requestCode) + ".jpg", options);
                            if (null == bitmap) {
                                return;
                            }
                        } else {
                            if (null != data && null != data.getData()) {
                                try {
                                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        ivPicture.setImageBitmap(bitmap);

                        controller.uploadPhoto(requestCode, file, 1);
                        break;
                    }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        verifyStoragePermissions(this);


        setContentView(R.layout.activity_personal_center);
        ButterKnife.bind(this);

        initController();

        initTitle();

        initView();


    }

    private void initController() {
        controller = new PersonalCenterController(this) {
            @Override
            public void savePersonalInfSuccess() {
                ToastUtils.showToast(R.string.save_inf_success);
                finish();
            }

            @Override
            public void savePersonalInfFailure(String message) {
                ToastUtils.showToast(message);
            }

            public void uploadPhotoSuccess(int imgId, String path) {
                GlideImageHelper.showImage(PersonalCenterActivity.this, path, -1, -1, ivPicture);
                MyselfInfo.getLoginUser().setPhoto(path);
                ToastUtils.showToast("头像上传成功");
            }

            @Override
            public void uploadPhotoFailure(String msg) {
                ToastUtils.showToast("头像上传失败");
            }

            @Override
            public void showLoadingDialog() {
                PersonalCenterActivity.this.showLoadingDialog("");
            }

            @Override
            public void closeLoadingDialog() {
                PersonalCenterActivity.this.closeLoadingDialog();
            }
        };
    }

    private void initTitle() {
        this.titleBar.showCenterText(R.string.person_data, R.drawable.personal_ata, 0);
        this.titleBar.setBackBtnVis(true);
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.titleBar.setRightText("保存", 0, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = tvName.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    ToastUtils.showToast("请输入姓名");
                    return;
                }
                String phoneNum = tvTelphone.getText().toString().trim();

                if (!VerificationUtil.isValidTelNumber(phoneNum)) {
                    ToastUtils.showToast("请输入正确的手机号");
                    return;
                }
                String gender = rdoMan.isChecked() ? "男" : "女";
                String ageStr = tvAge.getText().toString().trim();
                int age = 0;
                if (TextUtils.isEmpty(ageStr)) {
                    ToastUtils.showToast("请选择年龄");
                    return;
                } else {
                    age = Integer.valueOf(ageStr);
                }
                String area = tvRegion.getText().toString().trim();
                if (TextUtils.isEmpty(area)) {
                    ToastUtils.showToast("请设置地区");
                    return;
                }
                controller.savePersonalInf(name, phoneNum, gender, age, area);
            }
        });
    }

    @Override
    protected void initView() {

        this.rgSex.setOnCheckedChangeListener(this);

        User user = MyselfInfo.getLoginUser();

        tvName.setText(user.getName());
        tvTelphone.setText(user.getPhone());
        if (!TextUtils.isEmpty(user.getGender())) {
            if (user.getGender().equals("男")) {
                rdoMan.setChecked(true);
            } else {
                rdoWoman.setChecked(true);
            }
        }
        tvAge.setText(user.getAge());
        tvRegion.setText(user.getArea());

        GlideImageHelper.showImage(this, user.getPhoto(), R.mipmap.ic_launcher, R.mipmap.ic_launcher, ivPicture);

    }


    @OnClick({R.id.iv_picture/*, R.id.tv_name, R.id.tv_telphone*/, R.id.ll_age, R.id.tv_region})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_picture:
                controller.showGetPhotoPop(1);
                break;
//            case R.id.tv_name:
//                controller.shwoResetNamePop();
//                break;
//            case R.id.tv_telphone:
//                controller.shwoResetPhoneNumPop();
//                break;
            case R.id.ll_age:
                controller.showAgeOptions();
                break;
            case R.id.tv_region:
                controller.showCityChoies();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.CAMERA"
    };

    private void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
