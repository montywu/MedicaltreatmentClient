package com.kelin.client.gui.login.presenters.viewinterface;

/**
 * Created by monty on 2017/7/31.
 */

public interface ILoginView extends ILoadingDialogView {
    void startMainActivity();
    void startBindPhoneNumFragment(String openid,int flag,String photo);
    void finish();
}
