/*
package com.kelin.client.gui.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.kelin.client.R;
import com.kelin.client.gui.OnMenuToggleListener;
import com.kelin.client.gui.coupons.CouponsActivity;
import com.kelin.client.gui.live.DoctorVideoActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.adapter.AdAdapter;
import com.kelin.client.gui.mydoctor.adapter.DoctorAdapter2;
import com.kelin.client.gui.mydoctor.adapter.GridAdapter;
import com.kelin.client.gui.mydoctor.bean.BaseItem;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.kelin.client.gui.mydoctor.presenters.DoctorPresenter;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IDoctorView;
import com.kelin.client.widget.BaseTitleBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;


*/
/**
 * Created by wangbin on 2017/7/9.
 * 我的医生fragment
 *//*


public class MyDoctorFragment2 extends Fragment implements IDoctorView, DoctorAdapter2.OnMoreClickListener, DoctorAdapter2.OnItemClickListener {
    Unbinder unbinder;

    @BindView(R.id.roll_view_pager)
    RollPagerView rollViewPager;
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.diseaseList)
    GridView diseaseList;
    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;
    @BindView(R.id.lv_doctor_classify)
    RecyclerView recyclerView;
    @BindView(R.id.scrollLayout)
    ScrollView scrollLayout;
    @BindView(R.id.btn_search)
    TextView btnSearch;
//    @BindView(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;

    private DoctorPresenter doctorPresenter;

    private GridAdapter diseaseAdapter;

    private AdAdapter adAdapter;


    //    private DoctorClassifyAdapter doctorClassifyAdapter;
    private DoctorAdapter2 doctorAdapter;

    private List<BaseItem> list = new ArrayList<>();

    private OnMenuToggleListener onMenuToggleListener;

    public static MyDoctorFragment2 createInstance() {
        MyDoctorFragment2 fragment = new MyDoctorFragment2();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doctorPresenter = new DoctorPresenter(this);
    }

    @Override
    public void onAttach(Context context) {
        onMenuToggleListener = (OnMenuToggleListener) context;
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mydoctor_fragment_layout, null);
        unbinder = ButterKnife.bind(this, view);
        titleBar.showCenterText("", R.drawable.home_logo, 0);
        titleBar.setBackBtnImg(R.drawable.home_icon_personal);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuToggleListener.onMenuToggle();
            }
        });

        titleBar.setRightText("领券", 0, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CouponsActivity.class));
            }
        });

        initADPager();

        diseaseAdapter = new GridAdapter(getContext(), MyselfInfo.getTreatmentTypeId());

        diseaseList.setAdapter(diseaseAdapter);
        diseaseAdapter.setOnItemClickListener(new GridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mPtrFrame.autoRefresh();
//                swipeRefreshLayout.setRefreshing(true);
            }
        });
//        doctorClassifyAdapter = new DoctorClassifyAdapter(getContext(),new ArrayList<DoctorClassify>());

//        lvDoctorClassify.setAdapter(doctorClassifyAdapter);
        doctorAdapter = new DoctorAdapter2(getContext(), list);
        doctorAdapter.setOnMoreClickListener(this);
        doctorAdapter.setOnItemClickListener(this);
//        FullyGridLayoutManager gridLayoutManager = new FullyGridLayoutManager(getContext(),2);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() { // 动态改变Grid中的每项占几个位置
            @Override
            public int getSpanSize(int position) {
                if (list.get(position).getType() == BaseItem.DOCTOR_CLASSIFY || list.get(position).getType() == BaseItem.FOOTER) {
                    return 2;
                }
                return 1;
            }
        });

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(doctorAdapter);

        initPullRefresh();

        */
/*diseaseList.setLayoutManager(new FullyGridLayoutManager(getContext(), 4));
        DiseaseAdapter adapter = new DiseaseAdapter(getContext());
        diseaseList.setAdapter(adapter);*//*


        return view;
    }

    private void initPullRefresh() {
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                doctorPresenter.getAllTreatmentType(MyselfInfo.getLoginUser().getToken());
//            }
//        });


        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, scrollLayout, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                doctorPresenter.getAllTreatmentType(MyselfInfo.getLoginUser().getToken());
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initADPager() {
        //设置播放时间间隔
        rollViewPager.setPlayDelay(3000);
        //设置透明度
        rollViewPager.setAnimationDurtion(500);
        adAdapter = new AdAdapter(rollViewPager);
        //设置适配器
        rollViewPager.setAdapter(adAdapter);

        //mRollViewPager.setHintView(new IconHintView(this, R.drawable.point_focus, R.drawable.point_normal));
        rollViewPager.setHintView(new ColorPointHintView(this.getContext(), Color.parseColor("#f8b9cd"), R.color.black_cc000000));

        //mRollViewPager.setHintView(new TextHintView(this));
        //mRollViewPager.setHintView(null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getTreatmentTypeSuccess(List<TreatmentType> treatmentTypeList) {
//        diseaseAdapter.setSelectIndex(0);
        diseaseAdapter.notifyDataSetChanged(treatmentTypeList);
        for (TreatmentType treatmentType : treatmentTypeList) {
            if (MyselfInfo.getTreatmentTypeId() == treatmentType.getId()) {
                adAdapter.notifyData(Arrays.asList(treatmentType.getOtherPicture()));
                break;
            }
        }
        doctorPresenter.getRoomList(MyselfInfo.getLoginUser().getToken(), MyselfInfo.getTreatmentTypeId(), 4);
    }

    @Override
    public void getDoctorRoomSuccess(List<BaseItem> doctorClassifies) {
        this.list = doctorClassifies;
        doctorAdapter.notifyDataSetChanged(doctorClassifies);
        mPtrFrame.refreshComplete();
//        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void refreshComplete() {
        mPtrFrame.refreshComplete();
//        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onMoreClick(View view, DoctorClassify doctorClassify, int position) {
        MoreDoctorsActivity.goActivity(getContext(), doctorClassify);
    }

    @Override
    public void onItemClick(View view, Doctor doctor, int position) {
        DoctorVideoActivity.startLiveActivity(getContext(), doctor);
    }

    @OnClick(R.id.btn_search)
    public void onViewClicked() {
        SearchDoctorsActivity.goActivity(getContext());
    }
}
*/
