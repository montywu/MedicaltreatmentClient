package com.kelin.client.gui.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.adapter.DoctorAdapter;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.presenters.DoctorPresenter;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IMoreDoctorsView;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by monty on 2017/8/9.
 * 更多医生
 */
public class FreeDoctorsActivity extends BaseActivity implements IMoreDoctorsView {

    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;

    public static void goActivity(Context context, DoctorClassify doctorClassify) {
        Intent intent = new Intent(context, FreeDoctorsActivity.class);
        intent.putExtra("doctorClassify", doctorClassify);
        context.startActivity(intent);
    }

    @BindView(R.id.gv_doctors)
    GridView gvDoctors;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;

    private DoctorPresenter presenter;

    private DoctorAdapter doctorAdapter;

    private DoctorClassify doctorClassify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_doctors);
        ButterKnife.bind(this);
        initView();
        initData();

        doctorClassify = getIntent().getParcelableExtra("doctorClassify");

        this.titleBar.showCenterText(doctorClassify.getRole(), R.drawable.expert2, 0);
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mPtrFrame.autoRefresh();
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, gvDoctors, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                presenter.getFreeRoomList(MyselfInfo.getLoginUser().getToken(), doctorClassify.getId(), "",1);
//                doctorPresenter.getAllTreatmentType(MyselfInfo.getLoginUser().getToken());
//                mPtrFrame.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mPtrFrame.refreshComplete();
//                    }
//                }, 100);
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    @Override
    protected void initView() {
        btnConfirm.setVisibility(View.GONE);
        initPullRefresh();
    }

    @Override
    protected void initData() {
        presenter = new DoctorPresenter(this);
        doctorAdapter = new DoctorAdapter(LayoutInflater.from(this), new ArrayList<Doctor>());
        gvDoctors.setAdapter(doctorAdapter);

    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {

    }

    @Override
    public void getDoctorRoomSuccess(List<Doctor> doctors) {
        mPtrFrame.refreshComplete();
        doctorAdapter.notifyDataSetChanged(doctors);
    }

    @Override
    public void getDoctorRoomFailure(String errorMsg) {
        ToastUtils.showToast("请求失败:" + errorMsg);
    }

    @Override
    public void refreshComplete() {

    }
}
