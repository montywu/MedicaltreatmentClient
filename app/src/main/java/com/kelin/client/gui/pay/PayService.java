package com.kelin.client.gui.pay;

import com.monty.library.http.BaseCallModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/9/13.
 */

public interface PayService {
    /**
     *
     * @param token
     * @param consumeType 付款类型 0-微信 1-支付宝
     * @param couponIds 优惠券ID集合,多个用逗号隔开
     * @param diagnosticId 诊断记录ID
     * @return
     */
    @FormUrlEncoded
    @POST("record/payDiagnostic")
    Call<BaseCallModel<WechatPayEntity>> wechatPayDiagnostic(@Field("token") String token, @Field("consumeType") int consumeType, @Field("couponIds") String couponIds, @Field("diagnosticId") long diagnosticId);
    /**
     *
     * @param token
     * @param consumeType 付款类型 0-微信 1-支付宝
     * @param couponIds 优惠券ID集合,多个用逗号隔开
     * @param diagnosticId 诊断记录ID
     * @return
     */
    @FormUrlEncoded
    @POST("record/payDiagnostic")
    Call<BaseCallModel<AliPayEntity>> aliPayDiagnostic(@Field("token") String token, @Field("consumeType") int consumeType, @Field("couponIds") String couponIds, @Field("diagnosticId") long diagnosticId);

    /**
     * 使用现金券支付诊金
     * @param token
     * @param diagnosticId 诊断记录ID
     * @param couponIds 现金券ID集合，多个用英文逗号隔开
     * @return
     */
    @FormUrlEncoded
    @POST("record/payDiagnosticByCoupon")
    Call<BaseCallModel<AliPayEntity>> payDiagnosticByCoupon(@Field("token") String token, @Field("diagnosticId") long diagnosticId, @Field("couponIds") String couponIds);
    /**
     * 取消支付
     * @param token
     * @param orderId 诊断ID
     * @return
     */
    @GET("record/cancelPay")
    Call<BaseCallModel<PayResult>>  cancelPay(@Query("token") String token, @Query("orderId") String orderId);

    /**
     * 结束连麦之后，用户获取当前诊断记录的信息，包括诊断记录ID、诊断费、可使用的现金券等。
     * @param token
     * @param diagnosticId 诊断ID
     * @return
     */
    @GET("userAttach/getDiagnostic")
    Call<BaseCallModel<DiagnosticInfo>>  getDiagnostic(@Query("token") String token, @Query("diagnosticId") String diagnosticId);
    /**
     * 结束连麦之后，用户获取当前诊断记录的信息，包括诊断记录ID、诊断费、可使用的现金券等。
     * @param token
     * @return
     */
    @GET("userAttach/getDiagnostic")
    Call<BaseCallModel<DiagnosticInfo>>  getDiagnostic(@Query("token") String token);

    /**
     * 查询支付结果
     * @param token
     * @param orderId 诊断ID
     * @return
     */
    @GET("record/getPayResult")
    Call<BaseCallModel<PayResult>>  getPayResult(@Query("token") String token, @Query("orderId") String orderId);
}
