package com.kelin.client.gui.mydoctor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.gui.mydoctor.bean.BaseItem;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.util.DisplayUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 注释
 *
 * @e-mail mwu@szrlzz.com
 * Created by monty on 2017/8/18.
 */

public class DoctorAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DoctorAdapter2";

    private List<Doctor> list;
    private LayoutInflater inflater;
    private Context context;
    private RecyclerView recyclerView;

    public DoctorAdapter2(Context context, List<Doctor> list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
        this.recyclerView = recyclerView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder viewType -> " + viewType);
        RecyclerView.ViewHolder viewHolder = null;
        View view = inflater.inflate(R.layout.layout_doctor_item, parent, false);
        viewHolder = new DcotorViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder position -> " + position);
        DcotorViewHolder viewHolder = (DcotorViewHolder) holder;
        final Doctor doctor = list.get(position);
        String photo = doctor.getSittingPhotos();
        Glide.with(context).load(TextUtils.isEmpty(photo) ? R.drawable.doctor_photo2 : photo).centerCrop().into(viewHolder.ivDoctorPhoto);
        viewHolder.tvDoctorName.setText(doctor.getName());
        viewHolder.tvDoctorLevel.setText(doctor.getRole());
        viewHolder.tvDoctorLevelEn.setText(doctor.getEnglishName());
        viewHolder.tvPrice.setText("￥" + doctor.getPrice() + "/次");
        setTextSpannable(viewHolder.tvWaitingCount, doctor.getWaitCount() + "", "人等待");
        setTextSpannable(viewHolder.tvWaitingDuration, doctor.getWaitCount() * 5 + "", "分钟");
//        viewHolder.tvWaitingCount.setText(Html.fromHtml("<span style=\"color:#ffffff\">" + doctor.getWaitCount() + "</span><span style=\"color:#F5B8D3\">人等待</span>"));
//        viewHolder.tvWaitingDuration.setText(Html.fromHtml("<span style=\"color:#F5B8D3\">约</span><span style=\"color:#ffffff\">" + doctor.getWaitCount() * 5 + "</span><span style=\"color:#F5B8D3\">分钟</span>"));
        viewHolder.tvHistoryCount.setText(doctor.getCount() + "人已经面诊");
        viewHolder.tvHospital.setText(doctor.getLocalHospital());

        viewHolder.tvEducation.setText("");
        viewHolder.tvGood.setText("擅长:"+doctor.getTreatmentName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, doctor, position);
            }
        });

        ViewGroup.LayoutParams layoutParams = viewHolder.itemView.getLayoutParams();
        layoutParams.height = (recyclerView.getHeight()- DisplayUtil.dp2px(context,30)) / 3;
        viewHolder.itemView.setLayoutParams(layoutParams);
    }

    private void setTextSpannable(TextView textView, String text, String lastText) {
        textView.setText("");
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan countTextSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.pink_new));
        spannableString.setSpan(countTextSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.append(spannableString);
        spannableString=new SpannableString(lastText);
        ForegroundColorSpan lastTextSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.black));
        spannableString.setSpan(lastTextSpan, 0, lastText.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.append(spannableString);
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId -> " + position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType -> " + position);
        BaseItem baseItem = list.get(position);
        if (baseItem instanceof Doctor) {
            Doctor doctor = (Doctor) baseItem;
            if (doctor.isLeft()) {
                return BaseItem.DOCTOR_LEFT;
            }
            if (doctor.isRight()) {
                return BaseItem.DOCTOR_RIGHT;
            }
        }
        return list.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void notifyDataSetChanged(List<Doctor> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnMoreClickListener {
        void onMoreClick(View view, DoctorClassify doctorClassify, int position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Doctor doctor, int position);
    }


    static class DcotorViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_doctorPhoto)
        ImageView ivDoctorPhoto;
        @BindView(R.id.tv_enter_room)
        TextView tvEnterRoom;
        @BindView(R.id.tv_waitingCount)
        TextView tvWaitingCount;
        @BindView(R.id.tv_waitingDuration)
        TextView tvWaitingDuration;
        @BindView(R.id.tv_doctorLevelEn)
        TextView tvDoctorLevelEn;
        @BindView(R.id.tv_doctorLevel)
        TextView tvDoctorLevel;
        @BindView(R.id.tv_doctorName)
        TextView tvDoctorName;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_hospital)
        TextView tvHospital;
        @BindView(R.id.tv_historyCount)
        TextView tvHistoryCount;
        @BindView(R.id.tv_good)
        TextView tvGood;
        @BindView(R.id.tv_education)
        TextView tvEducation;
       /* @BindView(R.id.v_top_margin)
        View topMargin;
        @BindView(R.id.v_bottom_margin)
        View bottomMargin;*/

        DcotorViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static void setPaddings(View v, int l, int t, int r, int b) {
        v.setPadding(l, t, r, b);
        v.requestLayout();
    }
}
