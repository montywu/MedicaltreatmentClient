package com.kelin.client.bean;

import com.bigkoo.pickerview.model.IPickerViewData;

import java.util.List;

/**
 * 省份
 * Created by monty on 2017/7/25.
 */

public class ProvinceBean implements IPickerViewData {

    public long id;
    public String name;
    public String description;
    public String others;

    public ProvinceBean(long id,String name,String description,String others){
        this.id = id;
        this.name = name;
        this.description = description;
        this.others = others;
    }
    @Override
    public String getPickerViewText() {
        return name;
    }
}
