package com.kelin.client.db;

import com.kelin.client.gui.login.bean.User;

import org.greenrobot.greendao.AbstractDao;

import java.util.List;

/**
 * 当前登录用户数据库表管理类
 * Created by monty on 2017/7/24.
 */

public class UserDBManager extends AbstractDatabaseManager<User, Long> {
    @Override
    AbstractDao<User, Long> getAbstractDao() {
        return daoSession.getUserDao();
    }

    public boolean saveUser(User user) {
        // 保存之前先清除表中用户数据，保证此表中永远只有一条用户数据
        deleteAll();
        return insertOrReplace(user);
    }

    public User getUser() {
        List<User> users = loadAll();
        if (users != null && users.size() == 1) {
            return users.get(0);
        }
        return null;
    }
}
