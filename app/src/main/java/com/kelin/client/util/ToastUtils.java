package com.kelin.client.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.widget.Toast;

import com.kelin.client.MyApplication;

/**
 * Created by monty on 2017/7/21.
 */

public class ToastUtils {
    private static Toast toast = null;
    private static String oldMsg;
    private static long oneTime = 0;
    private static long twoTime = 0;


    public static void showToast(@StringRes int message) {
        showToast(MyApplication.getApplication(), message);
    }

    public static void showToast(Context context, @StringRes int message) {
        String msg = context.getString(message);
        showToast(msg);
    }

    public static void showToast(String message) {
        if(TextUtils.isEmpty(message)){
            return;
        }
        showToast(MyApplication.getApplication(), message);
    }

    public static void showToast(Context context, String message) {
        if (toast == null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (message.equals(oldMsg)) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast.show();
                }
            } else {
                oldMsg = message;
                toast.setText(message);
                toast.show();
            }
        }

        oneTime = twoTime;
    }
}
