package com.kelin.client.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 用户信息相关配置sp存储
 *
 * @author kelin
 */
public class UserPrefs {


    private static UserPrefs instance;

    private SharedPreferences prefs;

    public UserPrefs(Context mContext) {
        prefs = mContext.getSharedPreferences("com.kelin.user", 0);
    }

    public static UserPrefs getInstance(Context mContext) {
        if (instance == null) {
            instance = new UserPrefs(mContext);
        }
        return instance;
    }


}
