package com.kelin.client.util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyUtils;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * @author monty
 * @package com.kelin.client.util
 * @date 2018/4/10  下午10:11
 * @description TODO
 * @org www.szrlzz.com 深圳市瑞联智造科技有限公司
 * @email mwu@szrlzz.com
 */

public class FontUtil {
    public static String FONT_HYG5GJM = "font/hyg5gim.ttf";
    public static String FONT_ZHSRXT_GBK2_0 = "font/ZHSRXT_GBK2_0.ttf";

    public static boolean setFontWithHyg5gjm(Context context, TextView textView) {
        return setTypeFace(textView, getFontHyg5gim(context));
    }

    public static boolean setTypeFace(TextView textView, Typeface typeface) {
        return CalligraphyUtils.applyFontToTextView(textView, typeface);
    }

    public static Typeface getFontHyg5gim(Context context) {
        return TypefaceUtils.load(context.getAssets(), FONT_HYG5GJM);
    }

    public static boolean setFontWithZhsrxtGbk20(Context context, TextView textView) {
        return setTypeFace(textView, getZhsrxtGbk20(context));
    }

    public static Typeface getZhsrxtGbk20(Context context) {
        return TypefaceUtils.load(context.getAssets(), FONT_ZHSRXT_GBK2_0);
    }
}
