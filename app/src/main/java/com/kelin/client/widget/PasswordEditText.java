package com.kelin.client.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.util.DeviceInfoUtil;

import uk.co.chrisjenx.calligraphy.TypefaceUtils;


/**
 * Created by monty on 2017/7/12.
 */

public class PasswordEditText extends LinearLayout {
    private EditText edit;
    private CheckBox btnShow;
    private ImageButton btnDelete;

    public PasswordEditText(Context context) {
        super(context);
        init();
    }

    public PasswordEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PasswordEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PasswordEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setBackgroundResource(R.drawable.edit_bg);

        addEditText();

        addDeleteButton();

        addShowButton();

        setTypeFace(edit);

    }

    private void setTypeFace(TextView view){
        Typeface typeface = TypefaceUtils.load(getContext().getAssets(),"font/ZHSRXT_GBK2_0.ttf");
        view.setTypeface(typeface);
    }

    private static final int width = 30;

    private void addShowButton() {
        btnShow = new CheckBox(getContext());
        btnShow.setButtonDrawable(R.drawable.cb_showpwd);
        btnShow.setBackground(null);
        LayoutParams layoutParams = new LayoutParams(DeviceInfoUtil.dip2px(getContext(), width), DeviceInfoUtil.dip2px(getContext(), width));
        layoutParams.leftMargin = DeviceInfoUtil.dip2px(getContext(), 8);
        btnShow.setLayoutParams(layoutParams);
        btnShow.setPadding(0,0,0,0);
        btnShow.setChecked(false);
        btnShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (edit != null) {
                    if (isChecked) {
                        edit.setInputType(EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    } else {
                        edit.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
                    }
                    edit.setSelection(edit.length());
                    setTypeFace(edit);
                }
            }
        });
        addView(btnShow);
    }

    private void addDeleteButton() {
        btnDelete = new ImageButton(getContext());
        LayoutParams layoutParams = new LayoutParams(DeviceInfoUtil.dip2px(getContext(), width), DeviceInfoUtil.dip2px(getContext(), width));
        layoutParams.gravity = Gravity.CENTER;
        btnDelete.setLayoutParams(layoutParams);
        btnDelete.setImageResource(R.drawable.reg_icon_close);
        btnDelete.setPadding(0,0,0,0);
        btnDelete.setBackground(null);
        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit != null) {
                    edit.getText().clear();
                }
            }
        });
        addView(btnDelete);
        if (edit != null && edit.length() == 0) {
            btnDelete.setVisibility(GONE);
        }

    }

    private void addEditText() {
        edit = new EditText(getContext());
        edit.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        edit.setPadding(0,0,0,0);
        edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reg_icon_password, 0, 0, 0);
        edit.setHint("请设置6-20位密码");
        edit.setMaxLines(20);
        edit.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        edit.setCompoundDrawablePadding(DeviceInfoUtil.dip2px(getContext(), 8));
        edit.setBackground(null);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (btnDelete != null) {
                    if (s.length() == 0) {
                        btnDelete.setVisibility(GONE);
                    } else {
                        btnDelete.setVisibility(VISIBLE);
                    }
                }
            }
        });
        edit.setTextSize(18);
        addView(edit);
    }
    public void setHint(String hint){
        edit.setHint(hint);
    }
    public void setDrawableLeft(@DrawableRes int left){
        edit.setCompoundDrawablesWithIntrinsicBounds(left, 0, 0, 0);
    }
    public void setPassword(String text){
        edit.setText(text);
    }
    public String getPassword(){
        return edit.getText().toString();
    }

    public EditText getEditText() {
        return edit;
    }
}

