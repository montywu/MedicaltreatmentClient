package com.kelin.client.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.util.DeviceInfoUtil;

import uk.co.chrisjenx.calligraphy.TypefaceUtils;


/**
 * Created by monty on 2017/7/14.
 */

public class VerifyCodeEditText extends LinearLayout {

    private EditText edit;
    private Button btnVerifyCode;

    //new倒计时对象,总共的时间,每隔多少秒更新一次时间
    final MyCountDownTimer myCountDownTimer = new MyCountDownTimer(60000, 1000);

    public VerifyCodeEditText(Context context) {
        super(context);
        init();
    }

    public VerifyCodeEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VerifyCodeEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VerifyCodeEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setBackgroundResource(R.drawable.edit_bg);

        addEditText();
        addLine();
        addVerifyCodeButton();
        btnVerifyCode.setTextColor(getResources().getColor(R.color.pink_new));
        setTypeFace(edit);
        setTypeFace(btnVerifyCode);
    }

    private void setTypeFace(TextView view){
        Typeface typeface = TypefaceUtils.load(getContext().getAssets(),"font/ZHSRXT_GBK2_0.ttf");
        view.setTypeface(typeface);
    }

    private void addVerifyCodeButton() {
        btnVerifyCode = new Button(getContext());
        btnVerifyCode.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
        btnVerifyCode.setText("获取验证码");
        btnVerifyCode.setBackground(null);
        btnVerifyCode.setGravity(Gravity.CENTER);
        btnVerifyCode.setPadding(DeviceInfoUtil.dip2px(getContext(), 14), 0, DeviceInfoUtil.dip2px(getContext(), 14), 0);
        addView(btnVerifyCode);
    }

    private void addLine() {
        View line = new View(getContext());
        line.setLayoutParams(new LayoutParams(DeviceInfoUtil.dip2px(getContext(), 0.5f), LayoutParams.MATCH_PARENT));
        line.setBackgroundColor(Color.parseColor("#dddddd"));
        addView(line);
    }

    private void addEditText() {
        edit = new EditText(getContext());
        edit.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        edit.setPadding(0, 0, 0, 0);
        edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reg_icon_safe, 0, 0, 0);
        edit.setHint("请输入手机验证码");
        edit.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_CLASS_NUMBER);
        edit.setCompoundDrawablePadding(DeviceInfoUtil.dip2px(getContext(), 8));
        edit.setBackground(null);
        edit.setTextSize(18);
        addView(edit);
    }
    public void setDrawableLeft(@DrawableRes int left){
        edit.setCompoundDrawablesWithIntrinsicBounds(left, 0, 0, 0);
    }


    public String getVerifyCode() {
        return edit.getText().toString();
    }

    public void setOnVerifyCodeButtonClickListener(final OnVerifyCodeButtonClickListener onClickListener) {
        if (btnVerifyCode != null) {
            btnVerifyCode.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener.onClick(v))
                        myCountDownTimer.start();
                }
            });
        }
    }

    Runnable runable = new Runnable() {
        @Override
        public void run() {
            btnVerifyCode.postDelayed(runable, 1000);

        }
    };

    public interface OnVerifyCodeButtonClickListener {
        boolean onClick(View v);
    }

    private class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        //计时过程
        @Override
        public void onTick(long l) {
            //防止计时过程中重复点击
            btnVerifyCode.setClickable(false);
            btnVerifyCode.setText(l / 1000 + "s");
        }

        //计时完毕的方法
        @Override
        public void onFinish() {
            //重新给Button设置文字
            btnVerifyCode.setText("获取验证码");
            //设置可点击
            btnVerifyCode.setClickable(true);
        }
    }
}
