package com.kelin.client.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.IContentLayout;


/**
 * 界面层级布局管理器，定义了6个层：{@link #LAYER_LOADING}, {@link #LAYER_CONTENT},
 * {@link #LAYER_EMPTY}, {@link #LAYER_SERVER_ERROR},
 * {@link #LAYER_NETWORK_ERROR}, {@link #LAYER_NO_NETWORK}
 */
public class ContentLayout extends FrameLayout implements IContentLayout {

    protected View loadingView;

    protected View contentView;

    protected View emptyView;

    protected View serverErrorView;

    protected View networkErrorView;

    protected View noNetworkView;

    protected OnReloadListener onReloadListener;

    protected OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onReloadListener != null) {
                setViewLayer(LAYER_LOADING);
                onReloadListener.onReload();
            }
        }
    };

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public ContentLayout(Context context) {
        super(context);
    }

    public ContentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 指定显示某层视图，如果该层视图之前已经显示过了，可能会处于当前显示的层下面，<br>
     * 会将其从FrameLayout中删除，再重新加入到FrameLayout最上层即可覆盖在最上面显示，<br>
     * 主要用在LAYER_LOADING层需要覆盖显示在LAYER_CONTENT之上
     *
     * @param layer
     */
    public void showViewLayer(int layer) {
        View view = getView(layer);
        // 删除再重新加进去，就可以显示在最上层了
        removeView(view);
        addView(view);
        view.setVisibility(View.VISIBLE);
    }

    /**
     * 指定隐藏某层视图
     *
     * @param layer
     */
    public void hideViewLayer(int layer) {
        View view = getView(layer);
        view.setVisibility(View.GONE);
    }

    /**
     * 指定显示某层视图，并隐藏其他层
     *
     * @param layer
     */
    public void setViewLayer(int layer) {
        View view = getView(layer);

        boolean isFound = false;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child == view) {
                isFound = true;
            } else {
                child.setVisibility(View.GONE);
            }
        }
        if (!isFound) {
            addView(view);
        }
        view.setVisibility(View.VISIBLE);
    }

    public View getView(int layer) {
        View view = null;
        switch (layer) {
            case LAYER_LOADING:
                view = getLoadingView();
                break;
            case LAYER_CONTENT:
                view = getContentView();
                break;
            case LAYER_EMPTY:
                view = getEmptyView();
                break;
            case LAYER_SERVER_ERROR:
                view = getServerErrorView();
                break;
            case LAYER_NETWORK_ERROR:
                view = getNetworkErrorView();
                break;
            case LAYER_NO_NETWORK:
                view = getNoNetworkView();
                break;
        }
        return view;
    }

    public View getLoadingView() {
        if (loadingView == null) {
            loadingView = inflate(getContext(), R.layout.base_layout_loading,
                    null);
            loadingView.setVisibility(View.GONE);
        }
        return loadingView;
    }

    public void setLoadingView(View loadingView) {
        this.loadingView = loadingView;
    }

    public View getContentView() {
        if (contentView == null && getChildCount() > 0) {
            contentView = getChildAt(0);
        }
        return contentView;
    }

    public void setContentView(View contentView) {
        this.contentView = contentView;
    }

    public View getEmptyView() {
        if (emptyView == null) {
            emptyView = inflate(getContext(), R.layout.base_layout_empty, null);
            emptyView.findViewById(R.id.refresh_try_again).setOnClickListener(onClickListener);
        }
        return emptyView;
    }

    /**
     * 如果LAYER_EMPTY层布局需要自定义，布局替换了，这个方法可能会返回null
     *
     * @return
     */
    public TextView getEmptyMainView() {
        View view = getEmptyView();
        return (TextView) view.findViewById(R.id.tv_main);
    }

    /**
     * 如果LAYER_EMPTY层布局需要自定义，布局替换了，这个方法可能会返回null
     *
     * @return
     */
    public TextView getEmptyTipsView() {
        View view = getEmptyView();
        return (TextView) view.findViewById(R.id.tv_tips);
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }

    public View getServerErrorView() {
        if (serverErrorView == null) {
            serverErrorView = inflate(getContext(),
                    R.layout.base_layout_server_error, null);
            serverErrorView.findViewById(R.id.refresh_try_again)
                    .setOnClickListener(onClickListener);
        }
        return serverErrorView;
    }

    public void setServerErrorView(View serverErrorView) {
        this.serverErrorView = serverErrorView;
    }

    public View getNetworkErrorView() {
        if (networkErrorView == null) {
            networkErrorView = inflate(getContext(),
                    R.layout.base_layout_network_error, null);
            networkErrorView.findViewById(R.id.refresh_try_again)
                    .setOnClickListener(onClickListener);
        }
        return networkErrorView;
    }

    public void setNetworkErrorView(View networkErrorView) {
        this.networkErrorView = networkErrorView;
    }

    public View getNoNetworkView() {
        if (noNetworkView == null) {
            noNetworkView = inflate(getContext(),
                    R.layout.base_layout_no_network, null);
            noNetworkView.findViewById(R.id.refresh_try_again)
                    .setOnClickListener(onClickListener);
        }
        return noNetworkView;
    }

    public void setNoNetworkView(View noNetworkView) {
        this.noNetworkView = noNetworkView;
    }

    public OnReloadListener getOnReloadListener() {
        return onReloadListener;
    }

    public void setOnReloadListener(OnReloadListener onReloadListener) {
        this.onReloadListener = onReloadListener;
    }

    public interface OnReloadListener {
        void onReload();
    }
}
