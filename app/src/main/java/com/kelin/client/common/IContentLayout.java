package com.kelin.client.common;

import android.view.View;

/**
 * ContentLayout接口
 */
public interface IContentLayout {
    int LAYER_LOADING = 0;

    int LAYER_CONTENT = 1;

    int LAYER_EMPTY = 2;

    int LAYER_SERVER_ERROR = 3;

    int LAYER_NETWORK_ERROR = 4;

    int LAYER_NO_NETWORK = 5;

    void setViewLayer(int layer);

    void showViewLayer(int layer);

    void hideViewLayer(int layer);

    View getView(int layer);
}
