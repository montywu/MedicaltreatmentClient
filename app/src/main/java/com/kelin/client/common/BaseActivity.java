package com.kelin.client.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kelin.client.gui.login.presenters.viewinterface.ILoadingDialogView;
import com.kelin.client.widget.dialog.LoadingDialog;
import com.monty.library.AppManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by kelin on 2017/6/9.
 * activity基类
 */

public abstract class BaseActivity extends AppCompatActivity implements ILoadingDialogView,View.OnClickListener{
    private LoadingDialog dialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        AppManager.getAppManager().removeActivity(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void showLoadingDialog(String msg) {
        if (dialog == null) {
            dialog = new LoadingDialog(this);
        }
        dialog.show(msg);
    }
    @Override
    public void closeLoadingDialog() {
        dialog.close();
    }

    protected void initView(){}

    protected void initData(){}


}
