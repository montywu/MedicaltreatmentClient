
package com.hellodoctor.facerecord.entity;

/**
 * 记录片排名
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-19]
 */
public class RecordRankingEnt {

    /** 视频路径 */
    public String videoUrl;

    /** 视频预览图 */
    public String previewUrl;

    /** 视频最后一张图 */
    public String preFinalUrl;

    /** 点赞数量 */
    public int praiseAmount;

    /** 是否收藏（1=是 0=否） */
    public int isCollect;

    /** 是否点赞（1=是 0=否） */
    public int isPraise;

    /** 编号 */
    public int id;

    /** 标题 */
    public String title;

    /** 收藏数 */
    public int collectAmount;

    /** 价值 */
    public double worth;

    @Override
    public RecordRankingEnt clone() {
        try {
            return (RecordRankingEnt) super.clone();
        } catch (CloneNotSupportedException e) {
            // Ignore
        }
        RecordRankingEnt rankingEnt = new RecordRankingEnt();
        rankingEnt.videoUrl = this.videoUrl;
        rankingEnt.previewUrl = this.previewUrl;
        rankingEnt.preFinalUrl = this.preFinalUrl;
        rankingEnt.praiseAmount = this.praiseAmount;
        rankingEnt.isCollect = this.isCollect;
        rankingEnt.isPraise = this.isPraise;
        rankingEnt.id = this.id;
        rankingEnt.collectAmount = this.collectAmount;
        rankingEnt.worth = this.worth;
        return rankingEnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecordRankingEnt that = (RecordRankingEnt) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
