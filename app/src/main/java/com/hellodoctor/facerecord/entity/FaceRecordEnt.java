
package com.hellodoctor.facerecord.entity;

import com.hellodoctor.facerecord.common.entity.BaseEntity;

/**
 * 颜值记录实体
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-08]
 */
public class FaceRecordEnt extends BaseEntity implements Cloneable {

    /** 纪录片编号 */
    public int id;
    /** 用户编号 */
    public int userId;
    /** 标题 */
    public String title;
    /** 视频路径 */
    public String videoUrl;
    /** 视频天数 */
    public int videoDays;
    /** 是否连续（1是，2否） */
    public int isContinuous;
    /** 生成时间 */
    public String generateTime;
    /** 价值 */
    public double worth;
    /** 状态（gen生成(撤回视频）)  aud审核  ref拒绝  rel发布  del删除） */
    public String status;
    /** 审核人 */
    public String auditName;
    /** 审核时间 */
    public String auditTime;
    /** 拒绝原因 */
    public String refuseReason;
    /** 发布时间 */
    public String releaseTime;
    /** 收藏数量 */
    public int collectAmount;
    /** 点赞数量 */
    public int praiseAmount;
    /** 点赞数更新时间 */
    public String praiseTime;
    /** 视频预览图 */
    public String previewUrl;
    /** 视频最后一张图 */
    public String preFinalUrl;
    /** 是否收藏（1=是 0=否） */
    public int isCollect;
    /** 是否点赞（1=是 0=否） */
    public int isPraise;

    @Override
    public FaceRecordEnt clone() {
        try {
            return (FaceRecordEnt) super.clone();
        } catch (CloneNotSupportedException e) {
            // Ignore
        }
        FaceRecordEnt faceRecordEnt = new FaceRecordEnt();
        faceRecordEnt.id = this.id;
        faceRecordEnt.userId = this.userId;
        faceRecordEnt.title = this.title;

        faceRecordEnt.videoUrl = this.videoUrl;
        faceRecordEnt.videoDays = this.videoDays;
        faceRecordEnt.isContinuous = this.isContinuous;

        faceRecordEnt.generateTime = this.generateTime;
        faceRecordEnt.worth = this.worth;
        faceRecordEnt.status = this.status;

        faceRecordEnt.auditName = this.auditName;
        faceRecordEnt.auditTime = this.auditTime;
        faceRecordEnt.refuseReason = this.refuseReason;

        faceRecordEnt.releaseTime = this.releaseTime;
        faceRecordEnt.collectAmount = this.collectAmount;
        faceRecordEnt.praiseAmount = this.praiseAmount;

        faceRecordEnt.praiseTime = this.praiseTime;
        faceRecordEnt.previewUrl = this.previewUrl;
        faceRecordEnt.preFinalUrl = this.preFinalUrl;

        faceRecordEnt.isCollect = this.isCollect;
        faceRecordEnt.isPraise = this.isPraise;
        return faceRecordEnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FaceRecordEnt that = (FaceRecordEnt) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
