
package com.hellodoctor.facerecord.entity;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class FavoriteEnt {

    /** 视频路径 */
    public String video_url;

    /** 用户id */
    public int user_id;

    /** 收藏数量 */
    public int collect_amount;

    /** 标题 */
    public String title;

    /** 视频id */
    public int did;

    /** 收藏id */
    public int cid;

    /** 状态（ gen生成  aud审核  ref拒绝  rel发布  del删除） */
    public String status;

    /** 发布时间 */
    public String release_time;

    /** 点赞数量 */
    public int praise_amount;

    /** 视频预览图 */
    public String preview_url;

    /** 视频最后一张图 */
    public String pre_final_url;

    /** 价值 */
    public double worth;

}
