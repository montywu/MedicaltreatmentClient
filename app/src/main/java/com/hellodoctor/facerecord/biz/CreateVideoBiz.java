package com.hellodoctor.facerecord.biz;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-23]
 */
public class CreateVideoBiz {

    public boolean saveVideoToServer(FaceRecordEnt faceRecordEnt) {
        try {
            RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
            String token = MyselfInfo.getLoginUser().getToken();

            Map<String, String> params = new HashMap<>();
            params.put("token", token);
            params.put("days", String.valueOf(faceRecordEnt.videoDays));
            params.put("title", faceRecordEnt.title);
            params.put("videoUrl", faceRecordEnt.videoUrl);
            params.put("isContinuous", String.valueOf(faceRecordEnt.isContinuous));
            params.put("previewUrl", faceRecordEnt.previewUrl);
            params.put("preFinalUrl", faceRecordEnt.preFinalUrl);

            Call<BaseCallModel<Double>> generateVideo = service.generateVideo(params);
            Response<BaseCallModel<Double>> response = generateVideo.execute();
            if (response == null || response.code() != 200) {
                return false;
            }
            BaseCallModel<Double> body = response.body();
            if (body == null || body.data == null) {
                return false;
            }
            faceRecordEnt.worth = body.data;
            return true;
        } catch (Exception e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        }
        return false;
    }

}
