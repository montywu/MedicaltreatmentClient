
package com.hellodoctor.facerecord.biz;

import android.text.TextUtils;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.common.utils.Pic2VideoUtils;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.isoftstone.mis.mmsdk.common.intf.UiTask;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.util.ArrayList;
import java.util.List;

/**
 * 图片合成视频
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class Pic2VideoBiz {

    /**
     * 合成视频（同步）
     * @param pictures
     * @param audioPath
     * @param outputPath
     * @return
     */
    public boolean compose(List<PictureEnt> pictures, String audioPath, String outputPath) {
        if (pictures == null || pictures.size() == 0) {
            MMLog.it(LogTag.COMPOSE_VIDEO, "picturePaths is empty or null.");
            return false;
        }

        ArrayList<String> picPaths = new ArrayList<>();
        for (PictureEnt picture : pictures) {
            if (!TextUtils.isEmpty(picture.localPath)) {
                picPaths.add(picture.localPath);
            }
        }
        if (picPaths.size() == 0) {
            MMLog.it(LogTag.COMPOSE_VIDEO, "picturePaths is empty or null.");
            return false;
        }

        String picConfigPath = VideoConstants.PIC_CONFIG_PATH;
        if (!Pic2VideoUtils.writePicConfig(picConfigPath, picPaths, String.valueOf(VideoConstants.FRAME_DURATION))) {
            MMLog.it(LogTag.COMPOSE_VIDEO, "write picture paths failure.");
            return false;
        }

        // 视频时长
        long duration = (long) (picPaths.size() * VideoConstants.FRAME_DURATION + VideoConstants.FRAME_DURATION);
        return Pic2VideoUtils.composeVideo(picConfigPath, audioPath, outputPath, duration);
    }

    /**
     * 合成视频（异步）
     * @param pictures
     * @param audioPath
     * @param outputPath
     * @return
     */
    public void compose(List<PictureEnt> pictures, String audioPath, String outputPath, OnComposeListener listener) {
        if (pictures == null || pictures.size() == 0) {
            MMLog.it(LogTag.COMPOSE_VIDEO, "picturePaths is empty or null.");
            return;
        }

        ArrayList<String> picPaths = new ArrayList<>();
        for (PictureEnt picture : pictures) {
            picPaths.add(picture.localPath);
        }
        new ComposeVideoTask(picPaths, listener).start(VideoConstants.PIC_CONFIG_PATH, audioPath, outputPath);
    }

    public interface OnComposeListener {

        void composeResult(boolean result);

    }

    private static class ComposeVideoTask extends UiTask<String, Void, Boolean> {

        private OnComposeListener listener;

        private ArrayList<String> picturePaths;

        ComposeVideoTask(ArrayList<String> pictures, OnComposeListener listener) {
            this.picturePaths = pictures;
            this.listener = listener;
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            if (strings.length < 3) {
                MMLog.it(LogTag.COMPOSE_VIDEO, "compose params losses.");
                return false;
            }

            String picConfigPath = strings[0];
            if (!Pic2VideoUtils.writePicConfig(picConfigPath, picturePaths, String.valueOf(VideoConstants.FRAME_DURATION))) {
                MMLog.it(LogTag.COMPOSE_VIDEO, "write picture paths failure.");
                return false;
            }

            String audioPath = strings[1];
            String outputPath = strings[2];
            // 视频时长
            long duration = (long) (picturePaths.size() * 0.5 + 0.5);
            return Pic2VideoUtils.composeVideo(picConfigPath, audioPath, outputPath, duration);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (listener != null) {
                listener.composeResult(result);
            }
        }
    }

}
