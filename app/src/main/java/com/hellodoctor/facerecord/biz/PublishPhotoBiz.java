package com.hellodoctor.facerecord.biz;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-25]
 */
public class PublishPhotoBiz {

    public boolean savePhotoToServer(String photoUrl) {
        try {
            RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
            String token = MyselfInfo.getLoginUser().getToken();

            Call<BaseCallModel> addImage = service.addImage(photoUrl, token);
            Response<BaseCallModel> response = addImage.execute();
            if (response == null || response.code() != 200) {
                return false;
            }
            BaseCallModel body = response.body();
            if (body == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            MMLog.et(LogTag.TAKE_PHOTO, e);
        }
        return false;
    }

}
