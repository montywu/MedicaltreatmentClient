package com.hellodoctor.facerecord.biz;

import com.hellodoctor.facerecord.common.utils.FaceDetectorUtils;
import com.isoftstone.mis.mmsdk.common.intf.UiTask;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class TakePhotoBiz {

    public void checkFace(String picPath, OnCheckFaceListener listener) {
        new CheckFaceTask(listener).start(picPath);
    }

    public interface OnCheckFaceListener {

        void checkResult(int result);

    }

    private static class CheckFaceTask extends UiTask<String, Void, Integer> {

        private OnCheckFaceListener listener;

        CheckFaceTask(OnCheckFaceListener listener) {
            this.listener = listener;
        }

        @Override
        protected Integer doInBackground(String... strings) {
            return FaceDetectorUtils.isFaceAreaValid(strings[0]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (listener != null) {
                if (result == null) {
                    listener.checkResult(FaceDetectorUtils.CODE_FACE_NOT_FOUND);
                } else {
                    listener.checkResult(result);
                }
            }
        }
    }

}
