
package com.hellodoctor.facerecord.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * 拍照工具类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class TakePhotoUtils {

    private TakePhotoUtils() {
    }

    public static void setPreviewSize(Camera.Parameters parameters, int screenWidth, int screenHeight) {
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
//        int[] a = new int[sizes.size()];
//        int[] b = new int[sizes.size()];
//        for (int i = 0; i < sizes.size(); i++) {
//            int supportH = sizes.get(i).height;
//            int supportW = sizes.get(i).width;
//            a[i] = Math.abs(supportW - screenHeight);
//            b[i] = Math.abs(supportH - screenWidth);
//            MMLog.it(LogTag.TAKE_PHOTO, "supportW:" + supportW + "supportH:" + supportH);
//        }
//        int minW = 0, minA = a[0];
//        for (int i = 0; i < a.length; i++) {
//            if (a[i] <= minA) {
//                minW = i;
//                minA = a[i];
//            }
//        }
//        int minH = 0, minB = b[0];
//        for (int i = 0; i < b.length; i++) {
//            if (b[i] < minB) {
//                minH = i;
//                minB = b[i];
//            }
//        }

        Camera.Size cs = sizes.get(0);
        int realWidth = cs.width;
        int realHeight = cs.height;

        float viewScale = screenWidth * 1.0F / screenHeight;
        float targetScale = cs.width * 1.0F / cs.height;
        for (int i = 0; i < sizes.size(); i++) {
            int supportH = sizes.get(i).height;
            int supportW = sizes.get(i).width;
            float supportScale = supportH * 1.0F / supportW;
            if (supportScale - viewScale < targetScale - viewScale) {
                targetScale = supportScale;
                realWidth = supportW;
                realHeight = supportH;
            }

            MMLog.it(LogTag.TAKE_PHOTO, "supportW:" + supportW + ", supportH:" + supportH);
        }

        MMLog.it(LogTag.TAKE_PHOTO, "viewW:" + screenWidth + ", viewH:" + screenHeight);
        MMLog.it(LogTag.TAKE_PHOTO, "preview size:" + realWidth + "x" + realHeight);

        // 设置预览图像大小
        parameters.setPreviewSize(realWidth, realHeight);

        List<Integer> list = parameters.getSupportedPreviewFrameRates();
        parameters.setPreviewFrameRate(list.get(list.size() - 1));
    }

    public static String savePhoto(Context context, byte[] data, String saveDir) {
        if (data == null || data.length == 0) {
            MMLog.it(LogTag.TAKE_PHOTO, "Picture data is null or empty");
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        if (bitmap == null) {
            MMLog.it(LogTag.TAKE_PHOTO, "Picture decode is null");
            return null;
        }
        bitmap = BitmapUtils.rotateBitmapByDegree(bitmap, -90);
        drawWatermark(context, bitmap);
        try {
            String fileName = DateUtils.getCurrDate(PhotoConstants.PHOTO_TIME_PATTERN)
                    + PhotoConstants.PHOTO_FILE_SUFFIX;
            File file = new File(saveDir, fileName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            MediaResUtils.notifyScanFile(context, file);
            bitmap.recycle();
            return file.getAbsolutePath();
        } catch (Exception e) {
            MMLog.et(LogTag.TAKE_PHOTO, e);
        }
        return null;
    }

    private static void drawWatermark(Context context, Bitmap bitmap) {
        if (bitmap == null) {
            return;
        }

        Canvas canvas = new Canvas(bitmap);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setTextSize(35);
        paint.setAlpha(160);

        // 测量绘制拍照日期
        String photoDate = DateUtils.getCurrDate(PhotoConstants.PHOTO_PREVIEW_PATTERN);
        float photoDateLength = paint.measureText(photoDate);
        float xDate = width - photoDateLength - 35;
        float yDate = height - 38;
        canvas.drawText(photoDate, xDate, yDate, paint);

        // 测量绘制“颜值记录”
        String faceRecord = context.getString(R.string.face_record);
        paint.setTextSize(35);
        float faceRecordLength = paint.measureText(faceRecord);
        float xFace = width - faceRecordLength - 35 - (photoDateLength - faceRecordLength) / 2;
        float yFace = height - 45 - (height - yDate);
        canvas.drawText(faceRecord, xFace, yFace, paint);
    }

}
