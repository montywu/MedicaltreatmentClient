
package com.hellodoctor.facerecord.common.utils;

import com.hellodoctor.facerecord.common.constants.AppDirConstants;
import com.hellodoctor.facerecord.common.filedownload.HttpFileDownload;
import com.isoftstone.mis.mmsdk.common.utils.encryption.MD5;

import java.io.File;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-22]
 */
public class MusicUtils {

    /** 保存音乐文件的后缀 */
    private static final String MUSIC_SUFFIX = ".mp3";

    /**
     * 下载音乐文件
     * @param musicUrl 文件访问地址
     * @return 返回下载成功的文件，下载失败返回null
     */
    public static File download(String musicUrl) {
        String musicName = convertUrlToKey(musicUrl) + MUSIC_SUFFIX;
        File musicFile = new File(AppDirConstants.MUSICS_DIR, musicName);
        if (!musicFile.getParentFile().exists()) {
            boolean mkdirs = musicFile.getParentFile().mkdirs();
            if (!mkdirs) {
                return null;
            }
        }

        // 判断本地是否已经存在此音乐，如存在直接返回
        if (musicFile.exists() && musicFile.length() > 0) {
            return musicFile;
        }

        // 音乐不存在 ，下载
        HttpFileDownload fileDownload = new HttpFileDownload();
        if (fileDownload.download(musicUrl, musicFile.getAbsolutePath())) {
            return musicFile;
        }
        return null;
    }

    /**
     * 根据文件url生成缓存的key(名字)
     *
     * @param url 文件url
     * @return 缓存key
     */
    private static String convertUrlToKey(String url) {
        return MD5.getMD5Str(url);
    }

}
