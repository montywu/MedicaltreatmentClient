
package com.hellodoctor.facerecord.common.entity;

/**
 * 网络请求基类
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-10]
 */
public abstract class NetBaseEntity<T> extends BaseEntity {

    public String content;

    /** 请求响应码 */
    public String msgCode;

    public T retObj;

    /** 请求结果 */
    public boolean success;

    /** 提示消息 */
    public String msg;

}
