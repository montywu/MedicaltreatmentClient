package com.hellodoctor.facerecord.common.utils;

import java.text.DecimalFormat;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */

public class MoneyUtils {

    public static String formatMoney(double money) {
        DecimalFormat df = new DecimalFormat("0");
        return df.format(money);
    }

}
