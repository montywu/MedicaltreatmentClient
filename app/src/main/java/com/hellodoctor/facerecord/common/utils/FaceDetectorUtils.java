
package com.hellodoctor.facerecord.common.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.text.TextUtils;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

/**
 * 人脸检测工具类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class FaceDetectorUtils {

    /** 最大检测的人脸数 */
    private static final int MAX_FACE_COUNT = 1;

    /** 检测人脸成功 */
    public static final int CODE_SUCCESS = 0;
    /** 没有检测到人脸 */
    public static final int CODE_FACE_NOT_FOUND = 1;
    /** 脸部距离太远 */
    public static final int CODE_TOO_FAR = 2;
    /** 脸部距离太近 */
    public static final int CODE_TOO_CLOSE = 3;

    /**
     * 检测人脸是否在有效区域内
     * @param picPath 图片路径
     * @return 返回true表示照片人脸在有效区域内
     */
    public static int isFaceAreaValid(String picPath) {
        if (TextUtils.isEmpty(picPath)) {
            return CODE_FACE_NOT_FOUND;
        }

        // 构造位图生成的参数，必须为565。类名+enum
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap myBitmap = BitmapFactory.decodeFile(picPath, options);
        if (myBitmap == null) {
            return CODE_FACE_NOT_FOUND;
        }

        int imageWidth = myBitmap.getWidth();
        int imageHeight = myBitmap.getHeight();

        // 分配人脸数组空间
        FaceDetector.Face[] myFace = new FaceDetector.Face[MAX_FACE_COUNT];
        FaceDetector myFaceDetect = new FaceDetector(imageWidth, imageHeight, MAX_FACE_COUNT);

        // FaceDetector 构造实例并解析人脸
        // 实际检测到的人脸数
        int realFaceCount = myFaceDetect.findFaces(myBitmap, myFace);
        if (realFaceCount <= 0 || realFaceCount > MAX_FACE_COUNT) {
            return CODE_FACE_NOT_FOUND;
        }

        FaceDetector.Face face = myFace[0];
        if (face == null) {
            return CODE_FACE_NOT_FOUND;
        }

        PointF myMidPoint = new PointF();
        face.getMidPoint(myMidPoint);

        // 图像中心点
        int picCenterX = imageWidth / 2;
        int picCenterY = imageHeight / 2;

        // 得到人脸中心点和眼间距离参数，并对每个人脸进行画框
        float myEyesDistance = face.eyesDistance();
        MMLog.it(LogTag.TAKE_PHOTO, "myEyesDistance:" + myEyesDistance);
        if (myEyesDistance > 250) {
            return CODE_TOO_CLOSE;
        }

        if (myEyesDistance < 160) {
            return CODE_TOO_FAR;
        }

        RectF targetFaceArea = new RectF(picCenterX - 50, picCenterY - 50, picCenterX + 50, picCenterY + 50);
        if (targetFaceArea.contains(myMidPoint.x, myMidPoint.y)) {
            return CODE_SUCCESS;
        }
        return CODE_FACE_NOT_FOUND;

//
//        // 检测出来的人脸所在区域
//        RectF realFaceArea = new RectF(myMidPoint.x - myEyesDistance,
//                myMidPoint.y - myEyesDistance,
//                myMidPoint.x + myEyesDistance,
//                myMidPoint.y + myEyesDistance);
//
//        // 目标人脸所在区域
//        int faceWidth = imageWidth * 6 / 9;
//        int picCenterX = imageWidth / 2;
//        int startAreaX = picCenterX - faceWidth / 2;
//        int endAreaX = picCenterX + faceWidth / 2;
//        int startAreaY = imageHeight * 3 / 8;
//        int endAreaY = startAreaY + faceWidth;
//        RectF targetFaceArea = new RectF(startAreaX, startAreaY, endAreaX, endAreaY);
//
//        // 判断检测出的人脸位置是否在目标人脸区域内
//        return targetFaceArea.contains(realFaceArea);
    }

}
