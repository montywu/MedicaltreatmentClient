
package com.hellodoctor.facerecord.common.utils;

import com.hellodoctor.facerecord.entity.RecordRankingEnt;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-04-01]
 */
public class RecordSortUtils {

    public static void sortRanking(List<RecordRankingEnt> rankingEnts) {
        if (rankingEnts == null || rankingEnts.size() == 0) {
            return;
        }
        Collections.sort(rankingEnts, new RankingComparator());
    }

    static class RankingComparator implements Comparator<RecordRankingEnt> {

        @Override
        public int compare(RecordRankingEnt o1, RecordRankingEnt o2) {
            if (o1 == null || o2 == null) {
                return 0;
            }
            return o1.collectAmount - o2.collectAmount >= 0 ? -1 : 1;
        }
    }

}
