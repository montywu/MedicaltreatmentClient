
package com.hellodoctor.facerecord.common.entity;

import java.io.Serializable;

/**
 * 实体对象超类
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
public class BaseEntity implements Serializable {
}
