
package com.hellodoctor.facerecord.common.constants;

/**
 * 日志tag
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class LogTag {

    private LogTag() {
    }

    /** 拍照 */
    public static final String TAKE_PHOTO = "take_photo";

    /** 图片合成视频 */
    public static final String COMPOSE_VIDEO = "compose_video";

    /** 文件上传 */
    public static final String FILE_UPLOAD = "file_upload";

    /** 轮播图 */
    public static final String BANNER = "banner";

    /** 记录排名 */
    public static final String RANKING = "ranking";

    /** 我的颜值记录 */
    public static final String FACE_RECORD = "face_record";

    /** 播放视频 */
    public static final String PLAY_VIDEO = "play_video";

}
