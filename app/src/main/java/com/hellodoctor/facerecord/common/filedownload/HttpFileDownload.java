
package com.hellodoctor.facerecord.common.filedownload;

import android.os.Handler;
import android.os.Looper;

import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * http文件下载
 *
 * @author hubing
 * @version [1.0.0.0, 2017-03-07]
 */
public class HttpFileDownload implements Callback {

    /** LOG标记 */
    private static final String TAG = "HttpFileDownload";

    /** 文件服务器地址 */
    private String remoteFileUrl;

    /** 下载下来的文件保存地址 */
    private String localFilePath;

    private Handler mHandler;

    private Call call;

    private OnDownloadListener mOnDownloadListener;

    public HttpFileDownload() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * 下载文件（同步执行）
     *
     * @param remoteFileUrl 服务器文件地址
     * @param localFilePath 下载下来的文件本地保存地址
     */
    public boolean download(String remoteFileUrl, String localFilePath) {
        // 创建请求
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Request.Builder request = new Request.Builder();
        request.url(remoteFileUrl).tag(TAG).get();
        call = builder.build().newCall(request.build());

        // 执行请求
        OutputStream os = null;
        try {
            Response response = call.execute();
            if (!response.isSuccessful()) {
                return false;
            }
            // 保存下载的文件
            InputStream is = response.body().byteStream();
            os = new FileOutputStream(localFilePath);
            int lenght;
            byte[] buff = new byte[8192];
            while ((lenght = is.read(buff)) != -1) {
                os.write(buff, 0, lenght);
            }
            os.flush();
            // 下载成功
            return true;
        } catch (IOException e) {
            MMLog.et(TAG, e, "下载文件出错：" + remoteFileUrl);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
        return false;
    }
    
    /**
     * 下载文件
     * 
     * @param remoteFileUrl 服务器文件地址
     * @param localFilePath 下载下来的文件本地保存地址
     * @param listener 下载监听
     */
    public void download(String remoteFileUrl, String localFilePath, OnDownloadListener listener) {
        this.remoteFileUrl = remoteFileUrl;
        this.localFilePath = localFilePath;
        this.mOnDownloadListener = listener;

        // 创建请求
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Request.Builder request = new Request.Builder();
        request.url(remoteFileUrl).tag(TAG).get();
        call = builder.build().newCall(request.build());

        // 执行请求
        call.enqueue(this);
    }

    @Override
    public void onFailure(Call call, IOException e) {
        sendFailure(remoteFileUrl, e);
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        // 判断请求结果
        if (call.isCanceled()) {
            throw new IOException("请求被取消");
        }
        if (!response.isSuccessful()) {
            throw new IOException("request failed , reponse's code is : " + response.code());
        }

        // 文件总大小
        long totalSize = response.body().contentLength();
        // 文件当前下载大小
        long currentSize = 0;

        // 开始下载
        updateProgress(totalSize, currentSize);

        // 保存下载的文件
        InputStream is = response.body().byteStream();
        OutputStream os = null;
        try {
            os = new FileOutputStream(localFilePath);
            int lenght;
            byte[] buff = new byte[8192];
            while ((lenght = is.read(buff)) != -1) {
                os.write(buff, 0, lenght);
                currentSize += lenght;

                // 更新下载进度
                updateProgress(totalSize, currentSize);
            }
            os.flush();

            // 下载完成
            currentSize = totalSize;
            updateProgress(totalSize, currentSize);
            sendSuccess(remoteFileUrl, localFilePath);
        } catch (Exception e) {
            throw new IOException("保存下载文件出错", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
    }

    private void updateProgress(final long totalSize, final long currentSize) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnDownloadListener != null) {
                    mOnDownloadListener.onProgressUpdate(totalSize, currentSize);
                }
            }
        });
    }

    private void sendSuccess(final String remoteFileUrl, final String localFilePath) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnDownloadListener != null) {
                    mOnDownloadListener.onSuccess(remoteFileUrl, localFilePath);
                }
            }
        });
    }

    private void sendFailure(final String remoteFileUrl, final Exception e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnDownloadListener != null) {
                    mOnDownloadListener.onFailure(remoteFileUrl, e);
                }
            }
        });
    }

    /**
     * Cancels the request, if possible. Requests that are already complete cannot be canceled.
     */
    public void cancel() {
        if (call != null) {
            call.cancel();
        }
    }

    /**
     * http文件下载监听器，回调方法在UI线程中
     *
     * @author hubing
     * @version [1.0.0.0, 2017-03-07]
     */
    public interface OnDownloadListener {

        /**
         * 下载成功
         * 
         * @param remoteFileUrl 服务器文件地址
         * @param localFilePath 本地保存文件地址
         */
        void onSuccess(String remoteFileUrl, String localFilePath);

        /**
         * 下载失败
         * 
         * @param remoteFileUrl 服务器文件地址
         * @param e 异常对象
         */
        void onFailure(String remoteFileUrl, Exception e);

        /**
         * 下载进度更新
         * 
         * @param totalSize 下载文件总大小
         * @param currentSize 当前文件大小
         */
        void onProgressUpdate(long totalSize, long currentSize);

    }

}
