package com.hellodoctor.facerecord.common.utils;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-14]
 */
public class DateUtils {

    public static String secondToMinute(long second) {
        String timeStr = "";
        int min = (int) (second / 60);
        int s = (int) (second % 60);
        timeStr = min + "分" + s + "秒";
        return timeStr;
    }

    public static String getCurrDate(String pattern) {
        return format(pattern, System.currentTimeMillis());
    }

    public static String format(String pattern, long time) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        return sdf.format(new Date(time));
    }

    public static long parseDate(String pattern, String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        }
        return System.currentTimeMillis();
    }

}
