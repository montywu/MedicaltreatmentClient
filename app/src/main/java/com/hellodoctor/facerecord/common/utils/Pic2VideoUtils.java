
package com.hellodoctor.facerecord.common.utils;

import android.text.TextUtils;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.ys.test.testffmpeg.FFmpegManager;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * 图片合成视频工具类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class Pic2VideoUtils {

    public static boolean writePicConfig(String picConfigPath, List<String> picturePaths, String frameDuration) {
        if (TextUtils.isEmpty(picConfigPath) || picturePaths == null || picturePaths.size() == 0) {
            return false;
        }

        File file = new File(picConfigPath);
        if (!file.getParentFile().exists()) {
            boolean mkdirs = file.getParentFile().mkdirs();
            if (!mkdirs) {
                return false;
            }
        }

        FileWriter writer = null;
        try {
            writer = new FileWriter(picConfigPath, false);
            for (String picPath : picturePaths) {
                writer.append("file '").append(picPath).append("'").append("\n");
                writer.append("duration ").append(frameDuration).append("\n");
            }
            // 最后一个图要重复写一遍，但不用加duration。
            writer.append("file '").append(picturePaths.get(picturePaths.size() - 1)).append("'");
            writer.flush();
            return true;
        } catch (Exception e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (Exception e) {
                MMLog.et(LogTag.COMPOSE_VIDEO, e);
            }
        }
        return false;
    }

    /**
     * 图片、音频合成视频
     * @param picConfigPath 图片配置文件路径
     * @param audioPath 音频路径
     * @param outputPath 合成的视频输出路径
     * @param duration 合成的视频时长
     * @return 合成成功返回true
     */
    public static boolean composeVideo(String picConfigPath, String audioPath, String outputPath, long duration) {
        if (TextUtils.isEmpty(picConfigPath) || TextUtils.isEmpty(audioPath)
                || TextUtils.isEmpty(outputPath) || duration <= 0) {
            MMLog.it(LogTag.COMPOSE_VIDEO, "Compose params is empty or null.");
            return false;
        }

        File file = new File(outputPath);
        if (!file.getParentFile().exists()) {
            boolean mkdirs = file.getParentFile().mkdirs();
            if (!mkdirs) {
                return false;
            }
        }

        String cmdline = "ffmpeg -f concat -safe 0 -i %s -i %s -vsync vfr -pix_fmt yuv420p -b 2000k -t %d %s";
        try {
            cmdline = String.format(Locale.getDefault(), cmdline, picConfigPath, audioPath, duration, outputPath);
            // 拆分ffmpeg命令
            String[] argv = cmdline.split(" ");
            MMLog.dt(LogTag.COMPOSE_VIDEO, "argv=" + Arrays.toString(argv));

            int composeResult = FFmpegManager.ffmpegCore(argv.length, argv);
            return composeResult == FFmpegManager.EXECUTE_SUCCESS;
        } catch (Exception | Error e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        }
        return false;
    }

}
