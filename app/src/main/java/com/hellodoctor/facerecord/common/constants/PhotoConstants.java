
package com.hellodoctor.facerecord.common.constants;

/**
 * 照片相关常量
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class PhotoConstants {

    private PhotoConstants() {
    }

    /** 保存照片的时间格式 */
    public static final String PHOTO_TIME_PATTERN = "yyyy-MM-dd";

    /** 照片预览的时间格式 */
    public static final String PHOTO_PREVIEW_PATTERN = "yyyy/MM/dd";

    /** 保存照片文件的后缀 */
    public static final String PHOTO_FILE_SUFFIX = ".jpg";

    /** 我的相册服务器返回时间模板 */
    public static final String PHOTOS_SERVER_TITME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /** 相册显示年月模板 */
    public static final String PHOTOS_MONTH_PATTERN = "yyyy-MM";

    /** 相册显示年月日模板 */
    public static final String PHOTOS_DATE_PATTERN = "yyyy.MM.dd";

    /** 图片key */
    public final static String KEY_PICTURE = "picture";

}
