
package com.hellodoctor.facerecord.common.utils;

import android.content.Context;

import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.entity.ShareEntity;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-26]
 */
public class ShareUtils {

    /**
     * 分享
     * @param context 上下文
     * @param share_title 分享标题
     * @param share_url 分享跳转链接地址
     * @param share_image_url 分享缩略图
     * @param share_description 分享描述
     */
    public static void share(Context context, String share_title, String share_url,
                                String share_image_url, String share_description) {
        ShareEntity shareEntity = new ShareEntity();
        shareEntity.title = share_title;
        shareEntity.titleUrl = share_url;
        shareEntity.imageUrl = share_image_url;
        shareEntity.description = share_description;
        shareEntity.url = share_url;
        shareEntity.site = share_title;
        showShare(context, shareEntity);
    }

    /**
     * 显示分享
     *
     * @param shareEntity 分享对象
     */
    private static void showShare(Context context, ShareEntity shareEntity) {
        OnekeyShare oks = new OnekeyShare();
        oks.setTitle(shareEntity.title);
        oks.setTitleUrl(shareEntity.titleUrl);
        oks.setText(shareEntity.description);
        oks.setImageUrl(shareEntity.imageUrl);
        oks.setUrl(shareEntity.url);
        oks.setSite(shareEntity.site);
        oks.setSiteUrl(shareEntity.siteUrl);
        oks.show(context);
    }

}
