
package com.hellodoctor.facerecord.common.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

/**
 * 媒体资源工具类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class MediaResUtils {

    /**
     * 通知媒体库更新文件
     *
     * @param context 上下文
     * @param file 文件对象
     */
    public static void notifyScanFile(Context context, File file) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(Uri.fromFile(file));
        context.sendBroadcast(scanIntent);
    }

}
