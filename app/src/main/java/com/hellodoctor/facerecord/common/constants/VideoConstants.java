
package com.hellodoctor.facerecord.common.constants;

import com.hellodoctor.facerecord.common.utils.DateUtils;

import java.io.File;

/**
 * 视频相关常量
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class VideoConstants {

    private VideoConstants() {
    }

    /** 保存视频的时间格式 */
    public static final String VIDEO_TIME_PATTERN = "yyyy-MM-dd-HH-mm-ss";

    /** 保存视频文件的后缀 */
    public static final String VIDEO_FILE_SUFFIX = ".mp4";

    /** 图片配置文件输出路径 */
    public static final String PIC_CONFIG_PATH = AppDirConstants.ROOT_DIR + File.separator + "input.txt";

    /** 帧间隔时间（单位：秒） */
    public static final int FRAME_DURATION = 1;

    /** 照片key */
    public static final String KEY_PICTURES = "pictures";

    /** 视频记录key */
    public static final String KEY_VIDEO_RECORD = "video_record";

    public static String createVideoName() {
        return DateUtils.getCurrDate(VIDEO_TIME_PATTERN) + VIDEO_FILE_SUFFIX;
    }

}
