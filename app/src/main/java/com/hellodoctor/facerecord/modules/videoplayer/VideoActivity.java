
package com.hellodoctor.facerecord.modules.videoplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.common.utils.MoneyUtils;
import com.hellodoctor.facerecord.common.utils.ShareUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.modules.videoplayer.RecordVideoContract.IRecordVideoPresenter;
import com.hellodoctor.facerecord.modules.videoplayer.RecordVideoContract.IRecordVideoView;
import com.isoftstone.mis.mmsdk.common.intf.UITemplate;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.MyVideoView;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-19]
 */
public class VideoActivity extends CommonBaseActivity<IRecordVideoPresenter> implements UITemplate, IRecordVideoView {

    Unbinder unbinder;

    @BindView(R.id.videoView)
    MyVideoView videoView;

    @BindView(R.id.iv_play_video)
    ImageView playVideoIv;

    @BindView(R.id.tv_video_title)
    TextView titleTv;

    @BindView(R.id.tv_discreet_value)
    TextView discreetValueTv;

    @BindView(R.id.tv_record_ranked_favorite)
    TextView favoriteTv;

    @BindView(R.id.tv_record_ranked_praise)
    TextView praiseTv;

    private boolean isPlayer;

    private MediaPlayer mediaPlayer;

    private FaceRecordEnt recordEnt;

    public static void gotoVideoActivity(Context context, FaceRecordEnt recordEnt) {
        Intent intent = new Intent(context, VideoActivity.class);
        intent.putExtra(VideoConstants.KEY_VIDEO_RECORD, recordEnt);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNoTitleBar();
        fullScreen(this);
        setContentView(R.layout.activity_video2);
    }

    /**
     * 通过设置全屏，设置状态栏透明
     *
     * @param activity
     */
    private void fullScreen(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // 5.x开始需要把颜色设置透明，否则导航栏会呈现系统默认的浅灰色
                Window window = activity.getWindow();
                View decorView = window.getDecorView();
                // 两个 flag 要结合使用，表示让应用的主体内容占用系统状态栏的空间
                int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                decorView.setSystemUiVisibility(option);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.TRANSPARENT);
            } else {
                Window window = activity.getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                int flagTranslucentStatus = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
                int flagTranslucentNavigation = WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
                attributes.flags |= flagTranslucentStatus;
                window.setAttributes(attributes);
            }
        }
    }

    @Override
    public IRecordVideoPresenter createPresenter() {
        return new RecordVideoPresenter(this, this);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
    }

    @Override
    public void initListener() {
    }

    @Override
    public void loadData() {
        Serializable object = getIntent().getSerializableExtra(VideoConstants.KEY_VIDEO_RECORD);
        if (!(object instanceof FaceRecordEnt)) {
            ToastUtils.showToast("视频地址错误");
            return;
        }
        recordEnt = (FaceRecordEnt) object;

        // 设置视频信息
        titleTv.setText(recordEnt.title);

        // 价值
        discreetValueTv.setText(getString(R.string.money_value, MoneyUtils.formatMoney(recordEnt.worth)));

        // 收藏数
        favoriteTv.setText(String.valueOf(recordEnt.collectAmount));
        if (recordEnt.isCollect == 1) {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.praise_ic, 0, 0, 0);
        } else {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_praise_ic, 0, 0, 0);
        }

        // 点赞数
        praiseTv.setText(String.valueOf(recordEnt.praiseAmount));
        if (recordEnt.isPraise == 1) {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.favorite_ic, 0, 0, 0);
        } else {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_favorite_ic, 0, 0, 0);
        }

        if (TextUtils.isEmpty(recordEnt.videoUrl)) {
            ToastUtils.showToast("视频地址不存在");
        } else {
            try {
                Uri uri = Uri.parse(recordEnt.videoUrl);
                initVideoView(uri);
            } catch (Exception e) {
                MMLog.et(LogTag.PLAY_VIDEO, e);
                ToastUtils.showToast("视频地址错误");
            }
        }
    }

    private void initVideoView(Uri uri) {
        videoView.setVideoURI(uri);
        videoView.start();
        showLoading(getString(R.string.loading_video));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer = mp;
                hideLoading();
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video started
                            Log.d("monty", "videoView -> video started");
                            return true;
                        }
                        return false;
                    }
                });

                isPlayer = true;
                updatePlayBtnStatus(true);
                Log.d("monty", "videoView -> OnPrepared");
                mp.start();
                Log.d("monty", "videoView -> start");
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                isPlayer = false;
                updatePlayBtnStatus(false);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("monty", "videoView -> OnError");
                videoView.setVisibility(View.GONE);
                mp.release();
                return false;
            }
        });

    }

    @OnClick(R.id.rl_play_video_back)
    public void back(View v) {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @OnClick(R.id.fl_video_view)
    public void onVideoViewClick(View v) {
        if (isPlayer) {
            videoView.pause();
            isPlayer = false;
        } else {
            videoView.start();
            isPlayer = true;
        }
        updatePlayBtnStatus(isPlayer);
    }

    @OnClick(R.id.iv_play_video)
    public void play(View v) {
        if (!isPlayer) {
            videoView.start();
        }
        isPlayer = true;
        updatePlayBtnStatus(isPlayer);
    }

    private void updatePlayBtnStatus(boolean isPlay) {
        if (isPlayer) {
            videoView.start();
            playVideoIv.setVisibility(View.GONE);
        } else {
            playVideoIv.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.iv_share_record)
    public void share(View v) {
        ShareUtils.share(this, recordEnt.title, recordEnt.videoUrl, recordEnt.preFinalUrl, recordEnt.title);
    }

    @OnClick(R.id.tv_record_ranked_favorite)
    public void opFavorite(View v) {
        if (recordEnt.isCollect == 0) {
            presenter.addFavorite(recordEnt);
        } else {
            presenter.cancelFavorite(recordEnt);
        }
    }

    @Override
    public void opFavoriteSuccess(FaceRecordEnt recordEnt) {
        showToast(R.string.operation_success);
        this.recordEnt = recordEnt;
        favoriteTv.setText(String.valueOf(recordEnt.collectAmount));
        if (recordEnt.isCollect == 1) {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.praise_ic, 0, 0, 0);
        } else {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_praise_ic, 0, 0, 0);
        }
    }

    @OnClick(R.id.tv_record_ranked_praise)
    public void onPraise(View v) {
        if (recordEnt.isPraise == 0) {
            presenter.addPraise(recordEnt);
        } else {
            presenter.cancelPraise(recordEnt);
        }
    }

    @Override
    public void opPraiseSuccess(FaceRecordEnt recordEnt) {
        showToast(R.string.operation_success);
        this.recordEnt = recordEnt;
        praiseTv.setText(String.valueOf(recordEnt.praiseAmount));
        if (recordEnt.isPraise == 1) {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.favorite_ic, 0, 0, 0);
        } else {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_favorite_ic, 0, 0, 0);
        }
    }

    @Override
    public void operationFailure() {
        showToast(R.string.operation_failure);
    }

}
