package com.hellodoctor.facerecord.modules.history;

import android.content.Context;

import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.HistoryEnt;
import com.hellodoctor.facerecord.modules.history.HistoryContract.IHistoryPresenter;
import com.hellodoctor.facerecord.modules.history.HistoryContract.IHistoryView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class HistoryPresenter extends BasePresenter<IHistoryView> implements IHistoryPresenter {

    public HistoryPresenter(Context context, IHistoryView view) {
        super(context, view);
    }

    @Override
    public void loadHistories(int pageNo) {
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        Call<BaseCallModel<ArrayList<HistoryEnt>>> serviceFavorites = service.getHistories(pageNo,
                AppCommonCons.PAGE_SIZE, token);
        serviceFavorites.enqueue(new BaseCallBack<BaseCallModel<ArrayList<HistoryEnt>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<ArrayList<HistoryEnt>>> response) {
                if (response == null) {
                    getView().loadHistoriesFinish(false, null);
                    return;
                }
                BaseCallModel<ArrayList<HistoryEnt>> body = response.body();
                if (body == null) {
                    getView().loadHistoriesFinish(false, null);
                    return;
                }
                getView().loadHistoriesFinish(true, body.data);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.FACE_RECORD, "获取收藏记录片失败：" + message);
                getView().loadHistoriesFinish(false, null);
            }
        });
    }

    @Override
    protected void onRelease() {

    }

}
