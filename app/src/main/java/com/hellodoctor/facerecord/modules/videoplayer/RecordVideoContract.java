
package com.hellodoctor.facerecord.modules.videoplayer;

import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * 播放颜值记录界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class RecordVideoContract {

    /**
     * 播放颜值记录界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IRecordVideoPresenter extends IBasePresenter<IRecordVideoView> {

        void addPraise(FaceRecordEnt recordEnt);

        void cancelPraise(FaceRecordEnt recordEnt);

        void addFavorite(FaceRecordEnt recordEnt);

        void cancelFavorite(FaceRecordEnt recordEnt);

    }

    /**
     * 播放颜值记录界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IRecordVideoView extends IBaseView {

        void opPraiseSuccess(FaceRecordEnt recordEnt);

        void opFavoriteSuccess(FaceRecordEnt recordEnt);

        void operationFailure();

    }

}
