package com.hellodoctor.facerecord.modules.facerecord;

import android.content.Context;

import com.hellodoctor.facerecord.biz.RecordOperationBiz;
import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.entity.BaseEntity;
import com.hellodoctor.facerecord.common.utils.RecordSortUtils;
import com.hellodoctor.facerecord.entity.BannerEnt;
import com.hellodoctor.facerecord.entity.RecordRankingEnt;
import com.hellodoctor.facerecord.modules.facerecord.FaceRecordContract.IFaceRecordPresenter;
import com.hellodoctor.facerecord.modules.facerecord.FaceRecordContract.IFaceRecordView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.hellodoctor.facerecord.net.res.BannerRes;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-08]
 */
public class FaceRecordPresenter extends BasePresenter<IFaceRecordView> implements IFaceRecordPresenter {

    public FaceRecordPresenter(Context context, IFaceRecordView view) {
        super(context, view);
    }

    @Override
    public void getBanners() {
        // 先显示默认轮播图片
        List<BaseEntity> banners = new ArrayList<>();
        banners.add(new BannerEnt());
        getView().getBannerSuccess(banners);

        String token = MyselfInfo.getLoginUser().getToken();
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel<BannerRes>> serviceBanner = service.getBanner(token);
        serviceBanner.enqueue(new BaseCallBack<BaseCallModel<BannerRes>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<BannerRes>> response) {
                if (response == null) {
                    return;
                }
                BaseCallModel<BannerRes> body = response.body();
                if (body == null) {
                    return;
                }
                BannerRes bannerRes = body.data;
                List<BaseEntity> banners = new ArrayList<>();
                if (bannerRes.banner != null && bannerRes.banner.size() > 0) {
                    banners.addAll(bannerRes.banner);
                }
                if (bannerRes.documental != null) {
                    banners.add(bannerRes.documental);
                }
                getView().getBannerSuccess(banners);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.BANNER, "获取轮播图失败：" + message);
            }
        });
    }

    @Override
    public void getRankings(int pageNo) {
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        Call<BaseCallModel<ArrayList<RecordRankingEnt>>> serviceRankings = service.getRankings(pageNo,
                AppCommonCons.PAGE_SIZE, token);
        serviceRankings.enqueue(new BaseCallBack<BaseCallModel<ArrayList<RecordRankingEnt>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<ArrayList<RecordRankingEnt>>> response) {
                if (response == null) {
                    return;
                }
                BaseCallModel<ArrayList<RecordRankingEnt>> body = response.body();
                if (body == null) {
                    return;
                }
                // 按收藏量排序
                RecordSortUtils.sortRanking(body.data);
                getView().getRankingSuccess(body.data);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.RANKING, "获取颜记录排名失败：" + message);
            }
        });
    }

    @Override
    public void addFavorite(RecordRankingEnt rankingEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final RecordRankingEnt resultRanking = rankingEnt.clone();
        new RecordOperationBiz().addFavorite(rankingEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRanking.isCollect = 1;
                    resultRanking.collectAmount = resultRanking.collectAmount + 1;
                    getView().opFavoriteSuccess(resultRanking);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "添加收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelFavorite(RecordRankingEnt rankingEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final RecordRankingEnt resultRanking = rankingEnt.clone();
        new RecordOperationBiz().deleteFavorite(rankingEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRanking.isCollect = 0;
                    resultRanking.collectAmount = resultRanking.collectAmount - 1;
                    getView().opFavoriteSuccess(resultRanking);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void addPraise(RecordRankingEnt rankingEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final RecordRankingEnt resultRanking = rankingEnt.clone();
        new RecordOperationBiz().addPraise(rankingEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRanking.isPraise = 1;
                    resultRanking.praiseAmount = resultRanking.praiseAmount + 1;
                    getView().opFavoriteSuccess(resultRanking);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelPraise(RecordRankingEnt rankingEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final RecordRankingEnt resultRanking = rankingEnt.clone();
        new RecordOperationBiz().deletePraise(rankingEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRanking.isPraise = 0;
                    resultRanking.praiseAmount = resultRanking.praiseAmount - 1;
                    getView().opFavoriteSuccess(resultRanking);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    protected void onRelease() {

    }

}
