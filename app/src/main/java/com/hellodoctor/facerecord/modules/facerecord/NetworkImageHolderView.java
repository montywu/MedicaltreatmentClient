package com.hellodoctor.facerecord.modules.facerecord;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.common.entity.BaseEntity;
import com.hellodoctor.facerecord.entity.BannerEnt;
import com.hellodoctor.facerecord.entity.DocumentaryEnt;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder.MMCBViewHolder;
import com.kelin.client.R;

/**
 * 加载网络图片轮播图
 *
 * @author hubing
 * @version [1.0.0.0, 2017-04-27]
 */
class NetworkImageHolderView implements MMCBViewHolder<BaseEntity> {

    private ImageView imageView;
    /**播放按钮  */
    private ImageView playIv;

    @Override
    public View createView(Context context) {
        // 你可以通过layout文件来创建，也可以像我一样用代码创建，不一定是Image，任何控件都可以进行翻页
        View itemView = LayoutInflater.from(context).inflate(R.layout.banner_item, null, false);
        imageView = itemView.findViewById(R.id.iv_banner_image);
        playIv = itemView.findViewById(R.id.iv_banner_play);
        return itemView;
    }

    @Override
    public void updateUI(Context context, int position, BaseEntity baseEnt) {
        if (baseEnt instanceof BannerEnt) {
            BannerEnt bannerEnt = (BannerEnt) baseEnt;
            if (!TextUtils.isEmpty(bannerEnt.imageUrl)) {
                Glide.with(context).load(bannerEnt.imageUrl).placeholder(R.drawable.ua_convenientbanner_default).into(imageView);
            } else {
                imageView.setImageResource(R.drawable.ua_convenientbanner_default);
            }
            playIv.setVisibility(View.GONE);
        } else if (baseEnt instanceof DocumentaryEnt) {
            DocumentaryEnt documentaryEnt = (DocumentaryEnt) baseEnt;
            if (!TextUtils.isEmpty(documentaryEnt.previewUrl)) {
                Glide.with(context).load(documentaryEnt.previewUrl).placeholder(R.drawable.ua_convenientbanner_default).into(imageView);
            } else {
                imageView.setImageResource(R.drawable.ua_convenientbanner_default);
            }
            playIv.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.drawable.ua_convenientbanner_default);
        }
    }

}
