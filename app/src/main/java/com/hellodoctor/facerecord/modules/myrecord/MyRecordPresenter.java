
package com.hellodoctor.facerecord.modules.myrecord;

import android.content.Context;

import com.hellodoctor.facerecord.biz.RecordOperationBiz;
import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * 我的记录片
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-12]
 */
public class MyRecordPresenter extends BasePresenter<MyRecordContract.IMyRecordView> implements MyRecordContract.IMyRecordPresenter {

    public MyRecordPresenter(Context context, MyRecordContract.IMyRecordView view) {
        super(context, view);
    }

    @Override
    public void loadRecord(int pageNo) {
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        Call<BaseCallModel<ArrayList<FaceRecordEnt>>> serviceRecords = service.getRecords(pageNo,
                AppCommonCons.PAGE_SIZE, token);
        serviceRecords.enqueue(new BaseCallBack<BaseCallModel<ArrayList<FaceRecordEnt>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<ArrayList<FaceRecordEnt>>> response) {
                if (response == null) {
                    getView().loadRecordFinish(false, null);
                    return;
                }
                BaseCallModel<ArrayList<FaceRecordEnt>> body = response.body();
                if (body == null) {
                    getView().loadRecordFinish(false, null);
                    return;
                }
                getView().loadRecordFinish(true, body.data);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.FACE_RECORD, "获取我的记录片失败：" + message);
                getView().loadRecordFinish(false, null);
            }
        });
    }

    @Override
    public void modifyVideoStatus(FaceRecordEnt faceRecordEnt) {
        getView().showLoading(context.getString(R.string.operation_wait));

        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        final FaceRecordEnt resultRecord = faceRecordEnt.clone();
        Call<BaseCallModel> modifyVideoStatus = service.modifyVideoStatus(faceRecordEnt.id,
                faceRecordEnt.status, token);
        modifyVideoStatus.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                getView().hideLoading();
                if (response == null) {
                    getView().modifyVideoStatusFailure();
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    getView().modifyVideoStatusFailure();
                    return;
                }
                getView().modifyVideoStatusSuccess(resultRecord);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.FACE_RECORD, "修改视频状态失败：" + message);
                getView().hideLoading();
                getView().modifyVideoStatusFailure();
            }
        });
    }

    @Override
    public void addFavorite(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().addFavorite(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isCollect = 1;
                    resultRecordEnt.collectAmount = resultRecordEnt.collectAmount + 1;
                    getView().opFavoriteSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "添加收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelFavorite(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().deleteFavorite(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isCollect = 0;
                    resultRecordEnt.collectAmount = resultRecordEnt.collectAmount - 1;
                    getView().opFavoriteSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void addPraise(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().addPraise(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isPraise = 1;
                    resultRecordEnt.praiseAmount = resultRecordEnt.praiseAmount + 1;
                    getView().opPraiseSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelPraise(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().deletePraise(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isPraise = 0;
                    resultRecordEnt.praiseAmount = resultRecordEnt.praiseAmount - 1;
                    getView().opPraiseSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    protected void onRelease() {
    }

}
