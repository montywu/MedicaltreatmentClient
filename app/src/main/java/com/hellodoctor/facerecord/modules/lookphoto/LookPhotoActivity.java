
package com.hellodoctor.facerecord.modules.lookphoto;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.lookphoto.LookPhotoContract.ILookPhotoPresenter;
import com.hellodoctor.facerecord.modules.lookphoto.LookPhotoContract.ILookPhotoView;
import com.isoftstone.mis.mmsdk.common.widget.photoview.PhotoView;
import com.isoftstone.mis.mmsdk.common.widget.photoview.PhotoViewAttacher;
import com.kelin.client.R;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-25]
 */
public class LookPhotoActivity extends CommonBaseActivity<ILookPhotoPresenter> implements ILookPhotoView {

    Unbinder unbinder;

    @BindView(R.id.pv_big_picture)
    PhotoView picturePv;

    private PictureEnt pictureEnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.look_photo_activity);
    }

    @Override
    public ILookPhotoPresenter createPresenter() {
        return new LookPhotoPresenter(this, this);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);

        setTitleBg(R.drawable.look_photo_title);
        setRightBackground(R.drawable.delete_ic);
    }

    @Override
    public void initListener() {
        // 设置单监听器,点击时，关闭显示大图
        PhotoViewAttacher mAttacher = new PhotoViewAttacher(picturePv);
        mAttacher.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {

            /**
             * {@inheritDoc}
             */
            @Override
            public void onViewTap(View view, float x, float y) {
                // 关闭浏览大图页面
                finish();
            }

        });
    }

    @Override
    public void loadData() {
        Intent intent = getIntent();
        Serializable object = intent.getSerializableExtra(PhotoConstants.KEY_PICTURE);
        if (object instanceof PictureEnt) {
            pictureEnt = (PictureEnt) object;
            Glide.with(this).load(pictureEnt.photo).into(picturePv);
        }
    }

    @Override
    protected void onTitleRightClick(View v) {
        presenter.deletePhoto(pictureEnt);
    }

    @Override
    public void deleteSuccess() {
        showToast(R.string.delete_success);
        Intent intent = getIntent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void deleteFailure() {
        showToast(R.string.delete_failure);
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }
}
