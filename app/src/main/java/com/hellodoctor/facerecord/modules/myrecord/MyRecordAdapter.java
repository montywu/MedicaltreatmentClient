
package com.hellodoctor.facerecord.modules.myrecord;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.common.utils.MoneyUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.isoftstone.mis.mmsdk.common.intf.GeneralAdapterV2;
import com.kelin.client.R;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class MyRecordAdapter extends GeneralAdapterV2<FaceRecordEnt> {

    /** 未发布 */
    public static final int UNPUBLISHED = 0;

    /** 已发布 */
    public static final int PUBLISHED = 1;

    /**
     * 构造函数
     *
     * @param ctx  上下文
     */
    public MyRecordAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        FaceRecordEnt faceRecordEnt = getItem(position);
        if (faceRecordEnt == null) {
            return UNPUBLISHED;
        }
        if ("rel".equals(faceRecordEnt.status)) {
            return PUBLISHED;
        }
        return UNPUBLISHED;
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        switch (itemViewType) {
            case UNPUBLISHED:
                return R.layout.my_record_item_unpublished;
            case PUBLISHED:
                return R.layout.my_record_item_published;
        }
        return R.layout.my_record_item_unpublished;
    }

    @Override
    public void convert(ViewHolder holder, final FaceRecordEnt item, int position, int itemViewType) {
        // 播放视频
        ImageView iv_my_record_play = holder.getView(R.id.iv_my_record_play);
        iv_my_record_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPlay(item);
                }
            }
        });

        // 生成日期
        TextView recordCreateDateTv = holder.getView(R.id.tv_record_create_date);
        if (!TextUtils.isEmpty(item.generateTime)) {
            String generateTime = item.generateTime.split(" ")[0];
            recordCreateDateTv.setText(getContext().getString(R.string.create_in, generateTime));
        } else {
            recordCreateDateTv.setText("");
        }

        // 预估价值
        TextView discreetValueTv = holder.getView(R.id.tv_discreet_value);
        discreetValueTv.setText(getContext().getString(R.string.money_value, MoneyUtils.formatMoney(item.worth)));

        // 视频预览图
        ImageView previewIv = holder.getView(R.id.iv_my_record_thumbnail);
        Glide.with(getContext()).load(item.previewUrl).placeholder(R.drawable.img_default).into(previewIv);

        if (itemViewType == PUBLISHED) {
            // 分享
            ImageView shareIv = holder.getView(R.id.iv_share_record);
            shareIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onShare(item);
                    }
                }
            });

            // 收藏数
            TextView favoriteTv = holder.getView(R.id.tv_record_ranked_favorite);
            favoriteTv.setText(String.valueOf(item.collectAmount));
            favoriteTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.opFavorite(item);
                    }
                }
            });

            // 点赞数
            TextView praiseTv = holder.getView(R.id.tv_record_ranked_praise);
            praiseTv.setText(String.valueOf(item.praiseAmount));
            praiseTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onPraise(item);
                    }
                }
            });

            // 撤回
            Button btn_withdraw = holder.getView(R.id.btn_withdraw);
            btn_withdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onWithdraw(item);
                    }
                }
            });

        } else {
            // 发布
            Button btn_publish = holder.getView(R.id.btn_publish);
            View.OnClickListener publishListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onPublish(item);
                    }
                }
            };
            btn_publish.setOnClickListener(publishListener);
            // 长按弹出的发布
            Button longPublishBtn = holder.getView(R.id.btn_long_publish);
            longPublishBtn.setOnClickListener(publishListener);
            if ("aud".equalsIgnoreCase(item.status)) {
                btn_publish.setText(R.string.auditing);
                longPublishBtn.setText(R.string.auditing);
                btn_publish.setEnabled(false);
                longPublishBtn.setEnabled(false);
            } else {
                btn_publish.setText(R.string.publish);
                longPublishBtn.setText(R.string.publish);
                btn_publish.setEnabled(true);
                longPublishBtn.setEnabled(true);
            }
        }

        // 长按发布视频出现操作界面
        final RelativeLayout operationRl = holder.getView(R.id.rl_record_operation);
        operationRl.setVisibility(View.GONE);
        holder.getConvertView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (operationRl.getVisibility() == View.GONE) {
                    operationRl.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });

        // 关闭操作界面
        ImageView closeOperationIv = holder.getView(R.id.iv_close_operation);
        closeOperationIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationRl.setVisibility(View.GONE);
            }
        });

        // 删除
        Button btn_delete = holder.getView(R.id.btn_delete);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onDelete(item);
                }
            }
        });
    }

    private OnOperationListener listener;

    void setOnOperationListener(OnOperationListener listener) {
        this.listener = listener;
    }

    interface OnOperationListener {

        void onPlay(FaceRecordEnt faceRecordEnt);

        void onShare(FaceRecordEnt faceRecordEnt);

        void onWithdraw(FaceRecordEnt faceRecordEnt);

        void onDelete(FaceRecordEnt faceRecordEnt);

        void onPublish(FaceRecordEnt faceRecordEnt);

        void opFavorite(FaceRecordEnt recordEnt);

        void onPraise(FaceRecordEnt recordEnt);

    }

}
