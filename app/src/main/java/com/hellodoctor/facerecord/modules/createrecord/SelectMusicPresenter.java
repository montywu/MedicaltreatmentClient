package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Context;
import android.text.TextUtils;

import com.hellodoctor.facerecord.biz.CreateVideoBiz;
import com.hellodoctor.facerecord.biz.Pic2VideoBiz;
import com.hellodoctor.facerecord.common.constants.AppDirConstants;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.common.fileupload.HttpFileUpload;
import com.hellodoctor.facerecord.common.utils.BitmapUtils;
import com.hellodoctor.facerecord.common.utils.GlideUtils;
import com.hellodoctor.facerecord.common.utils.MusicUtils;
import com.hellodoctor.facerecord.common.utils.PictureUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.MusicEnt;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.createrecord.SelectMusicContract.ISelectMusicPresenter;
import com.hellodoctor.facerecord.modules.createrecord.SelectMusicContract.ISelectMusicView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.intf.UiTask;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class SelectMusicPresenter extends BasePresenter<ISelectMusicView> implements ISelectMusicPresenter{

    public SelectMusicPresenter(Context context, ISelectMusicView view) {
        super(context, view);
    }

    @Override
    public void getMusics() {
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel<ArrayList<MusicEnt>>> serviceBanner = service.getMusics();
        serviceBanner.enqueue(new BaseCallBack<BaseCallModel<ArrayList<MusicEnt>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<ArrayList<MusicEnt>>> response) {
                if (response == null) {
                    return;
                }
                BaseCallModel<ArrayList<MusicEnt>> body = response.body();
                if (body == null) {
                    return;
                }
                getView().getMusicsSuccess(body.data);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.COMPOSE_VIDEO, "获取背景音乐失败：" + message);
            }
        });
    }

    @Override
    public void composeVideo(final String videoName, final ArrayList<PictureEnt> pictureEnts, final MusicEnt musicEnt) {
        getView().showLoading(context.getString(R.string.compose_video_wait));
        new UiTask<String, Void, FaceRecordEnt>() {
            @Override
            protected FaceRecordEnt doInBackground(String... strings) {
                FaceRecordEnt faceRecordEnt = new FaceRecordEnt();
                // 排序
                ArrayList<PictureEnt> pictures = new ArrayList<>(pictureEnts.size());
                pictures.addAll(pictureEnts);
                PictureUtils.sort(pictures);
                // 是否连续
                faceRecordEnt.isContinuous = PictureUtils.isContinuous(pictures) ? 1 : 2;
                faceRecordEnt.title = videoName;
                // 视频天数
                faceRecordEnt.videoDays = pictures.size();

                // 下载图片
                for (PictureEnt picture : pictures) {
                    File pictureFile = GlideUtils.downloadOnly(context, picture.photo);
                    if (pictureFile.exists() && pictureFile.length() > 0) {
                        picture.localPath = pictureFile.getAbsolutePath();
                    } else {
                        // 下载某一图片失败，则表示视频生成失败
                        MMLog.it(LogTag.COMPOSE_VIDEO, "下载失败：", picture.photo);
                        return null;
                    }
                }

                // 下载音乐
                File musicFile = MusicUtils.download(musicEnt.musicUrl);
                if (musicFile == null || !musicFile.exists() || musicFile.length() <= 0) {
                    // 下载音乐失败，则表示视频生成失败
                    return null;
                }

                // 开始合成视频
                String audioPath = musicFile.getAbsolutePath();
                User user = MyselfInfo.getLoginUser();
                File outputFile = new File(AppDirConstants.getVideoDir(String.valueOf(user.getId())),
                        VideoConstants.createVideoName());
                String outputPath = outputFile.getAbsolutePath();
                boolean composeResult = new Pic2VideoBiz().compose(pictures, audioPath, outputPath);
                if (!composeResult) {
                    return null;
                }

                // 合成视频预览图
                String firstPicPath = pictures.get(0).localPath;
                PictureEnt lastPicture = pictures.get(pictures.size() - 1);
                faceRecordEnt.preFinalUrl = lastPicture.photo;
                File previewPicFile = new File(outputFile.getAbsolutePath() + PhotoConstants.PHOTO_FILE_SUFFIX);
                boolean splicingResult = BitmapUtils.splicingPic(firstPicPath, lastPicture.localPath,
                        previewPicFile.getAbsolutePath());

                // 合成失败，表示视频生成失败
                if (!splicingResult) {
                    return null;
                }

                // 上传视频
                HttpFileUpload httpFileUpload = new HttpFileUpload();
                String uploadVideoPath = httpFileUpload.uploadVideoFile(outputFile);
                if (TextUtils.isEmpty(uploadVideoPath)) {
                    return null;
                }
                faceRecordEnt.videoUrl = uploadVideoPath;

                // 上传视频预览图
                String token = MyselfInfo.getLoginUser().getToken();
                String uploadImagePath = httpFileUpload.uploadImage(token, "2", previewPicFile);
                if (TextUtils.isEmpty(uploadImagePath)) {
                    return null;
                }
                faceRecordEnt.previewUrl = uploadImagePath;

                // 调用服务器生成视频接口，保存
                if (!new CreateVideoBiz().saveVideoToServer(faceRecordEnt)) {
                    return null;
                }

                // 本地有视频，直接传入本地视频进行播放，减少流量消耗，加快播放速度
                faceRecordEnt.videoUrl = outputPath;
                return faceRecordEnt;
            }

            @Override
            protected void onPostExecute(FaceRecordEnt faceRecordEnt) {
                getView().hideLoading();
                if (faceRecordEnt == null) {
                    getView().composeVideoFailure();
                } else {
                    getView().composeVideoSuccess(faceRecordEnt);
                }
            }
        }.start();
    }

    @Override
    protected void onRelease() {

    }

}
