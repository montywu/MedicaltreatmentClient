package com.hellodoctor.facerecord.modules.search;

import com.hellodoctor.facerecord.net.req.SearchReq;
import com.hellodoctor.facerecord.net.res.SearchRes;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;

import java.util.List;

/**
 * Created by soon on 2018/3/18.
 */

public class SearchContract {

    public interface ISearchPresenter extends IBasePresenter<ISeasrchView> {
        void search(SearchReq req);
    }

    public interface ISeasrchView extends IBaseView {
        void success(BaseCallModel<List<SearchRes>> callBack);

        void error(int code, String msg);

        void refresh();

        void loadMore();
    }
}
