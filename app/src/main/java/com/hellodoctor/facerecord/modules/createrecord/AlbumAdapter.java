
package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.isoftstone.mis.mmsdk.common.intf.GeneralAdapterV2;
import com.kelin.client.R;

import java.util.ArrayList;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class AlbumAdapter extends GeneralAdapterV2<PictureEnt> {

    private boolean isEditable = false;

    private ArrayList<PictureEnt> selectedPictures = new ArrayList<>();

    private OnSelectedAllListener listener;

    /**
     * 构造函数
     *
     * @param ctx  上下文
     */
    public AlbumAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        return R.layout.album_item;
    }

    @Override
    public void convert(ViewHolder holder, final PictureEnt item, int position, int itemViewType) {
        // 月份
        TextView monthTv = holder.getView(R.id.tv_photo_month);
        monthTv.setText(item.yearMonth);
        if (position > 0 && !TextUtils.isEmpty(item.yearMonth) && item.yearMonth.equals(getItem(position - 1).yearMonth)) {
            monthTv.setVisibility(View.INVISIBLE);
        } else {
            monthTv.setVisibility(View.VISIBLE);
        }

        // 照片
        ImageView photoIv = holder.getView(R.id.iv_album_photo);
        Glide.with(getContext())
                .load(item.photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.img_default)
                .into(photoIv);

        // 照片拍摄日期
        TextView albumDateTv = holder.getView(R.id.tv_album_date);
        albumDateTv.setText(item.yearMonthDay);

        // 编辑时的布局，是否选中照片
        final RelativeLayout albumSelectedRl = holder.getView(R.id.rl_album_selected);
        if (selectedPictures.contains(item)) {
            albumSelectedRl.setVisibility(View.VISIBLE);
        } else {
            albumSelectedRl.setVisibility(View.GONE);
        }

        photoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEditable && listener != null) {
                    // 不可编辑，查看大图
                    listener.onLookBigPicture(item);
                    return;
                }

                boolean isSelected = selectedPictures.contains(item);
                if (isSelected) {
                    selectedPictures.remove(item);
                    albumSelectedRl.setVisibility(View.GONE);
                } else {
                    selectedPictures.add(item);
                    albumSelectedRl.setVisibility(View.VISIBLE);
                }
                if (listener != null) {
                    listener.onSelectedAll(getCount() == selectedPictures.size());
                }
            }
        });
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
        if (!isEditable) {
            selectedPictures.clear();
        }
        this.notifyDataSetChanged();
    }

    public void selectAll(boolean isSelectAll) {
        if (isSelectAll) {
            for (PictureEnt pictureEnt : getData()) {
                if (!selectedPictures.contains(pictureEnt)) {
                    selectedPictures.add(pictureEnt);
                }
            }
        } else {
            selectedPictures.clear();
        }
        this.notifyDataSetChanged();
    }

    void setOnSelectedAllListener(OnSelectedAllListener listener) {
        this.listener = listener;
    }

    interface OnSelectedAllListener {

        void onSelectedAll(boolean isSelectAll);

        void onLookBigPicture(PictureEnt pictureEnt);

    }

    public ArrayList<PictureEnt> getSelectedPictures() {
        return selectedPictures;
    }
}
