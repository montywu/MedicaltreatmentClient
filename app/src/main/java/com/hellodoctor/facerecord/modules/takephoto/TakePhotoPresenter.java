
package com.hellodoctor.facerecord.modules.takephoto;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.hellodoctor.facerecord.biz.PublishPhotoBiz;
import com.hellodoctor.facerecord.biz.TakePhotoBiz;
import com.hellodoctor.facerecord.common.constants.AppDirConstants;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.fileupload.HttpFileUpload;
import com.hellodoctor.facerecord.common.utils.FaceDetectorUtils;
import com.hellodoctor.facerecord.common.utils.TakePhotoUtils;
import com.hellodoctor.facerecord.modules.takephoto.TakePhotoContract.ITakePhotoPresenter;
import com.hellodoctor.facerecord.modules.takephoto.TakePhotoContract.ITakePhotoView;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.intf.UiTask;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.isoftstone.mis.mmsdk.common.utils.sys.MMAppInfo;
import com.kelin.client.R;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;

import java.io.File;
import java.util.List;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class TakePhotoPresenter extends BasePresenter<ITakePhotoView> implements ITakePhotoPresenter,
        SurfaceHolder.Callback, TakePhotoBiz.OnCheckFaceListener {

    /** 摄像头 */
    private Camera camera;

    private SurfaceHolder holder;
    private int screenHeight;
    private int screenWidth;

    public TakePhotoPresenter(Context context, ITakePhotoView view) {
        super(context, view);
        int[] wh = MMAppInfo.getDisplayResolutionWH(context);
        this.screenWidth = wh[0];
        this.screenHeight = wh[1];
    }

    @Override
    public void startPreview(SurfaceView cameraSv) {
        this.holder = cameraSv.getHolder();
        // 设置Surface不需要维护自己的缓冲区
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        // 设置该组件不会让屏幕自动关闭
        holder.setKeepScreenOn(true);
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            int cameraCount = 0;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount = Camera.getNumberOfCameras();
            for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                // 打开前置摄像头
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    camera = Camera.open(camIdx);
                }
            }
            if (camera != null) {
                camera.setPreviewDisplay(holder);
                // 设置预览选择角度，顺时针方向，因为默认是逆向90度的
                camera.setDisplayOrientation(90);
                camera.startPreview();
                Camera.Parameters parameters = camera.getParameters();
                TakePhotoUtils.setPreviewSize(parameters, screenWidth, screenHeight);

                /*Camera.Size cs = sizes.get(0);
                int mWidth = cs.width;
                int mHeight = cs.height;
                parameters.setPreviewSize(mWidth, mHeight);*/

                List<Camera.Size> picSizes = parameters.getSupportedPictureSizes();
                for (Camera.Size picSize : picSizes) {
                    if (picSize.width == 1280) {
                        parameters.setPictureSize(picSize.width, picSize.height);
                    }
                }

                parameters.setJpegQuality(100);
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                parameters.setPictureFormat(ImageFormat.JPEG);
                camera.setParameters(parameters);
            }
        } catch (Exception e) {
            MMLog.et(LogTag.TAKE_PHOTO, e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    /**
     * 拍照
     */
    @Override
    public void takePhoto() {
        if (camera != null) {
            camera.takePicture(shutterCallback, null, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    User user = MyselfInfo.getLoginUser();
                    String saveDir;
                    if (user == null) {
                        saveDir = AppDirConstants.getPhotosDir(null);
                    } else {
                        saveDir = AppDirConstants.getPhotosDir(String.valueOf(user.getId()));
                    }
                    String photoPath = TakePhotoUtils.savePhoto(context, data, saveDir);
                    new TakePhotoBiz().checkFace(photoPath, TakePhotoPresenter.this);
                    getView().takePhotoSuccess(photoPath);
                }
            });
        }
    }

    @Override
    public void checkResult(int result) {
        switch (result) {
            case FaceDetectorUtils.CODE_SUCCESS:
                getView().canPublish();
                break;
            case FaceDetectorUtils.CODE_TOO_FAR:
                getView().tooFar();
                break;
            case FaceDetectorUtils.CODE_TOO_CLOSE:
                getView().tooClose();
                break;
            default:
                getView().faceNotFound();
        }
    }

    @Override
    public void rephotograph() {
        if (camera != null) {
            camera.startPreview();
        }
    }

    @Override
    public void publishPhoto(String photoPath) {
        getView().showLoading(context.getString(R.string.publish_photo_wait));
        new UiTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... strings) {
                String photoPath = strings[0];
                if (TextUtils.isEmpty(photoPath)) {
                    return false;
                }
                File photoFile = new File(photoPath);
                if (!photoFile.exists()) {
                    return false;
                }

                // 上传视频预览图
                HttpFileUpload httpFileUpload = new HttpFileUpload();
                String token = MyselfInfo.getLoginUser().getToken();
                String uploadImagePath = httpFileUpload.uploadImage(token, "2", photoFile);
                if (TextUtils.isEmpty(uploadImagePath)) {
                    return false;
                }

                // 调用服务器发布照片接口，保存
                return new PublishPhotoBiz().savePhotoToServer(uploadImagePath);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                getView().hideLoading();
                if (result != null && result) {
                    getView().publishSuccess();
                } else {
                    getView().publishFailure();
                }
            }
        }.start(photoPath);
    }

    private ToneGenerator tone;

    private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            if (tone == null) {
                // 发出提示用户的声音
                tone = new ToneGenerator(AudioManager.AUDIOFOCUS_REQUEST_GRANTED, ToneGenerator.MIN_VOLUME);
            }
            tone.startTone(ToneGenerator.TONE_PROP_BEEP);
        }
    };

    @Override
    protected void onRelease() {
        if (holder != null) {
            holder.removeCallback(this);
            holder = null;
        }
    }

}
