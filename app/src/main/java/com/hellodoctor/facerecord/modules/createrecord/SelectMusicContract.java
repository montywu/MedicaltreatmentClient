
package com.hellodoctor.facerecord.modules.createrecord;

import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.MusicEnt;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

import java.util.ArrayList;

/**
 * 选择音乐界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class SelectMusicContract {

    /**
     * 选择音乐界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ISelectMusicPresenter extends IBasePresenter<ISelectMusicView> {

        /**
         * 获取背景音乐
         */
        void getMusics();

        /**
         * 生成视频
         * @param videoName
         * @param pictureEnts
         * @param musicEnt
         */
        void composeVideo(String videoName, ArrayList<PictureEnt> pictureEnts, MusicEnt musicEnt);

    }

    /**
     * 选择音乐界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ISelectMusicView extends IBaseLoadingView {

        void getMusicsSuccess(ArrayList<MusicEnt> musicEnts);

        /**
         * 生成视频成功
         */
        void composeVideoSuccess(FaceRecordEnt faceRecordEnt);

        /**
         * 生成视频失败
         */
        void composeVideoFailure();

    }

}
