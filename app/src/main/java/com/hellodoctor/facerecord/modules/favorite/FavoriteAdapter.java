package com.hellodoctor.facerecord.modules.favorite;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.entity.FavoriteEnt;
import com.isoftstone.mis.mmsdk.common.intf.GeneralAdapterV2;
import com.kelin.client.R;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */

public class FavoriteAdapter extends GeneralAdapterV2<FavoriteEnt> {

    /**
     * 构造函数
     * @param ctx  上下文
     */
    public FavoriteAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        return R.layout.favorite_item;
    }

    @Override
    public void convert(ViewHolder holder, final FavoriteEnt item, int position, int itemViewType) {
        // 播放视频
        ImageView playIv = holder.getView(R.id.iv_favorite_play);
        playIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPlay(item);
                }
            }
        });

        // 标题
        TextView titleTv = holder.getView(R.id.tv_favorite_title);
        titleTv.setText(item.title);

        // 视频预览图
        ImageView previewIv = holder.getView(R.id.iv_favorite_thumbnail);
        Glide.with(getContext()).load(item.preview_url).placeholder(R.drawable.img_default).into(previewIv);

        // 收藏数
        TextView favoriteTv = holder.getView(R.id.tv_favorite_count);
        favoriteTv.setText(String.valueOf(item.collect_amount));

        // 点赞数
        TextView praiseTv = holder.getView(R.id.tv_favorite_praise);
        praiseTv.setText(String.valueOf(item.praise_amount));

        // 发布日期
        TextView publishDateTv = holder.getView(R.id.tv_favorite_publish_date);
        if (!TextUtils.isEmpty(item.release_time)) {
            String publishDate = item.release_time.split(" ")[0];
            publishDateTv.setText(getContext().getString(R.string.publish_date, publishDate));
        } else {
            publishDateTv.setText("");
        }

        // 分享
        ImageView shareIv = holder.getView(R.id.iv_share_favorite);
        shareIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onShare(item);
                }
            }
        });
    }

    private OnOperationListener listener;

    void setOnOperationListener(OnOperationListener listener) {
        this.listener = listener;
    }

    interface OnOperationListener {

        void onPlay(FavoriteEnt favoriteEnt);

        void onShare(FavoriteEnt favoriteEnt);

    }

}
