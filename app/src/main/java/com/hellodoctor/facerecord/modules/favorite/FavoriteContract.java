
package com.hellodoctor.facerecord.modules.favorite;

import com.hellodoctor.facerecord.entity.FavoriteEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

import java.util.List;

/**
 * 收藏界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class FavoriteContract {

    /**
     * 收藏界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IFavoritePresenter extends IBasePresenter<IFavoriteView> {

        void loadFavorites(int pageNo);

    }

    /**
     * 收藏界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IFavoriteView extends IBaseLoadingView {

        /**
         * 加载完成
         * @param result 加载结果
         * @param favoriteEnts 收藏列表
         */
        void loadFavoritesFinish(boolean result, List<FavoriteEnt> favoriteEnts);

    }

}
