
package com.hellodoctor.facerecord.modules.takephoto;

import android.view.SurfaceView;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

/**
 * 每日一拍界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class TakePhotoContract {

    /**
     * 每日一拍界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ITakePhotoPresenter extends IBasePresenter<ITakePhotoView> {

        void startPreview(SurfaceView cameraSv);

        void takePhoto();

        void rephotograph();

        void publishPhoto(String photoPath);

    }

    /**
     * 每日一拍界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ITakePhotoView extends IBaseLoadingView {

        void takePhotoSuccess(String photoPath);

        /**
         * 人脸检测成功，可发布
         */
        void canPublish();

        void faceNotFound();

        void tooFar();

        void tooClose();

        void publishSuccess();

        void publishFailure();

    }

}
