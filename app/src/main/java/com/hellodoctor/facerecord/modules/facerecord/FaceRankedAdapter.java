
package com.hellodoctor.facerecord.modules.facerecord;

import android.content.Context;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.entity.RecordRankingEnt;
import com.isoftstone.mis.mmsdk.common.intf.GeneralAdapterV2;
import com.kelin.client.R;

/**
 * 颜值排行榜列表适配器
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-08]
 */
public class FaceRankedAdapter extends GeneralAdapterV2<RecordRankingEnt> {

    private static SparseIntArray rankedIcons = new SparseIntArray();

    private OnOperationListener mOnOperationListener;

    static {
        rankedIcons.put(1, R.drawable.top1_ic);
        rankedIcons.put(2, R.drawable.top2_ic);
        rankedIcons.put(3, R.drawable.top3_ic);
    }

    /**
     * 构造函数
     *
     * @param ctx 上下文
     */
    public FaceRankedAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        return R.layout.record_ranked_top_item;
    }

    @Override
    public void convert(ViewHolder holder, final RecordRankingEnt item, int position, int itemViewType) {
        if (item == null) {
            return;
        }

        // 设置排名图标和位置
        // 前三使用图片等级图标
        ImageView rankedIv = holder.getView(R.id.iv_face_record_ranked3);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rankedIv.getLayoutParams();

        TextView rankedTv = holder.getView(R.id.tv_face_record_ranked);
        if (position >= 0 && position < rankedIcons.size()) {
            // 前三名
            rankedIv.setVisibility(View.VISIBLE);
            rankedTv.setVisibility(View.GONE);
            rankedIv.setImageResource(rankedIcons.get(position + 1));
        } else {
            rankedIv.setVisibility(View.GONE);
            rankedTv.setVisibility(View.VISIBLE);
            rankedTv.setText(String.valueOf(position));
        }

        // 图片
        ImageView imageIv = holder.getView(R.id.iv_face_record_ranked_image);
        Glide.with(getContext()).load(item.preFinalUrl).placeholder(R.drawable.img_default).into(imageIv);
        imageIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnOperationListener != null) {
                    mOnOperationListener.onLookFaceRecord(item);
                }
            }
        });

        // 收藏数
        TextView favoriteTv = holder.getView(R.id.tv_record_ranked_favorite);
        if (item.isCollect == 1) {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.praise_ic, 0, 0, 0);
        } else {
            favoriteTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_praise_ic, 0, 0, 0);
        }

        favoriteTv.setText(String.valueOf(item.collectAmount));
        favoriteTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnOperationListener != null) {
                    mOnOperationListener.onFavorite(item);
                }
            }
        });

        // 点赞数
        TextView praiseTv = holder.getView(R.id.tv_record_ranked_praise);
        praiseTv.setText(String.valueOf(item.praiseAmount));
        if (item.isPraise == 1) {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.favorite_ic, 0, 0, 0);
        } else {
            praiseTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.not_favorite_ic, 0, 0, 0);
        }

        praiseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnOperationListener != null) {
                    mOnOperationListener.onPraise(item);
                }
            }
        });
    }

    public void setOnOperationListener(OnOperationListener listener) {
        this.mOnOperationListener = listener;
    }

    /**
     * 操作监听器
     */
    public interface OnOperationListener {

        /**
         * 查看颜值记录
         * @param recordRankingEnt
         */
        void onLookFaceRecord(RecordRankingEnt recordRankingEnt);

        /**
         * 收藏/取消收藏
         * @param recordRankingEnt
         */
        void onFavorite(RecordRankingEnt recordRankingEnt);

        /**
         * 点赞/取消点赞
         * @param recordRankingEnt
         */
        void onPraise(RecordRankingEnt recordRankingEnt);

    }

}
