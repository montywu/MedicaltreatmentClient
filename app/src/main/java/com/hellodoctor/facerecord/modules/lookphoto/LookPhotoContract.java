
package com.hellodoctor.facerecord.modules.lookphoto;

import com.hellodoctor.facerecord.entity.PictureEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

/**
 * 查看照片面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class LookPhotoContract {

    /**
     * 查看照片面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ILookPhotoPresenter extends IBasePresenter<ILookPhotoView> {

        void deletePhoto(PictureEnt pictureEnt);

    }

    /**
     * 查看照片面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ILookPhotoView extends IBaseLoadingView {

        void deleteSuccess();

        void deleteFailure();

    }

}
