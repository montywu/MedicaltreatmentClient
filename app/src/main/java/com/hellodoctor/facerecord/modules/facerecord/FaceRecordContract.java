
package com.hellodoctor.facerecord.modules.facerecord;

import com.hellodoctor.facerecord.common.entity.BaseEntity;
import com.hellodoctor.facerecord.entity.RecordRankingEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

import java.util.List;

/**
 * 颜值记录界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class FaceRecordContract {

    /**
     * 颜值记录界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IFaceRecordPresenter extends IBasePresenter<IFaceRecordView> {

        void getBanners();

        void getRankings(int pageNo);

        void addPraise(RecordRankingEnt rankingEnt);

        void cancelPraise(RecordRankingEnt rankingEnt);

        void addFavorite(RecordRankingEnt rankingEnt);

        void cancelFavorite(RecordRankingEnt rankingEnt);

    }

    /**
     * 颜值记录界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IFaceRecordView extends IBaseView {

        void getBannerSuccess(List<BaseEntity> banners);

        void getRankingSuccess(List<RecordRankingEnt> recordRankingEnts);

        void opPraiseSuccess(RecordRankingEnt rankingEnt);

        void opFavoriteSuccess(RecordRankingEnt rankingEnt);

        void operationFailure();

    }

}
