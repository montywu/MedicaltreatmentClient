package com.hellodoctor.facerecord.modules.search;

import android.text.TextUtils;
import android.util.Log;

import com.hellodoctor.facerecord.net.RecordApiService;
import com.hellodoctor.facerecord.net.req.SearchReq;
import com.hellodoctor.facerecord.net.res.SearchRes;
import com.kelin.client.gui.coupons.CouponsService;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by soon on 2018/3/18.
 */

public class SearchPresenter implements SearchContract.ISearchPresenter {

    private SearchContract.ISeasrchView iSeasrchView;

    public SearchPresenter(SearchContract.ISeasrchView view) {
        this.iSeasrchView = view;
    }

    @Override
    public SearchContract.ISeasrchView getView() {
        return iSeasrchView;
    }

    @Override
    public void onAttachView() {

    }

    @Override
    public void onDetachView() {

    }

    @Override
    public void search(final SearchReq req) {
        Map<String, String> map = new HashMap<>();
        map.put("token", req.getToken());
        map.put("pageNo", "" + req.getPageNo());
        map.put("pageSize", "" + req.getPageSize());
        if (!TextUtils.isEmpty(req.getKeyword())) {
            map.put("keyword", req.getKeyword());
        }
        RetrofitHelper.getInstance().createService(RecordApiService.class).search(map).enqueue(new BaseCallBack<BaseCallModel<List<SearchRes>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<SearchRes>>> response) {
                if (response.isSuccessful()) {
                    getView().success(response.body());
                } else {
                    getView().error(1, "没有获取到数据");
                }
            }

            @Override
            public void onFailure(String message) {
                getView().error(1, TextUtils.isEmpty(message) ? "没有获取到数据" : message);
            }
        });
    }
}
