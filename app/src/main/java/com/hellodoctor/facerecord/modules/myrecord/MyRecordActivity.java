
package com.hellodoctor.facerecord.modules.myrecord;

import android.os.Bundle;
import android.widget.ListView;

import com.canyinghao.canrefresh.CanRefreshLayout;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.utils.DateParserUtils;
import com.hellodoctor.facerecord.common.utils.ShareUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.modules.videoplayer.VideoActivity;
import com.isoftstone.mis.mmsdk.common.widget.refreshview.MMClassicRefreshView;
import com.kelin.client.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 我的记录片
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-12]
 */
public class MyRecordActivity extends CommonBaseActivity<MyRecordContract.IMyRecordPresenter>
        implements MyRecordContract.IMyRecordView, MyRecordAdapter.OnOperationListener,
        CanRefreshLayout.OnRefreshListener, CanRefreshLayout.OnLoadMoreListener {

    Unbinder unbinder;

    @BindView(R.id.can_content_view)
    ListView recordListLv;

    private MyRecordAdapter recordAdapter;

    @BindView(R.id.crl_refresh)
    CanRefreshLayout refreshCrl;

    @BindView(R.id.can_refresh_header)
    MMClassicRefreshView refreshHeaderView;

    /** 当前加载的页码数 */
    private int currentPage = 1;

    @Override
    public MyRecordContract.IMyRecordPresenter createPresenter() {
        return new MyRecordPresenter(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_record_activity);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.my_face_record_title);

        recordAdapter = new MyRecordAdapter(this);
        recordListLv.setAdapter(recordAdapter);
    }

    @Override
    public void initListener() {
        // 设置上拉、下拉监听
        refreshCrl.setOnRefreshListener(this);
        refreshCrl.setOnLoadMoreListener(this);
        recordAdapter.setOnOperationListener(this);
    }

    @Override
    public void loadData() {
        presenter.loadRecord(currentPage);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        presenter.loadRecord(currentPage);
    }

    @Override
    public void onLoadMore() {
        presenter.loadRecord(currentPage);
    }

    @Override
    public void loadRecordFinish(boolean result, List<FaceRecordEnt> faceRecordEnts) {
        refreshCrl.refreshComplete();
        refreshCrl.loadMoreComplete();
        if (!result) {
            return;
        }

        if (currentPage == 1) {
            // 刷新完成
            // 刷新成功，设置刷新时间
            currentPage++;
            refreshHeaderView.setHeadRefreshTime(DateParserUtils.parseDateToString(System.currentTimeMillis()));
            recordAdapter.setData(faceRecordEnts);
            if (recordAdapter.getCount() < AppCommonCons.PAGE_SIZE) {
                refreshCrl.setLoadMoreEnabled(false);
            } else {
                refreshCrl.setLoadMoreEnabled(true);
            }
        } else {
            // 加载更多完成
            if (faceRecordEnts == null || faceRecordEnts.size() == 0) {
                showToast(R.string.no_more_data);
                return;
            }
            // 加载更多成功，页码加1
            currentPage++;
            recordAdapter.addAll(faceRecordEnts);
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onPlay(FaceRecordEnt faceRecordEnt) {
        VideoActivity.gotoVideoActivity(this, faceRecordEnt);
    }

    @Override
    public void onShare(FaceRecordEnt faceRecordEnt) {
        ShareUtils.share(this, faceRecordEnt.title, faceRecordEnt.videoUrl, faceRecordEnt.preFinalUrl, faceRecordEnt.title);
    }

    @Override
    public void onWithdraw(FaceRecordEnt faceRecordEnt) {
        faceRecordEnt.status = "gen";
        presenter.modifyVideoStatus(faceRecordEnt);
    }

    @Override
    public void onDelete(FaceRecordEnt faceRecordEnt) {
        faceRecordEnt.status = "del";
        presenter.modifyVideoStatus(faceRecordEnt);
    }

    @Override
    public void onPublish(FaceRecordEnt faceRecordEnt) {
        faceRecordEnt.status = "aud";
        presenter.modifyVideoStatus(faceRecordEnt);
    }

    @Override
    public void modifyVideoStatusSuccess(FaceRecordEnt faceRecordEnt) {
        showToast(R.string.operation_success);
        updateRecordList(faceRecordEnt);
    }

    @Override
    public void modifyVideoStatusFailure() {
        showToast(R.string.operation_failure);
    }

    @Override
    public void opFavorite(FaceRecordEnt recordEnt) {
        if (recordEnt.isCollect == 0) {
            presenter.addFavorite(recordEnt);
        } else {
            presenter.cancelFavorite(recordEnt);
        }
    }

    @Override
    public void opFavoriteSuccess(FaceRecordEnt recordEnt) {
        showToast(R.string.operation_success);
        updateRecordList(recordEnt);
    }

    @Override
    public void onPraise(FaceRecordEnt recordEnt) {
        if (recordEnt.isPraise == 0) {
            presenter.addPraise(recordEnt);
        } else {
            presenter.cancelPraise(recordEnt);
        }
    }

    @Override
    public void opPraiseSuccess(FaceRecordEnt recordEnt) {
        showToast(R.string.operation_success);
        updateRecordList(recordEnt);
    }

    @Override
    public void operationFailure() {
        showToast(R.string.operation_failure);
    }

    /**
     * 更新记录列表
     * @param recordEnt
     */
    private void updateRecordList(FaceRecordEnt recordEnt) {
        int position = recordAdapter.getPosition(recordEnt);
        if (position >= 0 && position < recordAdapter.getCount()) {
            recordAdapter.remove(recordEnt);
            if (!"del".equalsIgnoreCase(recordEnt.status)) {
                recordAdapter.insert(recordEnt, position);
            }
        }
    }
}
