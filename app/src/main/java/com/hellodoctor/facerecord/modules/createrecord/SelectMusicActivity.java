
package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.MusicEnt;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.createrecord.SelectMusicContract.ISelectMusicPresenter;
import com.kelin.client.R;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class SelectMusicActivity extends CommonBaseActivity<ISelectMusicPresenter> implements SelectMusicContract.ISelectMusicView {

    Unbinder unbinder;

    @BindView(R.id.et_video_name)
    EditText videoNameEt;

    @BindView(R.id.lv_musics)
    ListView musicsLv;

    private SelectMusicAdapter musicAdapter;

    private ArrayList<PictureEnt> pictureEnts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_music_activity);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);

        setTitleBg(R.drawable.select_music);
        setRightTitle(R.string.confirm);

        musicAdapter = new SelectMusicAdapter(this);
        musicsLv.setAdapter(musicAdapter);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void loadData() {
        Intent intent = getIntent();
        Serializable serializableObject = intent.getSerializableExtra(VideoConstants.KEY_PICTURES);
        if (serializableObject instanceof ArrayList) {
            pictureEnts = (ArrayList<PictureEnt>) serializableObject;
        }
        presenter.getMusics();
    }

    @Override
    public ISelectMusicPresenter createPresenter() {
        return new SelectMusicPresenter(this, this);
    }

    @Override
    public void getMusicsSuccess(ArrayList<MusicEnt> musicEnts) {
        musicAdapter.setData(musicEnts);
    }

    @Override
    protected void onTitleRightClick(View v) {
        String videoName = videoNameEt.getText().toString();
        if (TextUtils.isEmpty(videoName)) {
            showToast(R.string.input_video_title);
            return;
        }
        if (musicAdapter.getSelectMusic() == null) {
            showToast(R.string.please_select_music);
            return;
        }
        // 开始生成视频
        presenter.composeVideo(videoName, pictureEnts, musicAdapter.getSelectMusic());
    }

    @Override
    public void composeVideoSuccess(FaceRecordEnt faceRecordEnt) {
        // 跳转到成功页面
        Intent intent = new Intent(this, SaveSuccessActivity.class);
        intent.putExtra(VideoConstants.KEY_VIDEO_RECORD, faceRecordEnt);
        startActivity(intent);
        finish();
    }

    @Override
    public void composeVideoFailure() {
        showToast(R.string.compose_video_failure);
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }
}
