package com.hellodoctor.facerecord.net;

import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.FavoriteEnt;
import com.hellodoctor.facerecord.entity.HistoryEnt;
import com.hellodoctor.facerecord.entity.MusicEnt;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.entity.RecordRankingEnt;
import com.hellodoctor.facerecord.net.res.BannerRes;
import com.hellodoctor.facerecord.net.res.SearchRes;
import com.monty.library.http.BaseCallModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by soon on 2018/3/18.
 * 颜值记录相关的接口
 */

public interface RecordApiService {

    String BASE_URL = "http://123.207.9.75";

    /**
     * 轮播图接口
     * @return
     */
    @GET(BASE_URL + "/banner")
    Call<BaseCallModel<BannerRes>> getBanner(@Query("token") String token);

    /**
     * 记录片排名接口
     * @return
     */
    @GET(BASE_URL + "/ranking")
    Call<BaseCallModel<ArrayList<RecordRankingEnt>>> getRankings(@Query("pageNo") int pageNo,
                                                                 @Query("pageSize") int pageSize,
                                                                 @Query("token") String token);

    /**
     * 相册接口
     * @return
     */
    @GET(BASE_URL + "/user/photoAlbum")
    Call<BaseCallModel<ArrayList<PictureEnt>>> getAlbums(@Query("pageNo") int pageNo,
                                                           @Query("pageSize") int pageSize,
                                                           @Query("token") String token);

    /**
     * 背景音乐接口
     * @return
     */
    @GET(BASE_URL + "/backgroundMusic")
    Call<BaseCallModel<ArrayList<MusicEnt>>> getMusics();

    /**
     * 自我介绍视频
     * @return 上传文件的地址
     */
    @Multipart
    @POST("http://123.207.9.75:8081/hello-doctor/uploadFile")
    Call<BaseCallModel<List<String>>> uploadVideoFile(@Part List<MultipartBody.Part> partList);

    /**
     * 上传图片文件
     * @return 上传文件的地址
     */
    @Multipart
    @POST(BASE_URL + "/user/uploadFile")
    Call<BaseCallModel<List<String>>> uploadImageFile(@Part List<MultipartBody.Part> partList);

    /**
     * 生成视频接口
     * @param params
     * @return
     */
    @GET(BASE_URL + "/user/uploadVideo")
    Call<BaseCallModel<Double>> generateVideo(@QueryMap Map<String, String> params);

    /**
     * 获取我的记录片
     * @return
     */
    @GET(BASE_URL + "/user/documental")
    Call<BaseCallModel<ArrayList<FaceRecordEnt>>> getRecords(@Query("pageNo") int pageNo,
                                                              @Query("pageSize") int pageSize,
                                                              @Query("token") String token);

    /**
     * 视频发布、撤回、删除接口
     * @return
     */
    @GET(BASE_URL + "/user/updateStatus")
    Call<BaseCallModel> modifyVideoStatus(@Query("did") int did, @Query("status") String status,
                                          @Query("token") String token);

    /**
     * 收藏
     * @return
     */
    @GET(BASE_URL + "/user/collectList")
    Call<BaseCallModel<ArrayList<FavoriteEnt>>> getFavorites(@Query("pageNo") int pageNo,
                                                             @Query("pageSize") int pageSize,
                                                             @Query("token") String token);

    /**
     * 获取浏览历史记录
     * @return
     */
    @GET(BASE_URL + "/user/historyList")
    Call<BaseCallModel<ArrayList<HistoryEnt>>> getHistories(@Query("pageNo") int pageNo,
                                                            @Query("pageSize") int pageSize,
                                                            @Query("token") String token);

    /**
     * 点赞
     * @param did 视频id
     * @return
     */
    @GET(BASE_URL + "/user/addPraise")
    Call<BaseCallModel> addPraise(@Query("did") int did, @Query("token") String token);

    /**
     * 取消点赞
     * @param did 视频id
     * @return
     */
    @GET(BASE_URL + "/user/deletePraise")
    Call<BaseCallModel> deletePraise(@Query("did") int did, @Query("token") String token);

    /**
     * 收藏
     * @param did 视频id
     * @return
     */
    @GET(BASE_URL + "/user/addCollect")
    Call<BaseCallModel> addFavorite(@Query("did") int did, @Query("token") String token);

    /**
     * 取消收藏
     * @param did 视频id
     * @return
     */
    @GET(BASE_URL + "/user/deleteCollect")
    Call<BaseCallModel> deleteFavorite(@Query("did") int did, @Query("token") String token);

    /**
     * 搜索接口
     * @param params
     * @return
     */
    @GET(BASE_URL + "/user/search")
    Call<BaseCallModel<List<SearchRes>>> search(@QueryMap Map<String, String> params);

    /**
     * 添加照片接口
     * @return
     */
    @POST(BASE_URL + "/user/photograph")
    Call<BaseCallModel> addImage(@Query("photoUrl") String photoUrl, @Query("token") String token);

    /**
     * 删除照片接口
     * @return
     */
    @GET(BASE_URL + "/user/deletePhoto")
    Call<BaseCallModel> deleteImage(@Query("photoId") int photoId, @Query("token") String token);

}
