package com.hellodoctor.facerecord.net.req;

/**
 * Created by soon on 2018/3/18.
 */

public class BaseReq {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
