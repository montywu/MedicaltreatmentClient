package com.hellodoctor.facerecord.net.res;

/**
 * Created by soon on 2018/3/18.
 */

public class SearchRes {

    /**
     * releaseTime : null
     * videoUrl : http://123.207.9.75:8080/upload-file/user/photo/80d900a6a39645e28bda46d55c6177e3.mp4
     * praiseAmount : 0
     * isCollect : 0
     * isPraise : 0
     * id : 7
     * title : test1
     * collectAmount : 0
     */

    private String releaseTime;
    private String videoUrl;
    private int praiseAmount;
    private int isCollect;
    private int isPraise;
    private int id;
    private String title;
    private int collectAmount;
    private String previewUrl;
    private String preFinalUrl;

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getPreFinalUrl() {
        return preFinalUrl;
    }

    public void setPreFinalUrl(String preFinalUrl) {
        this.preFinalUrl = preFinalUrl;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getPraiseAmount() {
        return praiseAmount;
    }

    public void setPraiseAmount(int praiseAmount) {
        this.praiseAmount = praiseAmount;
    }

    public int getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(int isCollect) {
        this.isCollect = isCollect;
    }

    public int getIsPraise() {
        return isPraise;
    }

    public void setIsPraise(int isPraise) {
        this.isPraise = isPraise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCollectAmount() {
        return collectAmount;
    }

    public void setCollectAmount(int collectAmount) {
        this.collectAmount = collectAmount;
    }
}
