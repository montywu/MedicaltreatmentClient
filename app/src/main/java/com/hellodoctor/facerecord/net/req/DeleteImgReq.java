package com.hellodoctor.facerecord.net.req;

/**
 * Created by soon on 2018/3/18.
 */

public class DeleteImgReq extends BaseReq {
    private Long photoId;

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long photoId) {
        this.photoId = photoId;
    }
}
