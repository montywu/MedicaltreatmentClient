/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.isoftstone.mis.mmsdk;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.isoftstone.mis.mmsdk";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: release
  public static final boolean LOG_ADB = false;
  public static final int LOG_DEGREE = 2;
  public static final String LOG_FILENAME = "HelloDoctor/log/app.log";
}
