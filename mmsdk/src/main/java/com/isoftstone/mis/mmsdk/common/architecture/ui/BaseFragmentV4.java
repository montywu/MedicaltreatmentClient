
package com.isoftstone.mis.mmsdk.common.architecture.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * 兼容3.0以下，android.support.v4下包中的Fragment基类
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-13]
 */
public abstract class BaseFragmentV4<P extends IBasePresenter> extends Fragment implements IBaseView {

    /** Presenter对象 */
    protected P presenter;

    /** fragment根view */
    private View rootView;

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(getRootViewResid(), container, false);
        presenter = createPresenter();
        return rootView;
    }

    /**
     * Look for a child view with the given id. If this view has the given id, return this view.
     *
     * @param id The id to search for.
     * @return The view that has the given id in the hierarchy or null
     */
    public final <T extends View> T findViewById(int id) {
        return (T) rootView.findViewById(id);
    }

    /**
     * 获取fragment页面布局资源id
     * 
     * @return 返回fragment页面布局资源id
     */
    protected abstract int getRootViewResid();

    /**
     * 创建presenter类对象
     * 
     * @return presenter类对象
     */
    public abstract P createPresenter();

    /**
     * Detach from view.
     */
    @Override
    public void onDestroyView() {
        if (presenter != null) {
            presenter.onDetachView();
        }
        super.onDestroyView();
    }

    /**
     * 展示给定资源的Toast提示
     *
     * @param resId The resource id of the string resource to use. Can be formatted text.
     */
    public void showToast(int resId) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 展示给定字符串的Toast提示
     *
     * @param text The text to show. Can be formatted text.
     */
    public void showToast(String text) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
        }
    }

}
