
package com.isoftstone.mis.mmsdk.common.utils.log;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * 未捕获的异常收集类
 * 
 * @author hubing
 * @version 1.0.0 2015-11-5
 */
public class CrashHandler implements UncaughtExceptionHandler {

    private static final String TAG = "CrashHandler";

    // 系统默认的UncaughtException处理类
    private UncaughtExceptionHandler mDefaultHandler;

    // CrashHandler实例
    private static CrashHandler mInstance = new CrashHandler();

    private CrashHandler() {
    }

    /**
     * 获取CrashHandler实例
     * 
     * @return 返回CrashHandler实例
     */
    public static CrashHandler getInstance() {
        return mInstance;
    }

    /**
     * 初始化
     */
    public void init() {
        // 获取系统默认的UncaughtException处理器
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();

        // 设置当前CrashHandler为程序的默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /**
     * 当UncaughtException发生时会转入该函数来处理
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        handleException(ex);
        // 让系统默认的异常处理器来处理
        mDefaultHandler.uncaughtException(thread, ex);
    }

    /**
     * 自定义错误处理,收集错误信息,发送错误报告等操作
     * 
     * @param ex 导演对象
     * @return true:如果处理了该异常信息, 否则返回false
     */
    private boolean handleException(Throwable ex) {
        if (ex == null) {
            return false;
        }
        MMLog.et(TAG, ex, "程序出现异常崩溃: ");
        return true;
    }

}
