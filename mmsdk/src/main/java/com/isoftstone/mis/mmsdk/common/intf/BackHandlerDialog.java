
package com.isoftstone.mis.mmsdk.common.intf;

import android.app.Dialog;
import android.content.Context;

/**
 * 自定义Dialog,实现监听Dialog的监听back键功能
 * 
 * @author hubing
 * @version 1.0.0 2015-8-10
 */
public class BackHandlerDialog extends Dialog {

    /** 监听器对象 */
    private BackHandlerListener backHandler;

    /**
     * Dialog按下back事件监听器
     */
    public interface BackHandlerListener {

        /**
         * 按下back事件
         * 
         * @return 返回事件处理结果，true表示已经处理back事件
         */
        boolean onBackPressed();

    }

    public BackHandlerDialog(Context context) {
        super(context);
    }

    public BackHandlerDialog(Context context, int theme) {
        super(context, theme);
    }

    public void setOnBackPressedListener(BackHandlerListener backHandler) {
        this.backHandler = backHandler;
    }

    @Override
    public void onBackPressed() {
        if (backHandler == null || !backHandler.onBackPressed()) {
            super.onBackPressed();
        }
    }

}
