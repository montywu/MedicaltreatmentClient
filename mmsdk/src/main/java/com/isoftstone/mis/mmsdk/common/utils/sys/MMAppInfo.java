
package com.isoftstone.mis.mmsdk.common.utils.sys;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取APP相关内容的工具类
 *
 * @author yansu
 */
public class MMAppInfo {

    private static final String TAG = "MMAppInfo";

    /**
     * 获取上下文对应的app的label
     *
     * @param context 上下文
     * @return label
     * @author yansu
     */
    public static String getAppLabel(Context context) {
        CharSequence label = context.getPackageManager().getApplicationLabel(context.getApplicationInfo());
        if (label != null) {
            return label.toString();
        }
        return null;
    }

    /**
     * 获取指定包名的app的label
     *
     * @param context 上下文
     * @param packageName 包名
     * @return label
     * @author yansu
     */
    public static String getAppLabel(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            CharSequence label = info.applicationInfo.loadLabel(pm);
            if (label != null) {
                return label.toString();
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 获取app的icon
     *
     * @param context 上下文
     * @return icon
     * @author yansu
     */
    public static Drawable getAppIcon(Context context) {
        return context.getPackageManager().getApplicationIcon(context.getApplicationInfo());
    }

    /**
     * 获取指定包名的app的icon
     *
     * @param context 上下文
     * @param packageName 包名
     * @return icon
     * @author yansu
     */
    public static Drawable getAppIcon(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            return info.applicationInfo.loadIcon(pm);
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 当前屏幕显示是否为竖屏
     *
     * @param context 上下文
     * @return true:竖屏, false:横屏
     * @author yansu
     */
    public static boolean isScreenPortrait(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    /**
     * 获取应用版本-VersionCode
     *
     * @param context 上下文
     * @return 获取失败，则返回1
     */
    public static int getAppVersionCode(Context context) {
        try {
            PackageInfo localPackageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return localPackageInfo.versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return 1;
    }

    /**
     * 获取应用版本-VersionName
     *
     * @param context 上下文
     * @return 获取失败，则返回null
     * @author yansu
     */
    public static String getAppVersionName(Context context) {
        try {
            PackageInfo localPackageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return localPackageInfo.versionName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 获取当前屏幕分辨率
     *
     * @param context 上下文
     * @return 屏幕分辨率(宽x高)，例如：“480x800”
     * @author yansu
     */
    public static String getDisplayResolution(Context context) {
        int[] wh = getDisplayResolutionWH(context);
        return wh[0] + "x" + wh[1];
    }

    /**
     * 获取当前屏幕分辨率
     *
     * @param context 上下文
     * @return 屏幕分辨率(宽 高)，例如：[480,800]
     * @author yansu
     */
    public static int[] getDisplayResolutionWH(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return new int[] {
                dm.widthPixels, dm.heightPixels
        };
    }

    /**
     * 检查权限
     *
     * @param context 上下文
     * @param permissionString 检查的权限
     * @return true:有该权限,false:没有该权限
     * @author yansu
     */
    public static boolean checkPermission(Context context, String permissionString) {
        return context.getPackageManager().checkPermission(permissionString, context.getPackageName()) == 0;
    }

    /**
     * 清空task堆栈中的activity
     *
     * @param context 上下文
     * @author yansu
     */
    @Deprecated
    public static void clearTask(Context context) {
        // 通过context获取系统服务，得到ActivityManager
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName baseActivity = null;
        try {
            // 获取当前运行中的TaskInfo
            // 获取的是一个List集合，也就是说当前系统中的task有多个
            // 关于该方法的参数，我从源码看了下，是指返回集合的最大可能条目数，
            // 数值为1代表获取当前运行栈，数值大于1则按最近活动栈的顺序获取指定数量的栈，
            // 实际返回数可能小于这个数目，取决于用户启动了几个task
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                // 使用该方法需要申明权限
                // <uses-permission android:name="android.permission.GET_TASKS"/>
                // getRunningTasks 方法在API21之后为了安全性已弃用，不再为非系统应用的三方应用开放
                List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
                if (tasks != null && !tasks.isEmpty()) {
                    baseActivity = tasks.get(0).baseActivity;
                }
            } else {
                List<ActivityManager.AppTask> appTasks = manager.getAppTasks();
                if (appTasks != null && !appTasks.isEmpty()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // baseActivity在API23之后才提供
                        baseActivity = appTasks.get(0).getTaskInfo().baseActivity;
                    }
                }
            }
            if (baseActivity != null) {
                Intent i = new Intent();
                i.setComponent(baseActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("exit", "1");
                context.startActivity(i);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    /**
     * 获取应用所拥有的权限
     *
     * @param context 上下文
     * @param packageName 包名
     * @return 如果包名不存在则返回null 否则返回权限信息数组
     * @author yuwenbo
     */
    public static List<PermissionInfo> getAppPermissions(Context context, String packageName) {
        List<PermissionInfo> permissions = null;
        try {
            String[] requestedPermissions = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_PERMISSIONS).requestedPermissions;
            if (requestedPermissions != null) {
                permissions = new ArrayList<PermissionInfo>(requestedPermissions.length);
                for (String str : requestedPermissions) {
                    PermissionInfo permissionInfo = context.getPackageManager().getPermissionInfo(str, 0);
                    permissions.add(permissionInfo);
                }
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
        return permissions;
    }

}
