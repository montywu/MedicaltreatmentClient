
package com.isoftstone.mis.mmsdk.common.architecture.presenter;

import android.content.Context;

import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * MVP模式中Presenter基类
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-12]
 */
public abstract class BasePresenter<V extends IBaseView> implements IBasePresenter<V> {

    private static final String TAG = "BasePresenter";

    /** View的虚引用 */
    private WeakReference<V> mView;

    /** 代理view对象 */
    private V proxyView;

    /** 上下文 */
    protected Context context;

    public BasePresenter(Context context, V view) {
        attachView(view);
        this.context = context.getApplicationContext();
    }

    private void attachView(V view) {
        mView = new WeakReference<V>(view);
        Class<?> viewClazz = view.getClass();
        proxyView = (V) Proxy.newProxyInstance(viewClazz.getClassLoader(), viewClazz.getInterfaces(), new ViewProxyHandler());

        onAttachView();
    }

    @Override
    public void onAttachView() {
        MMLog.dt(TAG, "onAttachView: ", getClass().getSimpleName());
    }

    /**
     * 代理view类，用于解决每获取view时，都要判断view是否为空的重复代码判断
     */
    private class ViewProxyHandler implements InvocationHandler {

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            // 判断引用view是否被释放掉
            if (mView == null) {
                return null;
            }
            V view = mView.get();
            if (view != null) {
                // 如果没有释放掉，执行真正的view方法
                return method.invoke(view, args);
            }
            return null;
        }

    }

    /**
     * 获取Presenter关联的View
     *
     * @return 返回Presenter关联的代理View
     */
    @Override
    public V getView() {
        return proxyView;
    }

    /**
     * 释放资源
     * <P>
     * 页面销毁时需要调用此方法，如Activity中的onDestroy方法
     */
    @Override
    public void onDetachView() {
        MMLog.dt(TAG, "onDetachView: ", getClass().getSimpleName());
        // 释放View的引用
        if (mView != null && mView.get() != null) {
            mView.clear();
        }
        mView = null;
        // 释放资源
        onRelease();
    }

    /**
     * 释放所有资源，如取消网络请求等操作
     */
    protected abstract void onRelease();

}
