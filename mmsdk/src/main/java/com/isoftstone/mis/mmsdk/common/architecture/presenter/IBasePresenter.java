
package com.isoftstone.mis.mmsdk.common.architecture.presenter;

import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * MVP模式中Presenter基类接口
 *
 * @author hubing
 * @version [1.0.0.0, 2016-12-29]
 */
public interface IBasePresenter<V extends IBaseView> {

    /** Presenter空实现，防止有些页面不需要用到presenter时，报空指针 */
    IBasePresenter<IBaseView> EMPTY = new IBasePresenter<IBaseView>() {

        @Override
        public IBaseView getView() {
            return new IBaseView() {
            };
        }

        @Override
        public void onAttachView() {
        }

        @Override
        public void onDetachView() {
        }

    };

    /**
     * 获取Presenter关联的View
     *
     * @return 返回Presenter关联的代理View
     */
    V getView();

    /**
     * 关联View
     */
    void onAttachView();

    /**
     * 解除关联View，页面销毁时需要调用此方法，以便释放资源
     * 如Activity中的onDestroy方法
     */
    void onDetachView();

}
