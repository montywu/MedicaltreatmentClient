
package com.isoftstone.mis.mmsdk.common.intf;

import android.view.animation.Animation;

/**
 * 动画监听适配器
 * <p>
 * 空实现所有动画监听方法，用于解决每次都需要实现所有方法的问题
 *
 * <p>An animation listener receives notifications from an animation.
 * Notifications indicate animation related events, such as the end or the
 * repetition of the animation.</p>
 *
 * @author hubing
 * @version [1.0.0.0, 2016-09-28]
 */
public abstract class AnimationListenerAdapter implements Animation.AnimationListener{

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

}
