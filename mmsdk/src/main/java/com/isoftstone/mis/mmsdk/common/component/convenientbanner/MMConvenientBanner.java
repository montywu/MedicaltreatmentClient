
package com.isoftstone.mis.mmsdk.common.component.convenientbanner;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.PageTransformer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.isoftstone.mis.mmsdk.R;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.adapter.MMCBPageAdapter;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder.MMCBViewHolderCreator;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.listener.MMCBPageChangeListener;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.listener.MMOnCBItemClickListener;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.view.MMCBLoopViewPager;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 页面翻转控件，极方便的广告栏 支持无限循环，自动翻页，翻页特效
 */
public class MMConvenientBanner<T> extends LinearLayout {
    /** 业务对象集合 */
    private List<T> mDatas;

    private int[] pageIndicatorId;

    /** 翻页指示的点集合 */
    private ArrayList<ImageView> mPointViews = new ArrayList<ImageView>();

    /** 翻页指示器改变监听 */
    private MMCBPageChangeListener pageChangeListener;

    /** 翻页改变监听 */
    private ViewPager.OnPageChangeListener onPageChangeListener;

    /** 翻页指示器适配器 */
    private MMCBPageAdapter pageAdapter;

    /** 翻页指示器循环视图滑动页 */
    private MMCBLoopViewPager viewPager;

    /** 滑动组件 */
    private MMViewPagerScroller scroller;

    /** 翻页指示组 */
    private ViewGroup loPageTurningPoint;

    /** 自动翻页时间 默认5秒 */
    private long autoTurningTime = 5000;

    /** 是否一开启翻页 */
    private boolean turning;

    /** 是否可翻页 */
    private boolean canTurn = false;

    /** 是否可滚动 */
    private boolean manualPageable = true;

    /** 是否可循环 */
    private boolean canLoop = true;

    /** 枚举 靠左、靠右、居中 */
    public enum PageIndicatorAlign {
        ALIGN_PARENT_LEFT, ALIGN_PARENT_RIGHT, CENTER_HORIZONTAL
    }

    private AdSwitchTask adSwitchTask;

    public MMConvenientBanner(Context context) {
        super(context);
        init(context);
    }

    public MMConvenientBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MMConvenientBanner);
        canLoop = a.getBoolean(R.styleable.MMConvenientBanner_canLoop, true);
        a.recycle();
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MMConvenientBanner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MMConvenientBanner);
        canLoop = a.getBoolean(R.styleable.MMConvenientBanner_canLoop, true);
        a.recycle();
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MMConvenientBanner(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MMConvenientBanner);
        canLoop = a.getBoolean(R.styleable.MMConvenientBanner_canLoop, true);
        a.recycle();
        init(context);
    }

    /**
     * 初始化
     * 
     * @param context 上下文
     */
    private void init(Context context) {
        View hView = LayoutInflater.from(context).inflate(R.layout.ua_convenientbanner_viewpager, this, true);
        viewPager = (MMCBLoopViewPager) hView.findViewById(R.id.cbLoopViewPager);
        loPageTurningPoint = (ViewGroup) hView.findViewById(R.id.loPageTurningPoint);
        initViewPagerScroll();

        adSwitchTask = new AdSwitchTask(this);
    }

    static class AdSwitchTask implements Runnable {

        private final WeakReference<MMConvenientBanner> reference;

        AdSwitchTask(MMConvenientBanner convenientBanner) {
            this.reference = new WeakReference<MMConvenientBanner>(convenientBanner);
        }

        @Override
        public void run() {
            MMConvenientBanner convenientBanner = reference.get();

            if (convenientBanner != null) {
                if (convenientBanner.viewPager != null && convenientBanner.turning) {
                    int page = convenientBanner.viewPager.getCurrentItem() + 1;
                    convenientBanner.viewPager.setCurrentItem(page);
                    convenientBanner.postDelayed(convenientBanner.adSwitchTask, convenientBanner.autoTurningTime);
                }
            }
        }
    }

    /***
     * 设置所有翻页界面
     * 
     * @param holderCreator
     * @param datas
     * @return
     */
    public MMConvenientBanner setPages(MMCBViewHolderCreator holderCreator, List<T> datas) {
        this.mDatas = datas;
        pageAdapter = new MMCBPageAdapter(holderCreator, mDatas);
        viewPager.setAdapter(pageAdapter, canLoop);

        if (pageIndicatorId != null)
            setPageIndicator(pageIndicatorId);
        return this;
    }

    public T getItem(int position) {
        return mDatas != null ? mDatas.get(position) : null;
    }

    /**
     * 通知数据变化 如果只是增加数据建议使用 notifyDataSetAdd()
     */
    public void notifyDataSetChanged() {
        viewPager.getAdapter().notifyDataSetChanged();
        if (pageIndicatorId != null)
            setPageIndicator(pageIndicatorId);
    }

    /**
     * 设置底部指示器是否可见
     * 
     * @param visible true-可见，false-不可见
     */
    public MMConvenientBanner setPointViewVisible(boolean visible) {
        loPageTurningPoint.setVisibility(visible ? View.VISIBLE : View.GONE);
        return this;
    }

    /**
     * 设置翻页指示器
     * 
     * @param pageIndicatorId 指示器资源图片id数组
     */
    public MMConvenientBanner setPageIndicator(int[] pageIndicatorId) {
        loPageTurningPoint.removeAllViews();
        mPointViews.clear();
        this.pageIndicatorId = pageIndicatorId;
        if (mDatas == null)
            return this;
        for (int count = 0; count < mDatas.size(); count++) {
            // 翻页指示的点
            ImageView pointView = new ImageView(getContext());
            pointView.setPadding(5, 0, 5, 0);
            if (mPointViews.isEmpty())
                pointView.setImageResource(this.pageIndicatorId[1]);
            else
                pointView.setImageResource(this.pageIndicatorId[0]);
            mPointViews.add(pointView);
            loPageTurningPoint.addView(pointView);
        }
        pageChangeListener = new MMCBPageChangeListener(mPointViews, this.pageIndicatorId);
        viewPager.setOnPageChangeListener(pageChangeListener);
        pageChangeListener.onPageSelected(viewPager.getRealItem());
        if (onPageChangeListener != null)
            pageChangeListener.setOnPageChangeListener(onPageChangeListener);

        return this;
    }

    /**
     * 指示器的方向
     * 
     * @param align 三个方向：居左 （RelativeLayout.ALIGN_PARENT_LEFT），居中 （RelativeLayout.CENTER_HORIZONTAL），居右 （RelativeLayout.ALIGN_PARENT_RIGHT）
     * @return 页面翻转控件
     */
    public MMConvenientBanner setPageIndicatorAlign(PageIndicatorAlign align) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) loPageTurningPoint.getLayoutParams();
        int verb = RelativeLayout.TRUE;
        int anchor = 0;
        if (align == PageIndicatorAlign.ALIGN_PARENT_LEFT) {

        } else if (align == PageIndicatorAlign.ALIGN_PARENT_RIGHT) {

        } else if (align == PageIndicatorAlign.CENTER_HORIZONTAL) {

        }
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, align == PageIndicatorAlign.ALIGN_PARENT_LEFT ? RelativeLayout.TRUE : 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, align == PageIndicatorAlign.ALIGN_PARENT_RIGHT ? RelativeLayout.TRUE : 0);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, align == PageIndicatorAlign.CENTER_HORIZONTAL ? RelativeLayout.TRUE : 0);
        loPageTurningPoint.setLayoutParams(layoutParams);
        return this;
    }

    /***
     * 是否开启了翻页
     * 
     * @return true-是，false-否
     */
    public boolean isTurning() {
        return turning;
    }

    /***
     * 开始翻页
     * 
     * @param autoTurningTime 自动翻页时间
     * @return 页面翻转控件
     */
    public MMConvenientBanner startTurning(long autoTurningTime) {
        // 如果是正在翻页的话先停掉
        if (turning) {
            stopTurning();
        }
        // 设置可以翻页并开启翻页
        canTurn = true;
        this.autoTurningTime = autoTurningTime;
        turning = true;
        postDelayed(adSwitchTask, autoTurningTime);
        return this;
    }

    /***
     * 开始翻页
     * 
     * @return 页面翻转控件
     */
    public MMConvenientBanner startTurning() {
        return startTurning(autoTurningTime);
    }

    /**
     * 停止翻页
     */
    public void stopTurning() {
        turning = false;
        removeCallbacks(adSwitchTask);
    }

    /**
     * 设置自定义翻页动画效果
     *
     * @param transformer 翻页动画样式对象
     * @return 页面翻转控件
     */
    public MMConvenientBanner setPageTransformer(PageTransformer transformer) {
        viewPager.setPageTransformer(true, transformer);
        return this;
    }

    /**
     * 初始化ViewPager的滑动
     */
    private void initViewPagerScroll() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            scroller = new MMViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取翻页是否可滚动
     * 
     * @return true-可滚动，false-不可滚动
     */
    public boolean isManualPageable() {
        return viewPager.isCanScroll();
    }

    /**
     * 设置翻页是否能滚动
     * 
     * @param manualPageable true-可滚动，false-不可滚动
     */
    public void setManualPageable(boolean manualPageable) {
        viewPager.setCanScroll(manualPageable);
    }

    /**
     * 触碰控件的时候，翻页应该停止，离开的时候如果之前是开启了翻页的话则重新启动翻页
     * 
     * @param ev 触碰事件
     * @return true-翻页中，false-已停止翻页
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        int action = ev.getAction();
        if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_OUTSIDE) {
            // 开始翻页
            if (canTurn)
                startTurning(autoTurningTime);
        } else if (action == MotionEvent.ACTION_DOWN) {
            // 停止翻页
            if (canTurn)
                stopTurning();
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 获取当前的页面index
     * 
     * @return 当前的页面index
     */
    public int getCurrentItem() {
        if (viewPager != null) {
            return viewPager.getRealItem();
        }
        return -1;
    }

    /**
     * 设置当前的页面为index
     * 
     * @param index 页面编号 从1开始
     */
    public void setcurrentitem(int index) {
        if (viewPager != null && index >= 0) {
            viewPager.setCurrentItem(index);
        }
    }

    /**
     * 获取翻页改变监听
     * 
     * @return 翻页改变监听
     */
    public ViewPager.OnPageChangeListener getOnPageChangeListener() {
        return onPageChangeListener;
    }

    /**
     * 设置翻页监听器
     * 
     * @param onPageChangeListener
     * @return 页面翻转控件
     */
    public MMConvenientBanner setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener;
        // 如果有默认的监听器（即是使用了默认的翻页指示器）则把用户设置的依附到默认的上面，否则就直接设置
        if (pageChangeListener != null)
            pageChangeListener.setOnPageChangeListener(onPageChangeListener);
        else
            viewPager.setOnPageChangeListener(onPageChangeListener);
        return this;
    }

    /**
     * 设置item点击监听
     * 
     * @param onItemCBClickListener item点击监听
     */
    public MMConvenientBanner setOnCBItemClickListener(MMOnCBItemClickListener onItemCBClickListener) {
        if (onItemCBClickListener == null) {
            viewPager.setOnItemClickListener(null);
            return this;
        }
        viewPager.setOnItemClickListener(onItemCBClickListener);
        return this;
    }

    /**
     * 设置ViewPager的滚动速度
     * 
     * @param scrollDuration 滚动速度
     */
    public void setScrollDuration(int scrollDuration) {
        scroller.setScrollDuration(scrollDuration);
    }

    /**
     * 获取滚动持续速度
     * 
     * @return 滚动持续速度
     */
    public int getScrollDuration() {
        return scroller.getScrollDuration();
    }

    /***
     * 获取 翻页指示器循环视图滑动页
     * 
     * @return 翻页指示器循环视图滑动页
     */
    public MMCBLoopViewPager getViewPager() {
        return viewPager;
    }

    /***
     * 设置是否能循环
     * 
     * @param canLoop 是否能循环 true-能，false-不能
     */
    public void setCanLoop(boolean canLoop) {
        this.canLoop = canLoop;
        viewPager.setCanLoop(canLoop);
    }

    /**
     * 是否能循环
     * 
     * @return true-可循环，false-不可循环
     */
    public boolean isCanLoop() {
        return viewPager.isCanLoop();
    }
}
