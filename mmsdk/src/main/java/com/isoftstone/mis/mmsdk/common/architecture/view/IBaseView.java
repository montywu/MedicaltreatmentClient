
package com.isoftstone.mis.mmsdk.common.architecture.view;

/**
 * MVP模式中view基类接口
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-12]
 */
public interface IBaseView {
}
