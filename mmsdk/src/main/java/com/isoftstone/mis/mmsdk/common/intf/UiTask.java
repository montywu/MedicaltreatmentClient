
package com.isoftstone.mis.mmsdk.common.intf;

import android.os.AsyncTask;
import android.os.Build;

/**
 * 耗时任务，在后台线程中执行
 *
 * @author hubing
 * @version [1.0.0.0, 2016-11-01]
 */
public abstract class UiTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    /**
     * <b>重要：调用默认的{@link #execute(Object...)} 方法，多个任务的话，会是串行执行，需要排队</b><br>
     * <b>如果需要并行执行，请调用此方法。</b>
     *
     * @param params 需要执行测参数
     * @return 并行执行UiTask
     */
    public UiTask<Params, Progress, Result> start(Params... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            this.execute(params);
        }
        return this;
    }

}
