
package com.isoftstone.mis.mmsdk.common.intf.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据库DAO部分实现基类V2
 * <p>
 * 实现基本的增删改查，继承此类，基本的增删改等操作无需再写数据库的操作代码了
 * 
 * @author hubing
 * @version 1.0.0 2015-11-26
 */

public abstract class BaseDaoImpl<E> implements BaseDao<E> {

    /** 数据库对象 */
    protected SQLiteDatabase db;

    /**
     * 构造方法
     * 
     * @param db 数据库对象实例
     */
    public BaseDaoImpl(SQLiteDatabase db) {
        this.db = db;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> queryAll() {
        List<E> entitys = null;
        if (db != null) {
            entitys = new ArrayList<E>();
            Cursor cs = null;
            try {
                // 执行查询操作
                cs = db.query(getTableName(), null, null, null, null, null, null);
                if (cs.getCount() <= 0) {
                    return entitys;
                }

                // 取出数据，生成列表
                while (cs.moveToNext()) {
                    E e = setEntityData(cs);
                    entitys.add(e);
                }
            } catch (Exception e) {
                // Ignore
            } finally {
                // 关闭游标，释放资源
                if (cs != null) {
                    cs.close();
                }
            }
        }

        return entitys;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E query(E entity) {
        E e = null;
        if (db != null) {
            Cursor cs = null;
            try {
                String selection = getSelection();
                String[] selectionArgs = getSelectionArgs(entity);

                // 查询
                cs = db.query(getTableName(), null, selection, selectionArgs, null, null, null);
                if (cs.getCount() > 0 && cs.moveToFirst()) {
                    e = setEntityData(cs);
                }

            } catch (Exception exception) {
                // Ignore
            } finally {
                // 关闭游标，释放资源
                if (cs != null) {
                    cs.close();
                }
            }
        }

        return e;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean insert(E e) {
        if (db != null) {
            ContentValues values = createEntityParams(e);
            // 插入数据
            long result = db.insert(getTableName(), null, values);

            if (result != -1) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean insert(List<E> entitys) {
        if (db != null) {
            // 开启事务
            db.beginTransaction();
            try {
                for (E e : entitys) {
                    boolean insertResult = insert(e);
                    // 插入数据失败，返回false
                    if (!insertResult) {
                        return false;
                    }
                }
                // 设置事务处理成功
                db.setTransactionSuccessful();
                return true;
            } finally {
                // 事务处理完成
                db.endTransaction();
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(E e) {
        if (db != null) {
            ContentValues values = createEntityParams(e);
            String whereClause = getWhereClause();
            String[] whereArgs = getWhereArgs(e);

            // 更新数据
            int result = db.update(getTableName(), values, whereClause, whereArgs);
            if (result > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(E e) {
        if (db != null) {
            String whereClause = getWhereClause();
            String[] whereArgs = getWhereArgs(e);

            // 删除数据
            int result = db.delete(getTableName(), whereClause, whereArgs);
            if (result > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * 删除当前表下所有数据
     * 
     * @return 删除结果，true表示成功，false表示失败
     */
    public boolean deleteDataAll() {
        if (db != null) {
            int result = db.delete(getTableName(), null, null);
            if (result > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取当前操作的数据库表名
     * 
     * @return 当前操作的数据库表名
     */
    protected abstract String getTableName();

    /**
     * 设置实体对象数据
     * 
     * @param cs 游标对象，数据集体
     * @return 返回新的实体对象
     */
    protected abstract E setEntityData(Cursor cs);

    /**
     * 创建写入数据库的ContentValues
     * 
     * @param e 生成参数的实体对象
     * @return 返回参数集
     */
    protected abstract ContentValues createEntityParams(E e);

    /**
     * 查询数据的条件
     * <p>
     * A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself).
     * <p>
     * Passing null will return all rows for the given table.
     * 
     * @return Passing null will return all rows for the given table.
     */
    protected abstract String getSelection();

    /**
     * 查询数据的条件参数
     * <p>
     * You may include ?s in selection, which will be replaced by the values from selectionArgs,
     * <p>
     * in order that they appear in the selection.
     * <p>
     * The values will be bound as Strings.
     * 
     * @param e 查询条目的实体对象
     * @return The values will be bound as Strings.
     */
    protected abstract String[] getSelectionArgs(E e);

    /**
     * 删除或更新数据库的条件
     * <p>
     * the optional WHERE clause to apply when deleting. Passing null will delete all rows.
     * <P>
     * {@link #getWhereArgs}
     * 
     * @return 删除或更新数据库的条件,如果返回null,将删除当前表中所有数据
     */
    protected abstract String getWhereClause();

    /**
     * 删除或更新数据库的条件参数
     * <P>
     * You may include ?s in the where clause, which will be replaced by the values from whereArgs.
     * <P>
     * The values will be bound as Strings.
     * <P>
     * {@link #getWhereClause}
     * 
     * @param e 更新的数据表对应的实体对象(待更新或删除的条目的实体对象)
     * @return The values will be bound as Strings.
     */
    protected abstract String[] getWhereArgs(E e);

}
