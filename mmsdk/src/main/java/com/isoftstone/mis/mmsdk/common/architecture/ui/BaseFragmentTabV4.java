
package com.isoftstone.mis.mmsdk.common.architecture.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * 兼容3.0以下，android.support.v4下包中的Fragment TAB页基类
 * <p/>
 * tab页使用
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-13]
 */
public abstract class BaseFragmentTabV4<V extends IBaseView, T extends BasePresenter<V>> extends Fragment implements IBaseView {

    /** Presenter对象 */
    protected T presenter;

    /** 是否用户可见 */
    protected boolean isUserVisible;

    /** ui是否进行初始化 */
    private boolean isUiInit = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getRootViewResid(), container, false);
        presenter = createPresenter();
        this.isUiInit = true;
        return rootView;
    }

    /**
     * 获取fragment页面布局资源id
     * 
     * @return 返回fragment页面布局资源id
     */
    protected abstract int getRootViewResid();

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // ViewPager预加载，如果当前Fragment对用户不可见，不刷新数据
        this.isUserVisible = isVisibleToUser;
        if (isUserVisible && isUiInit) {
            loadData();
        }
    }

    /**
     * 加载数据，由子类去实现<br>
     * 在界面可见时才会调用此方法
     */
    protected abstract void loadData();

    /**
     * 创建presenter类对象
     * 
     * @return presenter类对象
     */
    public abstract T createPresenter();

    /**
     * Detach from view.
     */
    @Override
    public void onDestroyView() {
        if (presenter != null) {
            presenter.onDetachView();
        }
        super.onDestroyView();
    }

    /**
     * 展示给定资源的Toast提示
     *
     * @param resId The resource id of the string resource to use. Can be formatted text.
     */
    public void showToast(int resId) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 展示给定字符串的Toast提示
     *
     * @param text The text to show. Can be formatted text.
     */
    public void showToast(String text) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
        }
    }

}
